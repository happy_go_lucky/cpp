#include "Date.h"

Date::Date() noexcept
{
	_year = 0;
	_month = 0;
	_day = 0;
}

Date::Date(short day, short month, short year)
{
	_year = year;
	_month = month;
	_day = day;
}

Date::Date(char* dateStr)
{
	computeDateFromString(dateStr);
}

Date::Date(const Date& date)
{
	_year = date._year;
	_month = date._month;
	_day = date._day;
}

Date::~Date()
{
	
}

ostream& operator <<(ostream& out, const Date& date)
{
	out << date._month << "/" << date._day << "/" << date._year;
	return out;
}

Date& Date::operator =(const Date& date)
{
	if (this != &date)
	{
		_year = date._year;
		_month = date._month;
		_day = date._day;
	}
	return *this;
}

Date& Date::operator =(Date&& date)
{
	_year = move(date._year);
	_month = move(date._month);
	_day = move(date._day);
	return *this;
}

Date& Date::operator =(char* dateStr)
{
	computeDateFromString(dateStr);
	return *this;
}

short Date::getYear() const noexcept
{
	return _year;
}

short Date::getMonth() const noexcept
{
	return _month;
}

short Date::getDay() const noexcept
{
	return _day;
}

void Date::setYear(short year)
{
	_year = year;
}

void Date::setMonth(short month)
{
	_month = month;
}

void Date::setDay(short day)
{
	_day = day;
}

string Date::getDateString() const
{
	char str[11];
	sprintf(str, "%d/%d/%d", _month, _day, _year);
	string date(str);
	return date;
}

void Date::computeDateFromString(char* dateStr)
{
	if (dateStr == nullptr || *dateStr == '\0')
	{
		_year = 0;
		_month = 0;
		_day = 0;
	}
	else
	{
		char* str = strtok(dateStr, "/");;
		while (str != NULL)
		{
			if (_month == 0)
			{
				_month = atoi(str);
			}
			else if (_day == 0)
			{
				_day = atoi(str);
			}
			else if (_year == 0)
			{
				_year = atoi(str);
			}
			
			str = strtok(NULL, "/");
		}
	}
}

bool Date::isValid() const noexcept
{
	return !(_year == 0 || _month == 0 || _day == 0);
}
