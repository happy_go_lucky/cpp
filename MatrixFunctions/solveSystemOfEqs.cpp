#include <iostream>
#include <cstdlib>
#include <fstream>

#include "AugmentedMatrix.h"
#include "AugmentedMatrix.cpp"

using std::cout;
using std::endl;
using std::ifstream;

void readInFile();
bool isLastColumn( const unsigned, const unsigned );
void solve( const AugmentedMatrix<double>* );

const char* FILENAME = "mq4data9.csv";
const int CONST_MATRIX_SIZE = 1;
const char SIZE_INDICATOR = '#';
const char MATRIX_DATA_SEPARATOR = ',';
const char SIZE_DATA_SEPARATOR = 'x';
const int UNKNOWNS_COLUMN = 1;


int main()
{
	readInFile();
	return EXIT_SUCCESS;
}

void readInFile()
{
	ifstream fileContents;
	
	fileContents.open( FILENAME );
	
	if ( !fileContents || fileContents.fail() )
	{
		cout << "unable to open file " << FILENAME << endl;
		exit( 1 );
	}
	
	unsigned int numRows = 0;
	unsigned int numCols = 0;
	AugmentedMatrix<double>* augmentedMatrix = nullptr;

	unsigned int byteCounter = 0;
	char byteRead;
	const int CHAR_BUFFER_SIZE = 1000;
	char charBuffer[CHAR_BUFFER_SIZE];
	
	bool flag_readSizeData = false;
	bool flag_readMatrixData = false;
	
	while ( !fileContents.eof() )
	{
		byteCounter++;
		
		fileContents >> byteRead;
		
		//cout << byteRead;
		if ( byteRead == SIZE_INDICATOR )
		{
			flag_readSizeData = true;
		}
		
		if ( flag_readSizeData )
		{
			// read rows
			fileContents.get( charBuffer, CHAR_BUFFER_SIZE, SIZE_DATA_SEPARATOR );
			numRows = atoi( charBuffer );
			
			// consume separator
			fileContents >> byteRead;
			
			// read cols
			fileContents.get( charBuffer, CHAR_BUFFER_SIZE );
			numCols = atoi( charBuffer ) + UNKNOWNS_COLUMN;
			
			flag_readSizeData = false;
			flag_readMatrixData = true;
			
			cout << "num rows: " << numRows << endl;
			cout << "num columns: " << numCols << endl;
			
			augmentedMatrix = new AugmentedMatrix<double>( numRows, numCols, numCols - CONST_MATRIX_SIZE );
		}
		
		if ( flag_readMatrixData )
		{
			unsigned int ii = 1, jj = 1;
			while ( ii <= numRows )
			{
				while ( jj <= numCols )
				{
					if ( isLastColumn( jj, numCols ) )
					{
						fileContents.get( charBuffer, CHAR_BUFFER_SIZE );
					}
					else
					{
						fileContents.get( charBuffer, CHAR_BUFFER_SIZE, MATRIX_DATA_SEPARATOR );
						// consume MATRIX_DATA_SEPARATOR
						fileContents >> byteRead;
					}
					
					augmentedMatrix->setDataAt( atof( charBuffer ), ii, jj );
					jj++;
				}
				
				jj = 1;
				ii++;
				flag_readMatrixData = false;
			}
			
		}
		if ( augmentedMatrix != nullptr )
		{
			solve( augmentedMatrix );
			
			delete augmentedMatrix;
			augmentedMatrix = nullptr;
		}
	}
	
	fileContents.close();
}

bool isLastColumn( const unsigned curColumn, const unsigned totalCols )
{
	return curColumn == totalCols;
}

void solve( const AugmentedMatrix<double>* augMatrix )
{
	cout << "----------------------------------------------------------" << endl;
	
	cout << "Printing Augmented Matrix read: " << endl;
	augMatrix->printMatrix();
	cout << "calculating inverse of coefficient matrix: " << endl;
	Matrix<double> invCoeffMatrix = augMatrix->getCoeffMatrix()->inverse();
	cout << "printing inverse of coefficient matrix: " << endl;
	invCoeffMatrix.printMatrix();
	cout << "calculating variables..: " << endl;
	cout << "printing constant matrix " << augMatrix->getConstMatrix()->numRows() << " x " << augMatrix->getConstMatrix()->numCols() << endl;
	augMatrix->getConstMatrix()->printMatrix();
	

	Matrix<double> constMatrix = *augMatrix->getConstMatrix();
	Matrix<double> resultMatrix = invCoeffMatrix * constMatrix;
	//cout << "printing result matrix" << endl;
	//resultMatrix.printMatrix();

	MatrixTraversor traversor( MatrixTraversor::ROW, resultMatrix.numRows(), resultMatrix.numCols() );
	unsigned ii = 0;
	while ( ii < resultMatrix.numRows() * resultMatrix.numCols() )
	{
		cout << "value of variable " << ii + 1 << ": " << resultMatrix.getDataAt( traversor.getRowPosition(), traversor.getColumnPosition() ) << endl;
		traversor.traverse();
		ii++;
	}

	cout << "-----------------------------------------------------------" << endl;
}