#include <iostream>
#include <ctime>
#include "BinarySearchTree.h"
#include "BinarySearchTree.cpp"

using std::cout;
using std::cin;
using std::endl;

void testTree();
int getRandomInt( int start, int range );

int main()
{
	testTree();
	return EXIT_SUCCESS;
}

void testTree()
{
	BinarySearchTree<int> bst;
	srand( time( NULL ) );
	
	cout << "is tree empty? " << ( bst.isEmpty() ? "Yes" : "No" ) << endl;
	
	const int NUM_ELEMENTS = 40;
	const int RANGE = 1000;
	for ( int ii = 0; ii < NUM_ELEMENTS; ii++ )
	{
		try
		{
			bst.insert( getRandomInt( 0, RANGE ) );
		}
		catch( BST_Exception& e )
		{
			if ( e.type() == BST_Exception::EXCEP_DUPLICATE_INSERT )
			{
				cout << "exception: " << e.what() << endl;
			}
		}
	}
	
	cout << "printing in order" << endl;
	bst.print( bst.IN_ORDER );
	cout << endl;
	
	cout << "printing pre order" << endl;
	bst.print( bst.PRE_ORDER );
	cout << endl;
	
	cout << "printing post order" << endl;
	bst.print( bst.POST_ORDER );
	cout << endl;
	
	cout << "printing breadth first order" << endl;
	bst.print( bst.BREADTH_FIRST );
	cout << endl;
	
	cout << "is tree empty? " << ( bst.isEmpty() ? "Yes" : "No" ) << endl;
	
	cout << "testing BST search" << endl;
	int searchValue = getRandomInt( 0, RANGE );
	bool found = false;
	while ( !found )
	{
		searchValue = getRandomInt( 0, RANGE );
		found = bst.search( searchValue );
		cout << "is " << searchValue << " in BST? " << ( found ? "Yes" : "No" ) << endl;
	}
	
	cout << "testing BST copy" << endl;
	BinarySearchTree<int> bst2;
	bst2 = bst;
	
	cout << "new bst copy is:" << endl;
	cout << "printing in order" << endl;
	bst2.print( bst2.IN_ORDER );
	cout << endl;
	
	cout << "deleting original BST" << endl;
	bst.clear();
	cout << "is tree empty? " << ( bst.isEmpty() ? "Yes" : "No" ) << endl;
	
	cout << "new bst copy is:" << endl;
	cout << "printing post order" << endl;
	bst2.print( bst2.POST_ORDER );
	cout << endl;
	
	// reset to default
	srand( 1 );
}

int getRandomInt( int start, int range )
{
	return ( rand() % range ) + start;
}
