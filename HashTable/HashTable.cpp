#include "HashTable.h"

HashTable::HashTable( const int size )
{
	this->size = size;
	hashTable = new LinkedList<string>[size];
	numCollisions = new int[size];
	initCollisionsArray();
}

HashTable::~HashTable()
{
	delete[] hashTable;
	delete[] numCollisions;
}

const int HashTable::getSize() const
{
	return size;
}

void HashTable::addValueToTable( string value )
{
	int index = computeHash( value );
	int numOps = 0;
	if ( hashTable[index].searchData( value, numOps ) == NULL )
	{
		if ( hashTable[index].isEmpty() == false )
		{
			numCollisions[index]++;
		}
		
		hashTable[index].addNodeToHead( value );
	}
}

bool HashTable::constainsValue( string value, int& numOperations ) const
{
	int index = computeHash( value );
	
	numOperations = 0;
	if ( hashTable[index].searchData( value, numOperations ) != NULL )
	{
		return true;
	}
	
	return false;
}

int* HashTable::getCollisionsArray() const
{
	return numCollisions;
}

int HashTable::computeHash( string s ) const
{
	unsigned hash = 0;
	for ( int ii = 0; ii < s.length(); ii++ )
	{
		hash = HASH_WEIGHT * hash + s[ii];
		if ( hash >= LARGE_INT )
		{
			hash %= size;
		}
	}
	return hash % size; //size of  hash table array
}

void HashTable::initCollisionsArray()
{
	for ( int ii = 0; ii < size; ii++ )
	{
		numCollisions[ii] = 0;
	}
	
}