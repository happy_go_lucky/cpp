#include "Rectangle.h"

ostream& operator << (ostream& out, const Rectangle& r)
{
	out << "[ x1: " << r.x1 << ", x2: " << r.x2 << ", y1: " << r.y1
		<< ", y2: " << r.y2 << ", cX: " << r.centerX() << ", cY: "
		<< r.centerY() <<  " ]" << endl;
	return out;
}

Rectangle& Rectangle::operator= (const Rectangle& rec)
{
	if (this != &rec)
	{
		x1 = rec.x1;
		x2 = rec.x2;
		y1 = rec.y1;
		y2 = rec.y2;
	}
	return *this;
}

bool Rectangle::operator== (const Rectangle& rec) const noexcept
{
	return x1 == rec.x1 && x2 == rec.x2 && y1 == rec.y1 && y2 == rec.y2;
}

int Rectangle::length() const
{
	return abs(x2) - abs(x1);
}

int Rectangle::width() const
{
	return abs(y2) - abs(y1);
}

int Rectangle::centerX() const
{
	return (x1 + x2) / 2;
}

int Rectangle::centerY() const
{
	return (y1 + y2) / 2;
}

bool Rectangle::isConverged() const
{
	return x1 == x2 && y1 == y2;
}
