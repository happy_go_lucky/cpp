#ifndef LED_H
#define LED_H

#include <iostream>
#include <string>
#include <memory>
#include "GpioPin.h"
#include "GpioDefines.h"
#include "GpioManager.h"

class LED
{
	public:
		enum class State : unsigned char
		{
			ON = 0,
			OFF
		};
		
		LED() = delete;
		LED(std::shared_ptr<gpio::GpioPin> gpioPin);
		~LED();
		
		void turnOn();
		void turnOff();
		State getState() const;
		void toggleOnOff();
		
	private:
		std::shared_ptr<gpio::GpioPin> _gpioPin;
		State _state;
};

#endif
