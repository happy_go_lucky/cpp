#ifndef SHARED_MEMORY_DEFS_H
#define SHARED_MEMORY_DEFS_H

namespace SharedMemoryDefs
{
	enum VisibilityType
	{
		NAMED = 0,
		ANONYMOUS
	};

	enum AccessType
	{
		READ_ONLY = 0,
		READ_WRITE
	};

	enum SharingType
	{
		SHARED = 0,
		PRIVATE
	};
}

#endif
