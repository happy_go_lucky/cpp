#ifndef FSM_TRANSITION_H
#define FSM_TRANSITION_H

#include <iostream>
#include "FSM_Helper.h"
#include "ExprElement.h"
#include "StateTransitConditions.h"
#include "GlobalVariables.h"

using std::cout;
using std::endl;

class FSM_Transition
{
	public:
		FSM_Transition() noexcept;
		explicit FSM_Transition(ExprStates targetStateType, 
								Base_TC* conditionFunc) noexcept;
		~FSM_Transition();
		
		void setTargetStateType(ExprStates stateType);
		void setTransitionFunction(Base_TC* func);
		ExprStates getTargetStateType() const;
		bool execute(ExprElement* inputElement, 
					const GlobalVariables::StateActionParams& actionParams) const;
		void debugPrint();
		
	private:
		ExprStates _targetStateType;
		Base_TC* _exitConditionFunc;
};

#endif
