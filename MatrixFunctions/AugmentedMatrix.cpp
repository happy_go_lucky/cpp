#include "AugmentedMatrix.h"

template <typename T>
AugmentedMatrix<T>::AugmentedMatrix( const unsigned int rows, const unsigned cols, const unsigned partition )
{
	_numRows = rows;
	_numCols = cols;
	_partitionValue = partition;
	
	cout << "rows: " << rows << endl;
	cout << "cols: " << cols << endl;
	cout << "partition: " << partition << endl;
	
	_coeffMatrix = new Matrix<T>( _numRows, _partitionValue );
	_constantMatrix = new Matrix<T>( _numRows, _numCols - _partitionValue );
}

template <typename T>
AugmentedMatrix<T>::AugmentedMatrix( const AugmentedMatrix& aMatrix )
{
	_numRows = aMatrix.numRows();
	_numCols = aMatrix.numCols();
	_partitionValue = aMatrix.getPartitionValue();
	
	_coeffMatrix = new Matrix<T>( aMatrix.getCoeffMatrix() );
	_constantMatrix = new Matrix<T>( aMatrix.getConstMatrix() );
	
}

template <typename T>
AugmentedMatrix<T>::~AugmentedMatrix()
{
	delete _coeffMatrix;
	delete _constantMatrix;
}

template <typename T>
void AugmentedMatrix<T>::setDataAt( const T data, const unsigned int row, const unsigned int col )
{
	if ( col <= _partitionValue )
	{
		_coeffMatrix->setDataAt( data, row, col );
	}
	else
	{
		_constantMatrix->setDataAt( data, row, col - _partitionValue );
	}
}

template <typename T>
T AugmentedMatrix<T>::getDataAt( const unsigned int row, const unsigned int col ) const
{
	if ( col <= _partitionValue )
	{
		return _coeffMatrix->getDataAt( row, col );
	}
	else
	{
		return _constantMatrix->getDataAt( row, col - _partitionValue );
	}
}

template <typename T>
unsigned int AugmentedMatrix<T>::numRows() const
{
	return _numRows;
}

template <typename T>
unsigned int AugmentedMatrix<T>::numCols() const
{
	return _numCols;
}

template <typename T>
void AugmentedMatrix<T>::setPartitionValue( const unsigned int value )
{
	_partitionValue = value;
}

template <typename T>
unsigned int AugmentedMatrix<T>::getPartitionValue() const
{
	return _partitionValue;
}

template <typename T>
Matrix<T>* AugmentedMatrix<T>::getCoeffMatrix() const
{
	return _coeffMatrix;
}

template <typename T>
Matrix<T>* AugmentedMatrix<T>::getConstMatrix() const
{
	return _constantMatrix;
}

template <typename T>
void AugmentedMatrix<T>::printMatrix() const
{
	unsigned int currRow = 1;
	unsigned int currColumn = 1;
	
	while ( currRow <= _numRows )
	{
		while ( currColumn <= _numCols )
		{
			if ( currColumn <= _partitionValue )
			{
				cout << _coeffMatrix->getDataAt( currRow, currColumn );
				if ( currColumn == _partitionValue )
				{
					cout  << "\t|\t";
				}
				else
				{
					cout << "\t";
				}
			}
			else
			{
				cout << _constantMatrix->getDataAt( currRow, currColumn - _partitionValue ) << "\t";
			}
			
			currColumn++;
		}
		
		currRow++;
		currColumn = 1;
		cout << endl;
	}
}