#ifndef SUB_DIVIDED_NODE_H
#define SUB_DIVIDED_NODE_H

#include "QuadNode.h"

class SubDividedNode : public QuadNode
{
	public:
		friend class QuadTree;
	
		SubDividedNode() noexcept;
		SubDividedNode(const SubDividedNode& node) noexcept;
		~SubDividedNode() noexcept;
		
		void set_ne(QuadNode* node);
		void set_nw(QuadNode* node);
		void set_se(QuadNode* node);
		void set_sw(QuadNode* node);
		
		QuadNode* get_ne() const noexcept;
		QuadNode* get_nw() const noexcept;
		QuadNode* get_se() const noexcept;
		QuadNode* get_sw() const noexcept;
		
		bool has_ne() const noexcept;
		bool has_nw() const noexcept;
		bool has_se() const noexcept;
		bool has_sw() const noexcept;
		
		void setEmpty() noexcept;
	
	private:
		QuadNode* _nw;
		QuadNode* _ne;
		QuadNode* _sw;
		QuadNode* _se;
		
		//~ Rectangle _range;
};

#endif
