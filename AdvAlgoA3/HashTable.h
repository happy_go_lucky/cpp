#ifndef HASH_TABLE_H
#define HASH_TABLE_H

#include <iostream>
#include <string>
#include <iomanip>
#include "HelperFunctions.h"
#include "HashRecord.h"

using namespace std;

class HashTable
{
	public:
		static const short INIT_CAPACITY = 1024;
		static const constexpr float LOAD_FACTOR = 0.7f;
		static const int INVALID_INDEX = -1;
		
		enum CellStatus
		{
			EMPTY = 0,
			OCCUPIED,
			DELETED
		};

		HashTable() noexcept;
		HashTable(size_t capacity, float loadFactor) noexcept;
		HashTable(const HashTable& hash);
		~HashTable();
		
		inline float getLoadFactor() const noexcept
		{
			return _loadFactor;
		}
		
		inline size_t getCapacity() const noexcept
		{
			return _capacity;
		}
		
		inline size_t getSize() const noexcept
		{
			return _size;
		}

		bool insert(const HashRecord& value) noexcept;
		bool search(const HashRecord& value, size_t& offset) noexcept;
		bool erase(const HashRecord& value);
		void printSnapshot(fstream* logPrint) const noexcept;
		inline size_t getLongestProbeSequence() const noexcept
		{
			return _longestProbeSequence;
		}
	
	private:
		HashRecord* _hashList;
		CellStatus* _cellStatusList;
		
		size_t _capacity;
		size_t _size;
		float _loadFactor;
		size_t _longestProbeSequence;
		
		size_t hash();
		size_t probe();
		void rehash();
		void increaseCapacity();
		void expandHash();
		void destroyHash();
		void createHash();
		size_t probeFunc(size_t index) const;
		size_t hashFunc(const char* str) const;
		inline bool isCapacityThreshold() const
		{
			return _size >= _loadFactor * _capacity;
		}
};

#endif
