#ifndef CIRCULAR_LINKED_LIST_CPP
#define CIRCULAR_LINKED_LIST_CPP

#include "../include/CircularLinkedList.h"

template <typename T>
CircularLinkedList<T>::CircularLinkedList() noexcept
{
	createEmptyList();
}

template <typename T>
CircularLinkedList<T>::CircularLinkedList(size_t size)
{
	createEmptyList();
	
	if (size == 0) return;
	_size = size;
	BetterStack<ListNode<T>*> stackNodes;
	
	for (size_t ii = 0; ii < size; ii++)
	{
		stackNodes.push(new ListNode<T>());
	}
	
	ListNode<T>* next = _head;
	_tail = stackNodes.top();
	while (!stackNodes.empty())
	{
		stackNodes.top()->setNextNode(next);
		next = stackNodes.pop();
	}
	_head->setNextNode(next);
}

template <typename T>
CircularLinkedList<T>::CircularLinkedList(std::initializer_list<T> list)
{
	createEmptyList();
	_size = list.size();
	if (_size == 0) return;
	
	ListNode<T>* itr = _head;
	for (size_t ii = 0; ii < _size; ii++)
	{
		itr->setNextNode(new ListNode<T>(*(list.begin() + ii)));
		itr = itr->getNextNode();
	}
	itr->setNextNode(_head);
	_tail = itr;
}

template <typename T>
CircularLinkedList<T>::CircularLinkedList(const CircularLinkedList<T>& list)
{
	copyList(list);
}

template <typename T>
CircularLinkedList<T>::CircularLinkedList(CircularLinkedList<T>&& list) noexcept
{
	_size = std::move(list._size);
	_head = std::move(list._head);
	_tail = std::move(list._tail);
	
	list._head = new ListNode<T>();	// dummy node, because head can not be null
	list._head->setNextNode(list._head);
	list._tail = _head;
	list._size = 0;
}

template <typename T>
CircularLinkedList<T>::~CircularLinkedList()
{
	destroyList();
}

template <typename T>
void CircularLinkedList<T>::push_front(const T& val)
{
	// different handling of empty push, and non-empty push for tail node
	_head->setNextNode(new ListNode<T>(val, _head->getNextNode()));
	
	if (++_size == 1)
	{
		_tail = _head->getNextNode();
	}
}

template <typename T>
void CircularLinkedList<T>::push_front(T&& val)
{
	// different handling of empty push, and non-empty push for tail node
	_head->setNextNode(new ListNode<T>(std::move(val), _head->getNextNode()));
	
	if (++_size == 1)
	{
		_tail = _head->getNextNode();
	}
}

template <typename T>
void CircularLinkedList<T>::push_back(const T& val)
{
	_tail->setNextNode(new ListNode<T>(val, _tail->getNextNode()));
	_tail = _tail->getNextNode();
	_size++;
}

template <typename T>
void CircularLinkedList<T>::push_back(T&& val)
{
	_tail->setNextNode(new ListNode<T>(std::move(val), _tail->getNextNode()));
	_tail = _tail->getNextNode();
	_size++;
}

template <typename T>
void CircularLinkedList<T>::insertAfter(ListNode<T>* targetNode, const T& val)
{
	// different handling of empty push, and non-empty push for tail node
	targetNode->setNextNode(new ListNode<T>(val, targetNode->getNextNode()));
	
	if (++_size == 1)
	{
		_tail = _head->getNextNode();
	}
}

template <typename T>
void CircularLinkedList<T>::insertAfter(ListNode<T>* targetNode, T&& val)
{
	// different handling of empty push, and non-empty push for tail node
	targetNode->setNextNode(new ListNode<T>(std::move(val), targetNode->getNextNode()));
	
	if (++_size == 1)
	{
		_tail = _head->getNextNode();
	}
}

template <typename T>
void CircularLinkedList<T>::remove(const T& val) noexcept
{
	if (isEmpty())
	{
		return;
	}
	ListNode<T>* itr = _head->getNextNode();
	ListNode<T>* prev = _head;
	
	do
	{
		if (itr->getData() == val)
		{
			prev->setNextNode(itr->getNextNode());
			if (_tail == itr) _tail = prev;
			delete itr;
			_size--;
			return;
		}
		prev = itr;
		itr = itr->getNextNode();
	} while(itr != _head);
}

template <typename T>
void CircularLinkedList<T>::remove(ListNode<T>* targetNode, ListNode<T>* parentNode) noexcept
{
	if (isEmpty()) return;
	
	if (parentNode == nullptr)
	{
		ListNode<T>* currItr = _head;
		ListNode<T>* prevItr = _head;
		do
		{
			if (currItr == targetNode) 
			{
				prevItr->setNextNode(currItr->getNextNode());
				delete currItr;
				_size--;
				return;
			}
			prevItr = currItr;
			currItr = currItr->getNextNode();
		} while (currItr != _head);
	}
	else
	{
		parentNode->setNextNode(targetNode->getNextNode());
		delete targetNode;
		_size--;
	}
}

template <typename T>
void CircularLinkedList<T>::pop_front() noexcept
{
	if (isEmpty()) return;
	
	ListNode<T>* temp = _head->getNextNode();
	_head->setNextNode(temp->getNextNode());
	if (_tail == temp) _tail = _head;
	delete temp;
	
	_size--;
}

template <typename T>
void CircularLinkedList<T>::pop_back() noexcept
{
	if (isEmpty()) return;
	
	ListNode<T>* currItr = _head;
	ListNode<T>* prevItr = _head;
	
	while (currItr != _tail)
	{
		prevItr = currItr;
		currItr = currItr->getNextNode();
	}
	
	prevItr->setNextNode(_head);
	delete currItr;
	_size--;
}

template <typename T>
bool CircularLinkedList<T>::search(const T& val) const noexcept
{
	if (isEmpty()) return false;
	
	ListNode<T>* itr = _head->getNextNode();
	
	do
	{
		if (itr->getData() == val)
		{
			return true;
		}
		itr = itr->getNextNode();
	} while(itr != _head);
	
	return false;
}

template <typename T>
const ListNode<T>* CircularLinkedList<T>::searchNode(const T& val) const noexcept
{
	if (isEmpty()) return nullptr;
	
	ListNode<T>* itr = _head->getNextNode();
	
	do
	{
		if (itr->getData() == val)
		{
			return itr;
		}
		itr = itr->getNextNode();
	} while(itr != _head);
	
	return nullptr;
}

template <typename T>
void CircularLinkedList<T>::reverseList() noexcept
{
	if (isEmpty()) return;
	
	BetterStack<ListNode<T>*> stackNodes(_size);
	ListNode<T>* itr = _head->getNextNode();
	_tail = itr;
	do
	{
		stackNodes.push(itr);
		itr = itr->getNextNode();
	} while (itr != _head);
	
	while (!stackNodes.empty())
	{
		itr->setNextNode(stackNodes.pop());
		itr = itr->getNextNode();
	}
	itr->setNextNode(_head);
}

template <typename T>
void CircularLinkedList<T>::clear()
{
	if (isEmpty()) return;
	
	destroyList();
	createEmptyList();
}

template <typename T>
void CircularLinkedList<T>::resize(size_t size)
{
	// TODO: implement it if needed
}

template <typename T>
ListNode<T>* CircularLinkedList<T>::getNextNode(ListNode<T>* node) const noexcept
{
	if (isEmpty()) return nullptr;
	
	if (node->getNextNode() == _head)
	{
		return _head->getNextNode();
	}
	else
	{
		return node->getNextNode();
	}
}

template <typename U>
std::ostream& operator << (std::ostream& out, const CircularLinkedList<U>& list) noexcept
{
	out << "capacity: " << list._size << std::endl;
	out << "[ ";
	if (!list.isEmpty())
	{
		ListNode<U>* itr = list._head->getNextNode();
		do
		{
			out << itr->getData();
			if (itr->getNextNode() != list._head)
			{
				out << ", ";
			}
			itr = itr->getNextNode();
		} while (itr != list._head);
	}
	out << " ]" << std::endl;
	
	return out;
}

template <typename T>
CircularLinkedList<T>& CircularLinkedList<T>::operator = (const CircularLinkedList<T>& list)
{
	if (this != &list)
	{
		destroyList();
		copyList(list);
	}
	return *this;
}

template <typename T>
CircularLinkedList<T>& CircularLinkedList<T>::operator = (CircularLinkedList<T>&& list)
{
	if (this != &list)
	{
		destroyList();
		_head = std::move(list._head);
		_tail = std::move(list._tail);
		_size = std::move(list._size);
		
		list._head = new ListNode<T>();	// dummy node, because head can not be null
		list._head->setNextNode(list._head);
		list._tail = list._head;
		list._size = 0;
	}
	return *this;
}

template <typename T>
bool CircularLinkedList<T>::operator == (const CircularLinkedList<T>& list) noexcept
{
	if (_size != list._size)
	{
		return false;
	}
	ListNode<T>* itrList1 = _head;
	ListNode<T>* itrList2 = list._head;
	do
	{
		if (itrList1->getData() != itrList2->getData())
		{
			return false;
		}
		itrList1 = itrList1->getNextNode();
		itrList2 = itrList2->getNextNode();
	} while (itrList1 != _head);
	
	return true;
}

template <typename T>
bool CircularLinkedList<T>::operator == (const CircularLinkedList<T>&& list) noexcept
{
	if (_size != list._size)
	{
		return false;
	}
	ListNode<T>* itrList1 = _head;
	ListNode<T>* itrList2 = list._head;
	do
	{
		if (itrList1->getData() != itrList2->getData())
		{
			return false;
		}
		itrList1 = itrList1->getNextNode();
		itrList2 = itrList2->getNextNode();
	} while (itrList1 != _head);
	
	return true;
}

template <typename T>		
void CircularLinkedList<T>::destroyList()
{
	BetterStack<ListNode<T>*> stackNodes;
	ListNode<T>* itr = _head;
	
	do
	{
		stackNodes.push(itr);
		itr = itr->getNextNode();
	} while (itr != _head);
	
	do
	{
		delete stackNodes.pop();
	} while (!stackNodes.empty());
	
	_head = nullptr;
	_tail = nullptr;
}

template <typename T>
void CircularLinkedList<T>::copyList(const CircularLinkedList<T>& list)
{
	createEmptyList();
	
	if (list.isEmpty()) return;
	_size = list.size();
	ListNode<T>* itrSrc = list.begin();
	ListNode<T>* itrDest = _head;
	do
	{
		itrDest->setNextNode(new ListNode<T>(itrSrc->getData()));
		if (itrSrc == list.end())
		{
			_tail = itrDest;
		}
		itrSrc = itrSrc->getNextNode();
		itrDest = itrDest->getNextNode();
	} while (itrSrc != list._head);
	
	itrDest->setNextNode(_head);
}

template <typename T>
void CircularLinkedList<T>::createEmptyList()
{
	_size = 0;
	_head = new ListNode<T>();	// dummy node
	_head->setNextNode(_head);
	_tail = _head;
}

template <typename T>
void CircularLinkedList<T>::checkValidity()
{
	assert(_head != nullptr);
	assert(_tail != nullptr);
}

#endif
