#include "../inc/GpioWriterWorker.h"

GpioWriterWorker::GpioWriterWorker() : posixthreads::Thread(posixthreads::Thread::COOPERATIVE)
{
	
}

GpioWriterWorker::~GpioWriterWorker()
{
	
}

void GpioWriterWorker::init(std::weak_ptr<posixthreads::ReadWriteLock> rwLock)
{
	_rwLock = rwLock;
}

void* GpioWriterWorker::threadRun()
{
	/*
	RectAreaImplArgs threadArgs = *((RectAreaImplArgs*) _threadArgs);
	
	double mid = 0.0;
	double area = 0.0;
	
	for (int ii = threadArgs.threadNumber; ii < threadArgs.numRects; ii += threadArgs.numThreads)
	{
		threadArgs.rectArray[ii].setWidth(threadArgs.deltaX); 
		mid = (ii + 0.5) * threadArgs.rectArray[ii].getWidth();
		threadArgs.rectArray[ii].setHeight(4.0 / (1.0 + mid * mid));
		area += threadArgs.rectArray[ii].getArea();
	}
	
	_mutexLock->lock();
	*(threadArgs.totalArea) += area;
	_mutexLock->unlock();
	*/
	return EXIT_SUCCESS;
}



int GpioWriterWorker::_write(const char* path, const char* gpioCmd, const char* value) const
{
	std::ofstream fs;
	char* cmdBuffer = new char[gpio::GpioDefines::GPIO_COMMAND_BUFFER_LEN];
	
	strcpy(cmdBuffer, path);
	strcat(cmdBuffer, gpioCmd);
	
	fs.open(cmdBuffer);
	if (!fs.is_open())
	{
		std::cerr << "GPIO: write failed to open file: " << gpioCmd << std::endl;
		return EXIT_FAILURE;
	}
	
	fs << value;
	fs.close();
	
	delete cmdBuffer;
	
	return EXIT_SUCCESS;
}
