#include "../inc/ConditionVariable.h"

namespace posixthreads
{
	ConditionVariable::ConditionVariable(MutexLock& mutex) noexcept : _mutexLock(mutex)
	{
		_cv = nullptr;
		_cvAttrib = nullptr;
	}

	ConditionVariable::~ConditionVariable()
	{
		if (_cv != nullptr) delete _cv;
		if (_cvAttrib != nullptr) delete _cvAttrib;
	}

	void ConditionVariable::initialize(ProcessAccessType accessType)
	{
		_cv = new pthread_cond_t();
		_cvAttrib = new pthread_condattr_t();
		
		pthread_condattr_init(_cvAttrib);
		_setAccessType(accessType);
		
		#ifndef NDEBUG
		int state = pthread_cond_init(_cv, NULL);
		if (state != EXIT_SUCCESS)
		{
			std::cout << strerror(state) << std::endl;
			throw ThreadException((char*) "unable to initialize condition variable");
		}
		#else
		pthread_cond_init(_cv, NULL);
		#endif
	}

	void ConditionVariable::cleanup()
	{
		#ifndef NDEBUG
		int stateCV = pthread_cond_destroy(_cv);
		int stateAtrrib = pthread_condattr_destroy(_cvAttrib);
		
		if (stateCV != EXIT_SUCCESS || stateAtrrib != EXIT_SUCCESS)
		{
			std::cout << strerror(stateCV) << std::endl;
			std::cout << strerror(stateAtrrib) << std::endl;
			throw ThreadException((char*) "unable to destroy condition variable");
		}
		#else
		pthread_cond_destroy(_cv);
		pthread_condattr_destroy(_cvAttrib);
		#endif
		
		delete _cv;
		delete _cvAttrib;
		_cv = nullptr;
		_cvAttrib = nullptr;
	}

	/**
	 * unblocks one of the waiting threads on the condition variable
	 * */
	template <typename Type>
	void ConditionVariable::signal(std::function<void(const Type&)> condUpdate, 
			const Type& value)
	{
		int state = EXIT_FAILURE;
		_mutexLock.lock();
		condUpdate(value);
		state = pthread_cond_signal(_cv);
		_mutexLock.unlock();
		#ifndef NDEBUG
		if (state != EXIT_SUCCESS)
		{
			std::cout << strerror(state) << std::endl;
			throw ThreadException((char*) "unable to signal the thread waiting on condition variable");
		}
		#endif
	}

	/**
	 * if more than one threads are being blocked on the condition variable
	 * this will unblock all of them
	 * */
	template <typename Type>
	void ConditionVariable::signalAll(std::function<void(const Type&)> condUpdate, 
			const Type& value)
	{
		int state = EXIT_FAILURE;
		_mutexLock.lock();
		condUpdate(value);
		state = pthread_cond_broadcast(_cv);
		_mutexLock.unlock();
		#ifndef NDEBUG
		if (state != EXIT_SUCCESS)
		{
			std::cout << strerror(state) << std::endl;
			throw ThreadException((char*) "unable broadcast the threads waiting on condition variable");
		}
		#endif
	}

	/**
	 * The calling thread will wait on the condition, and blocked.
	 * It will also unlock the mutex variable specified.
	 * The specified mutex variable has to be locked by the caller thread before waiting
	 * Once the condition is met, the mutex is returned to the caller thread.
	 * 
	 * variable Type has to have == and != operators defined
	 * */
	template <typename Type>
	void ConditionVariable::waitForSignal(std::function<bool(const Type&)> condPredicate, 
			Type expectedValue)
	{
		// wait call expects the mutex to be locked before it can wait
		_mutexLock.lock();
		int state = EXIT_FAILURE;
		while (!condPredicate(expectedValue))
		{
			// the calling thread is put to sleep and the mutex lock is released
			// once the calling thread is woken up, the mutex is locked again
			state = pthread_cond_wait(_cv, _mutexLock._mutex);
		}
		// mutex is supposed to be unlocked once the condition is valid and wait is over
		_mutexLock.unlock();

		#ifndef NDEBUG
		if (state != EXIT_SUCCESS)
		{
			std::cout << strerror(state) << std::endl;
			throw ThreadException((char*) "unable to wait on a condition variable");
		}
		#endif
	}

	/**
	 * block the calling thread for a given time and release the mutex
	 * also stops the wait if signal or broadcast is recieved
	 * */
	template <typename Type>
	void ConditionVariable::timedWait(int waitTimeInSeconds, 
			std::function<bool(const Type&)> condPredicate, Type expectedValue)
	{
		struct timespec time;
		int state = EXIT_FAILURE;
		
		_mutexLock.lock();
		helperfxns::getCurrentTimeSpec(time);
		// also look into the following if above doesn't work
		// clock_gettime(CLOCK_MONOTONIC, &time);
		time.tv_sec += waitTimeInSeconds;
		
		while (!condPredicate(expectedValue))
		{
			state = pthread_cond_timedwait(_cv, _mutexLock._mutex, &time);
		}
		_mutexLock.unlock();
		
		#ifndef NDEBUG
		if (state != EXIT_SUCCESS)
		{
			std::cout << strerror(state) << std::endl;
			throw ThreadException((char*) "unable to set timed wait on condition variable");
		}
		#endif
	}

	/**
	 * returns access type
	 * */
	ProcessAccessType ConditionVariable::getAccessType() const noexcept
	{
		int accessType = -1;
		int retVal = pthread_condattr_getpshared(_cvAttrib, &accessType);
		
		if (retVal == EXIT_SUCCESS)
		{
			if (accessType == PTHREAD_PROCESS_SHARED)
				return ProcessAccessType::SHARED;
			else
				return ProcessAccessType::PRIVATE;
		}
		else
		{
			return ProcessAccessType::INVALID;
		}
	}

	void ConditionVariable::_setAccessType(ProcessAccessType accessType)
	{
		if (accessType == ProcessAccessType::SHARED)
			pthread_condattr_setpshared(_cvAttrib, PTHREAD_PROCESS_SHARED);
		else
			pthread_condattr_setpshared(_cvAttrib, PTHREAD_PROCESS_PRIVATE);
	}
}
