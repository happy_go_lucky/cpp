#ifndef THREAD_H
#define THREAD_H

/**
 * This class is an oop wrapper around pthreads.
 * 
 * */

#include <iostream>
#include <pthread.h>
#include <limits.h>
#include <string.h>
#include "ThreadException.h"
#include "ThreadDefs.h"

namespace posixthreads
{
	class Thread
	{
		public:
			Thread(DetachType detachType, int stackSize = PTHREAD_STACK_MIN);
			Thread(const Thread& thread) = delete;
			Thread(Thread&& thread) = delete;
			
			virtual ~Thread();
			
			Thread& operator= (const Thread& thread) = delete;
			Thread& operator= (Thread&& thread) = delete;
			
			inline unsigned int getId() const noexcept
			{
				return _id;
			}
			
			inline DetachType getDetachType() const noexcept
			{
				return _detachType;
			}
			
			void setCancelAttribs(CancelState cancelState, 
								CancelType cancelType);
			void start(void* arg = nullptr, void* returnVal = nullptr);
			void join();
			void requestCancel();
			void terminateThread(bool forceExit = false) const;
			
			virtual void* threadRun() = 0;
			
		protected:
			// this function will be used as the global function for pthread_create
			static void* _execute(void* thr);	// should call the run()
			void* _threadArgs;
			void* _returnVal;
			
		private:
			pthread_t _id;
			pthread_attr_t _attributes;
			DetachType _detachType;
			CancelState _cancelState;
			int _stackSize;
			bool _isStarted;
			bool _isSuspended;
			
			void _initArribs();
			void _setDetachType();
			void _setStackSize();
	};
}
#endif
