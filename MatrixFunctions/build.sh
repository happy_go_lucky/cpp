#!/bin/sh

readonly MAKEFILENAME="makefile";

clean() {
	echo "cleaning all object and executable files";
	make -f $MAKEFILENAME remove
}

compileAndBuild() {
	echo "compiling and building";
	make -f $MAKEFILENAME
}

clean;
compileAndBuild;
