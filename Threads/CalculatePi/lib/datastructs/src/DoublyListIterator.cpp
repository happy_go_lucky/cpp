#ifndef DOUBLY_LIST_ITERATOR_CPP
#define DOUBLY_LIST_ITERATOR_CPP

#include "../inc/DoublyListIterator.h"
#include "DoublyListNode.cpp"

namespace datastructs
{
	template <typename Type>
	DoublyListIterator<Type>::DoublyListIterator() noexcept
	{
		_nodePtr = nullptr;
	}

	template <typename Type>
	DoublyListIterator<Type>& DoublyListIterator<Type>::operator =(const DoublyListIterator<Type>& itr) & noexcept
	{
		if (*this != itr)
		{
			_nodePtr = itr._nodePtr;
		}
		
		return *this;
	}

	template <typename Type>
	DoublyListIterator<Type>& DoublyListIterator<Type>::operator =(DoublyListIterator<Type>&& itr) & noexcept
	{
		_nodePtr = std::move(itr._nodePtr);
		return *this;
	}

	template <typename Type>
	DoublyListIterator<Type>& DoublyListIterator<Type>::operator =(DoublyListNode<Type>* nodePtr) & noexcept
	{
		_nodePtr = nodePtr;
	}

	template <typename Type>
	Type& DoublyListIterator<Type>::operator *() const noexcept
	{
		return _nodePtr->_data;
	}

	template <typename Type>
	DoublyListIterator<Type>& DoublyListIterator<Type>::operator ++() noexcept
	{
		_nodePtr = _nodePtr->_nextNode;
		return *this;
	}

	template <typename Type>
	DoublyListIterator<Type>& DoublyListIterator<Type>::operator --() noexcept
	{
		_nodePtr = _nodePtr->_prevNode;
		return *this;
	}

	template <typename Type>
	DoublyListIterator<Type> DoublyListIterator<Type>::operator ++(int) noexcept
	{
		DoublyListIterator<Type> oldValue = *this;
		_nodePtr = _nodePtr->_nextNode;
		return oldValue;
	}

	template <typename Type>
	DoublyListIterator<Type> DoublyListIterator<Type>::operator --(int) noexcept
	{
		DoublyListIterator<Type> oldValue = *this;
		_nodePtr = _nodePtr->_prevNode;
		return oldValue;
	}
	
	template <typename Type>
	bool DoublyListIterator<Type>::operator ==(const DoublyListIterator<Type>& itr) const noexcept
	{
		return _nodePtr == itr._nodePtr;
	}

	template <typename Type>
	bool DoublyListIterator<Type>::operator !=(const DoublyListIterator<Type>& itr) const noexcept
	{
		return _nodePtr != itr._nodePtr;
	}

	template <typename Type>
	void DoublyListIterator<Type>::advance(unsigned int n) noexcept
	{
		while (n > 0 && _nodePtr != nullptr)
		{
			_nodePtr = _nodePtr->_nextNode;
			n--;
		}
	}

	template <typename Type>
	void DoublyListIterator<Type>::retreat(unsigned int n) noexcept
	{
		while (n > 0 && _nodePtr != nullptr)
		{
			_nodePtr = _nodePtr->_prevNode;
			n--;
		}
	}

	template <typename Type>
	Type& DoublyListIterator<Type>::getData() const noexcept
	{
		return _nodePtr->_data;
	}

	template <typename Type>
	bool DoublyListIterator<Type>::isNextNodeValid() const noexcept
	{
		return isNodeValid() && _nodePtr->_nextNode != nullptr;
	}

	template <typename Type>
	bool DoublyListIterator<Type>::isPrevNodeValid() const noexcept
	{
		return isNodeValid() && _nodePtr->prevNode != nullptr;
	}

	template <typename Type>
	bool DoublyListIterator<Type>::isNodeValid() const noexcept
	{
		return _nodePtr != nullptr;
	}
}
#endif
