#ifndef SEMAPHORE_NAMED_H
#define SEMAPHORE_NAMED_H

#include <cstdlib>
#include <climits>
#include <fcntl.h>
#include <sys/stat.h>
#include <semaphore.h>
#include <cstdio>
#include <iostream>
#include "HelperFxns.h"
#include "ThreadException.h"

namespace posixthreads
{
	class SemaphoreNamed
	{
		public:
			static const char NAME_PREFIX = '/';
			static const char NULL_CHAR = '\0';
			// named semaphores are located under /dev/shm, that's why -4
			static const int MAX_NAME_LENGTH = NAME_MAX - 4;
			static const int BASE_HEX = 16;
		
			SemaphoreNamed(const char* nameStr = nullptr);
			~SemaphoreNamed();
			
			SemaphoreNamed(const SemaphoreNamed& sem) = delete;
			SemaphoreNamed(SemaphoreNamed&& sem) = delete;
			SemaphoreNamed& operator =(const SemaphoreNamed& sem) = delete;
			SemaphoreNamed& operator =(SemaphoreNamed&& sem) = delete;
			
			void init(int semaphoreCount, mode_t accessPermissions);
			void wait();
			void post();
			void timedWait(int waitTimeInSeconds);
			const char* getName() const;
			void cleanup();

		private:
			sem_t* _semaphore;
			int _semaphoreCount;
			char* _semaphoreName;
	};
}
#endif
