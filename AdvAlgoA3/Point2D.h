#ifndef POINT_2D_H
#define POINT_2D_H

#include <initializer_list>
#include <iostream>

using std::initializer_list;
using std::ostream;
using std::cout;
using std::endl;

template <class T>
class Point2D
{
	public:
		Point2D() noexcept;
		Point2D(T& x, T& y) noexcept;
		Point2D(T&& x, T&& y) noexcept;
		Point2D(const Point2D<T>& point);
		Point2D(Point2D<T>&& point) noexcept;
		Point2D(initializer_list<T> list);
		~Point2D();
		
		template <typename U>
		friend ostream& operator <<(ostream& out, const Point2D<U>& point);
		
		Point2D<T>& operator =(const Point2D<T>& point);
		Point2D<T>& operator =(Point2D<T>&& point) noexcept;
		
		template <typename U>
		friend bool operator ==(const Point2D<U>& pointL, const Point2D<U>& pointR) noexcept;
		
		template <typename U>
		friend bool operator ==(Point2D<U>&& pointL, Point2D<U>&& pointR) noexcept;
		
		template <typename U>
		friend bool operator !=(const Point2D<U>& pointL, const Point2D<U>& pointR) noexcept;
		
		template <typename U>
		friend bool operator !=(Point2D<U>&& pointL, Point2D<U>&& pointR) noexcept;
		
		template <typename U>
		friend Point2D<U>& operator +(const Point2D<U>& pointL, const Point2D<U>& pointR);
		
		template <typename U>
		friend Point2D<U>& operator +(Point2D<U>&& pointL, Point2D<U>&& pointR);
		
		template <typename U>
		friend Point2D<U>& operator -(const Point2D<U>& pointL, const Point2D<U>& pointR);
		
		template <typename U>
		friend Point2D<U>& operator -(Point2D<U>&& pointL, Point2D<U>&& pointR);
		
		template <typename U>
		friend Point2D<U>& operator *(const Point2D<U>& pointL, const Point2D<U>& pointR);
		
		template <typename U>
		friend Point2D<U>& operator *(Point2D<U>&& pointL, Point2D<U>&& pointR);
		
		template <typename U>
		friend Point2D<U>& operator /(const Point2D<U>& pointL, const Point2D<U>& pointR);
		
		template <typename U>
		friend Point2D<U>& operator /(Point2D<U>&& pointL, Point2D<U>&& pointR);
		
		void setX(T& val);
		void setX(T&& val);
		void setY(T& val);
		void setY(T&& val);
		T getX() const noexcept;
		T getY() const noexcept;
	
	protected:
		T _x;
		T _y;
};

#endif
