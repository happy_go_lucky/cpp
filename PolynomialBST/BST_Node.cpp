#include "BST_Node.h"

template <typename T>
BST_Node<T>::BST_Node( const T& data, BST_Node<T>* leftNode, BST_Node<T>* rightNode )
{
	_data = data;
	_left = leftNode;
	_right = rightNode;
}

template <typename T>
BST_Node<T>::BST_Node( const BST_Node<T>& node )
{
	_data = node.data;
	_left = node.leftNode;
	_right = node.rightNode;
}

template <typename T>
BST_Node<T>::~BST_Node()
{
	
}

template <typename T>
T BST_Node<T>::getData() const
{
	return _data;
}

template <typename T>
BST_Node<T>* BST_Node<T>::getLeft() const
{
	return _left;
}

template <typename T>
BST_Node<T>* BST_Node<T>::getRight() const
{
	return _right;
}

template <typename T>
void BST_Node<T>::setData( const T data )
{
	_data = data;
}

template <typename T>
void BST_Node<T>::setLeft( BST_Node<T>* left )
{
	_left = left;
}

template <typename T>
void BST_Node<T>::setRight( BST_Node<T>* right )
{
	_right = right;
}

//~ template <typename T>
//~ T operator = ( const BST_Node<T>& otherNode )
//~ {
	//~ _data = otherNode.data;
	//~ _left = otherNode.leftNode;
	//~ _right = otherNode.rightNode;
//~ }
