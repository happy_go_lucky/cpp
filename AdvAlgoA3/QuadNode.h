#ifndef QUAD_NODE_H
#define QUAD_NODE_H

class QuadNode
{
	public:
		enum NodeType
		{
			DATA_NODE = 0,
			SUBDIVIDED_NODE,
			UNINITIALISED
		};
	
		QuadNode() noexcept;
		QuadNode(NodeType type) noexcept;
		~QuadNode();
		
		QuadNode::NodeType getType() const noexcept;
		bool isData() const noexcept;
		bool isSubDivided() const noexcept;
		
	private:
		NodeType _nodeType;
};

#endif
