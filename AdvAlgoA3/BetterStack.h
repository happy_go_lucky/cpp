#ifndef BETTER_STACK_H
#define BETTER_STACK_H

#include <utility>
#include <iostream>
#include <assert.h>
#include <initializer_list>

using std::cout;
using std::endl;
using std::cerr;

template <class T>
class BetterStack
{
	public:
		const int INIT_CAPACITY = 10;
	
		BetterStack() noexcept;
		BetterStack(size_t size);
		BetterStack(const BetterStack<T>& aStack);
		BetterStack(BetterStack<T>&& aStack) noexcept;
		BetterStack(std::initializer_list<T> list);
		
		~BetterStack();
		
		inline size_t size() const noexcept
		{
			return _size;
		}
		
		inline bool empty() const noexcept
		{
			return _size == 0; 
		}
		
		void push(const T& value);
		void push(T&& value);
		const T& pop() noexcept;	// undefined behaviour on empty
		const T& top() noexcept;	// undefined behaviour on empty
		
		template <typename U>
		friend std::ostream& operator <<(std::ostream& out, const BetterStack<U>& aStack);
		BetterStack<T>& operator =(const BetterStack<T>& aStack);
		BetterStack<T>& operator =(BetterStack<T>&& aStack) noexcept;
		bool operator ==(const BetterStack<T>& aStack) noexcept;
		bool operator ==(const BetterStack<T>&& aStack) noexcept;
		bool operator !=(const BetterStack<T>& aStack) noexcept;
		bool operator !=(const BetterStack<T>&& aStack) noexcept;
		
		void checkValidity() noexcept;
		void debugPrint() const noexcept;

	private:
		size_t _size;
		size_t _capacity;
		T* _stackArr;
		
		void increaseCapacity();
};

#endif
