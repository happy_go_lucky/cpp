#ifndef MATRIX_EXCEPTION_H
#define MATRIX_EXCEPTION_H

#include <exception>

class MatrixException : public std::exception
{
	public:
		MatrixException( char* = ( char* ) "Polynomial Exception!" );
		~MatrixException();
		virtual const char* what() const throw();
		
	private:
		char* _message;
};

#endif
