runme.exe: testHash.obj HashTable.obj LinkedList.obj ListNode.obj
	link testHash.obj HashTable.obj LinkedList.obj ListNode.obj /out:runme.exe
testHash.obj: testHash.cpp HashTable.h
	cl /c testHash.cpp
HashTable.obj: HashTable.cpp HashTable.h LinkedList.h
	cl /c HashTable.cpp
LinkedList.obj: LinkedList.cpp LinkedList.h ListNode.h
	cl /c LinkedList.cpp
ListNode.obj: ListNode.cpp ListNode.h
	cl /c ListNode.cpp
.PHONY: clean
clean:
	del *.obj
	del runme.exe