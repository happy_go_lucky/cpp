#include "BST_HelperFunctors.h"

template <typename T>
BaseFunct<T>::BaseFunct()
{
	
}

template <typename T>
void BaseFunct<T>::operator () ( BST_Node<T>* node ) //const
{
	if ( node == nullptr )
	{
		cout << '\0';
	}
}

template <typename T>
NullFunctor<T>::NullFunctor()
{
	
}

template <typename T>
void NullFunctor<T>::operator () ( BST_Node<T>* node ) //const
{
	if ( node == nullptr )
	{
		cout << '\0';
	}
}

template <typename T>
Counter<T>::Counter()
{
	_count = 0;
}

template <typename T>
void Counter<T>::operator () ( BST_Node<T>* node )
{
	if ( node != nullptr )
	{
		_count++;
	}
}

template <typename T>
int Counter<T>::getCount() const
{
	return _count;
}

template <typename T>
PrintNode<T>::PrintNode( char* delimiter )
{
	_delimiter = delimiter;
}

template <typename T>
void PrintNode<T>::operator () ( BST_Node<T>* node ) //const
{
	cout << node->getData() << _delimiter;
}

template <typename T>
DeleteNode<T>::DeleteNode()
{
	
}

template <typename T>
void DeleteNode<T>::operator () ( BST_Node<T>* node ) //const
{
	if ( node != nullptr )
	{
		delete node;
	}
}
