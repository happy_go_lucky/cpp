#include "QuadTree.h"

QuadTree::QuadTree() noexcept
{
	_root = nullptr;
	_numNodes = 0;
}

QuadTree::QuadTree(const QuadTree& tree)
{
	_root = tree._root;
	_numNodes = tree._numNodes;
	if (tree.isEmpty())
	{
		return;
	}
	
	//~ cloneTree(tree._root, _root);
}

QuadTree::~QuadTree()
{
	
}

ostream& operator <<(ostream& out, const QuadTree& tree)
{
	return out;
}

bool QuadTree::insertNode(const QuadTreeRecord& data)
{
	PRQuadData convertedData = data;
	Rectangle region = _worldRect;
	insert(data, &_root, region);
	
	return true;
}

void QuadTree::insert(const PRQuadData& data, PRQuadNode** ptrToNode, Rectangle& region)
{
	if (ptrToNode == nullptr)
	{
		return;
	}
	
	QuadrantType quadrant;
	if (region.isConverged())
	{
		cout << "the region can't be subdivided" << endl;
		return;
	}
	quadrant = getSubdivisionAndQuadrant(region, 
				data._long.getTotalSeconds(),
				data._lat.getTotalSeconds());
	
	//~ cout << "inserting data " << data << " at " << quadrant << endl;
	//~ cout << "in region: " << region << endl;
	if (quadrant == QuadrantType::NW)
	{
		if ((*ptrToNode) == nullptr)
		{
			*ptrToNode = new PRQuadNode();
			(*ptrToNode)->setDataNW(data);
			return;
		}
		if ((*ptrToNode)->hasSubDivided_NW())
		{
			insert(data, &((*ptrToNode)->_nextNW), region);
			return;
		}
		else
		{
			if ((*ptrToNode)->isEmpty_NW())
			{
				(*ptrToNode)->setDataNW(data);
			}
			else
			{
				PRQuadData existingData = (*ptrToNode)->getDataNW();
				
				if (existingData == data)
				{
					// ignore the duplicate data
					return;
				}
				else if (existingData.isSameCoords(data))
				{
					// append to list of offset
					(*ptrToNode)->updateOffsetNW(data);
					return;
				}
				else
				{
					// subdfvide this node.
					// save existing data and new data
					// insert both data one after other
					(*ptrToNode)->setLinkNW(new PRQuadNode(data));
					insert(existingData, &((*ptrToNode)->_nextNW), region);
					insert(data, &((*ptrToNode)->_nextNW), region);
					return;
				}
			}
		}
	}
	else if (quadrant == QuadrantType::NE)
	{
		if ((*ptrToNode) == nullptr)
		{
			*ptrToNode = new PRQuadNode();
			(*ptrToNode)->setDataNE(data);
			return;
		}
		if ((*ptrToNode)->hasSubDivided_NE())
		{
			insert(data, &((*ptrToNode)->_nextNE), region);
		}
		else
		{
			if ((*ptrToNode)->isEmpty_NE())
			{
				(*ptrToNode)->setDataNE(data);
			}
			else
			{
				PRQuadData existingData = (*ptrToNode)->getDataNE();
				
				if (existingData == data)
				{
					// ignore the duplicate data
					return;
				}
				else if (existingData.isSameCoords(data))
				{
					// append to list of offset
					(*ptrToNode)->updateOffsetNE(data);
					return;
				}
				else
				{
					// subdfvide this node.
					// save existing data and new data
					// insert both data one after other
					(*ptrToNode)->setLinkNE(new PRQuadNode(data));
					insert(existingData, &((*ptrToNode)->_nextNE), region);
					insert(data, &((*ptrToNode)->_nextNE), region);
					return;
				}
			}
		}
	}
	else if (quadrant == QuadrantType::SE)
	{
		if ((*ptrToNode) == nullptr)
		{
			*ptrToNode = new PRQuadNode();
			(*ptrToNode)->setDataSE(data);
			return;
		}
		if ((*ptrToNode)->hasSubDivided_SE())
		{
			insert(data, &((*ptrToNode)->_nextSE), region);
		}
		else
		{
			if ((*ptrToNode)->isEmpty_SE())
			{
				(*ptrToNode)->setDataSE(data);
			}
			else
			{
				PRQuadData existingData = (*ptrToNode)->getDataSE();
				
				if (existingData == data)
				{
					// ignore the duplicate data
					return;
				}
				else if (existingData.isSameCoords(data))
				{
					// append to list of offset
					(*ptrToNode)->updateOffsetSE(data);
					return;
				}
				else
				{
					// subdfvide this node.
					// save existing data and new data
					// insert both data one after other
					(*ptrToNode)->setLinkSE(new PRQuadNode(data));
					insert(existingData, &((*ptrToNode)->_nextSE), region);
					insert(data, &((*ptrToNode)->_nextSE), region);
					return;
				}
			}
		}
	}
	else  //QuadrantType::SW
	{
		if ((*ptrToNode) == nullptr)
		{
			*ptrToNode = new PRQuadNode();
			(*ptrToNode)->setDataSW(data);
			return;
		}
		if ((*ptrToNode)->hasSubDivided_SW())
		{
			insert(data, &((*ptrToNode)->_nextSW), region);
		}
		else
		{
			if ((*ptrToNode)->isEmpty_SW())
			{
				(*ptrToNode)->setDataSW(data);
			}
			else
			{
				PRQuadData existingData = (*ptrToNode)->getDataSW();
				
				if (existingData == data)
				{
					// ignore the duplicate data
					return;
				}
				else if (existingData.isSameCoords(data))
				{
					// append to list of offset
					(*ptrToNode)->updateOffsetSW(data);
					return;
				}
				else
				{
					// subdfvide this node.
					// save existing data and new data
					// insert both data one after other
					(*ptrToNode)->setLinkSW(new PRQuadNode(data));
					insert(existingData, &((*ptrToNode)->_nextSW), region);
					insert(data, &((*ptrToNode)->_nextSW), region);
					return;
				}
			}
		}
	}
}


bool QuadTree::isInBounds(float value) const noexcept
{
	return isValidLongitude(value) && isValidLatitude(value);
}

bool QuadTree::isValidLongitude(float value) const noexcept
{
	return value >= QuadTree::MIN_LONGITUDE && value <= QuadTree::MAX_LONGITUDE;
}

bool QuadTree::isValidLatitude(float value) const noexcept
{
	return value >= QuadTree::MIN_LATITUDE && value <= QuadTree::MAX_LATITUDE;
}

// clockwise starting from nw
template <typename Traverse>
void QuadTree::traverseTree(PRQuadNode* node,
							TraversalType traversalType,
							Traverse& action)
{
	if (node == nullptr)
	{
		cout << "tree is empty" << endl;
		return;
	}
	
	if (traversalType == LEVELORDER)
	{
		queue<PRQuadNode*> nodeQueue;
		nodeQueue.push(node);
		while (!nodeQueue.empty())
		{
			PRQuadNode* currNode = nodeQueue.front();
			
			if (currNode->hasSubDivided_NW())
				nodeQueue.push(currNode->_nextNW);
			if (currNode->hasSubDivided_NE())
				nodeQueue.push(currNode->_nextNE);
			if (currNode->hasSubDivided_SE())
				nodeQueue.push(currNode->_nextSE);
			if (currNode->hasSubDivided_SW())
				nodeQueue.push(currNode->_nextSW);
			
			action(currNode);
			nodeQueue.pop();
		}
	}
	else
	{
		BetterStack<PRQuadNode*> stack;
		stack.push(node);
		while (!stack.empty())
		{
			PRQuadNode* currNode = stack.pop();
			
			if (traversalType == PREORDER)
				action(currNode);
			
			if (currNode->hasSubDivided_NW())
				stack.push(currNode->_nextNW);
			if (currNode->hasSubDivided_NE())
				stack.push(currNode->_nextNE);
			
			if (traversalType == INORDER)
				action(currNode);
			
			if (currNode->hasSubDivided_SE())
				stack.push(currNode->_nextSE);
			if (currNode->hasSubDivided_SW())
				stack.push(currNode->_nextSW);
				
			if (traversalType == POSTORDER)
				action(currNode);
		}
	}
}
/*
void QuadTree::cloneTree(PRQuadNode* srcNode, PRQuadNode*& destNode)
{
	if (srcNode == nullptr)
	{
		return;
	}
	
	queue<PRQuadNode*> srcQueue;
	queue<PRQuadNode*> destQueue;
	
	destNode = srcNode;
	
	srcQueue.push(srcNode);
	destQueue.push(destNode);
	
	while (!srcQueue->empty())
	{
		PRQuadNode* currSrcNode = srcQueue.front();
		PRQuadNode* currDestNode = destQueue.front();
		
		if (currSrcNode->hasSubDivided_NW())
		{
			srcQueue.push(currSrcNode->_nextNW);
			destNode->setLinkNW(new PRQuadNode(currSrcNode->_nextNW));
			destQueue.push(destNode->_nextNW);
		}
		if (currSrcNode->hasSubDivided_NE())
		{
			srcQueue.push(currSrcNode->_nextNE);
			destNode->setLinkNE(new PRQuadNode(currSrcNode->_nextNE));
			destQueue.push(destNode->_nextNE);
		}
		if (currSrcNode->hasSubDivided_SE())
		{
			srcQueue.push(currSrcNode->_nextSE);
			destNode->setLinkSE(new PRQuadNode(currSrcNode->_nextSE));
			destQueue.push(destNode->_nextSE);
		}
		if (currSrcNode->hasSubDivided_SW())
		{
			srcQueue.push(currSrcNode->_nextSW);
			destNode->setLinkSW(new PRQuadNode(currSrcNode->_nextSW));
			destQueue.push(destNode->_nextSW);
		}
		
		srcQueue.pop();
		destQueue.pop();
	}
}
*/
void QuadTree::printTree(TraversalType traversalType, fstream* logPrint)
{
	(*logPrint) << "[ ";
	TraversalPrint printAction;
	printAction.logPrint = logPrint;
	traverseTree(_root, traversalType, printAction);
	(*logPrint) << " ]" << endl;
}

QuadTree::QuadrantType QuadTree::getQuadrant(int targetX, 
											int targetY, 
											int referenceX, 
											int referenceY) const noexcept
{
	// longiture on x axis
	// latitude on y axis
	if (targetX <= referenceX && targetY < referenceY) // SW or S
	{
		return QuadrantType::SW;
	}
	if (targetX >= referenceX && targetY > referenceY) // NE or N
	{
		return QuadrantType::NE;
	}
	if (targetX > referenceX && targetY <= referenceY) // SE or E
	{
		return QuadrantType::SE;
	}
	if (targetX < referenceX && targetY >= referenceY) // NW or W
	{
		return QuadrantType::NW;
	}
	return QuadrantType::INVALID;
}

void QuadTree::setWorldRect(const DMSCoord& left, const DMSCoord& right,
						const DMSCoord& bottom, const DMSCoord& top)
{
	_worldRect.x1 = left.getTotalSeconds();	// long
	_worldRect.x2 = right.getTotalSeconds(); // long
	_worldRect.y1 = bottom.getTotalSeconds(); // lat
	_worldRect.y2 = top.getTotalSeconds(); // lat
}

QuadTree::QuadrantType QuadTree::getSubdivisionAndQuadrant(Rectangle& subRegion,
															int targetX, int targetY)
{
	Rectangle region = subRegion;
	
	QuadrantType quadType = getQuadrant(targetX, targetY, 
								region.centerX(), region.centerY());
	
	if (quadType == QuadrantType::NW)
	{
		subRegion.x1 = region.x1;
		subRegion.x2 = region.centerX();
		subRegion.y1 = region.centerY();
		subRegion.y2 = region.y2;
	}
	else if (quadType == QuadrantType::NE)
	{
		subRegion.x1 = region.centerX();
		subRegion.x2 = region.x2;
		subRegion.y1 = region.centerY();
		subRegion.y2 = region.y2;
	}
	else if (quadType == QuadrantType::SE)
	{
		subRegion.x1 = region.centerX();
		subRegion.x2 = region.x2;
		subRegion.y1 = region.y1;
		subRegion.y2 = region.centerY();
	}
	else if (quadType == QuadrantType::SW)
	{
		subRegion.x1 = region.x1;
		subRegion.x2 = region.centerX();
		subRegion.y1 = region.y1;
		subRegion.y2 = region.centerY();
	}
	
	return quadType;
}

bool QuadTree::searchNode(const QuadTreeRecord& value, vector<size_t>& offsetList)
{
	return search(value, offsetList, _root);
}

bool QuadTree::search(const QuadTreeRecord& value,
					vector<size_t>& offsetList,
					PRQuadNode* node)
{
	if (node == nullptr)
	{
		return false;
	}
	bool isFound = true;
	if (node->hasSubDivided_NW())
	{
		return search(value, offsetList, node->_nextNW);
	}
	else
	{
		if (node->isEmpty_NW())
		{
			return false;
		}
		if (node->_dataNW.getLat() == value.primary_lat_dms &&
			node->_dataNW.getLong() == value.primary_long_dms)
		{
			offsetList = node->_dataNW.getOffsetList();
			return true;
		}
	}
	if (node->hasSubDivided_NE())
	{
		return search(value, offsetList, node->_nextNE);
	}
	else
	{
		if (node->isEmpty_NE())
		{
			return false;
		}
		if (node->_dataNE.getLat() == value.primary_lat_dms &&
			node->_dataNE.getLong() == value.primary_long_dms)
		{
			offsetList = node->_dataNE.getOffsetList();
			return true;
		}
	}
	if (node->hasSubDivided_SE())
	{
		return search(value, offsetList, node->_nextSE);
	}
	else
	{
		if (node->isEmpty_SE())
		{
			return false;
		}
		if (node->_dataSE.getLat() == value.primary_lat_dms &&
			node->_dataSE.getLong() == value.primary_long_dms)
		{
			offsetList = node->_dataSE.getOffsetList();
			return true;
		}
	}
	if (node->hasSubDivided_SW())
	{
		return search(value, offsetList, node->_nextSW);
	}
	else
	{
		if (node->isEmpty_SW())
		{
			return false;
		}
		if (node->_dataSW.getLat() == value.primary_lat_dms &&
			node->_dataSW.getLong() == value.primary_long_dms)
		{
			offsetList = node->_dataSW.getOffsetList();
			return true;
		}
	}
	
	return false;
}
