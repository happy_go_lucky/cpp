#include <iostream> 
#include <string>
#include <ctime>	// for random numbers
#include "SkipList.h"
#include "SkipList.cpp"

using std::cout;
using std::cin;
using std::endl;
using std::string;

const int MAX_RAND_STR_LEN = 5;
const int ASCII_a = 97;
const int NUM_ALPHA = 26;

int num_list_nodes = 0;
string outputDelimiter = " ";

int getRandom( int );
void setSeedForRandom();
void resetRandomGeneratorToDefault();

template <typename T>
void testCopyList( SkipList<T>&, bool );
template <typename T>
void testInsertList( SkipList<T>&, bool );
template <typename T>
void genRandValue( T&, int );
template <typename T>
void testFindList( SkipList<T>& );

void testWorstCase( SkipList<int>& );
void testBestCase( SkipList<int>& );

void getRand( string&, int );
void getRand( int&, int );
void getRand( char&, int );

int main()
{
	setSeedForRandom();
	
	bool testBestWorstCase = true;
	bool printList = false;	// set this flag to true to print the contents of the lists 
	cout << "Enter the size of list that you would like to test: ";
	cin >> num_list_nodes;
	
	if ( num_list_nodes < 1 )
	{
		num_list_nodes = 10;
	}
	
	SkipList<int> someList;
	testInsertList( someList, printList );
	testFindList( someList );
	testCopyList( someList, printList );
	
	if ( testBestWorstCase )
	{
		someList.reset();
		testBestCase( someList );
		someList.reset();
		testWorstCase( someList );
	}
	
	resetRandomGeneratorToDefault();
	return EXIT_SUCCESS;
}

template <typename T>
void testInsertList( SkipList<T>& list, bool printlist )
{
	T data;
	int ii = 0;
	while ( ii < num_list_nodes )
	{
		genRandValue( data, num_list_nodes );
		list.insert( data );
		ii++;
	}
	
	if ( printlist )
	{
		list.setOutputDelimiter( outputDelimiter );
		list.dumpTopList();
		cout << endl;
		cout << endl;
		list.printAllInOrder();
	}
	
	cout << "The number of traversals to add a value " << num_list_nodes << " nodes are: " << list.getTraversals() << endl;
}


template <typename T>
void testFindList( SkipList<T>& list )
{
	T data;
	genRandValue( data, num_list_nodes );
	cout << "Trying to find " << data << " in the list." << endl;
	cout << "Data is " << ( list.found( data ) ? "found" : "not found" ) << endl;
	cout << "It took " << list.getComparisons() << " comparisons" << endl;
}

template <typename T>
void testCopyList( SkipList<T>& list, bool printlist )
{
	cout << endl << "testing list copy and reset" << endl;
	cout << "copying list...." << endl;
	SkipList<T> myList = list;
	
	cout << "reseting original list" << endl;
	list.reset();
	
	if ( printlist )
	{
		cout << "printing original list" << endl;
		list.dumpTopList();
		cout << endl;
		cout << endl;
		list.printAllInOrder();
		
		cout << "printing new list" << endl;
		myList.dumpTopList();
		cout << endl;
		cout << endl;
		myList.printAllInOrder();
	}
	// extra test to show num traversals of one element in new copied list
	cout << "Testing insert to new list" << endl;
	int numTraversals = myList.getTraversals();
	T data;
	genRandValue( data, num_list_nodes );
	myList.insert( data );
	cout << "The number of traversals to add \"" << data << "\" in new list is: " << myList.getTraversals() - numTraversals << endl;
	
	if ( printlist )
	{
		myList.dumpTopList();
		cout << endl;
		cout << endl;
		myList.printAllInOrder();
	}
}

template <typename T>
void genRandValue( T& data, int max )
{
	getRand( data, max );
}

void getRand( string& data, int max )
{
	max = MAX_RAND_STR_LEN;
	int ii = 0;
	char* str = ( char* ) malloc( sizeof( char ) * max + 1 );
	
	while ( ii < max )
	{
		str[ii] = ASCII_a + getRandom( NUM_ALPHA );
		ii++;
	}
	str[ii] = '\0';	// terminate c string
	
	data = string( str );
	delete[] str;
}

void getRand( int& data, int max )
{
	data = getRandom( max );
}

void getRand( char& data, int max )
{
	max = NUM_ALPHA;
	data = ASCII_a + getRandom( max );
}

// implement random gen
void setSeedForRandom()
{
	srand( time( NULL ) );
}

void resetRandomGeneratorToDefault()
{
	unsigned int defaultSeed = 1;
	srand( defaultSeed );
}

// returns a vlaue from 0 to max - 1
int getRandom( int max )
{
	return rand() % max;
}

// works for list of type int only
void testBestCase( SkipList<int>& list )
{
	cout << "testing best case for decending order data" << endl;
	int ii = num_list_nodes;
	while ( ii > 0 )
	{
		list.insert( ii );
		ii--;
	}
	
	cout << "The number of traversals to add " << num_list_nodes << " nodes are: " << list.getTraversals() << endl;
	
	int data;
	getRand( data, num_list_nodes );
	cout << "Trying to find " << data << " in the list." << endl;
	cout << "Data is " << ( list.found( data ) ? "found" : "not found" ) << endl;
	cout << "It took " << list.getComparisons() << " comparisons" << endl;
}

// works for list of type int only
void testWorstCase( SkipList<int>& list )
{
	cout << "testing worst case for ascending order data" << endl;
	int ii = 0;
	while ( ii < num_list_nodes )
	{
		list.insert( ii );
		ii++;
	}
	int numTraversals = list.getTraversals();
	cout << "The number of traversals to add a value " << num_list_nodes << " nodes are: " << numTraversals << endl;
	list.insert( num_list_nodes );
	cout << "The worst case insert in a list of " << num_list_nodes << " is: " << list.getTraversals() - numTraversals << endl;
	
	int data;
	getRand( data, num_list_nodes );
	cout << "Trying to find " << data << " in the list." << endl;
	cout << "Data is " << ( list.found( data ) ? "found" : "not found" ) << endl;
	cout << "It took " << list.getComparisons() << " comparisons" << endl;
}
