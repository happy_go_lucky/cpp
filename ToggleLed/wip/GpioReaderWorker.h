#ifndef GPIO_READER_IMPL_H
#define GPIO_READER_IMPL_H

#include <iostream>
#include <fstream>
#include <string.h>
#include <functional>
#include <memory>

#include <posixthreads/inc/Thread.h>
#include <posixthreads/inc/MutexLock.h>
#include <posixthreads/inc/ReadWriteLock.h>
#include <posixthreads/inc/ConditionVariable.h>
#include <datastructs/inc/TaskQueue.h>
#include "GpioDefines.h"
#include "ThreadExecutionData.h"

class GpioReaderWorker : public posixthreads::Thread
{
	public:
		enum class ExecutionCondition : unsigned int
		{
			EXECUTE = 0,
			WAIT
		};
		
		GpioReaderWorker() = delete;

		GpioReaderWorker(const char* gpioPath, 
				std::shared_ptr<posixthreads::ReadWriteLock> rwLock) noexcept;
		~GpioReaderWorker();

		void* threadRun() override;
		std::string readGpio(gpio::GpioCommand gpioCommand) const;

	private:
		const char* _gpioPath;
		std::shared_ptr<posixthreads::ReadWriteLock> _rwLock;
		MutexLock _cvLock;
		posixthreads::ConditionVariable* _executionCV;
		ExecutionCondition _waitCondition;
		std::function<bool, const ExecutionCondition&> _condPredicateFunc;
		datastructs::TaskQueue<gpio::GpioCommand> _commandQueue;
		posixthreads::MutexLock _taskMutex;

		std::string _read(const char* gpioCmd) const;
		bool _shouldWait(ExecutionCondition condition);
		bool _shouldExecute(ExecutionCondition condition);
};

#endif
