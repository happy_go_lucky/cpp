#include "BST_Exception.h"

BST_Exception::BST_Exception( char* message, int eType )
{
	_message = message;
	_type = eType;
}

BST_Exception::~BST_Exception()
{
	
}

const char* BST_Exception::what() const throw()
{
	return _message;
}

int BST_Exception::type() const
{
	return _type;
}
