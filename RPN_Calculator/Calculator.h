#ifndef CALCULATOR_H
#define CALCULATOR_H

/**
 * implements an RPN (reverse polish notation/postfix) calculator
 * 
 * precondition: 
 * expects the input to have '=' as a flag for evaluation of commands
 * expects input to have a "quit" to signal end of input, otherwise, it will result in an infinite loop
 * 
 * sample input:
 * 
 * 			2 20 ^^ =
 * 			2 2 2 2 2 2 3 3 3 +=
 * 			quit
 * 
 * */

#include "Deque.h"
#include "Deque.cpp"
#include <fstream>
#include <iostream>

using std::cout;
using std::endl;
using std::cerr;
using std::ifstream;
using std::noskipws;	// flag to read whitespaces

class Calculator
{
	public:
		static const int ASCII_0 = 48;
		static const int BASE = 10;
		
		// calculator commands
		static constexpr char* CMD_QUIT = ( char* ) "quit";
		static const char CMD_EVAL = '=';
		static const char CMD_ADD = '+';
		static const char CMD_SUB = '-';
		static const char CMD_DIV = '/';
		static const char CMD_EXP = '^';
		static const char CMD_TIMES = '*';
	
		Calculator();
		Calculator( const Calculator& );
		~Calculator();
		
		// reads in input one character at a time and builds a queue of expression
		// returns true if enounters quit command
		bool input( const char );
		
		// loads a file to parse, supply a filename to it. It uses input() to process input commands
		void loadFile( const char* );
		
		Calculator& operator=( const Calculator& );
		
	private:
		int _buffer;			// used for evaluting value of integers more than one digit
		Deque<int> _exprDeque;	// expression deque, saves all the intergers to compute
		bool _isBuffered;		// flag used for evaluating integers more than one digit 
		bool _isValidExpr;		// flag used to validate/inavalidate expression used by eval()
		bool _quitReading;		// flag used to stop reading from file
		int _validatorIndex;	// used to evaluate quit command
		
		void _addToExprQueue( int );
		bool _isDigit( const char );					// checks if the character is valid digit between 0-9
		void _resetBuffer();							// resets the buffer used for evaluting value of integers
		int _charToInt_0_9( const char );				// converts char to int for 0-9 only
		// power function for exponents
		// precondtion: expects exponents to be non negative
		// postcondition: returns a positive value
		int _power( const int, int );
		void _printEvaluationSteps( int, int, char );	// used for debug puposes, prints the evaluation steps
		int _c_string_len( const char* );				// returns the c-string lenght
		bool _loadInputFile( ifstream&, const char* );	// checks for valid input file
		void _parseInputFileForCommands( ifstream& );	// starts reading input file one character at a time
		void _clearCalcMemory();						// resets calculator
		void _copyCalcMemory( const Calculator& );		// copies the member variables
		
		void _eval();		// evaluate expression deque when encountered '=' command
		bool _add();		// performs add operation on the expression deque when '+' command is encountered
		bool _subtract();	// performs subtraction operation on the expression deque when '+' command is encountered
		bool _divide();		// performs divide operation on the expression deque when '+' command is encountered
		bool _multiply();	// performs multiply operation on the expression deque when '+' command is encountered
		bool _exponent();	// performs exponent operation on the expression deque when '+' command is encountered
		bool _evalQuitFlag( const char );	// since calculator reads one character at a time, it evaluates quit command
};

#endif
