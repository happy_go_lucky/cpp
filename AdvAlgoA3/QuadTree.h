#ifndef QUAD_TREE_H
#define QUAD_TREE_H

#include <iostream>
#include <queue>
#include <vector>
#include "BetterStack.h"
#include "BetterStack.cpp"
#include "PRQuadNode.h"
#include "PRQuadData.h"
#include "QuadTreeRecord.h"
#include "TraversalActions.h"
#include "Rectangle.h"

using std::cout;
using std::endl;
using std::queue;
using std::vector;

class QuadTree
{
	public:
		const static short MIN_LATITUDE = -90;
		const static short MAX_LATITUDE = 90;
		const static short MIN_LONGITUDE = -180;
		const static short MAX_LONGITUDE = 180;
	
		enum TraversalType
		{
			INORDER = 0,
			PREORDER,
			POSTORDER,
			LEVELORDER
		};
		
		enum QuadrantType
		{
			NE = 0,
			NW,
			SE,
			SW,
			INVALID
		};
		
		QuadTree() noexcept;
		QuadTree(const QuadTree& tree);
		~QuadTree();
		
		friend ostream& operator <<(ostream& out, const QuadTree& tree);
		
		inline bool isEmpty() const noexcept
		{
			return _root == nullptr;
		}
		
		void printTree(TraversalType traversalType, fstream* logPrint);
		bool insertNode(const QuadTreeRecord& value);
		bool searchNode(const QuadTreeRecord& value, vector<size_t>& offsetList);
		void setWorldRect(const DMSCoord& left, const DMSCoord& right,
						const DMSCoord& bottom, const DMSCoord& top);
	
	private:
		PRQuadNode* _root;
		
		Rectangle _worldRect;
		size_t _numNodes;
		vector<Rectangle> _rectListNW;
		vector<Rectangle> _rectListNE;
		vector<Rectangle> _rectListSE;
		vector<Rectangle> _rectListSW;
		
		template <typename Traverse>
		void traverseTree(PRQuadNode* node, TraversalType traversalType, Traverse& action);
		void cloneTree(PRQuadNode* srcNode, PRQuadNode*& destNode);
		bool isInBounds(float value) const noexcept;
		bool isValidLongitude(float value) const noexcept;
		bool isValidLatitude(float value) const noexcept;
		void insert(const PRQuadData& data, PRQuadNode** ptrToNode, Rectangle& region);
		bool search(const QuadTreeRecord& value,
					vector<size_t>& offsetList,
					PRQuadNode* node);
		QuadTree::QuadrantType getQuadrant(int targetLat, 
											int targetLong, 
											int referenceLat, 
											int referenceLong) const noexcept;
		QuadTree::QuadrantType getSubdivisionAndQuadrant(Rectangle& subRegion,
														int targetX, int targetY);
		void printNode(PRQuadNode* node);
};

#endif
