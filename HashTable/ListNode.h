#ifndef LISTNODE_H
#define LISTNODE_H

template <class TYPE>
class ListNode
{
	public:
		ListNode();
		ListNode( const TYPE&, ListNode<TYPE>* );
		~ListNode();
		
		const TYPE& getData() const;
		ListNode<TYPE>* getPtr() const;
		void setData( const TYPE& );
		void setPtr( ListNode<TYPE>* );
		
	private:
		TYPE data;	// note to self: cannot assign NULL to every type.
		ListNode<TYPE>* next = NULL;
};

#endif