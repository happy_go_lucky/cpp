#ifndef	LIST_NODE_CPP
#define LIST_NODE_CPP

#include "../include/ListNode.h"

template <typename T>
ListNode<T>::ListNode() noexcept
{
	_data = T();
	_nextNode = nullptr;
}

template <typename T>
ListNode<T>::ListNode(const T& data, ListNode<T>* nextPtr) noexcept
{
	_data = data;
	_nextNode = nextPtr;
}

template <typename T>
ListNode<T>::ListNode(T&& data, ListNode<T>* nextPtr) noexcept
{
	_data = std::move(data);
	_nextNode = nextPtr;
}

template <typename T>
ListNode<T>::ListNode(const ListNode<T>& node)
{
	_data = node._data;
	_nextNode = nullptr;
}

template <typename T>
ListNode<T>::ListNode(ListNode<T>&& node) noexcept
{
	_data = std::move(node.data);
	_nextNode = std::move(node._nextNode);
	node._nextNode = nullptr;
}

template <typename T>
ListNode<T>::~ListNode()
{
	
}

template <typename T>
ListNode<T>& ListNode<T>::operator =(const ListNode<T>& node)
{
	if (this != &node)
	{
		_data = node._data;
		_nextNode = nullptr;
	}
	
	return *this;
}

template <typename T>
ListNode<T>& ListNode<T>::operator =(ListNode<T>&& node) noexcept
{
	if (this != &node)
	{
		_data = std::move(node._data);
		_nextNode = std::move(node._nextNode);
		node._nextNode = nullptr;
	}
	
	return *this;
}

template <typename T>
bool ListNode<T>::operator ==(const ListNode<T>& node) noexcept
{
	return _data == node._data;
}

template <typename T>
bool ListNode<T>::operator ==(const ListNode<T>&& node) noexcept
{
	return _data != node._data;
}

template <typename T>
bool ListNode<T>::operator !=(const ListNode<T>& node) noexcept
{
	return _data != node._data;
}

template <typename T>
bool ListNode<T>::operator !=(const ListNode<T>&& node) noexcept
{
	return _data != node._data;
}

template <typename T>
bool ListNode<T>::operator <(const ListNode<T>& node) noexcept
{
	return _data < node._data;
}

template <typename T>
bool ListNode<T>::operator <(const ListNode<T>&& node) noexcept
{
	return _data < node._data;
}

template <typename T>
bool ListNode<T>::operator >(const ListNode<T>& node) noexcept
{
	return _data > node._data;
}

template <typename T>
bool ListNode<T>::operator >(const ListNode<T>&& node) noexcept
{
	return _data > node._data;
}

template <typename U>
std::ostream& operator <<(std::ostream& out, const ListNode<U>& node)
{
	out << "data: " << node._data << std::endl;
	out << "ptr: " << node._nextNode << std::endl;
	return out;
}

#endif
