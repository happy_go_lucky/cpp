#include "../inc/GpioWaitForEdgeWorker.h"

namespace gpio
{
	GpioWaitForEdgeWorker::GpioWaitForEdgeWorker(std::string pinPath, int debounceTimeMillis, int timeout) : 
			posixthreads::Thread(posixthreads::DetachType::COOPERATIVE),
			_pinPath(pinPath),
			_timeout(timeout),
			_debounceTime(debounceTimeMillis)
	{
		#ifndef NDEBUG
		std::cout << "GpioWaitForEdgeWorker::GpioWaitForEdgeWorker" << std::endl;
		#endif
		
		_isWaiting = false;
		_waitMutex.initResources();
		_waitCV = new posixthreads::ConditionVariable(_waitMutex);
		_waitCV->initialize(posixthreads::ProcessAccessType::SHARED);
		conditionUpdate(posixthreads::ThreadCondition::WAIT);
		_command = ExecutionCommand::WAIT_FOR_COMMAND;
		
		_predicateFuncWrapper = 
				std::bind(&GpioWaitForEdgeWorker::conditionPredicate, this, std::placeholders::_1);
	}
	
	GpioWaitForEdgeWorker::~GpioWaitForEdgeWorker()
	{
		#ifndef NDEBUG
		std::cout << "GpioWaitForEdgeWorker::~GpioWaitForEdgeWorker" << std::endl;
		#endif
		
		_waitMutex.unlock();
		_waitMutex.cleanupResources();
		
		_waitCV->cleanup();
		delete _waitCV;
	}
	
	void GpioWaitForEdgeWorker::setDebounceTime(int timeInMillis)
	{
		_debounceTime = timeInMillis;
	}
	
	void* GpioWaitForEdgeWorker::threadRun()
	{
		/*
		 * The thread should wait for signal to do a task.
		 * There could be multiple tasks for a thread to do, and we would like this
		 * thread to wait for the signal and read the appropriate command to do it's
		 * tasks. It shouldn't automatically exit either, hence there will be a 
		 * command to exit.
		 * 
		 * */
		#ifndef NDEBUG
		std::cout << "GpioWaitForEdgeWorker::threadRun" << std::endl;
		#endif
		
		// if command is to exit the thread, then exit
		// if otherwise command is to run a task, then run that task
		switch (_command)
		{
			case ExecutionCommand::WAIT_FOR_COMMAND:
				#ifndef NDEBUG
				std::cout << "GpioWaitForEdgeWorker ExecutionCommand::WAIT_FOR_COMMAND" << std::endl;
				#endif
				_waitMutex.lock();
				_waitCV->waitForSignal(_predicateFuncWrapper, posixthreads::ThreadCondition::RUN);
				_waitMutex.unlock();
				threadRun();
			break;
			
			case ExecutionCommand::WAIT_FOR_EDGE:
				#ifndef NDEBUG
				std::cout << "GpioWaitForEdgeWorker ExecutionCommand::WAIT_FOR_EDGE" << std::endl;
				#endif
				_waitMutex.lock();
				_waitForEdge();
				conditionUpdate(posixthreads::ThreadCondition::WAIT);
				_command = ExecutionCommand::WAIT_FOR_COMMAND;
				_isWaiting = false;
				_waitMutex.unlock();
				threadRun();
			break;
			
			case ExecutionCommand::EXIT_THREAD:
			default:
				#ifndef NDEBUG
				std::cout << "GpioWaitForEdgeWorker ExecutionCommand::EXIT_THREAD" << std::endl;
				#endif
			break;
		}
		
		return EXIT_SUCCESS;
	}
	
	void GpioWaitForEdgeWorker::signalExit()
	{
		#ifndef NDEBUG
		std::cout << "GpioWaitForEdgeWorker::signalExit" << std::endl;
		#endif
		
		_waitMutex.lock();
		_isWaiting = false;
		_command = ExecutionCommand::EXIT_THREAD;
		std::function<void (posixthreads::ThreadCondition)> func = 
				std::bind(&GpioWaitForEdgeWorker::conditionUpdate, this, std::placeholders::_1);
		_waitCV->signal(func, posixthreads::ThreadCondition::RUN);
		_waitMutex.unlock();
	}
	
	void GpioWaitForEdgeWorker::signalWaitForEdge(std::function<void()> callbackFunction)
	{
		#ifndef NDEBUG
		std::cout << "GpioWaitForEdgeWorker::signalWaitForEdge" << std::endl;
		#endif
		
		_waitMutex.lock();
		_isWaiting = true;
		_callbackFunction = callbackFunction;
		_command = ExecutionCommand::WAIT_FOR_EDGE;
		std::function<void (posixthreads::ThreadCondition)> func = 
				std::bind(&GpioWaitForEdgeWorker::conditionUpdate, this, std::placeholders::_1);
		_waitCV->signal(func, posixthreads::ThreadCondition::RUN);
		_waitMutex.unlock();
	}
	
	int GpioWaitForEdgeWorker::_waitForEdge()
	{
		#ifndef NDEBUG
		std::cout << "GpioWaitForEdgeWorker::_waitForEdge" << std::endl;
		#endif
		
		int fd;	// the file descriptor we want to add to polling list
		const short MAX_EVENTS = 10;
		// this is the structure that stores the event that we want to monitor
		struct epoll_event epollEvent;
		struct epoll_event epollEventArr[MAX_EVENTS];
		
		// the epoll instance in the kernel
		// Since Linux 2.6.8, the size argument is ignored, but must be greater than zero
		int epollfd = epoll_create(1);
		if (epollfd == INVALID)
		{
		   std::cerr << "failed to open file " << std::strerror(errno) << std::endl;
		   return INVALID;
		}
		
		// open the file descriptor for the target pin
		#ifndef NDEBUG
		std::cout << "opening " << (_pinPath + GPIO_VALUE_CMD) << " for epolling" << std::endl;
		#endif
		if ((fd = open((_pinPath + GPIO_VALUE_CMD).c_str(), O_RDONLY | O_NONBLOCK)) == INVALID)
		{
		   std::cerr << "failed to open file " << std::strerror(errno) << std::endl;
		   return INVALID;
		}

		// ev.events = read operation | edge triggered | urgent data
		// events stores bitmasked data for the predefined constants
		// the constants represents the events that will be monitored for a file descriptor
		epollEvent.events = EPOLLIN | EPOLLET | EPOLLPRI;
		epollEvent.data.fd = fd;  // attach the file file descriptor

		//Register the file descriptor on the epoll instance
		if (epoll_ctl(epollfd, EPOLL_CTL_ADD, fd, &epollEvent) == INVALID)
		{
		   std::cerr << "failed to add fd to epoll list " << std::strerror(errno) << std::endl;
		   return INVALID;
		}
		
		int numEvents = 0;
		sigset_t currentSignalSet, originalSignalSet;	// used for interrupting wait
		sigemptyset(&currentSignalSet);	// initialize and empty a signal set
		sigaddset(&currentSignalSet, SIGUSR1);
		struct sigaction signalCatchAction;
		//~ signalCatchAction.sa_handler = &_sigHandler;	// this is used for sig handler
		
		sigemptyset(&signalCatchAction.sa_mask);
		signalCatchAction.sa_flags = SA_SIGINFO;	// when set returning from handler returns normally
		signalCatchAction.sa_sigaction = &_sigHandler;
		sigaction(SIGUSR1, &signalCatchAction, NULL);
		
		/*
		 * epoll_wait triggers an extra event at the start. The following is
		 * is a workaround to ignore that
		 * */
		const int MAX_BUFF = 10;
		char buff[MAX_BUFF];
		//--------------------------------------------------------------------
		
		while (_isWaiting) // should take a signal from parent thread to exit 
		{	
			/* 
			 * epoll_wait blocks the caller thread if no events are available
			 * instead of using epoll_pwait, the following 3 command eqivalent of 
			 * epoll_pwait is used becuase we are using pthreads.
			 * make sure these 3 calls are atomic for everything to work correctly
			 * */
			// the originalSignal set is also updated with old value
			// let the wait be interrupted by signal on request
			pthread_sigmask(SIG_SETMASK, &currentSignalSet, &originalSignalSet);
			numEvents = epoll_wait(epollfd, epollEventArr, MAX_EVENTS, _timeout);
			// prevent signal spamming until processing is done
			pthread_sigmask(SIG_SETMASK, &originalSignalSet, NULL);
			//~ epoll_ctl(epollfd, EPOLL_CTL_DEL, fd, &epollEvent);
			
			if (numEvents == INVALID)
			{
				std::cerr << "failed to epoll_wait " << std::strerror(errno) << std::endl;
				close(fd);
				return INVALID;
			}
			else if (numEvents == 0)
			{
				std::cerr << "call timedout before fd became ready " << std::endl;
				close(fd);
				return INVALID;
			}
			else
			{
				for (int ii = 0; ii < numEvents; ii++)
				{
					// epoll_wait will always report for EPOLLERR
					if (epollEventArr[ii].events & EPOLLIN)
					{
						/*
						 * if we have written 0 to the value of the pin at initialization
						 * the epoll event will read '0' character, hence the first trigger will
						 * be automatic and it will read a 0. and the last character will be a 
						 * carriage return character marking end of the file
						 * ignore if bytes are read, we are not reading anything
						 * */
						if (read(epollEventArr[ii].data.fd, buff, MAX_BUFF) == 0)
						{
							_callbackFunction();
							/**
							 * to respect the physical properties of the button we have to set
							 * the debounce time so that enough time is give to the button to
							 * return back to it's place
							 * */
							 std::cout << "sleeping for: " << _debounceTime * 1000 << std::endl;
							 usleep(_debounceTime * 1000);
						}
					}
				}
			}
		}
		
		// deregister the file descriptor from the epoll list
		if (epoll_ctl(epollfd, EPOLL_CTL_DEL, fd, &epollEvent) == INVALID)
		{
		   std::cerr << "failed to delete fd from epoll list " << std::strerror(errno) << std::endl;
		   return INVALID;
		}
		
		close(fd);
		close(epollfd);
		return EXIT_SUCCESS;
	}
	
	bool GpioWaitForEdgeWorker::conditionPredicate(posixthreads::ThreadCondition value)
	{
		return _condition == value;
	}
	
	void GpioWaitForEdgeWorker::conditionUpdate(posixthreads::ThreadCondition value)
	{
		_condition = value;
	}
	
	/**
	 * check struct ucontext for info. It's mostly meta data, and hardly used
	 * */
	void GpioWaitForEdgeWorker::_sigHandler(int signalNumber, siginfo_t* info, void* ucontext)
	{
		/**
		 * typedef struct {
				int si_signo;
				int si_code;
				union sigval si_value;
				int si_errno;
				pid_t si_pid;
				uid_t si_uid;
				void *si_addr;
				int si_status;
				int si_band;
			} siginfo_t;
		* */
		switch (signalNumber)
		{
			case SIGUSR1:
			#ifndef NDEBUG
			std::cout << "caught SIGUSR1" << std::endl;
			#endif
			if (info == nullptr)
			{
				#ifndef NDEBUG
				std::cerr << "signal info. is null" << std::endl;
				#endif
			}
			else
			{
				GpioWaitForEdgeWorker* obj = ((GpioWaitForEdgeWorker*) info->si_value.sival_ptr);
				obj->_isWaiting = false;
			}
			break;
			
			default:
			#ifndef NDEBUG
			std::cout << "unknown signal with signal number: " << signalNumber 
				<< " is received" << std::endl;
			if (ucontext == nullptr)
			{
				std::cout << "something went wrong" << std::endl;
			}
			#endif
			break;
		}
	}
}
