#include "Node.h"

template <typename TYPE>
Node<TYPE>::Node(  TYPE data, Node* next, Node* prev )
{
	this->data = data;
	this->next = next;
	this->prev = prev;
}

template <typename TYPE>
Node<TYPE>::~Node()
{
	
}

template <typename TYPE>
TYPE Node<TYPE>::getData() const
{
	return data;
}

template <typename TYPE>
void Node<TYPE>::setData( TYPE data )
{
	this->data = data;
}

template <typename TYPE>
Node<TYPE>* Node<TYPE>::getNext() const
{
	return next;
}

template <typename TYPE>
Node<TYPE>* Node<TYPE>::getPrev() const
{
	return prev;
}

template <typename TYPE>
void Node<TYPE>::setNext( Node<TYPE>* next )
{
	this->next = next;
}

template <typename TYPE>
void Node<TYPE>::setPrev( Node<TYPE>* prev )
{
	this->prev = prev;
}
