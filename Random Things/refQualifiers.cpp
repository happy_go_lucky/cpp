#include <iostream>

using namespace std;

struct Foo
{
    Foo()
    {
        std::cout << "Foo cons" << std::endl;
    }
    
    ~Foo() = default;
    Foo(const Foo& foo)
    {
        std::cout << "Foo copy cons" << std::endl;
    }
    
    Foo(Foo&& foo)
    {
        std::cout << "Foo move cons" << std::endl;
    }
    
    Foo(Foo* fooptr)
    {
        std::cout << "Foo ptr cons" << std::endl;
    }
    
	void bar() const & noexcept
	{
		std::cout << "const lvalue Foo\n";
	}
	void bar() & noexcept
	{
		std::cout << "lvalue Foo\n";
	}
	void bar() const && noexcept
	{
		std::cout << "const rvalue Foo\n";
	}
	void bar() && noexcept
	{
		std::cout << "rvalue Foo\n";
	}
	
	Foo& operator=(const Foo& value) & noexcept
	{
		std::cout << "assignment copy" << std::endl;
		return *this;
	}

	Foo& operator=(const Foo&& value) && noexcept
	{
		std::cout << "assignement move" << std::endl;
		return *this;
	}
	
	Foo& operator=(const Foo* value)  noexcept
	{
		std::cout << "assignement pointer" << std::endl;
		return *this;
	}
	
};

const Foo&& getConstFoo()
{
	return std::move(Foo());
}

Foo&& getFoo()
{
	return std::move(Foo());
}

int main()
{
	const Foo c_foo;
	Foo foo;
	Foo* fooPtr = new Foo();
	
	c_foo.bar();			// const lvalue Foo
	foo.bar();			  // lvalue Foo
	getConstFoo().bar();		 // [prvalue] const rvalue Foo
	Foo().bar();			// [prvalue] rvalue Foo
	//getFoo() = foo;
	getFoo() = getFoo();
	Foo foo2 = fooPtr;
	
	// xvalues bind to rvalue references, and overload resolution
	// favours selecting the rvalue ref-qualifier overloads.
	std::move(c_foo).bar(); // [xvalue] const rvalue Foo
	std::move(foo).bar();   // [xvalue] rvalue Foo
}
