#ifndef HASH_RECORD_H
#define HASH_RECORD_H

#include <cstring>
#include <string>
#include <iostream>

using namespace std;

struct HashRecord
{
	string feature_name;
	string state_alpha;
	size_t recordByteOffset;
	
	HashRecord& operator =(const HashRecord& record);
	bool operator ==(const HashRecord& record);
	friend ostream& operator <<(ostream& out, const HashRecord& record);
};

#endif
