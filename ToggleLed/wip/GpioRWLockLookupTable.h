#ifndef GPIO_RW_LOCK_LOOKUP_TABLE_H
#define GPIO_RW_LOCK_LOOKUP_TABLE_H

#include <vector>
#include <memory>
#include "GpioDefines.h"
#include <posixthreads/inc/ReadWriteLock.h>

using std::vector;

class GpioRWLockLookupTable
{
	public:
		GpioRWLockLookupTable() noexcept;
		~GpioRWLockLookupTable();
		
		GpioRWLockLookupTable(const GpioRWLockLookupTable& lookupTable) = delete;
		GpioRWLockLookupTable(GpioRWLockLookupTable&& lookupTable) = delete;
		GpioRWLockLookupTable& operator =(const GpioRWLockLookupTable& lookupTable) = delete;
		GpioRWLockLookupTable& operator =(GpioRWLockLookupTable&& lookupTable) = delete;
		
		std::shared_ptr<posixthreads::ReadWriteLock> 
				getReferenceToLock(gpio::PinNumber pinNumber) noexcept;
		std::shared_ptr<posixthreads::ReadWriteLock> 
				getReferenceToLock(gpio::GpioNumber gpioPinNumber) noexcept;
	
	private:
		vector<std::shared_ptr<posixthreads::ReadWriteLock>>* _rwLocks;
		
		std::shared_ptr<posixthreads::ReadWriteLock> _initLock(size_t pin);
};

#endif
