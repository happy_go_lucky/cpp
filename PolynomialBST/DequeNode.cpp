#include "DequeNode.h"

template <typename TYPE>
DequeNode<TYPE>::DequeNode(  TYPE data, DequeNode* next, DequeNode* prev )
{
	this->data = data;
	this->next = next;
	this->prev = prev;
}

template <typename TYPE>
DequeNode<TYPE>::~DequeNode()
{
	
}

template <typename TYPE>
TYPE DequeNode<TYPE>::getData() const
{
	return data;
}

template <typename TYPE>
void DequeNode<TYPE>::setData( TYPE data )
{
	this->data = data;
}

template <typename TYPE>
DequeNode<TYPE>* DequeNode<TYPE>::getNext() const
{
	return next;
}

template <typename TYPE>
DequeNode<TYPE>* DequeNode<TYPE>::getPrev() const
{
	return prev;
}

template <typename TYPE>
void DequeNode<TYPE>::setNext( DequeNode<TYPE>* next )
{
	this->next = next;
}

template <typename TYPE>
void DequeNode<TYPE>::setPrev( DequeNode<TYPE>* prev )
{
	this->prev = prev;
}
