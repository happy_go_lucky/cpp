#ifndef STATE_TRANSIT_CONDITIONS_H
#define STATE_TRANSIT_CONDITIONS_H

/**
 * following transit conditions in expression are identified
 * 1. bracket open: element is operator and element == '('
 * 2. valid number: element is a number
 * 3. valid operator: element is an operator other than '(', ')', '='
 * 4. bracket close: element is operator and element == ')'
 * 5. equals: element is operator and element == '='
 * 
 * */

#include "GlobalVariables.h"
#include "ExprElement.h"
#include "ElementOperand.h"
#include "ElementOperator.h"
#include "FSM_Helper.h"

class Base_TC
{
	public:
		Base_TC() = default;
		~Base_TC() = default;
		virtual bool operator ()(ExprElement* elem, 
								const GlobalVariables::StateActionParams& actionParams) = 0;
		
		static GlobalVariables globalVars;
		
		inline bool isOperator(ExprElement* elem)
		{
			return elem->getType() == ElementType::TYPE_OPERATOR;
		}
		
		inline char getOperatorType(ExprElement* elem)
		{
			return static_cast<ElementOperator*>(elem)->getData();
		}
		
		inline bool areParamsValid(const GlobalVariables::StateActionParams& actionParams)
		{
			return actionParams.bracketCount >= 0 &&
					actionParams.operandCount >= 0 &&
					actionParams.operatorCount >= 0 &&
					actionParams.avgOpCount >= 0;
		}
};

class OperandInfix_TC : public Base_TC
{
	public:
		OperandInfix_TC() = default;
		~OperandInfix_TC() = default;
		bool operator ()(ExprElement* elem, 
						const GlobalVariables::StateActionParams& actionParams);
};

class OperatorInfix_TC : public Base_TC
{
	public:
		OperatorInfix_TC() = default;
		~OperatorInfix_TC() = default;
		bool operator ()(ExprElement* elem, 
						const GlobalVariables::StateActionParams& actionParams);
};

class AvgBeginInfix_TC : public Base_TC
{
	public:
		AvgBeginInfix_TC() = default;
		~AvgBeginInfix_TC() = default;
		bool operator ()(ExprElement* elem, 
						const GlobalVariables::StateActionParams& actionParams);
};

class AvgEndInfix_TC : public Base_TC
{
	public:
		AvgEndInfix_TC() = default;
		~AvgEndInfix_TC() = default;
		bool operator ()(ExprElement* elem, 
						const GlobalVariables::StateActionParams& actionParams);
};

class BracketCloseToOpen_TC : public Base_TC
{
	public:
		BracketCloseToOpen_TC() = default;
		~BracketCloseToOpen_TC() = default;
		bool operator ()(ExprElement* elem, 
						const GlobalVariables::StateActionParams& actionParams);
};

class BracketCloseToOperand_TC : public Base_TC
{
	public:
		BracketCloseToOperand_TC() = default;
		~BracketCloseToOperand_TC() = default;
		bool operator ()(ExprElement* elem, 
						const GlobalVariables::StateActionParams& actionParams);
};

class AvgEndLoop_TC : public Base_TC
{
	public:
		AvgEndLoop_TC() = default;
		~AvgEndLoop_TC() = default;
		bool operator ()(ExprElement* elem, 
						const GlobalVariables::StateActionParams& actionParams);
};


class OperandPrefix_TC : public Base_TC
{
	public:
		OperandPrefix_TC() = default;
		~OperandPrefix_TC() = default;
		bool operator ()(ExprElement* elem, 
						const GlobalVariables::StateActionParams& actionParams);
};

class OperatorPrefix_TC : public Base_TC
{
	public:
		OperatorPrefix_TC() = default;
		~OperatorPrefix_TC() = default;
		bool operator ()(ExprElement* elem, 
						const GlobalVariables::StateActionParams& actionParams);
};

class OperandPostfix_TC : public Base_TC
{
	public:
		OperandPostfix_TC() = default;
		~OperandPostfix_TC() = default;
		bool operator ()(ExprElement* elem, 
						const GlobalVariables::StateActionParams& actionParams);
};

class OperatorPostfix_TC : public Base_TC
{
	public:
		OperatorPostfix_TC() = default;
		~OperatorPostfix_TC() = default;
		bool operator ()(ExprElement* elem, 
						const GlobalVariables::StateActionParams& actionParams);
};

class EvalOperatorInfix_TC : public Base_TC
{
	public:
		EvalOperatorInfix_TC() = default;
		~EvalOperatorInfix_TC() = default;
		bool operator ()(ExprElement* elem, 
						const GlobalVariables::StateActionParams& actionParams);
};

class BracketOpen_TC : public Base_TC
{
	public:
		BracketOpen_TC() = default;
		~BracketOpen_TC() = default;
		bool operator ()(ExprElement* elem, 
						const GlobalVariables::StateActionParams& actionParams);
};

class BracketClose_TC : public Base_TC
{
	public:
		BracketClose_TC() = default;
		~BracketClose_TC() = default;
		bool operator ()(ExprElement* elem, 
						const GlobalVariables::StateActionParams& actionParams);
};

class EvalOperatorPnP_TC : public Base_TC
{
	public:
		EvalOperatorPnP_TC() = default;
		~EvalOperatorPnP_TC() = default;
		bool operator ()(ExprElement* elem, 
						const GlobalVariables::StateActionParams& actionParams);
};

class ToAvgPostfix_TC : public Base_TC
{
	public:
		ToAvgPostfix_TC() = default;
		~ToAvgPostfix_TC() = default;
		bool operator ()(ExprElement* elem, 
						const GlobalVariables::StateActionParams& actionParams);
};

class ToAvgPrefix_TC : public Base_TC
{
	public:
		ToAvgPrefix_TC() = default;
		~ToAvgPrefix_TC() = default;
		bool operator ()(ExprElement* elem, 
						const GlobalVariables::StateActionParams& actionParams);
};

#endif
