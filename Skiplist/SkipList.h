#ifndef SKIPLIST_H
#define SKIPLIST_H

#include "Node.h"
#include "Node.cpp"
#include <ctime>	// for random numbers
#include <string>
#include <iostream>

using std::string;
using std::cout;
using std::endl;

// the list is implemented using a dummy node
// the list uses coin flip method to build helper list and can be extened by changing MAX_LEVEL to more than 2 levels
template <class T>
class SkipList
{
	public:
		static const string DEFAULT_DELIM;
		static const int SKIP_RATIO = 2;
		static const int MAX_LEVEL = 2;
		static const int MAIN_LIST = 0;
		static const int NUM_COIN_SIDES = 2;
		static const int HEADS = 0;
		static const int TAILS = 1;
		
		SkipList();
		SkipList( const SkipList& );
		~SkipList();
		
		// inserts a node to main list and builds the helper list based on coin flip method
		void insert( const T );
		// searches for a node using the helper lists
		bool found( const T );
		bool isEmpty() const;
		// returns number of traversals done to build the list using insert
		int getTraversals() const;
		// returns the number of comparisons done to search for an element in the list
		int getComparisons() const;
		
		// prints the list nodes of the main(bottom) list delimited by deleimiter 
		void printAllInOrder() const;
		// sets the string output delimited, default delimiter: " \n"
		void setOutputDelimiter( const string );
		// prints the top most skip (helper) list
		void dumpTopList();
		
		// length of skiplist
		int numberOfElements() const;
		
		// deletes all nodes in the skiplist except the dummy node, to reset skiplist to 0 elements
		void reset();
		
		SkipList<T>& operator= ( const SkipList<T>& );
		
	private:
		Node<T>** heads;	// collection of the heads for all lists used in the system of lists
		string delimiter;
		int numComparisons;
		int numTraversals;
		
		// reset the number of comparison and traversals to 0
		void _resetStats();
		// copies the contents of the provided list to current list
		void _copyList( const SkipList<T>& );
		// deletes all nodes (including the dummy node) of all of the lists used in the system of lists
		void _deleteSkipList();
		// helper funtion to delete individual linked list
		void _deleteList( Node<T>* );
		// helper function to print a linked list
		void _print( int, string ) const;
		// initializes the heads to point to a dummy node for all the lists depending on the mam levels of lists used in the skiplist
		void _initHeadArray();
		// helper function to reset the skip lists. Deletes all nodes expcet dummy node and points them to nullptr
		void _resetSkipList();
		
		// returns a random positive integer HEADS=0 or TAILS=1
		// used to build the system of list for the skiplist
		int _coinFlip();
		void _setSeedForRandomInt();
		void _resetRandomGeneratorToDefault();
};

#endif
