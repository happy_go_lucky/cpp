#ifndef TRAVERSAL_BASE_ACTION_H
#define TRAVERSAL_BASE_ACTION_H

#include "PRQuadNode.h"

class TraversalBaseAction
{
	public:
		TraversalBaseAction() = default;
		~TraversalBaseAction() = default;
		
		virtual void operator ()(PRQuadNode* node) = 0;
};

class TraversalBaseActionTwoNodes
{
	public:
		TraversalBaseActionTwoNodes() = default;
		~TraversalBaseActionTwoNodes() = default;
		
		virtual bool operator ()(PRQuadNode* node1, PRQuadNode* node2) = 0;
};


#endif
