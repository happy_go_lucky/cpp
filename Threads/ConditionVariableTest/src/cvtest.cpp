/**
 * This program implements message passing events between threads of a
 * process using condition variables.
 * 
 * The main thread creates server threads that are waiting for the task signals
 * from the main thread, and based on the task signals they complete their work
 * and signal back to main thread the results
 * 
 * */

#include <iostream>
#include <posixthreads/inc/Thread.h>
#include <posixthreads/inc/ConditionVariable.h>
#include <posixthreads/inc/MutexLock.h>

#include "../inc/ServerThread.h"

int main(int argC, char* argV[])
{
	std::cout << "Main thread created" << std::endl;
	
	std::cout << "Creating worker threads" << std::endl;
	ServerThread
}
