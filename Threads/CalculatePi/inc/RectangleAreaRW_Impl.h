#ifndef RECTANGLE_AREA_RW_IMPL_H
#define RECTANGLE_AREA_RW_IMPL_H

#include <posixthreads/inc/ThreadDefs.h>
#include <posixthreads/inc/Thread.h>
#include "Rectangle.h"
#include <posixthreads/inc/MutexLock.h>
#include <posixthreads/inc/ReadWriteLock.h>
#include "RectAreaImplArgs.h"

class RectangleAreaRW_Impl : public posixthreads::Thread
{
	public:
		RectangleAreaRW_Impl();
		~RectangleAreaRW_Impl();
		
		void* threadRun();
	
	private:
		posixthreads::MutexLock* _mutexLock;
		posixthreads::ReadWriteLock rwLock;
	
};

#endif
