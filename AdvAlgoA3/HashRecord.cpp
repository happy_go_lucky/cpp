#include "HashRecord.h"

HashRecord& HashRecord::operator =(const HashRecord& record)
{
	if (this != &record)
	{
		feature_name = record.feature_name;
		state_alpha = record.state_alpha;
		recordByteOffset = record.recordByteOffset;
	}
	return *this;
}

bool HashRecord::operator ==(const HashRecord& record)
{
	return feature_name.compare(record.feature_name) == 0 &&
			state_alpha.compare(record.state_alpha) == 0;
}

ostream& operator <<(ostream& out, const HashRecord& record)
{
	out << "[" << record.feature_name << ":" << record.state_alpha;
	out << ", " << "[" << record.recordByteOffset << "]]";
	return out;
}
