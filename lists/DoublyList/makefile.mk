ver = c++14

project = doublyTest
obj_dir = obj
src_dir = src
bin_dir = bin
inc_dir = inc
dep_dir = dep
lib_dir = lib

#0-3 >
optimize_lvl = 0
# 0: no info, 3 max info.
debug_lvl = 3

c_flags = -O$(optimize_lvl) -pedantic -Wpedantic -Wall \
	-Wextra -pthread -std=$(ver) -g$(debug_lvl) -ggdb$(debug_lvl)

# include the shared/static libraries. -labc loads libabc.a/libabc.so
# the typical location of these libraries is /usr/lib
libs = -lpthread

# -MF $(dep_dir)/$*.d: set the dependency file to depname.d
# MT is target
# MF is the filename where the dependencies are saved to
gen_deps = -MT $(@:.d=.o) -MM -MF $(dep_dir)/$*.d

# specify all the include directories here
vpath %.cpp $(src_dir)
vpath %.h $(inc_dir)
vpath %.d $(dep_dir)

# this path is used to direct linker to look for libs outside of /usr/lib/
datastruct_lib = -I$(lib_dir)/source/datastructs/
libs_inc_path = $(datastruct_lib)

# this path is needed for runtime execution of library file since it's
# present at a non-default location 
libs_runtime_path = -Wl,-rpath=$(lib_dir)/

# this path is to point the gcc to non-default library location 
libs_path = -L$(lib_dir)/

subdirs = $(obj_dir) $(dep_dir) $(bin_dir) $(lib_dir)

src_files = doublyTest.cpp\
	DoublyList.cpp\
	DoublyListIterator.cpp\
	DoublyListNode.cpp

obj_files = $(src_files:.cpp=.o)
dep_files = $(src_files:.cpp=.d)
obj_file_loc = $(addprefix $(obj_dir)/,$(obj_files))
dep_file_loc = $(addprefix $(dep_dir)/,$(dep_files))

all: $(project)

$(project): $(obj_files) | setup
	$(CXX) $(obj_file_loc) $(libs_inc_path) $(libs_path) $(libs) $(libs_runtime_path) -o $(bin_dir)/$@

%.o: %.cpp
	$(CXX) $(c_flags) $(libs_inc_path) -c $< -o $(obj_dir)/$@

%.d: %.cpp
	$(CXX) $(c_flags) $< $(gen_deps)

#include must not precede the default target
include $(dep_files)

setup:
	@mkdir -p $(obj_dir)
	@mkdir -p $(dep_dir)
	@mkdir -p $(bin_dir)
	@mkdir -p $(lib_dir)

#.DEFAULT_GOAL = help

.PHONY: setup
.PHONY: clean
.PHONY: all

clean:
	@rm -f $(obj_dir)/*.o
	@rm -f $(dep_dir)/*.d
	@rm -f $(bin_dir)/$(project)
	@echo cleaned all temporary files

