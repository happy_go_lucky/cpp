#ifndef	DOUBLY_LIST_NODE_CPP
#define DOUBLY_LIST_NODE_CPP

#include "../inc/DoublyListNode.h"

namespace datastructs
{
	template <typename Type>
	DoublyListNode<Type>::DoublyListNode(const Type& data, DoublyListNode<Type>* nextPtr, 
					DoublyListNode<Type>* prevPtr) noexcept : _data (data)
	{
		_nextNode = nextPtr;
		_prevNode = prevPtr;
	}

	template <typename Type>
	DoublyListNode<Type>::DoublyListNode(Type&& data, DoublyListNode<Type>* nextPtr, 
					DoublyListNode<Type>* prevPtr) noexcept : _data (std::move(data))
	{
		_nextNode = nextPtr;
		_prevNode = prevPtr;
	}

	template <typename Type>
	DoublyListNode<Type>::DoublyListNode(const DoublyListNode<Type>& node) noexcept : _data (node.data)
	{
		_nextNode = nullptr;
		_prevNode = nullptr;
	}

	template <typename Type>
	DoublyListNode<Type>::DoublyListNode(DoublyListNode<Type>&& node) noexcept : _data(std::move(node.data))
	{
		_nextNode = std::move(node._nextNode);
		_prevNode = std::move(node._prevNode);
		
		node._nextNode = nullptr;
		node._nextNode = nullptr;
	}

	template <typename Type>
	DoublyListNode<Type>::~DoublyListNode()
	{
		// nothing to do
	}

	template <typename Type>
	DoublyListNode<Type>& DoublyListNode<Type>::operator =(const DoublyListNode<Type>& node) & noexcept
	{
		if (this != &node)
		{
			_data = node._data;
			_nextNode = node._nextNode;
			_prevNode = node._prevNode;
		}
		
		return *this;
	}

	template <typename Type>
	DoublyListNode<Type>& DoublyListNode<Type>::operator =(DoublyListNode<Type>&& node) & noexcept
	{
		_data = std::move(node._data);
		_nextNode = std::move(node._nextNode);
		_prevNode = std::move(node._prevNode);
		
		node._nextNode = nullptr;
		node._prevNode = nullptr;
		
		return *this;
	}

	template <typename Type>
	bool DoublyListNode<Type>::operator ==(const DoublyListNode<Type>& node) const noexcept
	{
		return _data == node._data;
	}

	template <typename Type>
	bool DoublyListNode<Type>::operator !=(const DoublyListNode<Type>& node) const noexcept
	{
		return _data != node._data;
	}

	template <typename U>
	std::ostream& operator <<(std::ostream& out, const DoublyListNode<U>& node)  noexcept
	{
		out << "data: " << node._data << std::endl;
		out << "next ptr: " << node._nextNode << std::endl;
		out << "prev ptr: " << node._prevNode << std::endl;
		
		return out;
	}
}
#endif
