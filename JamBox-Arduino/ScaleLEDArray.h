#ifndef SCALE_LED_H
#define SCALE_LED_H

#include "Arduino.h"

class ScaleLEDArray
{
  public:
    const static int MAX_SCALE = 6;
    enum SCALE_LED_PINS { SCALE_1_LED = 18, SCALE_2_LED = 19, SCALE_3_LED = 20, SCALE_4_LED = 21, SCALE_5_LED = 22, SCALE_6_LED = 24 };
    const static int SCALE_LED_ARR[MAX_SCALE];
    
    ScaleLEDArray();
    ~ScaleLEDArray();

    void setScale( const int );
    
  private:
    void _initScaleLEDs();
    void _resetScale();
};

#endif
