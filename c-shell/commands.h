#ifndef COMMANDS_H
#define COMMANDS_H

const char* SHELL_NAME = "mini-shell >";

const char* QUIT_COMMAND = "exit";
const char* QUIT_COMMAND_ALT = "quit";

const short LINE_LEN = 80;
const short MAX_ARGS = 64;
const short MAX_PATHS = 64;
const short MAX_PATH_LEN = 96;
const char* WHITESPACE = " ,\t&\n";
const short COMMAND_BUFFER = 1024;

const char* SHELL_META_CHARACTERS = "|&<>";
const char META_PIPE = '|';
const char META_BG = '&';
const char META_OUT = '>';
const char META_IN = '<';

const short PIPE_READ_END = 0;
const short PIPE_WRITE_END = 1;

enum COMMAND_TYPE { COMMAND = 0, ENV_VAR };

const char* PIPE_BUFFER_FILE = "pipe_file.txt";


#endif
