#ifndef MATRIX_CPP
#define	MATRIX_CPP

#include "../include/Matrix.h"

template <typename Object>
Matrix<Object>::Matrix() noexcept
{
	
}

template <typename Object>
Matrix<Object>::Matrix(size_t rows, size_t cols) noexcept : _array{ rows }
{
	for (auto& thisRow : _array)
	{
		thisRow.resize(cols);
	}
}

template <typename Object>
Matrix<Object>::Matrix(initializer_list<BetterVector<Object>> lst) : _array(lst.size())
{
	size_t i = 0;
	for(auto& v : lst)
	{
		_array[i++] = std::move(v);
	}
}

template <typename Object>
Matrix<Object>::~Matrix()
{
	
}

template <typename Object>
const BetterVector<Object>& Matrix<Object>::operator[](size_t row) const
{
	return _array[row];
}

template <typename Object>
BetterVector<Object>& Matrix<Object>::operator[](size_t row)
{
	return _array[row];
}

template <typename Object>
size_t Matrix<Object>::numrows() const noexcept
{
	return _array.size();
}

template <typename Object>
size_t Matrix<Object>::numcols() const noexcept
{
	return numrows() ? _array[0].size() : 0;
}

template <typename T>
ostream &operator << (ostream& s, const Matrix<T>& m)
{
	size_t rows = m.numrows();
	size_t cols = m.numcols();
	for (size_t i = 0; i < rows; i++)
	{
		BetterVector<int> row = m[i];
		for (size_t j = 0; j < cols; j++)
			s << row[j] << " ";
		s << endl;
	}
	return s;
}

template <typename Object>
void Matrix<Object>::resize(size_t rows, size_t cols)
{
	_array.resize(rows);
	const size_t& size = _array.size();
	for (size_t ii = 0; ii < size; ii++)
	{
		_array[ii].resize(cols);
	}
}

#endif
