#include "HashTable.h"

HashTable::HashTable() noexcept
{
	_capacity = INIT_CAPACITY;
	_loadFactor = LOAD_FACTOR;
	_size = 0;
	_longestProbeSequence = 0;
	
	createHash();
}

HashTable::HashTable(size_t capacity, float loadFactor) noexcept
{
	_capacity = capacity;
	_loadFactor = loadFactor;
	_size = 0;
	_longestProbeSequence = 0;
	
	createHash();
}

HashTable::HashTable(const HashTable& hash)
{
	_capacity = hash._capacity;
	_loadFactor = hash._loadFactor;
	_size = hash._size;
	_longestProbeSequence = hash._longestProbeSequence;
	
	destroyHash();
	createHash();
	
	for (size_t ii = 0; ii < _capacity; ii++)
	{
		_hashList[ii] = hash._hashList[ii];
		_cellStatusList[ii] = hash._cellStatusList[ii];
	}
}

HashTable::~HashTable()
{
	destroyHash();
}

bool HashTable::insert(const HashRecord& value) noexcept
{
	if (isCapacityThreshold())
	{
		expandHash();
	}
	int probeSequence = 0;
	size_t index = hashFunc((value.feature_name + value.state_alpha).c_str()) % _capacity;
	while (_cellStatusList[index] == OCCUPIED)
	{
		if (_hashList[index] == value)
		{
			return false; // Key already exists
		}
		index = (index + probeFunc(index)) % _capacity;
		probeSequence++;
	}
	_cellStatusList[index] = OCCUPIED;
	_hashList[index] = value;
	_size++;
	
	if (probeSequence > _longestProbeSequence)
	{
		_longestProbeSequence = probeSequence;
	}
	
	return true; // Key inserted successfully
}

bool HashTable::search(const HashRecord& value, size_t& offset) noexcept
{
	size_t index = hashFunc((value.feature_name + value.state_alpha).c_str()) % _capacity;
	while (_cellStatusList[index] != EMPTY)
	{
		if (_cellStatusList[index] == OCCUPIED && _hashList[index] == value) {
			// Key found
			offset = _hashList[index].recordByteOffset;
			return true;
		}
		index = (index + probeFunc(index)) % _capacity;
	}
	// Key not found. Hit an empty bucket.
	return false;
}

bool HashTable::erase(const HashRecord& value)
{
	size_t index = hashFunc((value.feature_name + value.state_alpha).c_str()) % _capacity;
	while (_cellStatusList[index] != EMPTY)
	{
		if (_cellStatusList[index] == OCCUPIED && _hashList[index] == value) {
			// Key found
			_cellStatusList[index] = EMPTY;
			_size--;
			return true;
		}
		cout << index << endl;
		index = (index + probeFunc(index)) % _capacity;
	}
	// Key not found. Hit an empty bucket.
	return false;
}

void HashTable::increaseCapacity()
{
	_capacity *= 2;
}

void HashTable::expandHash()
{
	size_t oldCapacity = _capacity;
	HashRecord* oldHashList = _hashList;
	CellStatus* oldStatusList = _cellStatusList;
	increaseCapacity();
	createHash();
	
	// rehash
	for (size_t ii = 0; ii < oldCapacity; ii++)
	{
		if (oldStatusList[ii] == OCCUPIED)
		{
			insert(oldHashList[ii]);
		}
	}
	
	delete[] oldHashList;
	delete[] oldStatusList;
}

void HashTable::destroyHash()
{
	delete[] _hashList;
	delete[] _cellStatusList;
}

void HashTable::createHash()
{
	_hashList = new HashRecord[_capacity];
	_cellStatusList = new CellStatus[_capacity];
	for (size_t ii = 0; ii < _capacity; ii++)
	{
		_cellStatusList[ii] = EMPTY;
	}
}

void HashTable::printSnapshot(fstream* logPrint) const noexcept
{
	cout << right;
	for (size_t ii = 0; ii < _capacity; ii++)
	{
		if (_cellStatusList[ii] == OCCUPIED)
		{
			(*logPrint) << setw(9) << ii << ":  " << _hashList[ii] << endl;
		}
	}
}

// used from here: https://en.wikipedia.org/wiki/PJW_hash_function
size_t HashTable::hashFunc(const char* str) const
{
	size_t h = 0;
	size_t high = 0;
	while (*str)
	{
		h = (h << 4) + *str++;
		if (high = h & 0xF0000000)
			h ^= high >> 24;
		h &= ~high;
	}
	return h;
}

size_t HashTable::probeFunc(size_t index) const
{
	return ((index * index) + index) / 2;
}
