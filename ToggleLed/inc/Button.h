#ifndef BUTTON_H
#define BUTTON_H

#include <iostream>
#include <functional>
#include <unistd.h>
#include <signal.h>
#include "GpioPin.h"
#include "GpioDefines.h"
#include "GpioManager.h"

class Button
{
	public:
		enum class State : unsigned char
		{
			ENGAGED,
			IDLE
		};
		
		enum class Event : unsigned char
		{
			ON_CLICK,
			ON_HOLD,
			ON_DOUBLE_CLICK,
		};
		
		static const int DEBOUNCE_TIME_MS = 200;
		static const short NUM_EVENTS = 3;
		
		Button() = delete;
		Button(std::shared_ptr<gpio::GpioPin> gpioPin);
		~Button();
		
		inline State getState() const
		{
			return _state;
		}
		
		void addEventListener(Event eventType, std::function<void (void)> callback);
		void removeEventListener(Event eventType);
		void removeAllListeners();
		
	private:
		State _state;
		std::shared_ptr<gpio::GpioPin> _gpioPin;
		std::function<void (void)>* _callbackArray;
		
		void _onHold();
		void _onClick();
		void _onDoubleClick();
		void _clearCallbackArray();
		void _updateButtonState();
};

#endif
