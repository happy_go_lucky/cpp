#ifndef GPIO_READ_MUTEX_TABLE_H
#define GPIO_READ_MUTEX_TABLE_H

#include <vector>
#include <memory>
#include "GpioDefines.h"
#include <posixthreads/inc/MutexLock.h>

using std::vector;

class GpioReaderMutexTable
{
	public:
		GpioReaderMutexTable() noexcept;
		~GpioReaderMutexTable();
		
		GpioReaderMutexTable(const GpioReaderMutexTable& lookupTable) = delete;
		GpioReaderMutexTable(GpioReaderMutexTable&& lookupTable) = delete;
		GpioReaderMutexTable& operator =(const GpioReaderMutexTable& lookupTable) = delete;
		GpioReaderMutexTable& operator =(GpioReaderMutexTable&& lookupTable) = delete;
		
		std::shared_ptr<MutexLock> getReferenceToLock(gpio::PinNumber pinNumber) noexcept;
		std::shared_ptr<MutexLock> getReferenceToLock(gpio::GpioNumber gpioPinNumber) noexcept;
	
	private:
		vector<std::shared_ptr<MutexLock>>* _readMutexes;
		
		std::shared_ptr<MutexLock> _initLock(size_t pin);
};

#endif
