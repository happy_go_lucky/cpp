#include "FSM_State.h"

FSM_State::FSM_State() noexcept
{
	_entryAction = nullptr;
	_exitAction = nullptr;
	_stateType = ExprStates::STATE_NONE;
}

FSM_State::FSM_State(ExprStates state) noexcept
{
	_entryAction = nullptr;
	_exitAction = nullptr;
	_stateType = state;
}

FSM_State::~FSM_State()
{
	clearVector(_transitions);
	clearVector(_actions);
	delete _entryAction;
	delete _exitAction;
}

void FSM_State::setExitAction(BaseAction* action)
{
	_exitAction = action;
}

void FSM_State::setEntryAction(BaseAction* action)
{
	_entryAction = action;
}

void FSM_State::setStateType(ExprStates state)
{
	_stateType = state;
}

void FSM_State::addUpdateAction(BaseAction* action)
{
	_actions.push_back(action);
}

void FSM_State::addTransition(FSM_Transition* transition)
{
	_transitions.push_back(transition);
}

BaseAction* FSM_State::getEntryAction() const
{
	return _entryAction;
}

BaseAction* FSM_State::getExitAction() const
{
	return _exitAction;
}

ExprStates FSM_State::getStateType() const
{
	return _stateType;
}

void FSM_State::execute(GlobalVariables::StateActionParams& actionParams)
{
	for (int ii = 0; ii < _actions.size(); ii++)
	{
		(*_actions[ii])(actionParams);
	}
}

ExprStates FSM_State::update(ExprElement* inputElement, 
							GlobalVariables::StateActionParams& actionParams, 
							bool& validity)
{	
	for (int ii = 0; ii < _transitions.size(); ii++)
	{
		if (_transitions[ii]->execute(inputElement, actionParams))
		{
			// the expression is still valid
			validity = true;
			return _transitions[ii]->getTargetStateType();
		}
	}
	
	// if we are here, that means no possible transition exists
	// this is a bad input and should be notified to the state machine
	validity = false;
	
	return _stateType;
}

template <typename T>
void FSM_State::clearVector(vector<T>& vec)
{
	for (int ii = 0; ii < vec.size(); ii++)
	{
		delete vec[ii];
	}
	
	vec.clear();
}

void FSM_State::debugPrint()
{
	#ifndef NDEBUG
	cout << "number of transitions: " << _transitions.size() << endl;
	cout << "number of actions: " << _actions.size() << endl;
	cout << "state type: " << static_cast<int>(_stateType) << endl;
	#endif
}
