#ifndef BETTER_STACK_CPP
#define BETTER_STACK_CPP

#include "BetterStack.h"

template <typename T>
BetterStack<T>::BetterStack() noexcept
{
	_capacity = INIT_CAPACITY;
	_size = 0;
	_stackArr = new T[_capacity]();
}

template <typename T>
BetterStack<T>::BetterStack(size_t capacity)
{
	_capacity = capacity;
	_size = 0;
	_stackArr = new T[_capacity]();
}

template <typename T>
BetterStack<T>::BetterStack(const BetterStack<T>& aStack)
{
	_capacity = aStack._capacity;
	_size = aStack._size;
	_stackArr = new T[_capacity];
	for (size_t ii = 0; ii < _size; ii++)
	{
		_stackArr[ii] = aStack._stackArr[ii];
	}
}

template <typename T>
BetterStack<T>::BetterStack(BetterStack<T>&& aStack) noexcept
{
	_capacity = std::move(aStack._capacity);
	_size = std::move(aStack._size);
	_stackArr = std::move(aStack._stackArr);
	
	aStack._stackArr = nullptr;
	aStack._size = 0;
	aStack._capacity = 0;
}

template <typename T>
BetterStack<T>::BetterStack(std::initializer_list<T> list)
{
	_size = list.size();
	_capacity = _size;
	_stackArr = new T[_capacity];
	for (size_t ii = 0; ii < _size; ii++)
	{
		_stackArr[ii] = *(list.begin() + ii);
	}
}

template <typename T>
BetterStack<T>::~BetterStack()
{
	delete[] _stackArr;
}

template <typename T>
void BetterStack<T>::push(const T& value)
{
	increaseCapacity();
	_size++;
	_stackArr[_size - 1] = value;
}

template <typename T>
void BetterStack<T>::push(T&& value)
{
	increaseCapacity();
	_size++;
	_stackArr[_size - 1] = std::move(value);
}

template <typename T>
const T& BetterStack<T>::pop() noexcept
{
	if (_size > 0)
	{
		return _stackArr[--_size];
	}
	else
	{
		cerr << "popping from empty stack" << endl;
		return _stackArr[_size - 1];	// undefined behaviour on empty
	}
}

template <typename T>
const T& BetterStack<T>::top() noexcept
{
	if (_size > 0)
	{
		return _stackArr[_size - 1];
	}
	else
	{
		return _stackArr[_size - 1];	// undefined behaviour on empty
	}
}

template <typename U>
std::ostream& operator <<(std::ostream& out, const BetterStack<U>& aStack)
{
	out << "capacity: " << aStack._capacity << std::endl;
	out << "size: " << aStack._size << std::endl;
	out << "[ ";
	
	for (size_t ii = 0; ii < aStack._size; ii++)
	{
		out << aStack._stackArr[ii];
		if (ii < aStack._size - 1)
			out << ", ";
	}
	out << " ]" << std::endl;
	
	return out;
}

template <typename T>
BetterStack<T>& BetterStack<T>::operator =(const BetterStack<T>& aStack)
{
	if (this != &aStack)
	{
		_capacity = aStack._capacity;
		_size = aStack._size;
		delete[] _stackArr;
		_stackArr = new T[_capacity];
		
		for (size_t ii = 0; ii < _size; ii++)
		{
			_stackArr[ii] = aStack[ii];
		}
	}
	
	return *this;
}

template <typename T>
BetterStack<T>& BetterStack<T>::operator =(BetterStack<T>&& aStack) noexcept
{
	_capacity = std::move(aStack._capacity);
	_size = std::move(aStack._size);
	delete[] _stackArr;
	_stackArr = std::move(aStack._stackArr);
	
	aStack._stackArr = nullptr;
	aStack._size = 0;
	aStack._capacity = 0;
	
	return *this;
}

template <typename T>
bool BetterStack<T>::operator ==(const BetterStack<T>& aStack) noexcept
{
	if (_size != aStack._size)
		return false;
	
	for (size_t ii = 0; ii < _size; ii++)
	{
		if (_stackArr[ii] != aStack._stackArr[ii])
		{
			return false;
		}
	}
	
	return true;
}

template <typename T>
bool BetterStack<T>::operator ==(const BetterStack<T>&& aStack) noexcept
{
	if (_size != aStack._size)
		return false;
	
	for (size_t ii = 0; ii < _size; ii++)
	{
		if (_stackArr[ii] != aStack._stackArr[ii])
		{
			return false;
		}
	}
	
	return true;
}


template <typename T>
bool BetterStack<T>::operator !=(const BetterStack<T>& aStack) noexcept
{
	if (_size != aStack._size)
		return true;
	
	for (size_t ii = 0; ii < _size; ii++)
	{
		if (_stackArr[ii] != aStack[ii])
		{
			return true;
		}
	}
	
	return false;
}

template <typename T>
bool BetterStack<T>::operator !=(const BetterStack<T>&& aStack) noexcept
{
	if (_size != aStack._size)
		return true;
	
	for (size_t ii = 0; ii < _size; ii++)
	{
		if (_stackArr[ii] != aStack[ii])
		{
			return true;
		}
	}
	
	return false;
}

template <typename T>
void BetterStack<T>::checkValidity() noexcept
{
	assert(_size <= _capacity);
}

template <typename T>
void BetterStack<T>::increaseCapacity()
{
	if (_size == _capacity)
	{
		size_t oldSize = _capacity;
		_capacity = _capacity * 2;
		T* temp = new T[_capacity];
		for (size_t ii = 0; ii < oldSize; ii++)
		{
			temp[ii] = _stackArr[ii];
		}
		delete[] _stackArr;
		_stackArr = temp;
	}
}

template <typename T>
void BetterStack<T>::debugPrint() const noexcept
{
	#ifndef NDEBUG
	cout << "[ ";
	for (int ii = _size - 1; ii >= 0; ii--)
	{
		cout << _stackArr[ii];
		if (ii > 0)
			cout << ", ";
	}
	cout << " ]" << endl;
	#endif
}

#endif
