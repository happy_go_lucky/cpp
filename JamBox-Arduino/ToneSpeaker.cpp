#include "ToneSpeaker.h"

ToneSpeaker::ToneSpeaker( const int speakerPin )
{
  _speakerPin = speakerPin;
  _scale = SCALE_1;
  pinMode( _speakerPin, OUTPUT );
  analogWrite( _speakerPin, MIN_TONE_VOLUME );  // 0 - 255
  noTone( _speakerPin );
}

ToneSpeaker::~ToneSpeaker()
{
  analogWrite( _speakerPin, MIN_TONE_VOLUME );
}

void ToneSpeaker::playTone( const int note )
{
  #ifdef PRINT_DEBUG
  Serial.print( "playing note: " );
  Serial.println( note );
  Serial.print( "scale: " );
  Serial.println( _scale );
  #endif
  
  if ( note < NOTE_A || note > NOTE_G )
  {
    tone( _speakerPin, NOTES_SCALE_MATRIX[_scale][NOTE_A] );
  }
  else
  {
    tone( _speakerPin, NOTES_SCALE_MATRIX[_scale][note] );
  }
}

void ToneSpeaker::stopTone()
{
  noTone( _speakerPin );
}

void ToneSpeaker::setScale( const int scale )
{
  if ( scale < SCALE_1 || scale > SCALE_6 )
  {
    return;
  }
  _scale = scale;
}

