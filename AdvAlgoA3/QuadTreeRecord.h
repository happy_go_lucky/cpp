#ifndef QUAD_TREE_RECORD_H
#define QUAD_TREE_RECORD_H

#include <cstring>
#include <string>
#include <iostream>
#include "DMSCoord.h"

using namespace std;

struct QuadTreeRecord
{
	DMSCoord primary_lat_dms;
	DMSCoord primary_long_dms;
	size_t recordByteOffset;
	
	QuadTreeRecord& operator =(const QuadTreeRecord& record);
	bool operator ==(const QuadTreeRecord& record);
	friend ostream& operator <<(ostream& out, const QuadTreeRecord& record);
	
	bool isDitto(const QuadTreeRecord& record) const;
	bool isSameOffset(const QuadTreeRecord& record) const;
	bool isSameCoords(const QuadTreeRecord& record) const;
};

#endif
