#ifndef BINARY_SEARCH_TREE_H
#define BINARY_SEARCH_TREE_H

#include <iostream>
#include <vector>
#include <queue>
#include "UnderflowException.h"
#include "BinaryNode.h"
#include "BinaryNode.cpp"
#include "BetterStack.h"
#include "BetterStack.cpp"

using namespace std;

// BinarySearchTree class
//
// CONSTRUCTION: zero parameter
//
// ******************PUBLIC OPERATIONS*********************
// void insert( x )	   --> Insert x
// void remove( x )	   --> Remove x
// bool contains( x )	 --> Return true if x is present
// T findMin( )  --> Return smallest item
// T findMax( )  --> Return largest item
// boolean isEmpty( )	 --> Return true if empty; else false
// void makeEmpty( )	  --> Remove all items
// void printTree( )	  --> Print tree in sorted order
// ******************ERRORS********************************
// Throws UnderflowException as warranted

template <class T>
class BinarySearchTree
{
public:
	const static int AVL_BALANCE_FACTOR = 2;
	const static int EMPTY_HEIGHT = -1;
	BinarySearchTree() noexcept;
	BinarySearchTree(const BinarySearchTree<T>& bst);
	BinarySearchTree(BinarySearchTree<T>&& bst) noexcept;
	~BinarySearchTree();

	BinarySearchTree<T>& operator =(const BinarySearchTree<T>& bst) noexcept;
	BinarySearchTree<T>& operator =(BinarySearchTree<T>&& bst) noexcept;

	const T& findMin() const;
	const T& findMax() const;
	void balance();	// delete the lazy deleted elements, and balance
	bool contains(const T& x) const;
	bool isEmpty() const noexcept;

	void printTree(ostream& out = cout, bool printDeleted = false) const;
	void makeEmpty();
	void insert(const T& x);
	void insert(T&& x);
	void remove(const T& x);	// lazy remove
	inline int getNumNodes() const noexcept
	{
		return _numNodes - _numDeleted;
	}
	void removeLazyMarkedNodes();
	bool isBalancedOnAVL();
	void printLevelOrder();

  private:
	BinaryNode<T>* _root;
	int _numDeleted;
	int _numNodes;

	void insert(const T& x, BinaryNode<T>* t);
	void insert(T&& x, BinaryNode<T>* t);
	void remove(const T& x, BinaryNode<T>* t);
	BinaryNode<T>* findMin(BinaryNode<T>* t) const;
	BinaryNode<T>* findMax(BinaryNode<T>* t) const;
	BinaryNode<T>** findMinRef(BinaryNode<T>** t);
	bool contains(const T& x, BinaryNode<T>* t) const;
	void makeEmpty(BinaryNode<T>* t);
	void printTree(BinaryNode<T>* t, ostream& out, bool printDeleted = false) const;
	BinaryNode<T>* clone(BinaryNode<T>* t) const;
	BinaryNode<T>** findMinRef();
	void balanceTreeOnAVL();
	void computeHeightForAVL();
	void checkapplyAVLRotations(BinaryNode<T>** node);
	void avl_LR_rotation(BinaryNode<T>** parent, 
						BinaryNode<T>** child, 
						BinaryNode<T>** grandChild);
	void avl_RL_rotation(BinaryNode<T>** parent, 
						BinaryNode<T>** child, 
						BinaryNode<T>** grandChild);
	void avl_L_rotation(BinaryNode<T>** parent, 
						BinaryNode<T>** child, 
						BinaryNode<T>** grandChild);
	void avl_R_rotation(BinaryNode<T>** parent, 
						BinaryNode<T>** child, 
						BinaryNode<T>** grandChild);
						
	bool is_LR_imbalanced(BinaryNode<T>* node);
	bool is_RL_imbalanced(BinaryNode<T>* node);
	bool is_L_imbalanced(BinaryNode<T>* node);
	bool is_R_imbalanced(BinaryNode<T>* node);
	
	void rotationL(BinaryNode<T>** pivot);
	void rotationR(BinaryNode<T>** pivot);
	void applyAVL();
};

#endif
