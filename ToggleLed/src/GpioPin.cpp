#include "../inc/GpioPin.h"
namespace gpio
{
	GpioPin::GpioPin(GpioNumber gpioPinNumber)
	{
		#ifndef NDEBUG
		std::cout << "GpioPin::GpioPin at gpio pin: " << static_cast<int>(gpioPinNumber) << std::endl;
		#endif
		
		_isBitStreamOpen = false;
		_pinNumber = gpioPinNumber;
		_pinPath = std::string(GPIO_PATH) + std::string(GPIO_PIN_PREFIX) + 
					to_string(static_cast<int> (_pinNumber)) + std::string("/");
		_waitForEdgeWorker = new GpioWaitForEdgeWorker(_pinPath);
		_waitForEdgeWorker->start();
		
		_exportGPIO();
		usleep(GPIO_EXPORT_TIME);	// ensure GPIO is exported
		setMode(SignalMode::OUTPUT);
	}

	GpioPin::~GpioPin()
	{
		#ifndef NDEBUG
		std::cout << "GpioPin::~GpioPin at pin: " << static_cast<int>(_pinNumber) << std::endl;
		#endif
		
		_bitStream.close();
		_waitForEdgeWorker->signalExit();
		_waitForEdgeWorker->join();
		delete _waitForEdgeWorker;
		_unexportGPIO();
	}


	int GpioPin::setMode(SignalMode pinMode)
	{
		if (pinMode == SignalMode::INPUT)
			return _write(_pinPath.c_str(), GPIO_MODE_CMD, GPIO_MODE_IN);
		else
			return _write(_pinPath.c_str(), GPIO_MODE_CMD, GPIO_MODE_OUT);
	}

	SignalMode GpioPin::getMode() const
	{
		if (_read(_pinPath.c_str(), GPIO_MODE_CMD).compare(GPIO_MODE_IN) == 0)
			return SignalMode::INPUT;
		else
			return SignalMode::OUTPUT;
	}

	int GpioPin::setValue(SignalValue pinValue)
	{
		if (pinValue == SignalValue::HIGH)
			return _write(_pinPath.c_str(), GPIO_VALUE_CMD, VALUE_HIGH_STR);
		else
			return _write(_pinPath.c_str(), GPIO_VALUE_CMD, VALUE_LOW_STR);
	}

	SignalValue GpioPin::getValue() const
	{
		if (_read(_pinPath.c_str(), GPIO_VALUE_CMD).compare(VALUE_LOW_STR) == 0)
			return SignalValue::LOW;
		else
			return SignalValue::HIGH;
	}

	int GpioPin::_exportGPIO() const
	{
		return _write(GPIO_PATH, GPIO_EXPORT_CMD, to_string(static_cast<int>(_pinNumber)).c_str());
	}

	int GpioPin::_unexportGPIO() const
	{
		return _write(GPIO_PATH, GPIO_UNEXPORT_CMD, to_string(static_cast<int>(_pinNumber)).c_str());
	}
	
	int GpioPin::setSignalEdge(SignalEdge value)
	{
		switch(value)
		{
			case SignalEdge::NONE:
			return _write(_pinPath.c_str(), GPIO_EDGE_CMD, GPIO_EDGE_NONE);

			case SignalEdge::RISING:
			return _write(_pinPath.c_str(), GPIO_EDGE_CMD, GPIO_EDGE_RISING);
			
			case SignalEdge::FALLING:
			return _write(_pinPath.c_str(), GPIO_EDGE_CMD, GPIO_EDGE_FALLING);
			
			case SignalEdge::BOTH:
			return _write(_pinPath.c_str(), GPIO_EDGE_CMD, GPIO_EDGE_BOTH);
		}
		return EXIT_FAILURE;
	}
	
	SignalEdge GpioPin::getSignalEdge() const
	{
		std::string signalEdge = _read(_pinPath.c_str(), GPIO_EDGE_CMD);
		
		if (signalEdge.compare(GPIO_EDGE_RISING) == 0)
			return SignalEdge::RISING;
		else if (signalEdge.compare(GPIO_EDGE_FALLING) == 0)
			return SignalEdge::FALLING;
		else if (signalEdge.compare(GPIO_EDGE_BOTH) == 0)
			return SignalEdge::BOTH;
		else
			return SignalEdge::NONE;
		
	}
	
	bool GpioPin::waitForSignalEdge(SignalEdge value, std::function<void()> callbackFunction,
			bool freshStart)
	{
		if (freshStart || !_waitForEdgeWorker->isWaiting())
		{
			#ifndef NDEBUG
			std::cout << "begin waiting for a signal edge" << std::endl;
			#endif
			if (getMode() != SignalMode::INPUT)
			{
				setMode(SignalMode::INPUT); // must be an input pin to poll its value
			}
			setSignalEdge(value);
			_waitForEdgeWorker->signalWaitForEdge(callbackFunction);
			
			return true;
		}
		#ifndef NDEBUG
		std::cerr << "already waiting for a signal edge" << std::endl;
		#endif
		
		return false;
	}
	
	void GpioPin::setDebounceTime(int timeInMillis)
	{
		_waitForEdgeWorker->setDebounceTime(timeInMillis);
	}
	
	void GpioPin::bitStreaamOpen()
	{
		_isBitStreamOpen = true;
		_bitStream.open((_pinPath + GPIO_VALUE_CMD).c_str());
	}
	
	/*
	 * the sysfs uses characters for signal and a new line character is appended
	 * after each write
	 * */
	void GpioPin::bitStreamWrite(SignalValue value)
	{
		if (_isBitStreamOpen)
		{
			if (value == SignalValue::HIGH)
				_bitStream << VALUE_HIGH_STR << std::flush;
			else
				_bitStream << VALUE_LOW_STR << std::flush;
		}
		else
		{
			#ifndef NDEBUG
			std::cerr << "bitstream is not open, open it before writing" << std::endl;
		}
	}
	
	void GpioPin:::bitStreamClose()
	{
		_bitStream.close();
		_isBitStreamOpen = false;
	}
	
	std::string GpioPin::_read(const char* path, const char* gpioCmd) const
	{
		std::ifstream fs;
		char* cmdBuffer = new char[GPIO_COMMAND_BUFFER_LEN];
		
		// safety check
		if (_isBitStreamOpen)
		{
			_bitStream.close();
			_isBitStreamOpen = false;
		}
		
		strcpy(cmdBuffer, path);
		strcat(cmdBuffer, gpioCmd);
		
		fs.open(cmdBuffer);
		std::string dataRead = "";
		
		if(!fs.is_open())
		{
			std::cerr << "GPIO: read failed to open file: " << gpioCmd << std::endl;
			return dataRead;
		}
		
		getline(fs, dataRead);
		fs.close();
		delete cmdBuffer;
		
		return dataRead;
	}

	int GpioPin::_write(const char* path, const char* gpioCmd, const char* value) const
	{
		std::ofstream fs;
		char* cmdBuffer = new char[GPIO_COMMAND_BUFFER_LEN];
		
		// safety check
		if (_isBitStreamOpen)
		{
			_bitStream.close();
			_isBitStreamOpen = false;
		}
		
		strcpy(cmdBuffer, path);
		strcat(cmdBuffer, gpioCmd);
		std::cout << "GpioPin writing " << value << " to: " << cmdBuffer << std::endl;
		fs.open(cmdBuffer);
		if (!fs.is_open())
		{
			std::cerr << "GPIO: write failed to open file: " << gpioCmd << std::endl;
			return EXIT_FAILURE;
		}
		
		fs << value;
		fs.close();
		delete cmdBuffer;
		
		return EXIT_SUCCESS;
	}
}
