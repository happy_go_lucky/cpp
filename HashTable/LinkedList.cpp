#include "LinkedList.h"

using namespace std;

template<typename TYPE>
LinkedList<TYPE>::LinkedList(): head( NULL ), tempNode( NULL )
{
	// cout << "LinkedList() " << head << endl;
}

template<typename TYPE>
LinkedList<TYPE>::~LinkedList()
{
	// cout << "~LinkedList()" << endl;
	if ( head == NULL )
	{
		return;
	}
	_cleanupAllNodes( head );
	tempNode = head = NULL;
}

template<typename TYPE>
void LinkedList<TYPE>::deleteHeadNode()
{
	// cout << "LinkedList::deleteHeadNode()" << endl;
	if ( head == NULL )
	{
		return;
	}
	tempNode = head;
	head = tempNode->getPtr();
	delete tempNode;
	tempNode = NULL;
}

template<typename TYPE>
void LinkedList<TYPE>::deleteNode( ListNode<TYPE>* node )
{
	// cout << "LinkedList::deleteNode()" << endl;
	if ( head == NULL )
	{
		return;
	}
	
	if ( node == head )
	{
		deleteHeadNode();
	}
	else
	{
		tempNode = head;
		while ( tempNode->getPtr() != node && tempNode->getPtr() != NULL )
		{
			tempNode = tempNode->getPtr();
		}
		
		if ( tempNode->getPtr() != NULL )
		{
			tempNode->setPtr( node->getPtr() );
			delete node;
		}
	}
}

template<typename TYPE>
void LinkedList<TYPE>::addNodeToHead( const TYPE& data )
{
	// cout << "LinkedList::addNodeToHead()" << endl;
	ListNode<TYPE>* temp = new ListNode<TYPE>( data, head );
	head = temp;
}

template<typename TYPE>
void LinkedList<TYPE>::addNodeAfter( const ListNode<TYPE>* node, const TYPE& data )
{
	// cout << "LinkedList::addNodeAfter()" << endl;
	if ( head == NULL )
	{
		head = new ListNode<TYPE>( data, NULL );
		return;
	}
	
	if ( node == head )
	{
		addNodeToHead( data );
	}
	else
	{
		ListNode<TYPE>* temp = new ListNode<TYPE>( data, NULL );
		tempNode = head;
	
		while ( tempNode->getPtr() != node && tempNode->getPtr() != NULL )
		{
			tempNode = tempNode->getPtr();
		}
		
		temp->setPtr( tempNode->getPtr() );
		tempNode->setPtr( temp );
	}
}

template<typename TYPE>
void LinkedList<TYPE>::addNodeToEnd( const TYPE& data )
{
	// cout << "LinkedList::addNodeToEnd()" << endl;
	if ( head == NULL )
	{
		addNodeToHead( data );
	}
	else
	{
		tempNode = head;
	
		while ( tempNode->getPtr() != NULL )
		{
			tempNode = tempNode->getPtr();
		}
		
		tempNode->setPtr( new ListNode<TYPE>( data, NULL ) );
	}
}

template<typename TYPE>
ListNode<TYPE>* LinkedList<TYPE>::searchData( const TYPE& data, int& numOps ) const
{
	// cout << "LinkedList::searchData()" << endl;
	if ( head == NULL )
	{
		return NULL;
	}
	
	ListNode<TYPE>* temp = head;
	
	while ( temp != NULL )
	{
		numOps++;
		if ( temp->getData() == data )
		{
			return temp;
		}
		
		temp = temp->getPtr();
	}
	
	return NULL;
}

template<typename TYPE>
void LinkedList<TYPE>::printList() const
{
	// cout << "LinkedList::printList()" << endl;
	
	if ( head == NULL )
	{
		// cout << "list is empty" << endl;
		return;
	}
	
	ListNode<TYPE>* temp = head;
	
	do
	{
		cout << temp->getData() << ", ";
		temp = temp->getPtr();
	} while ( temp != NULL );
	cout << endl;
}

template<typename TYPE>
int LinkedList<TYPE>::length() const
{
	int count = 0;
	ListNode<TYPE>* temp = head;
	
	while ( temp != NULL )
	{
		count++;
		temp = temp->getPtr();
	}
	
	return count;
}

template<typename TYPE>
bool LinkedList<TYPE>::isEmpty() const
{
	return head == NULL;
}

template<typename TYPE>
void LinkedList<TYPE>::_cleanupAllNodes( const ListNode<TYPE>* curNode )
{
	// cout << "LinkedList::_cleanupAllNodes()" << endl;
	if ( curNode->getPtr() == NULL )
	{
		// cout << "_cleanupAllNodes() NULL" << endl;
		delete curNode;
		return;
	}
	_cleanupAllNodes( curNode->getPtr() );
	
	delete curNode;
}