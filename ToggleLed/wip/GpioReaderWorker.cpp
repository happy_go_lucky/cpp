#include "../inc/GpioReaderWorker.h"

GpioReaderWorker::GpioReaderWorker(const char* gpioPath, 
		std::shared_ptr<ReadWriteLock> rwLock) : Thread(Thread::COOPERATIVE),
		_gpioPath(gpioPath)
{
	_rwLock = rwLock;
	_cvLock.initResources();
	_executionCV = new ConditionVariable(_cvLock);
	_updateCondition(ExecutionCondition::WAIT);
	_taskMutex.initResources();
}

GpioReaderWorker::~GpioReaderWorker()
{
	_commandQueue.emptyList();
	_taskMutex.cleanupResources();
	_cvLock.cleanupResources();
	delete _executionCV;
}

void* GpioReaderWorker::threadRun()
{
	_executionCV->waitForSignal(_shouldExecute, _waitCondition);
	_read(gpio::getCommandStr(gpioCommand));
	
	return EXIT_SUCCESS;
}

void GpioReaderWorker::readGpio(gpio::GpioCommand gpioCommand, std::string& valueRead)
{	
	// should send the signal to resume and read the value
	_taskMutex.lock();
	commandQueue.addTask(gpioCommand);
	_taskMutex.unlock();
	_executionCV->signal(_updateCondition, ExecuteCondition);
	
	return ;
}

std::string GpioReaderWorker::_read(const char* gpioCmd) const
{
	std::ifstream fs;
	char* cmdBuffer = new char[gpio::GpioDefines::GPIO_COMMAND_BUFFER_LEN];
	
	strcpy(cmdBuffer, _gpioPath);
	strcat(cmdBuffer, gpioCmd);
	
	fs.open(cmdBuffer);
	
	if(!fs.is_open())
	{
		std::cerr << "GPIO: read failed to open file: " << gpioCmd << std::endl;
		return dataRead;
	}
	
	string dataRead;
	getline(fs, dataRead);
	fs.close();
	
	return dataRead;
}

bool GpioReaderWorker::_shouldWait(ExecutionCondition condition)
{
	return condition == ExecutionCondition::WAIT;
}

bool GpioReaderWorker::_shouldExecute(ExecutionCondition condition)
{
	return condition == ExecutionCondition::EXECUTE;
}

void GpioReaderWorker::_updateCondition(ExecutionCondition condition)
{
	_waitCondition = condition;
}
