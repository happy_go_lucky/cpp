#include "ElementOperator.h"

ElementOperator::ElementOperator(char data) noexcept : ExprElement(ElementType::TYPE_OPERATOR)
{
	_data = data;
}

ostream& operator <<(ostream& out, const ElementOperator& elem)
{
	out << elem._data;
	return out;
}
