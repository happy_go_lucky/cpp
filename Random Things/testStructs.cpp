#include <iostream>

using std::cout;
using std::endl;

struct Cube_VBO
{
	float x, y, z;		// for vertex position
	float nx, ny, nz;	// for vertex normals
	float r, g, b, a;	// for vertex color
};

int main()
{
	struct Cube_VBO vbo;
	struct Cube_VBO vboArr[10];

	cout << "size of float: " << sizeof( float ) << endl;
	cout << "size of struct is: " << sizeof( vbo ) << endl;	
	cout << "size of struct array is: " << sizeof( vboArr ) << endl;	
	
	return EXIT_SUCCESS;
}

