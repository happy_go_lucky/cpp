/**
 * This is the interface class for waveshare 1.54 inch e-paper module v2
 * the pin layout is explained as follows
 * 
 * RST (LOW for reset) INPUT pin
 * DC (HIGH for data, LOW for command) INPUT pin
 * CS (LOW Active) INPUT pin
 * BUSY busy state OUTPUT pin (LOW when busy) read below
 * CLK SPI communication pin/clock. This is where data is transferred. INPUT pin (max 20 MHz)
 * DIN is data line INPUT pin
 * VCC is 3.3 V
 * BS Pin (optional) for selecting 3 or 4-line SPI. LOW for 4 line, and HIGH for 3 line
 * the default is set to LOW, hence 4 line.
 * 
 * when BUSY pin is HIGH, the operation of chip should not be interrupted,
 * and no commands should be issued to the device.
 * 
 * There are 4 SPI communication modes, and SPI0 is commondly used
 * CPHL is 0, and CPOL is 0 in SPI0
 * Data transmission starts at falling edge of SCLK, 
 * and 8 bits are transferred per clock cycle in MSB
 * 
 * */

#ifndef EINK_DISPLAY_H
#define EINK_DISPLAY_H

#include "GpioPin.h"
#include "GpioDefines.h"
#include "GpioManager.h"

class EinkDisplay
{
	public:
		static const short SCREEN_WIDTH = 200;
		static const short SCREEN_HEIGHT = 200;
		
		static const short WR_MODE_MAX_CLK_FREQUENCY = 20; // MHz
		static const float RD_MODE_MAX_CLK_FREQUENCY = 2.5f; // MHz
		// the minimum time before first rising edge of CLK is detected after CS is set active
		static const short WR_MODE_MIN_CS_DELAY_BEFORE_RISING = 20; // nano seconds
		static const short RD_MODE_MIN_CS_DELAY_BEFORE_RISING = 100; // nano second
		// the minimum time after the last falling edge of CLK is detected before CS is set inactive
		static const short WR_MODE_MIN_CS_DELAY_AFTER_FALLING = 20; // nano seconds
		static const short RD_MODE_MIN_CS_DELAY_AFTER_FALLING = 50; // nano seconds
		// the minimum time CS has to remain high (inactive) between two transfers
		static const short WR_MODE_MIN_CS_DELAY_BETWEEN_TRANSFERS = 100; // nano seconds
		static const short RD_MODE_MIN_CS_DELAY_BETWEEN_TRANSFERS = 250; // nano seconds
		// the minimum time CLK has to remain HIGH in one clock period
		static const short WR_MODE_MIN_HIGH_TIME_PER_CLK_PERIOD = 25; // nano seconds
		static const short RD_MODE_MIN_HIGH_TIME_PER_CLK_PERIOD = 180; // nano seconds
		// the minimum time CLK has to remain LOW in one clock period
		static const short WR_MODE_MIN_LOW_TIME_PER_CLK_PERIOD = 25; // nano seconds
		static const short RD_MODE_MIN_LOW_TIME_PER_CLK_PERIOD = 180; // nano seconds
		// SI stable time before next rising edge of CLK
		static const short WR_MODE_SI_STABLE_TIME_BEFORE_RISING_EDGE = 10; // nano seconds
		static const short RD_MODE_SI_STABLE_TIME_BEFORE_RISING_EDGE = 50; // nano seconds
		// SI stable ti;me after the rising edge of CLK
		static const short WR_MODE_SI_STABLE_TIME_AFTER_RISING_EDGE = 40; // nano seconds
		static const short RD_MODE_SI_STABLE_TIME_AFTER_RISING_EDGE = 0; // nano seconds
		
		gpio::GpioNumber RST_PIN = gpio::GpioNumber::GPIO_17;
		gpio::GpioNumber DC_PIN = gpio::GpioNumber::GPIO_25;
		gpio::GpioNumber CS_PIN = gpio::GpioNumber::GPIO_8;
		gpio::GpioNumber BUSY_PIN = gpio::GpioNumber::GPIO_24;
		
		EinkDisplay();
		~EinkDisplay();
		
		EinkDisplay(const EinkDisplay& display) = delete;
		EinkDisplay(EinkDisplay&& display) = delete;
		EinkDisplay& operator =(const EinkDisplay& display) = delete;
		EinkDisplay& operator =(EinkDisplay&& display) = delete;
		
		void init();
		void clear();
		void display(gpio::uByte* image);
		void displayPartImage(gpio::uByte* image);
		void displayPartBaseImage(gpio::uByte* image);
		void sleep();
	
	private:
		gpio::GpioManager* _gpioManager;
		std::shared_ptr<gpio::GpioPin> _resetPin;
		std::shared_ptr<gpio::GpioPin> _dataCmdControlPin;
		std::shared_ptr<gpio::GpioPin> _spiChipSelectPin;
		std::shared_ptr<gpio::GpioPin> _busyPin;
};

#endif
