#include "../inc/Button.h"

Button::Button(std::shared_ptr<gpio::GpioPin> gpioPin)
{
	_gpioPin = gpioPin;
	#ifndef NDEBUG
	std::cout << "Button::Button at gpio pin: " << static_cast<int> (_gpioPin->getPinNumber())
		<< std::endl;
	#endif
	
	_state = State::IDLE;
	_gpioPin->setMode(gpio::SignalMode::INPUT);
	_gpioPin->setDebounceTime(DEBOUNCE_TIME_MS);
	//~ _gpioPin->setValue(gpio::SignalValue::LOW);
	_callbackArray = new std::function<void (void)>[NUM_EVENTS];
	_clearCallbackArray();
	
	std::function<void (void)> func = 
				std::bind(&Button::_updateButtonState, this);
	_gpioPin->waitForSignalEdge(gpio::SignalEdge::RISING, func);
}

Button::~Button()
{
	#ifndef NDEBUG
	cout << "Button::~Button at gpio pin: " << static_cast<int> (_gpioPin->getPinNumber()) 
		<< std::endl;
	#endif
	union sigval dataToPass;
	dataToPass.sival_ptr = _gpioPin->getEdgeWorkerRef();
	pthread_sigqueue(_gpioPin->getEdgeWorkerId(), SIGUSR1, dataToPass);
	delete[] _callbackArray;
	std::cout << "button is cleared" << std::endl;
}

void Button::addEventListener(Event eventType, std::function<void (void)> callback)
{
	_callbackArray[static_cast<short>(eventType)] = callback;
}

void Button::removeEventListener(Event eventType)
{
	_callbackArray[static_cast<short>(eventType)] = nullptr;
}

void Button::removeAllListeners()
{
	_clearCallbackArray();
}

// TODO: implement this functionality
void Button::_onHold()
{
	std::function<void (void)> func = _callbackArray[static_cast<short>(Event::ON_HOLD)];
	if (func != nullptr)
	{
		func();
	}
}

void Button::_onClick()
{
	std::function<void (void)> func = _callbackArray[static_cast<short>(Event::ON_CLICK)];
	if (func != nullptr)
	{
		func();
	}
}

// TODO: implement this functionality
void Button::_onDoubleClick()
{
	std::function<void (void)> func = _callbackArray[static_cast<short>(Event::ON_DOUBLE_CLICK)];
	if (func != nullptr)
	{
		func();
	}
}

void Button::_clearCallbackArray()
{
	for (short ii = 0; ii < NUM_EVENTS; ii++)
	{
		_callbackArray[ii] = nullptr;
	}
}

void Button::_updateButtonState()
{
	std::cout << "button is pressed" << std::endl;
	for (short ii = 0; ii < NUM_EVENTS; ii++)
	{
		if (_callbackArray[ii] != nullptr)
		{
			_callbackArray[ii]();
		}
	}
}
