#include "TraversalActions.h"

void TraversalVector::operator ()(PRQuadNode* node)
{
	vec.push_back(node);
}

void TraversalPrint::operator ()(PRQuadNode* node)
{
	if (node->hasSubDivided_NW())
	{
		(*logPrint) << "@";
	}
	else
	{
		(*logPrint) << node->getDataNW() << ", ";		
	}
	
	if (node->hasSubDivided_NE())
	{
		(*logPrint) << "@";
	}
	else
	{
		(*logPrint) << node->getDataNE() << ", ";		
	}
	
	if (node->hasSubDivided_SE())
	{
		(*logPrint) << "@";
	}
	else
	{
		(*logPrint) << node->getDataSE() << ", ";		
	}
	
	if (node->hasSubDivided_SW())
	{
		(*logPrint) << "@";
	}
	else
	{
		(*logPrint) << node->getDataSW() << ", ";		
	}
}
