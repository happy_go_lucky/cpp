#include <iostream>
#include <assert.h>
#include <cstdlib>
#include <ctime>
#include "DoublyList.cpp"
#include "DoublyListIterator.cpp"

int main ()
{
	srand(time(NULL));
	datastructs::DoublyList<int> list;
	
	std::cout << "testing the doubly list" << std::endl;
	
	std::cout << "testing emptiness  list" << std::endl;
	std::cout << "list size: " << list.size() << std::endl;
	assert(list.isEmpty());
	
	std::cout << "printing empty list" << std::endl;
	std::cout << list << std::endl;
	
	std::cout << "appending to empty" << std::endl;
	list.appendNode(2);
	std::cout << list << std::endl;
	std::cout << "list size: " << list.size() << std::endl;
	
	std::cout << "deleting only node at back" << std::endl;
	list.deleteNodeAtBack();
	assert(list.isEmpty());
	std::cout << list << std::endl;
	std::cout << "list size: " << list.size() << std::endl;
	
	std::cout << "prepending to empty" << std::endl;
	list.prependNode(2);
	std::cout << list << std::endl;
	std::cout << "list size: " << list.size() << std::endl;
	
	std::cout << "deleting only node at front" << std::endl;
	list.deleteNodeAtFront();
	assert(list.isEmpty());
	std::cout << list << std::endl;
	std::cout << "list size: " << list.size() << std::endl;
	
	std::cout << "filling up list with 10 elements" << std::endl;
	int testSize = 10;
	int knownIndex = rand() % testSize;
	int knownValue = 101;
	for (int ii = 0; ii < testSize; ii++)
	{
		if (ii == knownIndex)
			list.prependNode(knownValue);
		else
			list.prependNode(rand() % 100);
	}
	assert(list.size() == 10);
	std::cout << list << std::endl;
	std::cout << "head node value: " << *(list.head()) << std::endl;
	std::cout << "tail node value: " << *(list.tail()) << std::endl;
	
	std::cout << "finding unknown value " << std::endl;
	assert(list.search(102) == false);
	
	std::cout << "finding known value " << std::endl;
	assert(list.search(knownValue) == true);
	
	std::cout << "finding unknown value node" << std::endl;
	datastructs::DoublyListIterator<int> testItr = list.findNode(102);
	assert(testItr.isNodeValid() == false);
	
	std::cout << "finding known value node" << std::endl;
	testItr = list.findNode(knownValue);
	assert(testItr.isNodeValid() == true);
	
	std::cout << "testing head node" << std::endl;
	testItr = list.head();
	std::cout << *testItr << std::endl;
	assert(testItr.isNodeValid() == true);
	
	std::cout << "testing tail node" << std::endl;
	testItr = list.tail();
	std::cout << *testItr << std::endl;
	assert(testItr.isNodeValid() == true);
	
	std::cout << "testing copy cons" << std::endl;
	datastructs::DoublyList<int> listCopy = list;
	std::cout << list << std::endl;
	std::cout << "list size: " << listCopy.size() << std::endl;
	std::cout << "head node value: " << *(listCopy.head()) << std::endl;
	std::cout << "tail node value: " << *(listCopy.tail()) << std::endl;
	
	std::cout << "testing assignment" << std::endl;
	listCopy = list;
	std::cout << list << std::endl;
	std::cout << "list size: " << listCopy.size() << std::endl;
	std::cout << "head node value: " << *(listCopy.head()) << std::endl;
	std::cout << "tail node value: " << *(listCopy.tail()) << std::endl;
	
	std::cout << "testing equality" << std::endl;
	assert(list == listCopy);
	
	std::cout << "testing deletion of all nodes" << std::endl;
	list.emptyList();
	std::cout << "list size: " << list.size() << std::endl;
	assert(list.isEmpty());
	std::cout << list << std::endl;
	
	std::cout << "testing inequality" << std::endl;
	assert(list != listCopy);
	
	return EXIT_SUCCESS;
}
