#include "DDCoord.h"

DDCoord::DDCoord() noexcept
{
	_degVal = nanf(NAN_VALUE);
}

DDCoord::DDCoord(float value)
{
	_degVal = value;
}

DDCoord::DDCoord(char* coordStr)
{
	convertFromString(coordStr);
}

DDCoord::DDCoord(const DDCoord& value)
{
	_degVal = value._degVal;
}

DDCoord::DDCoord(DDCoord&& value) noexcept
{
	_degVal = std::move(value._degVal);
}

DDCoord::~DDCoord()
{
	
}

ostream& operator <<(ostream& out, const DDCoord& value)
{
	out << value._degVal;
	return out;
}

DDCoord& DDCoord::operator =(const DDCoord& value)
{
	if (this != &value)
	{
		_degVal = value._degVal;
	}
	return *this;
}

DDCoord& DDCoord::operator =(DDCoord&& value)
{
	_degVal = std::move(value._degVal);
	
	return *this;
}

DDCoord& DDCoord::operator =(char* coordStr)
{
	convertFromString(coordStr);
	
	return *this;
}

void DDCoord::setDegrees(float value) noexcept
{
	_degVal = value;
}

float DDCoord::getDegrees() const noexcept
{
	return _degVal;
}

bool DDCoord::isValid() const noexcept
{
	return !(isnan(_degVal));
}

void DDCoord::convertFromString(char* coordStr)
{
	
	if (coordStr == nullptr || *coordStr == '\0')
	{
		_degVal = nanf(NAN_VALUE);
	}
	else
	{
		_degVal = stof(coordStr);
	}
}
