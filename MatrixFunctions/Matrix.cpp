#include "Matrix.h"

template <typename T>
Matrix<T>::Matrix( const unsigned int rows, const unsigned int cols )
{
	//cout << "cons" << endl;
	//cout << "CPU threads: " << get_nprocs() << endl;
	_numRows = rows;
	_numCols = cols;
	_matrix = nullptr;
	_allocMatrix( _matrix, _numRows, _numCols );
}

template <typename T>
Matrix<T>::Matrix( const Matrix& aMatrix )
{
	//cout << "copy cons" << endl;
	_numRows = aMatrix.numRows();
	_numCols = aMatrix.numCols();
	_matrix = nullptr;
	_allocMatrix( _matrix, _numRows, _numCols );
	_copyMatrix( aMatrix );
}

template <typename T>
Matrix<T>::~Matrix()
{
	//cout << "destructor" << endl;
	_deallocMatrix( _matrix, _numRows );
}

template <typename T>
Matrix<T> Matrix<T>::operator* ( const Matrix<T>& aMatrix ) const
{
	if ( !isMultiplicationPossible( aMatrix ) )
	{
		throw MatrixException( ( char* ) "the dimensions are different. Multiplication is not possible" );
	}
	
	Matrix<T> resultMatrix( _numRows, aMatrix.numCols() );
	
	// this * other : A * B
	unsigned resultMatrixElement = 1;
	
	MatrixTraversor traversorA( MatrixTraversor::ROW, _numRows, _numCols );
	MatrixTraversor traversorB( MatrixTraversor::COLUMN, aMatrix.numRows(), aMatrix.numCols() );
	MatrixTraversor traversorResult( MatrixTraversor::ROW, _numRows, aMatrix.numCols() );
	
	while ( resultMatrixElement <= _numRows * aMatrix.numCols() )
	{
		T value = 0;
		bool condition = true;
		while ( condition )
		{
			T product = _matrix[traversorA.getRowPosition() - 1][traversorA.getColumnPosition() - 1] * aMatrix.getDataAt( traversorB.getRowPosition(), traversorB.getColumnPosition() );
			value += product;
			//cout << _matrix[traversorA.getRowPosition() - 1][traversorA.getColumnPosition() - 1] << " * " << aMatrix.getDataAt( traversorB.getRowPosition(), traversorB.getColumnPosition() ) << " = " << product;
			if ( traversorA.getColumnPosition() == _numCols )
			{
				// all elements of current row and column are traversed
				condition = false;
				//cout << endl;
			}
			else
			{
				//cout << " + ";
			}
			
			traversorA.traverse();
			traversorB.traverse();
		}
		
		resultMatrix.setDataAt( value, traversorResult.getRowPosition(), traversorResult.getColumnPosition() );
		
		traversorResult.traverse();

		traversorA.resetTraversor( ( resultMatrixElement / aMatrix.numCols() ) + 1 );
		resultMatrixElement++;
	}
	
	return resultMatrix;
}

template <typename T>
Matrix<T> Matrix<T>::operator* ( const double scalar ) const
{
	// TODO: test and fix it
	Matrix<T> resultMatrix( _numRows, _numCols );
	MatrixTraversor traversor( MatrixTraversor::ROW, _numRows, _numCols );
	
	unsigned int elementsTraversed = 0;
	while ( elementsTraversed < _numRows * _numCols )
	{
		T data = _matrix[traversor.getRowPosition() - 1][traversor.getColumnPosition() - 1] * scalar;
		resultMatrix.setDataAt( data, traversor.getRowPosition(), traversor.getColumnPosition() );
		traversor.traverse();
		elementsTraversed++;
	}
	
	return resultMatrix;
}

template <typename F>
Matrix<F> operator* ( const double scalar, const Matrix<F>& matrix )
{
	// TODO: test and fix it
	Matrix<double> resultMatrix( matrix._numRows, matrix._numCols );
	MatrixTraversor traversor( MatrixTraversor::ROW, matrix._numRows, matrix._numCols );
	
	unsigned int elementsTraversed = 0;
	while ( elementsTraversed < matrix._numRows * matrix._numCols )
	{
		F data = matrix._matrix[traversor.getRowPosition() - 1][traversor.getColumnPosition() - 1] * scalar;
		resultMatrix.setDataAt( data, traversor.getRowPosition(), traversor.getColumnPosition() );
		traversor.traverse();
		elementsTraversed++;
	}
	
	return resultMatrix;
}

template <typename T>
Matrix<T> Matrix<T>::operator+ ( const Matrix& aMatrix ) const
{
	if ( !isSameDimension( &aMatrix ) )
	{
		throw MatrixException( ( char* ) "the dimensions are different. Addition is not possible" );
	}
	
	Matrix<T> resultMatrix( _numRows, _numCols );
	unsigned int currRow = 0;
	unsigned int currColumn = 0;
	
	while ( currRow < _numRows )
	{
		while ( currColumn < _numCols )
		{
			resultMatrix.setDataAt( _matrix[currRow][currColumn] + aMatrix.getDataAt( currRow + 1, currColumn + 1 ), currRow + 1, currColumn + 1 );
			currColumn++;
		}
		
		currRow++;
		currColumn = 0;
	}
	
	return resultMatrix;
}

template <typename T>
Matrix<T> Matrix<T>::operator- ( const Matrix& aMatrix ) const
{
	if ( !isSameDimension( &aMatrix ) )
	{
		throw MatrixException( ( char* ) "the dimensions are different. Subtraction is not possible" );
	}
	
	Matrix<T> resultMatrix( _numRows, _numCols );
	unsigned int currRow = 0;
	unsigned int currColumn = 0;
	
	while ( currRow < _numRows )
	{
		while ( currColumn < _numCols )
		{
			resultMatrix.setDataAt( _matrix[currRow][currColumn] - aMatrix.getDataAt( currRow + 1, currColumn + 1 ), currRow + 1, currColumn + 1 );
			currColumn++;
		}
		
		currRow++;
		currColumn = 0;
	}
	
	return resultMatrix;
}

template <typename T>
const Matrix<T>& Matrix<T>::operator= ( const Matrix& aMatrix )
{
	if ( this != &aMatrix )
	{
		_numRows = aMatrix.numRows;
		_numCols = aMatrix.numCols;
		
		_allocMatrix( _matrix, _numRows, _numCols );
		_copyMatrix( aMatrix );
	}
	
	return *this;
}

template <typename T>
bool Matrix<T>::operator== ( const Matrix& aMatrix ) const
{
	if ( !isSameDimension( &aMatrix ) )
	{
		return false;
	}
	unsigned int currRow = 0;
	unsigned int currColumn = 0;
	
	while ( currRow < _numRows )
	{
		while ( currColumn < _numCols )
		{
			if ( _matrix[currRow][currColumn] != aMatrix.getDataAt( currRow + 1, currColumn + 1 ) )
			{
				return false;
			}
			currColumn++;
		}
		
		currRow++;
		currColumn = 0;
	}
	
	return true;
}

template <typename T>
bool Matrix<T>::isMultiplicationPossible( const Matrix& aMatrix ) const
{
	return ( _numCols == aMatrix.numRows() );
}

/*
template <typename T>
Matrix Matrix<T>::transpose() const
{
	Matrix resultMatrix( _numRows, _numCols );
	return resultMatrix;
}*/

template <typename T>
void Matrix<T>::printMatrix() const
{
	MatrixTraversor traversor( MatrixTraversor::ROW, _numRows, _numCols );
	_printMatrix( _matrix, _numRows, _numCols, traversor );
}

template <typename T>
void Matrix<T>::_printMatrix( T** matrix, unsigned rows, unsigned cols, MatrixTraversor& traversor ) const
{
	unsigned int elementCount = 0;
	while ( elementCount < cols * rows )
	{
		cout << matrix[traversor.getRowPosition() - 1][traversor.getColumnPosition() - 1];
		( ( elementCount + 1 ) % _numCols == 0 ) ? cout << endl : cout << "\t";
		
		traversor.traverse();
		elementCount++;
	}
	cout << endl;
}

template <typename T>
void Matrix<T>::printMatrixTranspose() const
{
	MatrixTraversor traversor( MatrixTraversor::COLUMN, _numRows, _numCols );
	_printMatrix( _matrix, _numRows, _numCols, traversor );
}

template <typename T>
inline bool Matrix<T>::isSquareMatrix() const
{
	return _numCols == _numRows;
}

template <typename T>
bool Matrix<T>::isSameDimension( const Matrix& aMatrix ) const
{
	return _numRows == aMatrix.numRows() && _numCols == aMatrix.numCols();
}

template <typename T>
inline unsigned int Matrix<T>::numRows() const
{
	return _numRows;
}

template <typename T>
inline unsigned int Matrix<T>::numCols() const
{
	return _numCols;
}


template <typename T>
double Matrix<T>::determinant() const
{
	return _computeDeterminant( _numRows, nullptr );
}

template <typename T>
double Matrix<T>::_computeDeterminant( unsigned subMatrixNumRows, DeterminantHelper* ptrDetHelper ) const
{
	if ( subMatrixNumRows == 2 )
	{
		DeterminantHelper* detHelper = ( ptrDetHelper == nullptr ) ? new DeterminantHelper( _numRows, _numCols ) : new DeterminantHelper( *( ptrDetHelper ) );
		unsigned firstRow = detHelper->getFirstRow() - 1;
		unsigned firstCol = detHelper->getFirstCol() - 1;
		unsigned lastRow = detHelper->getLastRow_2x2() - 1;
		unsigned lastCol = detHelper->getLastCol_2x2() - 1;

		//cout << _matrix[firstRow][firstCol] << " * " << _matrix[lastRow][lastCol] << " - " << _matrix[firstRow][lastCol] << " * " << _matrix[lastRow][firstCol] << " )" << endl;
		delete detHelper;
		return ( _matrix[firstRow][firstCol] * _matrix[lastRow][lastCol] ) - ( _matrix[firstRow][lastCol] * _matrix[lastRow][firstCol] );
	}
	
	double det = 0;
	unsigned ii = 0;
	
	while ( ii < subMatrixNumRows )
	{
		DeterminantHelper* detHelper = ( ptrDetHelper == nullptr ) ? new DeterminantHelper( _numRows, _numCols ) : new DeterminantHelper( *( ptrDetHelper ) );
		unsigned currRow = detHelper->getFirstRow();
		unsigned currCol = detHelper->getFirstCol() + ii;
		int sign = ( currRow + currCol ) % 2 == 0 ? 1 : -1;
		//cout << sign * _matrix[currRow - 1][currCol - 1] << " * ( ";
		detHelper->setInvalidRowAndCol( currRow, currCol );
		det += sign * _matrix[currRow - 1][currCol - 1] * _computeDeterminant( subMatrixNumRows - 1, detHelper );
		ii++;
		delete detHelper;
	}
	
	return det;
}


template <typename T>
Matrix<T> Matrix<T>::inverse() const
{
	double det = determinant();
	
	if ( det == 0 )
	{
		throw MatrixException( ( char* ) "Determinant is 0, therefore Inverse doesn't exist" );
	}
	// 1/det * adj.
	double detInverse = ( double ) 1 / det;
	//cout << "det: " << det << endl;
	//cout << "det inverse: " << detInverse << endl;

	return detInverse * getAdjugateMatrix();
}

template <typename T>
Matrix<T> Matrix<T>::getCofactorMatrix() const
{
	// alternate signs on minor matrix
	Matrix<T> resultMatrix( _numRows, _numCols );
	Matrix<T> minorMatrix = getMinorMatrix();
	MatrixTraversor traversor( MatrixTraversor::ROW, _numRows, _numCols );
	
	int sign = 1;
	unsigned currRow = 0;
	unsigned currCol = 0;
	unsigned int elementCount = 0;
	while ( elementCount < _numCols * _numRows )
	{
		currRow = traversor.getRowPosition();
		currCol = traversor.getColumnPosition();

		sign = ( currRow + currCol ) % 2 == 0 ? 1 : -1;
		T data = sign * minorMatrix.getDataAt( currRow, currCol );
		resultMatrix.setDataAt( data, currRow, currCol );
		
		traversor.traverse();
		elementCount++;
	}
	
	//cout << "printing cofactor matrix:" << endl;
	//resultMatrix.printMatrix();
	
	return resultMatrix;
}

template <typename T>
Matrix<T> Matrix<T>::getAdjugateMatrix() const
{
	// transpose of cofactor matrix
	Matrix<T> resultMatrix( _numRows, _numCols );
	Matrix<T> cofactorMatrix = getCofactorMatrix();
	MatrixTraversor traversorRow( MatrixTraversor::ROW, _numRows, _numCols );
	MatrixTraversor traversorCol( MatrixTraversor::COLUMN, _numRows, _numCols );
	
	unsigned int elementCount = 0;
	while ( elementCount < _numCols * _numRows )
	{
		T data = cofactorMatrix.getDataAt( traversorCol.getRowPosition(), traversorCol.getColumnPosition() );
		resultMatrix.setDataAt( data, traversorRow.getRowPosition(), traversorRow.getColumnPosition() );
		
		traversorRow.traverse();
		traversorCol.traverse();
		elementCount++;
	}
	
	//cout << "printing adjugate matrix:" << endl;
	//resultMatrix.printMatrix();
	
	return resultMatrix;
}

template <typename T>
Matrix<T> Matrix<T>::getMinorMatrix() const
{
	Matrix<T> resultMatrix( _numRows, _numCols );
	MatrixTraversor traversor( MatrixTraversor::ROW, _numRows, _numCols );
	DeterminantHelper* detHelper = new DeterminantHelper( _numRows, _numCols );
	
	unsigned int elementCount = 0;
	while ( elementCount < _numCols * _numRows )
	{
		detHelper->setInvalidRowAndCol( traversor.getRowPosition(), traversor.getColumnPosition() );
		T data = _computeDeterminant( _numRows - 1, detHelper );
		resultMatrix.setDataAt( data, traversor.getRowPosition(), traversor.getColumnPosition() );
		detHelper->setValidRowAndCol( traversor.getRowPosition(), traversor.getColumnPosition() );

		traversor.traverse();
		elementCount++;
	}

	delete detHelper;
	return resultMatrix;
}

template <typename T>
T Matrix<T>::getDataAt( const unsigned int row, const unsigned int col ) const
{
	return _matrix[row - 1][col - 1];
}

template <typename T>
void Matrix<T>::setDataAt( const T data, const unsigned int row, const unsigned int col )
{
	_matrix[row - 1][col - 1] = data;
}

template <typename T>
void Matrix<T>::_copyMatrix( const Matrix& aMatrix )
{
	MatrixTraversor traversor( MatrixTraversor::ROW, aMatrix.numRows(), aMatrix.numCols() );
	unsigned elementsTraversed = 0;
	while ( elementsTraversed < aMatrix.numRows() * aMatrix.numCols() )
	{
		_matrix[traversor.getRowPosition() - 1][traversor.getColumnPosition() - 1] = aMatrix.getDataAt( traversor.getRowPosition(), traversor.getColumnPosition() );
		traversor.traverse();
		elementsTraversed++;
	}
}

template <typename T>
void Matrix<T>::_allocMatrix( T**& matrix, unsigned int numRows, unsigned int numCols )
{
	if ( matrix != nullptr ) _deallocMatrix( matrix, numRows );
	
	//numRows is 1 based
	matrix = new T*[numRows];
	
	while ( numRows > 0 )
	{
		matrix[--numRows] = new T[numCols];
	}
}

template <typename T>
void Matrix<T>::_deallocMatrix( T**& matrix, unsigned int numRows )
{
	if ( matrix == nullptr )
	{
		return;
	}
	// numRows is 1 based
	while ( numRows > 0 )
	{
		delete[] matrix[--numRows];
	}
	
	delete[] matrix;
	matrix = nullptr;
}