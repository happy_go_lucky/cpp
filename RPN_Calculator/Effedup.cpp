#include "Effedup.h"

Effedup::Effedup( char* message )
{
	_message = message;
}

const char* Effedup::what() const throw()
{
	return _message;
}
