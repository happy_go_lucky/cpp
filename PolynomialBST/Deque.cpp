#include "Deque.h"

template <typename TYPE>
Deque<TYPE>::Deque()
{
	_head = nullptr;
	_tail = nullptr;
	_size = 0;
}

template <typename TYPE>
Deque<TYPE>::Deque( const Deque<TYPE>& deque )
{
	_head = nullptr;
	_tail = nullptr;
	_size = deque._size;
	_copyDeque( deque._head );
}

template <typename TYPE>
Deque<TYPE>::~Deque()
{
	_deleteList( _head );
}

template <typename TYPE>
bool Deque<TYPE>::isEmpty() const
{
	return _head == nullptr;
}

template <typename TYPE>
void Deque<TYPE>::pushBack( TYPE data )
{
	if ( isEmpty() )
	{
		_tail = _head = new DequeNode<TYPE>( data );
	}
	else
	{
		_tail->setNext( new DequeNode<TYPE>( data, nullptr, _tail ) );
		_tail = _tail->getNext();
	}
	_size++;
}

template <typename TYPE>
void Deque<TYPE>::pushFront( TYPE data )
{
	if ( isEmpty() )
	{
		_tail = _head = new DequeNode<TYPE>( data );
	}
	else
	{
		_head = new DequeNode<TYPE>( data, _head, _head->getPrev() );
		_head->getNext()->setPrev( _head );
	}
	_size++;
}

template <typename TYPE>
TYPE Deque<TYPE>::popFront()
{
	if ( isEmpty() )
	{
		//~ throw Effedup( ( char* ) "trying to pop from an empty deque" );
		//~ cout << "testing" << endl;
		return TYPE();
	}
	else
	{
		DequeNode<TYPE>* discard = _head;
		_head = _head->getNext();
		if ( _head != nullptr )
		{
			_head->setPrev( nullptr );
		}
		else 	// there is no more nodes left, point head and tail to nullptr
		{
			_tail = _head;
		}	
		_size--;
		TYPE tempRet = discard->getData();
		delete discard;
		return tempRet;
	}
}

template <typename TYPE>
TYPE Deque<TYPE>::popBack()
{
	if ( isEmpty() )
	{
		//~ throw Effedup( ( char* ) "trying to pop from an empty deque" );
		//~ return TYPE();
	}
	else
	{
		DequeNode<TYPE>* discard = _tail;
		_tail = _tail->getPrev();
		if ( _tail != nullptr )
		{
			_tail->setNext( nullptr );
		}
		else 	// there is no more nodes left, point head and tail to nullptr
		{
			_head = _tail;
		}
		_size--;
		TYPE tempRet = discard->getData();
		delete discard;
		return tempRet;
	}
}

template <typename TYPE>
TYPE Deque<TYPE>::peekFront() const
{
	if ( isEmpty() )
	{
		return TYPE();
	}
	else
	{
		return _head->getData();
	}
}

template <typename TYPE>
TYPE Deque<TYPE>::peekBack() const
{
	if ( isEmpty() )
	{
		return TYPE();
	}
	else
	{
		return _tail->getData();
	}
}

template <typename TYPE>
void Deque<TYPE>::clear()
{
	_deleteList( _head );
	_tail = _head = nullptr;
	_size = 0;
}

template <typename TYPE>
void _deleteList( DequeNode<TYPE>* node )
{
	if ( node == nullptr )
	{
		return;
	}
	_deleteList( node->getNext() );
	delete node;
}

template <typename TYPE>
int Deque<TYPE>::size() const
{
	return _size;
}

template <typename TYPE>
Deque<TYPE>& Deque<TYPE>::operator =( const Deque<TYPE>& deque )
{
	if ( this != &deque )
	{
		clear();
		_copyDeque( deque._head );
		_size = deque._size;
	}
	
	return *this;
}

template <typename TYPE>
void Deque<TYPE>::_copyDeque( DequeNode<TYPE>* node )
{
	if ( node == nullptr )
	{
		return;
	}
	
	pushBack( node->getData() );
	_copyDeque( node->getNext() );
}

template <typename TYPE>
void Deque<TYPE>::print() const
{
	if ( isEmpty() )
	{
		cout << "[  ]" << endl;
		return;
	}
	
	DequeNode<TYPE>* itr = _head;
	while ( itr != nullptr )
	{
		cout << itr->getData();
		if ( itr->getNext() != nullptr )
		{
			cout << " <--> ";
		}
		itr = itr->getNext();
	}
	cout << endl;
}
