#ifndef ELEMENT_OPERAND_H
#define ELEMENT_OPERAND_H

#include <iostream>
#include "FSM_Helper.h"
#include "ExprElement.h"

using std::ostream;

class ElementOperand : public ExprElement
{
	public:
		ElementOperand() = delete;
		
		explicit ElementOperand(const double& data) noexcept;
		ElementOperand(const ElementOperand& val) = default;
		
		~ElementOperand() noexcept = default;
		
		ElementOperand& operator =(const ElementOperand& val) = default;
		friend ostream& operator <<(ostream& out, const ElementOperand& elem);
		
		inline double getData() const
		{
			return _data;
		}
		
		inline void setData(double data)
		{
			_data = data;
		}
	
	private:
		double _data;
};

#endif
