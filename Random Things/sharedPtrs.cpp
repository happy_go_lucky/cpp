#include <iostream>
#include <memory>
#include <vector>

void passShared(std::shared_ptr<int> sp)
{
    std::cout << "sp use count: " << sp.use_count() << std::endl;
    std::cout << "-----------------------" << std::endl;
}

void printStats(std::weak_ptr<int> wp)
{
    std::cout<<"wp is ";
    if (wp.expired())
    {
        std::cout << "expired" << std::endl;
    }
    else
    {
        std::cout << "valid" << std::endl;
        std::shared_ptr<int> sp = wp.lock();
        std::cout << "has value: " << *sp << std::endl;
    }
    std::cout << "wp use count: " << wp.use_count() << std::endl;
    std::cout << "-----------------------" << std::endl;
}

void testVectorShared(std::vector<std::shared_ptr<int>>& spVector)
{
    for (int ii = 0; ii < spVector.size(); ii++)
    {
        passShared(spVector[ii]);
    }
}

void testVectorWeak(std::vector<std::shared_ptr<int>>& wpVector)
{
    for (int ii = 0; ii < wpVector.size(); ii++)
    {
        printStats(wpVector[ii]);
    }
}

void initVector(std::vector<std::shared_ptr<int>>& spVector)
{
    for (int ii = 0; ii < spVector.size(); ii++)
    {
        spVector[ii] = std::make_shared<int>(3);
    }
}

void testInitialization(std::shared_ptr<int> ptr)
{
	if (ptr)
	{
		std::cout << "shared ptr is initalized" << std::endl;
	}
	else
	{
		std::cout << "shared ptr is uninitalized" << std::endl;
	}
}

int main()
{
	std::cout << "testing initialization of shared ptrs" << std::endl;
	std::shared_ptr < int >testPtr;
	testInitialization(testPtr);
	testPtr = nullptr;
	testInitialization(testPtr);
	testPtr = std::make_shared<int>(11);
	testInitialization(testPtr);
	
    std::cout << "creating shared pointer vector" << std::endl;
    std::vector<std::shared_ptr<int>> spVector(5);
    std::cout << "initializing shared pointer vector" << std::endl;
    initVector(spVector);
    std::cout << "testing pointer vector for weak pointers" << std::endl;
    testVectorWeak(spVector);
    std::cout << "testing pointer vector for shared pointers" << std::endl;
    testVectorShared(spVector);
    
    std::cout << "clearing shared pointer vector" << std::endl;
    spVector.clear();
    std::cout << "testing pointer vector for shared pointers" << std::endl;
    testVectorShared(spVector);
    
    std::weak_ptr<int> wp;
    printStats(wp);
    
    std::shared_ptr<int> sp_uninit;
    printStats(sp_uninit);
    passShared(sp_uninit);
    
    std::shared_ptr<int> sp = std::make_shared<int>(10);
    std::weak_ptr<int> wp_sp = std::make_shared<int>(11);
    wp = sp;
    
    printStats(wp);
    printStats(wp_sp);
    passShared(sp);
    
    sp.reset();
    printStats(wp);

    return 0;
}
