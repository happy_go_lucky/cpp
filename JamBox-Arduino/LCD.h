/*
 * 16x2 LCD
 * 16 columns x 2 rows
 * each 8 x 4 matrix represents one character
 * 
 * RX (receive)  Serial receive (input to the display). 5V TTL level, 9600 baud (default rate, can be changed), 8 bits, 1 stop, no parity.
 * GND (ground)  Ground for the power supply.
 * VDD (power)   Power supply, this should be +5V at up to 60mA if the backlight is fully on.
 * 
 * To move the cursor, send the special character 254 decimal (0xFE hex), followed by the cursor position you'd like to set.
 * Each cursor position is represented by a number, see the table below to determine the number to send: 
 * 
 * position  1   2   3   4   5   6   7   8   9   10  11  12  13  14  15  16
 *   line 1  128   129   130   131   132   133   134   135   136   137   138   139   140   141   142   143
 *   line 2  192   193   194   195   196   197   198   199   200   201   202   203   204   205   206   207
 *   
 * For example, if you want to move to the beginning of the second line, send the bytes 254 192 (decimal).
 */

#ifndef LCD_H
#define LCD_H

// Use the softwareserial library to create a new "soft" serial port
// for the display. This prevents display corruption when uploading code.
#include <SoftwareSerial.h>
#include "Arduino.h"

class LCD
{
  public:
    const static int NUM_ROWS = 2;
    const static int NUM_COLS = 16;
    
    enum LCD_ROW_1 { VAL_BLOCK_1_1 = 128, VAL_BLOCK_1_2, VAL_BLOCK_1_3, VAL_BLOCK_1_4, VAL_BLOCK_1_5, VAL_BLOCK_1_6, VAL_BLOCK_1_7, VAL_BLOCK_1_8, 
                      VAL_BLOCK_1_9, VAL_BLOCK_1_10, VAL_BLOCK_1_11, VAL_BLOCK_1_12, VAL_BLOCK_1_13, VAL_BLOCK_1_14, VAL_BLOCK_1_15, VAL_BLOCK_1_16
    };
    enum LCD_ROW_2 { VAL_BLOCK_2_1 = 192, VAL_BLOCK_2_2, VAL_BLOCK_2_3, VAL_BLOCK_2_4, VAL_BLOCK_2_5, VAL_BLOCK_2_6, VAL_BLOCK_2_7, VAL_BLOCK_2_8, 
                      VAL_BLOCK_2_9, VAL_BLOCK_2_10, VAL_BLOCK_2_11, VAL_BLOCK_2_12, VAL_BLOCK_2_13, VAL_BLOCK_2_14, VAL_BLOCK_2_15, VAL_BLOCK_2_16
    };
    enum BLOCKS { BLOCK_1_1 = 0, BLOCK_1_2, BLOCK_1_3, BLOCK_1_4, BLOCK_1_5, BLOCK_1_6, BLOCK_1_7, BLOCK_1_8, 
                  BLOCK_1_9, BLOCK_1_10, BLOCK_1_11, BLOCK_1_12, BLOCK_1_13, BLOCK_1_14, BLOCK_1_15, BLOCK_1_16,
                  BLOCK_2_1, BLOCK_2_2, BLOCK_2_3, BLOCK_2_4, BLOCK_2_5, BLOCK_2_6, BLOCK_2_7, BLOCK_2_8, 
                  BLOCK_2_9, BLOCK_2_10, BLOCK_2_11, BLOCK_2_12, BLOCK_2_13, BLOCK_2_14, BLOCK_2_15, BLOCK_2_16
    };
    const static int LCD_BLOCK[NUM_ROWS * NUM_COLS];
  
    LCD( const int, const int );
    ~LCD();

    void writeString( const char*, const int ) const;
    void clearLCD() const;
    
  private:
    int _pin;
    int _serialBaudRate;
    SoftwareSerial* _lcd;
    char _stringToWrite[( NUM_ROWS * NUM_COLS ) + 1];

    void _copyStr( const char*, char* );
};

#endif
