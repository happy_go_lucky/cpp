#ifndef READ_WRITE_LOCK_H
#define READ_WRITE_LOCK_H

#include <pthread.h>
#include <iostream>

namespace posixthreads
{
	class ReadWriteLock
	{
		public:
			ReadWriteLock();
			~ReadWriteLock();
			
			ReadWriteLock(const ReadWriteLock& lock) = delete;
			ReadWriteLock(ReadWriteLock&& lock) = delete;
			ReadWriteLock& operator =(const ReadWriteLock& lock) = delete;
			ReadWriteLock& operator =(ReadWriteLock&& lock) = delete;
			
			void unlock();
			int tryWriteLock();
			void applyWriteLock();
			int tryReadLock();
			void applyReadLock();
			
		private:
			pthread_rwlockattr_t _attrib;
			pthread_rwlock_t _lock;
			
			void _destroyLock();
	};
}

#endif
