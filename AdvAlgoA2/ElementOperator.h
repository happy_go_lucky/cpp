#ifndef ELEMENT_OPERATOR_H
#define ELEMENT_OPERATOR_H

#include <iostream>
#include "FSM_Helper.h"
#include "ExprElement.h"

using std::ostream;

class ElementOperator : public ExprElement
{
	public:
		ElementOperator() = delete;
		
		explicit ElementOperator(char data) noexcept;		
		ElementOperator(const ElementOperator& val) = default;
		
		~ElementOperator() noexcept = default;
		
		ElementOperator& operator =(const ElementOperator& val) = default;
		friend ostream& operator <<(ostream& out, const ElementOperator& elem);
		
		inline char getData() const
		{
			return _data;
		}
		
		inline void setData(char data)
		{
			_data = data;
		}
	
	private:
		char _data;
};

#endif
