#ifndef CIRCULAR_QUEUE_CPP
#define CIRCULAR_QUEUE_CPP

#include "CircularQueue.h"

template <typename Object>
CircularQueue<Object>::CircularQueue(size_t s)
{
	_capacity = s;
	_values = new Object[_capacity];
	_back = _front = INVALID_INDEX;
}

template <typename Object>
CircularQueue<Object>::CircularQueue(const CircularQueue<Object>& aQueue)
{
	_front = aQueue._front;
	_back = aQueue._back;
	_capacity = aQueue._capacity;
	_values = new Object[_capacity];
	memcpy(_values, aQueue._values, _capacity);
}

template <typename Object>
CircularQueue<Object>::CircularQueue(const CircularQueue<Object>&& aQueue)
{
	_front = std::move(aQueue._front);
	_back = std::move(aQueue._back);
	_capacity = std::move(aQueue._capacity);
	*_values = aQueue._values;
	aQueue._values = nullptr;
}

template <typename Object>
CircularQueue<Object>::CircularQueue(initializer_list<Object> list)
{
	_values = new Object[list.size()];
	for (int ii = 0; ii < list.size(); ii++)
	{
		_values[ii] = std::move(list[ii]);
	}
}

template <typename Object>
CircularQueue<Object>::~CircularQueue()
{
	delete[] _values;
}

template <typename T>
ostream& operator <<(ostream& out, const CircularQueue<T>& aQueue) noexcept
{
	out << "[ ";
	const size_t& size = aQueue.size();
	size_t printFromPos;
	for (int ii = 0; ii < size; ii++)
	{
		printFromPos = (aQueue._front + ii) % aQueue._capacity;
		out << aQueue._values[printFromPos];
		if (ii < size - 1)
			out << ", ";
	}
	out << " ]" << endl;
	
	return out;
}

template <typename T>
ostream& operator <<(ostream& out, const CircularQueue<T>&& aQueue) noexcept
{
	out << "[ ";
	const size_t& size = aQueue.size();
	size_t printFromPos;
	for (int ii = 0; ii < size; ii++)
	{
		printFromPos = (aQueue._front + ii) % aQueue._capacity;
		out << aQueue._values[printFromPos];
		if (ii < size - 1)
			out << ", ";
	}
	out << " ]" << endl;
	
	return out;
}

template <typename Object>
CircularQueue<Object>& CircularQueue<Object>::operator =(const CircularQueue<Object>& aQueue)
{
	if (*this != aQueue)
	{
		_front = aQueue._front;
		_back = aQueue._back;
		_capacity = aQueue._capacity;
		_values = new Object[_capacity];
		memcpy(_values, aQueue._values, _capacity);
	}
	
	return *this;
}

template <typename Object>
CircularQueue<Object>& CircularQueue<Object>::operator =(const CircularQueue<Object>&& aQueue) noexcept
{
	_front = std::move(aQueue._front);
	_back = std::move(aQueue._back);
	_capacity = std::move(aQueue._capacity);
	*_values = aQueue._values;
	aQueue._values = nullptr;
	
	return *this;
}

template <typename Object>
size_t CircularQueue<Object>::size() const noexcept
{
	if (empty())
	{
		return 0;
	}
	else
	{
		if (_back >= _front)
		{
			return _back - _front + 1;
		}
		else
		{
			return (_capacity + _back + 1) - _front;
		}
		
	}
}

template <typename Object>
void CircularQueue<Object>::enque(const Object& obj)
{
	if (full())
	{
		increaseCapacity();
	}
	
	if (empty())
	{
		_front++;
		_back++;
	}
	else
	{
		_back++;
	}
	_values[_back] = obj;
}

template <typename Object>
void CircularQueue<Object>::enque(Object&& obj)
{
	if (full())
	{
		increaseCapacity();
	}
	
	if (empty())
	{
		_front++;
		_back++;
	}
	else
	{
		_back = (_back + 1) % _capacity;
	}
	_values[_back] = std::move(obj);
}

template <typename Object>
Object CircularQueue<Object>::deque()
{
	Object obj;
	if (!empty())
	{
		obj = _values[_front];
		if (size() == 1)
		{
			_front = _back = INVALID_INDEX;
		}
		else
		{
			_front = (_front + 1) % _capacity;
		}
	}
	else
	{
		cerr << "deque from empty queue is undefined. Check for empty before deque" << endl;
	}
	return obj;
}

template <typename Object>
void CircularQueue<Object>::print() noexcept
{
	cout << *this;
}

template <typename Object>
void CircularQueue<Object>::increaseCapacity()
{
	size_t newCapacity = _capacity * 2;
	Object* temp = new Object[newCapacity];
	size_t addFromPos;
	
	for (int ii = 0; ii < _capacity; ii++)
	{
		addFromPos = (_front + ii) % _capacity;
		temp[ii] = _values[addFromPos];
	}
	
	_front = 0;
	_back = _capacity - 1;
	_capacity = newCapacity;
	
	delete[] _values;
	_values = temp;
}

template <typename Object>
void CircularQueue<Object>::debugPrint() const noexcept
{
	#ifndef NDEBUG
	cout << "queue size: " << size() << endl;
	cout << "queue capacity: " << _capacity << endl;
	cout << "queue front: " << _front << endl;
	cout << "queue back: " << _back << endl;
	#endif
}

#endif
