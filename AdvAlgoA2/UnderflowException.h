#ifndef UNDERFLOW_EXCEPTION_H
#define UNDERFLOW_EXCEPTION_H

#include <exception>

using std::exception;

class UnderflowException : public exception
{
	public:
		UnderflowException( char* = ( char* ) "Exception!" );
		virtual const char* what() const throw();
		
	private:
		char* _message;
};

#endif
