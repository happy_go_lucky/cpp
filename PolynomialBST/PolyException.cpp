#include "PolyException.h"

PolyException::PolyException( char* message )
{
	_message = message;
}

PolyException::~PolyException()
{
	// use try catch
}

const char* PolyException::what() const throw()
{
	return _message;
}
