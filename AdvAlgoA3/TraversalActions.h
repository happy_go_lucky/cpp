#ifndef TRAVERSAL_ACTIONS_H
#define TRAVERSAL_ACTIONS_H

#include <vector>
#include <iostream>
#include "TraversalBaseAction.h"

using std::vector;
using std::cout;
using std::endl;

class TraversalVector : public TraversalBaseAction
{
	public:
		TraversalVector() = default;
		~TraversalVector() = default;
		
		void operator ()(PRQuadNode* node);
	
	private:
		vector<PRQuadNode*> vec;
};

class TraversalPrint : public TraversalBaseAction
{
	public:
		fstream* logPrint;
	
		TraversalPrint() = default;
		~TraversalPrint() = default;
		
		void operator ()(PRQuadNode* node);
};

#endif
