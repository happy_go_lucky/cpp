#ifndef GPIO_MANAGER_H
#define GPIO_MANAGER_H

/**
 * This a GpioPin factory class.
 * This class is responsible for managing and accessing the interaface to gpio pins
 *  on the Raspberry PI board
 * */

#include <iostream>
#include <vector>
#include <memory>
#include <functional>
#include "GpioDefines.h"
#include <posixthreads/inc/ReadWriteLock.h>
#include <posixthreads/inc/MutexLock.h>
#include "GpioPin.h"

namespace gpio
{
	class GpioManager
	{
		public:
			GpioManager() noexcept;
			~GpioManager();
			
			GpioManager(const GpioManager& gpioManager) = delete;
			GpioManager(GpioManager&& gpioManager) = delete;
			GpioManager& operator =(const GpioManager& gpioManager) = delete;
			GpioManager& operator =(GpioManager&& gpioManager) = delete;
			
			// these are factory methods, hence unique ptr is used
			std::shared_ptr<GpioPin> reservePinByGpio(GpioNumber gpioNumber);
			
		private:
	};
}
#endif
