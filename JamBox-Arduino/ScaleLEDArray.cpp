#include "ScaleLEDArray.h"

const int ScaleLEDArray::SCALE_LED_ARR[MAX_SCALE] = { SCALE_1_LED, SCALE_2_LED, SCALE_3_LED, SCALE_4_LED, SCALE_5_LED, SCALE_6_LED };

ScaleLEDArray::ScaleLEDArray()
{
  _initScaleLEDs();
}

ScaleLEDArray::~ScaleLEDArray()
{
  
}

void ScaleLEDArray::_initScaleLEDs()
{
  for ( int ii = 0; ii < MAX_SCALE; ii++ )
  {
    pinMode( SCALE_LED_ARR[ii], OUTPUT );
    digitalWrite( SCALE_LED_ARR[ii], LOW );
  }
}

void ScaleLEDArray::_resetScale()
{
  for ( int ii = 0; ii < MAX_SCALE; ii++ )
  {
    digitalWrite( SCALE_LED_ARR[ii], LOW );
  }
}

void ScaleLEDArray::setScale( const int value )
{
  _resetScale();
  for ( int ii = 0; ii < value; ii++ )
  {
    digitalWrite( SCALE_LED_ARR[ii], HIGH );
  }
}

