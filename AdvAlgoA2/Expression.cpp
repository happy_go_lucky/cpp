#include "Expression.h"

Expression::Expression(const char* expr) : _expression(expr)
{
	buildExpression();
	initFSM();
}

Expression::Expression(const string& expr)
{
	_expression = expr;
	buildExpression();
	initFSM();
}

Expression::Expression(const string&& expr) noexcept
{
	_expression = std::move(expr);
	buildExpression();
	initFSM();
	
	// TODO: fix the copy of elments
	/*
	vector<ExprElement*> _exprElements;
	bool _hasValidElements;
	ExprFSM* _fsmInfix;
	ExprFSM* _fsmPrefix;
	ExprFSM* _fsmPostfix;
	GlobalVariables _globalVars;
	*/
}

Expression::Expression(Expression& expr)
{
	_globalVars = expr._globalVars;
	_expression = expr._expression;
	_hasValidElements = expr._hasValidElements;
	for (int ii = 0; ii < expr._exprElements.size(); ii++)
	{
		_exprElements.push_back(expr._exprElements[ii]);
	}
}

Expression::Expression(Expression&& expr)
{
	_globalVars = std::move(expr._globalVars);
	_expression = std::move(expr._expression);
	_hasValidElements = std::move(expr._hasValidElements);
	_exprElements = std::move(expr._exprElements);
	expr._exprElements.clear();
}

Expression::~Expression() noexcept
{
	cleanupElementVector();
	cleanupStateMachines();
}

ostream& operator <<(ostream& out, const Expression& expr)
{
	out << expr._expression << endl;
	return out;
}

istream& operator >>(istream& in, Expression& expr)
{
	char* str = new char[Expression::MAX_EXPR_SIZE];
	in.getline(str, Expression::MAX_EXPR_SIZE, '\n');
	expr._expression = str;
	delete str;
	
	expr.buildExpression();
	expr.initFSM();
	
	return in;
}

Expression& Expression::operator =(const Expression& expr) noexcept
{
	if (this != &expr)
	{
		_expression = expr._expression;
		_hasValidElements = expr._hasValidElements;
		for (int ii = 0; ii < expr._exprElements.size(); ii++)
		{
			_exprElements.push_back(expr._exprElements[ii]);
		}
	}
	
	return *this;
}

Expression& Expression::operator =(Expression&& expr) noexcept
{
	_expression = std::move(expr._expression);
	_hasValidElements = std::move(expr._hasValidElements);
	_exprElements = std::move(expr._exprElements);
	expr._exprElements.clear();
	
	return *this;
}

double Expression::evaluate(ExpressionType expressionType) const
{
	if (_hasValidElements)
	{
		switch (expressionType)
		{
			case ExpressionType::POSTFIX_EXPR:
				return evalPostFix();
			
			case ExpressionType::PREFIX_EXPR:
				return evalInFix();
			
			default:
			case ExpressionType::INFIX_EXPR:
				return evalPreFix();
		}
	}
	
	return 0;
}

double Expression::evalPostFix() const
{
	// validate the expression first
	stack<double> evalStack;
	
	for (int ii = 0; ii < _exprElements.size(); ii++)
	{
		if (_exprElements[ii]->getType() == ElementType::TYPE_OPERAND)
		{
			ElementOperand* elem = static_cast<ElementOperand*>(_exprElements[ii]);
			evalStack.push(elem->getData());
		}
		else
		{
			ElementOperator* elem = static_cast<ElementOperator*>(_exprElements[ii]);
			double& num = evalStack.top();
			evalStack.push(computePostfix(evalStack, elem->getData()));
		}
	}
	
	return evalStack.top();
}

// TODO: implement this
double Expression::evalInFix() const
{
	double result = 0;
	return result;
}

// TODO: implement this
double Expression::evalPreFix() const
{
	double result = 0;
	return result;
}

bool Expression::isValid(ExpressionType expressionType) const
{
	if (_hasValidElements)
	{
		switch (expressionType)
		{
			case ExpressionType::POSTFIX_EXPR:
				return isValid_postfix();
			
			case ExpressionType::PREFIX_EXPR:
				return isValid_prefix();
			
			default:
			case ExpressionType::INFIX_EXPR:
				return isValid_infix();
		}
	}
	return false;
}

bool Expression::isValid_infix() const
{
	for (int ii = 0; ii < _exprElements.size(); ii++)
	{
		if (_fsmInfix->update(_exprElements[ii]))
		{
			continue;
		}
		else
		{
			return false;
		}
	}
	return _fsmInfix->getCurrentState()->getStateType() == ExprStates::STATE_END;
}

bool Expression::isValid_prefix() const
{
	for (int ii = 0; ii < _exprElements.size(); ii++)
	{
		if (_fsmPrefix->update(_exprElements[ii]))
		{
			continue;
		}
		else
		{
			return false;
		}
	}
	
	return _fsmPrefix->getCurrentState()->getStateType() == ExprStates::STATE_END;
}

bool Expression::isValid_postfix() const
{
	for (int ii = 0; ii < _exprElements.size(); ii++)
	{
		if (_fsmPostfix->update(_exprElements[ii]))
		{
			continue;
		}
		else
		{
			return false;
		}
	}
	
	return _fsmPostfix->getCurrentState()->getStateType() == ExprStates::STATE_END;
}

bool Expression::isValidOperator(char op) const
{
	for (int ii = 0; ii < _globalVars.LIST_OF_OPERATORS.length(); ii++)
	{
		if (op == _globalVars.LIST_OF_OPERATORS[ii])
		{
			 return true;
		}
	}
	return false;
}

void Expression::buildExpression()
{
	cleanupElementVector();
	cleanupStateMachines();
	vector<string> values;
	parseForElements(&_expression, &values, _globalVars.WHITESPACES.c_str());
	_hasValidElements = validateAndBuildExpr(values);
	printElementVector();
}

void Expression::parseForElements(const string* str, vector<string>* values, const char* whitespace) const
{
	if (str->length() == 0)
	{
		return;
	}
	
	if (*whitespace == _globalVars.NULL_TERM)
	{
		values->push_back(*str);
		return;
	}
	
	size_t startPos = 0;
	size_t endPos = 0;
	
	string partialExpr;
	
	while (endPos != string::npos)
	{
		endPos = str->find(*whitespace, startPos);
		partialExpr = str->substr(startPos, endPos - startPos);
		
		parseForElements(&partialExpr, values, whitespace + 1);
		
		while ((*str)[endPos] == *whitespace)
		{
			endPos++;
		}
		startPos = endPos;
	}
}

bool Expression::validateAndBuildExpr(vector<string>& values)
{
	char* ptrNext;
	for (int ii = 0; ii < values.size(); ii++)
	{
		if (values[ii].length() == 1 && isValidOperator(values[ii][0]))
		{
			// operator is verified
			_exprElements.push_back(new ElementOperator(values[ii][0]));
		}
		else
		{
			// if it is not an operator, it has to be a value
			// not using atof, using strtod instead
			double numericValue = strtod(values[ii].c_str(), &ptrNext);
			
			if (*ptrNext != _globalVars.NULL_TERM)
			{
				return false;
			}
			
			_exprElements.push_back(new ElementOperand(numericValue));
		}
	}
	return true;
}

bool Expression::isWhitespace(char c) const
{
	return string::npos != _globalVars.WHITESPACES.find(c);
}

void Expression::printElementVector() const
{
	cout << "printing element list" << endl << "[ ";
	const size_t& size = _exprElements.size();
	for (int ii = 0; ii < size; ii++)
	{
		if (_exprElements[ii]->getType() == ElementType::TYPE_OPERATOR)
		{
			ElementOperator* val = static_cast<ElementOperator*>(_exprElements[ii]);
			cout << val->getData();
		}
		else
		{
			ElementOperand* val = static_cast<ElementOperand*>(_exprElements[ii]);
			cout << val->getData();
		}
		
		if (ii < size - 1) cout << ", ";
	}
	cout << " ]" << endl;
}

void Expression::cleanupElementVector()
{
	if (_exprElements.empty())
	{
		return;
	}
	
	const size_t& size = _exprElements.size();
	for (int ii = 0; ii < size; ii++)
	{
		delete _exprElements[ii];
	}
	
	_exprElements.clear();
}

void Expression::initFSM()
{	
	_fsmInfix = new ExprFSM(ExpressionType::INFIX_EXPR, ExprStates::STATE_START);
	_fsmPrefix = new ExprFSM(ExpressionType::PREFIX_EXPR, ExprStates::STATE_START);
	_fsmPostfix = new ExprFSM(ExpressionType::POSTFIX_EXPR, ExprStates::STATE_START);
}

void Expression::cleanupStateMachines()
{
	delete _fsmInfix;
	_fsmInfix = nullptr;
	delete _fsmPostfix;
	_fsmPostfix = nullptr;
	delete _fsmPrefix;
	_fsmPrefix = nullptr;
}

template<typename T>
T Expression::computePostfix(stack<T>& data, char oper) const
{
	if (oper == _globalVars.EVAL_OPERATOR)
	{
		double val = data.top();
		data.pop();
		return val;
	}
	
	double rightOperand = data.top();
	data.pop();
	double leftOperand = data.top();
	data.pop();
	
	if (oper == _globalVars.SUB_OPERATOR)
	{
			return subtract(leftOperand, rightOperand);
	}
	else if (oper == _globalVars.MUL_OPERATOR)
	{
		return multiply(leftOperand, rightOperand);
	}
	else if (oper == _globalVars.DIV_OPERATOR)
	{
		return divide(leftOperand, rightOperand);
	}
	else if (oper == _globalVars.EXPO_OPERATOR)
	{
		return exponent(leftOperand, rightOperand);
	}
	else if (oper == _globalVars.AVG_OPERATOR)
	{
		double third = data.top();
		data.pop();
		return average(leftOperand, rightOperand, third);
	}
	else
	{
		return add(leftOperand, rightOperand);
	}
}

template<typename T>
T Expression::add(T leftOperand, T rightOperand) const
{
	return leftOperand + rightOperand;
}

template<typename T>
T Expression::subtract(T leftOperand, T rightOperand) const
{
	return leftOperand - rightOperand;
}

template<typename T>
T Expression::divide(T leftOperand, T rightOperand) const
{
	return leftOperand / rightOperand;
}

template<typename T>
T Expression::multiply(T leftOperand, T rightOperand) const
{
	return leftOperand * rightOperand;
}

template<typename T>
T Expression::exponent(T leftOperand, T rightOperand) const
{
	return pow(leftOperand, rightOperand);
}

template<typename T>
T Expression::average(T operandA, T operandB, T operandC) const
{
	return (operandA + operandB + operandC) / 3;
}
