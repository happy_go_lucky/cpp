#ifndef EFFEDUP_H
#define EFFEDUP_H

#include <exception>

using std::exception;

class Effedup: public exception
{
	public:
		Effedup( char* = ( char* ) "Exception!" );
		virtual const char* what() const throw();
		
	private:
		char* _message;
};

#endif
