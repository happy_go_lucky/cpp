#include "GISData.h"

ostream& operator <<(ostream& out, const GISData& data)
{
	cout << left;
	if (data.feature_id != GISData::INVALID_NUMERIC)
	{
		out << "\t" << setw(GISData::PRINT_WIDTH) << "Feature ID" << ": "
			<< data.feature_id << endl;
	}
	if (data.feature_name.compare(GISData::INVALID_STR) != 0)
	{
		out << "\t" << setw(GISData::PRINT_WIDTH) << "Feature Name" << ": "
			<< data.feature_name << endl;
	}
	if (data.feature_class.compare(GISData::INVALID_STR) != 0)
	{
		out << "\t" << setw(GISData::PRINT_WIDTH) << "Feature Cat" << ": "
			<< data.feature_class << endl;
	}
	if (data.state_alpha.compare(GISData::INVALID_STR) != 0)
	{
		out << "\t" << setw(GISData::PRINT_WIDTH) << "State" << ": "
			<< data.state_alpha << endl;
	}
	if (data.state_numeric != GISData::INVALID_NUMERIC)
	{
		out << "\t" << setw(GISData::PRINT_WIDTH) << "State code" << ": "
			<< data.state_numeric << endl;
	}
	if (data.county_name.compare(GISData::INVALID_STR) != 0)
	{
		out << "\t" << setw(GISData::PRINT_WIDTH) << "County" << ": "
			<< data.county_name << endl;
	}
	if (data.county_numeric != GISData::INVALID_NUMERIC)
	{
		out << "\t" << setw(GISData::PRINT_WIDTH) << "State code" << ": "
			<< data.county_numeric << endl;
	}
	if (data.primary_long_dms.isValid())
	{
		out << "\t" << setw(GISData::PRINT_WIDTH) << "Longitude dms" << ": "
			<< data.primary_long_dms << endl;
	}
	if (data.primary_lat_dms.isValid())
	{
		out << "\t" << setw(GISData::PRINT_WIDTH) << "Latitude dms" << ": "
			<< data.primary_lat_dms << endl;
	}
	if (data.primary_long_dec.isValid())
	{
		out << "\t" << setw(GISData::PRINT_WIDTH) << "Longitude deg" << ": "
			<< data.primary_long_dec << endl;
	}
	if (data.primary_lat_dec.isValid())
	{
		out << "\t" << setw(GISData::PRINT_WIDTH) << "Latitude deg" << ": "
			<< data.primary_lat_dec << endl;
	}
	if (data.source_long_dms.isValid())
	{
		out << "\t" << setw(GISData::PRINT_WIDTH) << "src longitude dms" << ": "
			<< data.source_long_dms << endl;
	}
	if (data.source_lat_dms.isValid())
	{
		out << "\t" << setw(GISData::PRINT_WIDTH) << "src latitude dms" << ": "
			<< data.source_lat_dms << endl;
	}
	if (data.source_long_dec.isValid())
	{
		out << "\t" << setw(GISData::PRINT_WIDTH) << "src longitude deg" << ": "
			<< data.source_long_dec << endl;
	}
	if (data.source_lat_dec.isValid())
	{
		out << "\t" << setw(GISData::PRINT_WIDTH) << "src latitude deg" << ": "
			<< data.source_lat_dec << endl;
	}
	if (data.elev_in_ft != GISData::INVALID_NUMERIC)
	{
		out << "\t" << setw(GISData::PRINT_WIDTH) << "Elev in ft" << ": "
			<< data.elev_in_ft << endl;
	}
	if (data.elev_in_m != GISData::INVALID_NUMERIC)
	{
		out << "\t" << setw(GISData::PRINT_WIDTH) << "Elev in m" << ": "
			<< data.elev_in_m << endl;
	}
	if (data.map_name.compare(GISData::INVALID_STR) != 0)
	{
		out << "\t" << setw(GISData::PRINT_WIDTH) << "map name" << ": "
			<< data.map_name << endl;
	}
	if (data.date_created.isValid())
	{
		out << "\t" << setw(GISData::PRINT_WIDTH) << "Date created" << ": "
			<< data.date_created << endl;
	}
	if (data.date_edited.isValid())
	{
		out << "\t" << setw(GISData::PRINT_WIDTH) << "Date mod" << ": "
			<< data.date_edited << endl;
	}
	return out;
}
