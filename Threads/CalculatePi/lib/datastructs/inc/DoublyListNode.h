#ifndef DOUBLY_LIST_NODE_H
#define DOUBLY_LIST_NODE_H

#include <utility>
#include <iostream>

namespace datastructs
{
	template <class Type>
	class DoublyListNode
	{
		public:
			// This class is a helper class for the DoublyList class hence the
			// friendship is defined.
			template <typename T>
			friend class DoublyList;
			template <typename T>
			friend class DoublyListIterator;
			
			DoublyListNode() = delete;
			DoublyListNode(const Type& data, DoublyListNode<Type>* nextPtr = nullptr,
							DoublyListNode<Type>* prevPtr = nullptr) noexcept;
			DoublyListNode(Type&& data, DoublyListNode<Type>* nextPtr = nullptr,
							DoublyListNode<Type>* prevPtr = nullptr) noexcept;
			DoublyListNode(const DoublyListNode<Type>& node) noexcept;
			DoublyListNode(DoublyListNode<Type>&& node) noexcept;
			
			~DoublyListNode();
			
			DoublyListNode<Type>& operator =(const DoublyListNode<Type>& node) & noexcept;
			DoublyListNode<Type>& operator =(DoublyListNode<Type>&& node) & noexcept;
			bool operator ==(const DoublyListNode<Type>& node) const noexcept;
			bool operator !=(const DoublyListNode<Type>& node) const noexcept;
			
			template <typename U>
			friend std::ostream& operator <<(std::ostream& out, const DoublyListNode<U>& node) noexcept;
			
		private:
			Type _data;
			DoublyListNode<Type>* _nextNode;
			DoublyListNode<Type>* _prevNode;
	};
}
#endif
