#ifndef POINT_2D_CPP
#define POINT_2D_CPP

#include "Point2D.h"

template <typename T>
Point2D<T>::Point2D() noexcept
{
	// nothing to do here
}

template <typename T>
Point2D<T>::Point2D(T& x, T& y) noexcept
{
	_x = x;
	_y = y;
}

template <typename T>
Point2D<T>::Point2D(T&& x, T&& y) noexcept
{
	_x = std::move(x);
	_y = std::move(y);
}

template <typename T>
Point2D<T>::Point2D(const Point2D<T>& point)
{
	_x = point.x;
	_y = point.y;
}

template <typename T>
Point2D<T>::Point2D(Point2D<T>&& point) noexcept
{
	_x = std::move(point.x);
	_y = std::move(point.y);
}

template <typename T>
Point2D<T>::Point2D(initializer_list<T> list)
{
	if (list.size() != 2)
	{
		return;
	}
	_x = list[0];
	_y = list[1];
}

template <typename T>
Point2D<T>::~Point2D()
{
	// nothing to do here
}

template <typename U>
ostream& operator <<(ostream& out, const Point2D<U>& point)
{
	out << "x, y: " << point._x << ", " << point._y << endl;
	return out;
}

template <typename T>
Point2D<T>& Point2D<T>::operator =(const Point2D<T>& point)
{
	if (this != &point)
	{
		_x = point._x;
		_y = point._y;
	}
	return *this;
}

template <typename T>
Point2D<T>& Point2D<T>::operator =(Point2D<T>&& point) noexcept
{
	_x = std::move(point._x);
	_y = std::move(point._y);
	return *this;
}

template <typename U>
bool operator ==(const Point2D<U>& pointL, const Point2D<U>& pointR) noexcept
{
	return pointL._x == pointR._x && pointL._y == pointR._y;
}

template <typename U>
bool operator ==(Point2D<U>&& pointL, Point2D<U>&& pointR) noexcept
{
	return pointL._x == pointR._x && pointL._y == pointR._y;
}

template <typename U>
bool operator !=(const Point2D<U>& pointL, const Point2D<U>& pointR) noexcept
{
	return pointL._x != pointR._x || pointL._y != pointR._y;
}

template <typename U>
bool operator !=(Point2D<U>&& pointL, Point2D<U>&& pointR) noexcept
{
	return pointL._x != pointR._x || pointL._y != pointR._y;
}

template <typename U>
bool operator +(const Point2D<U>& pointL, const Point2D<U>& pointR)
{
	Point2D<U> point(pointL._x + pointR._x, pointL._y + pointR._y);
	return point;
}

template <typename U>
bool operator +(Point2D<U>&& pointL, Point2D<U>&& pointR)
{
	Point2D<U> point(pointL._x + pointR._x, pointL._y + pointR._y);
	return point;
}

template <typename U>
bool operator -(const Point2D<U>& pointL, const Point2D<U>& pointR)
{
	Point2D<U> point(pointL._x - pointR._x, pointL._y - pointR._y);
	return point;
}

template <typename U>
bool operator -(Point2D<U>&& pointL, Point2D<U>&& pointR)
{
	Point2D<U> point(pointL._x - pointR._x, pointL._y - pointR._y);
	return point;
}

template <typename U>
bool operator *(const Point2D<U>& pointL, const Point2D<U>& pointR)
{
	Point2D<U> point(pointL._x * pointR._x, pointL._y * pointR._y);
	return point;
}

template <typename U>
bool operator *(Point2D<U>&& pointL, Point2D<U>&& pointR)
{
	Point2D<U> point(pointL._x * pointR._x, pointL._y * pointR._y);
	return point;
}

template <typename U>
bool operator /(const Point2D<U>& pointL, const Point2D<U>& pointR)
{
	Point2D<U> point(pointL._x / pointR._x, pointL._y / pointR._y);
	return point;
}

template <typename U>
bool operator /(Point2D<U>&& pointL, Point2D<U>&& pointR)
{
	Point2D<U> point(pointL._x / pointR._x, pointL._y / pointR._y);
	return point;
}

template <typename T>
void Point2D<T>::setX(T& val)
{
	_x = val;
}

template <typename T>
void Point2D<T>::setX(T&& val)
{
	_x = std::move(val);
}

template <typename T>
void Point2D<T>::setY(T& val)
{
	_y = val;
}

template <typename T>
void Point2D<T>::setY(T&& val)
{
	_y = std::move(val);
}

template <typename T>
T Point2D<T>::getX() const noexcept
{
	return _x;
}

template <typename T>
T Point2D<T>::getY() const noexcept
{
	return _y;
}

#endif
