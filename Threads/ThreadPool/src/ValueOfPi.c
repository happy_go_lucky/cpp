/**
 * This program calculates the value of PI using area under the curve.
 * The mid-point rectangle rule is used to calculate the approximate area
 * under the curve. 
 * pi = lim(0-1) (4/(1 + x^2))dx
 * */

#include <iostream>
#include <pthread.h>
#include <iomanip>
#include "../inc/Rectangle.h"
#include <posixthreads/inc/MutexLock.h>
#include <posixthreads/inc/ConditionVariable.h>
#include "../inc/HelperFxns.h"
#include "../inc/RectangleArea_Impl.h"
#include "../inc/RectAreaImplArgs.h"
#include "../inc/RectAreaNonMutex_Impl.h"

using namespace std;

void singleThreadedVersion();
void multiThreadedVersion();
void multiThreadedNonMutexVersion();
void localFunction();

// also decides the precision of calculation
// larger number means more precision, and longer calculation
static const long NUM_RECTS = 10000000;
static const double LOWER_LIMIT = 0.0;
static const double UPPER_LIMIT = 1.0;
static const short NUM_THREADS = 4;

int main(int argCount, char** argVector)
{
	clock_t begin = clock(); // Start the stopwatch
	singleThreadedVersion();
	clock_t end = clock(); // Stop the stopwatch
	cout << "singlethreaded time: " << setw(16) 
		<< helperfxns::getTimeDiffInSeconds(begin, end) << endl;
	
	
	begin = clock(); // Start the stopwatch
	multiThreadedVersion();
	end = clock(); // Stop the stopwatch
	cout << "multithreaded time: " << setw(16)
		<< helperfxns::getTimeDiffInSeconds(begin, end) << endl;
	
	begin = clock(); // Start the stopwatch
	multiThreadedNonMutexVersion();
	end = clock(); // Stop the stopwatch
	cout << "multithreaded non mutex time: " << setw(16) 
	 	<< helperfxns::getTimeDiffInSeconds(begin, end) << endl;
	
	return EXIT_SUCCESS;
}

void multiThreadedVersion()
{
	Rectangle* rectArray = new Rectangle[NUM_RECTS]();
	RectAreaImplArgs* threadArgs = new RectAreaImplArgs[NUM_THREADS]();
	RectangleArea_Impl* rectAreaImplArray = new RectangleArea_Impl[NUM_THREADS]();
	
	double totalArea = 0.0;
	double deltaX = (UPPER_LIMIT - LOWER_LIMIT) / (double) NUM_RECTS;
	
	for (int ii = 0; ii < NUM_THREADS; ii++)
	{
		threadArgs[ii].deltaX = deltaX;
		threadArgs[ii].numRects = NUM_RECTS;
		threadArgs[ii].numThreads = NUM_THREADS;
		threadArgs[ii].rectArray = rectArray;
		threadArgs[ii].totalArea = &totalArea;
		threadArgs[ii].threadNumber = ii;
		
		rectAreaImplArray[ii].start(&threadArgs[ii]);
		rectAreaImplArray[ii].join();
	}
	
	printf("Computed pi = %f\n", totalArea);
	delete[] rectArray;
	delete[] rectAreaImplArray;
	delete[] threadArgs;
}

void multiThreadedNonMutexVersion()
{
	Rectangle* rectArray = new Rectangle[NUM_RECTS]();
	RectAreaImplArgs* threadArgs = new RectAreaImplArgs[NUM_THREADS]();
	RectAreaNonMutex_Impl* rectAreaImplArray = new RectAreaNonMutex_Impl[NUM_THREADS]();
	
	double totalArea = 0.0;
	double area[NUM_THREADS] = { 0.0 };
	double deltaX = (UPPER_LIMIT - LOWER_LIMIT) / (double) NUM_RECTS;
	
	for (int ii = 0; ii < NUM_THREADS; ii++)
	{
		threadArgs[ii].deltaX = deltaX;
		threadArgs[ii].numRects = NUM_RECTS;
		threadArgs[ii].numThreads = NUM_THREADS;
		threadArgs[ii].rectArray = rectArray;
		threadArgs[ii].threadNumber = ii;
		
		rectAreaImplArray[ii].start(&threadArgs[ii], &area[ii]);
		rectAreaImplArray[ii].join();
	}
	
	for (int ii = 0; ii < NUM_THREADS; ii++)
	{
		totalArea += area[ii];
	}
	
	printf("Computed pi = %f\n", totalArea);
	delete[] rectArray;
	delete[] rectAreaImplArray;
	delete[] threadArgs;
}


void singleThreadedVersion()
{
	Rectangle* rectArray = new Rectangle[NUM_RECTS]();
	double mid, area = 0.0;
	double deltaX = (UPPER_LIMIT - LOWER_LIMIT) / (double) NUM_RECTS;
	
	for (int ii = 0; ii < NUM_RECTS; ii++)
	{
		rectArray[ii].setWidth(deltaX); 
		mid = (ii + 0.5) * rectArray[ii].getWidth();
		rectArray[ii].setHeight(4.0 / (1.0 + mid * mid));
		area += rectArray[ii].getArea();
	}
	printf("Computed pi = %f\n", area);
	
	delete[] rectArray;
}

void localFunction()
{
	printf("testing a local function outside of thread space\n");
}
