#include "Polynomial.h"

Polynomial::Polynomial()
{
	//~ cout << "Polynomial" << endl;
}

Polynomial::Polynomial( const Polynomial& otherPoly )
{
	_poly = otherPoly._poly;
}

Polynomial::~Polynomial()
{
	//~ cout << "~Polynomial" << endl;
}

bool Polynomial::isZero() const
{
	return _poly.isEmpty();
}

void Polynomial::print() const
{
	_poly.print( _poly.IN_ORDER );
}

int Polynomial::evalulate( int val_indeterminate )
{
	Evaluate<Term> eval( val_indeterminate );
	_poly.traverseTree( _poly.BREADTH_FIRST, &eval );
	return eval.getResult();
}

int Polynomial::degree()
{
	GetDegree<Term> polyDegree;
	_poly.traverseTree( _poly.BREADTH_FIRST, &polyDegree );
	return polyDegree.getDegree();
}

//~ int Polynomial::getCoeffOfDegree( int deg )
//~ {
	//~ return 0;
//~ }

bool Polynomial::insertTerm( int coeff, int deg )
{
	try
	{
		_poly.insert( Term( coeff, deg ) );
	}
	catch( BST_Exception& e )
	{
		if ( e.type() == BST_Exception::EXCEP_DUPLICATE_INSERT )
		{
			// implement the getCoeff and updateCoeff function and update the coeff here
		}
	}
	return true;
}

void Polynomial::diff_x()
{
	DifferentiatePoly<Term> diff;
	_poly.traverseTree( _poly.IN_ORDER, &diff );
}

ostream& operator << ( ostream& out, const Polynomial& poly )
{
	if ( poly.isZero() )
	{
		out << 0;
	}
	else
	{
		poly.print();
	}
	return out;
}

Polynomial Polynomial::operator + ( const Polynomial& otherPoly ) const
{
	Polynomial retPoly;
	if ( otherPoly.isZero() )
	{
		retPoly = *this;
	}
	else if ( isZero() )
	{
		retPoly = otherPoly;
	}
	else
	{
		TreeToQueue<Term> nodeQueue1;
		TreeToQueue<Term> nodeQueue2;
		
		_poly.traverseTree( _poly.IN_ORDER, &nodeQueue1 );
		Deque<Term> queue1 = nodeQueue1.getQueue();
		otherPoly._poly.traverseTree( otherPoly._poly.IN_ORDER, &nodeQueue2 );
		Deque<Term> queue2 = nodeQueue2.getQueue();
		
		Term temp;
		while ( !queue1.isEmpty() && !queue2.isEmpty() )
		{
			if ( queue1.peekFront() == queue2.peekFront() )
			{
				temp = queue1.peekFront() + queue2.peekFront();
				
				if ( temp.getCoeff() != 0 )
				{
					retPoly.insertTerm( temp.getCoeff(), temp.getExponent_x() );
				}
				queue1.popFront();
				queue2.popFront();
			}
			else if ( queue1.peekFront() < queue2.peekFront() )
			{
				if ( queue1.peekFront().getCoeff() != 0 )
				{
					retPoly.insertTerm( queue1.peekFront().getCoeff(), queue1.peekFront().getExponent_x() );
				}
				queue1.popFront();
			}
			else
			{
				if ( queue2.peekFront().getCoeff() != 0 )
				{
					retPoly.insertTerm(queue2.peekFront().getCoeff(), queue2.peekFront().getExponent_x() );
				}
				queue2.popFront();
			}
		}
		
		// remaining elements
		while ( !queue1.isEmpty() )
		{
			if ( queue1.peekFront().getCoeff() != 0 )
			{
				retPoly.insertTerm( queue1.peekFront().getCoeff(), queue1.peekFront().getExponent_x() );
			}
			queue1.popFront();
		}
		
		while ( !queue2.isEmpty() )
		{
			if ( queue2.peekFront().getCoeff() != 0 )
			{
				retPoly.insertTerm( queue2.peekFront().getCoeff(), queue2.peekFront().getExponent_x() );
			}
			queue2.popFront();
		}
	}
	
	return retPoly;
}

const Polynomial& Polynomial::operator = ( const Polynomial& otherPoly )
{
	if ( this != &otherPoly )
	{
		_poly = otherPoly._poly;
	}
	
	return *this;
}


// --------------------

template <typename T>
GetDegree<T>::GetDegree()
{
	_degree = 0;
	_isDegreeSet = false;
}

template <typename T>
void GetDegree<T>::operator () ( BST_Node<T>* node )
{
	int currentDegree = node->getData().getExponent_x();
	if ( !_isDegreeSet )
	{
		_degree = currentDegree;
		_isDegreeSet = true;
	}
	else
	{
		if ( currentDegree > _degree )
		{
			_degree = currentDegree;
		}
	}
}

template <typename T>
int GetDegree<T>::getDegree() const
{
	return _degree;
}

template <typename T>
Evaluate<T>::Evaluate( int value )
{
	_indeterminateValue = value;
	_result = 0;
}

template <typename T>
void Evaluate<T>::operator () ( BST_Node<T>* node )
{
	int coeff = node->getData().getCoeff();
	int exponent = node->getData().getExponent_x();
	
	if ( coeff == 0 )
	{
		return;
	}
	if ( exponent == 0 )
	{
		_result += coeff;
	}
	else
	{
		_result += ( coeff * computePower( _indeterminateValue, exponent ) );
	}
}

template <typename T>
int Evaluate<T>::computePower( int val, int power ) const
{
	int retVal = 1;
	while ( power > 0 )
	{
		retVal *= val;
		power--;
	}
	
	return retVal;
}

template <typename T>
int Evaluate<T>::getResult() const
{
	return _result;
}

template <typename T>
TreeToQueue<T>::TreeToQueue()
{
	
}

template <typename T>
TreeToQueue<T>::~TreeToQueue()
{
	_deque.clear();
}

template <typename T>
void TreeToQueue<T>::operator () ( BST_Node<T>* node )
{
	if ( node != nullptr )
	{
		_deque.pushBack( node->getData() );
	}
}

template <typename T>
Deque<T> TreeToQueue<T>::getQueue() const
{
	return _deque;
}

template <typename T>
DifferentiatePoly<T>::DifferentiatePoly()
{
	
}

template <typename T>
void DifferentiatePoly<T>::operator () ( BST_Node<T>* node )
{
	int coeff = node->getData().getCoeff();
	int exponent = node->getData().getExponent_x();
	//~ cout << "DifferentiatePoly " << coeff << ", " << exponent << endl;
	if ( exponent == 0 )
	{
		node->getData().setCoeff( 0 );
		node->getData().setExponent_x( 0 );
	}
	else
	{
		node->getData().setCoeff( coeff * exponent );
		node->getData().setExponent_x( exponent - 1 );
	}
}
