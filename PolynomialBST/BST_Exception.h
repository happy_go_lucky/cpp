#ifndef BST_EXCEPTION_H
#define BST_EXCEPTION_H

#include <exception>

class BST_Exception:public std::exception
{
	public:
		static const int EXCEP_UNKNOWN = 0;
		static const int EXCEP_DUPLICATE_INSERT = 1;
		
		BST_Exception( char* = ( char* ) "BST Exception!", int = EXCEP_UNKNOWN );
		~BST_Exception();
		
		virtual const char* what() const throw();
		int type() const;
		
	private:
		char* _message;
		int _type;
};

#endif
