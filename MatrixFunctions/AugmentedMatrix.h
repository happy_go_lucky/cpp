#ifndef AUGMENTED_MATRIX_H
#define AUGMENTED_MATRIX_H

#include "Matrix.h"
#include "Matrix.cpp"

template <class T>
class AugmentedMatrix
{
	public:
		AugmentedMatrix( const unsigned int, const unsigned int, const unsigned int );
		AugmentedMatrix( const AugmentedMatrix& );
		~AugmentedMatrix();
		
		void setDataAt( const T, const unsigned int, const unsigned int );
		T getDataAt( const unsigned int, const unsigned int ) const;
		unsigned int numRows() const;
		unsigned int numCols() const;
		void setPartitionValue( const unsigned int );
		unsigned int getPartitionValue() const;
		Matrix<T>* getCoeffMatrix() const;
		Matrix<T>* getConstMatrix() const;
		void printMatrix() const;
	
	private:
		Matrix<T>* _coeffMatrix;
		Matrix<T>* _constantMatrix;
		
		unsigned int _numRows;
		unsigned int _numCols;
		unsigned int _partitionValue;
};

#endif