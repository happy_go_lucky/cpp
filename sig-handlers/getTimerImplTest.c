/**
 * This file engages a kernel real timer and fires a signal every time the timer expires 
 * 
 * */

#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <signal.h>
#include <unistd.h>
#include <stdbool.h>
#include <stdint.h>
#include <sys/types.h>
#include <sys/wait.h>

#include "TimerHelper.h"

#include <string.h>

long int secondsElapsed = 0;

void sigHandler( int signalNumber );
void enableSignalHandlers( struct sigaction* signalAction, void handlerFunction(int) );
void disableSignalHandlers( struct sigaction* signalAction );
void getTimerOfDay( int timerLimit, int timerSignalFrequencyPerSecond );

int main( int agrCount, char** argList )
{
	getTimerOfDay( 10, 1 );
	return EXIT_SUCCESS;
}

void sigHandler( int signalNumber )
{
	switch ( signalNumber )
	{
		case SIGALRM:
			printf( "SIGALRM timer signal\n" );
			secondsElapsed++;
		break;
		
		case SIGVTALRM:
			printf( "SIGVTALRM timer signal\n" );
		break;
		
		case SIGPROF:
			printf( "SIGPROF timer signal\n" );
		break;
		
		default:
			fprintf( stderr, "unknown signal with signal number: %d\n", signalNumber );
		break;
	}
}

void enableSignalHandlers( struct sigaction* signalAction, void handlerFunction(int) )
{
	#ifdef DEBUG_PRINT
	printf( "enabling signal handlers for process\n" );
	#endif
	
	signalAction->sa_handler = handlerFunction;
	
	if ( sigaction( SIGALRM, signalAction, NULL ) == -1 )
	{
		fprintf( stderr, "unable to create SIGALRM signal handler\n" );
	}
}

void disableSignalHandlers( struct sigaction* signalAction )
{
	#ifdef DEBUG_PRINT
	printf( "disabling signal handlers\n" );
	#endif
	signalAction->sa_handler = SIG_DFL;
	sigaction( SIGALRM, signalAction, NULL );
}

void getTimerOfDay( int timerLimit, int timerSignalFrequencyPerSecond )
{
	struct sigaction signalAction;
	struct itimerval aTimer;
	secondsElapsed = 0;
	memset( &signalAction, 0, sizeof( signalAction ) );
	
	initTimer( &aTimer, ITIMER_REAL );
	enableSignalHandlers( &signalAction, &sigHandler );
	armTimer( &aTimer, ITIMER_REAL, timerSignalFrequencyPerSecond, 0, timerSignalFrequencyPerSecond, 0 );
	
	while ( secondsElapsed < timerLimit );
	disarmTimer( &aTimer, ITIMER_REAL );
	disableSignalHandlers( &signalAction );
	printf( "num seconds elapsed: %lu\n", secondsElapsed );
}
