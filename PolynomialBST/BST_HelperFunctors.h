#ifndef BST_HELPER_FUNCTORS_H
#define BST_HELPER_FUNCTORS_H

#include <iostream>
#include "BST_Node.h"
#include "BST_Node.cpp"

using std::cout;
using std::endl;

template <class T>
class BaseFunct
{
	public:
		BaseFunct();
		virtual void operator () ( BST_Node<T>* ) /*const*/;
};

template <class T>
class NullFunctor: public BaseFunct<T>
{
	public:
		NullFunctor();
		void operator () ( BST_Node<T>* ) /*const*/;
};

template <class T>
class Counter: public BaseFunct<T>
{
	public:
		Counter();
		void operator () ( BST_Node<T>* );
		int getCount() const;
		
	private:
		int _count;
};

template <class T>
class PrintNode: public BaseFunct<T>
{
	public:
		PrintNode( char* = ( char* ) " " );
		void operator () ( BST_Node<T>* ) /*const*/;
		
	private:
		char* _delimiter;
};

template <class T>
class DeleteNode: public BaseFunct<T>
{
	public:
		DeleteNode();
		void operator () ( BST_Node<T>* ) /*const*/;
};

#endif
