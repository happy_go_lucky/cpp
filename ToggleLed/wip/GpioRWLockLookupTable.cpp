#include "../inc/GpioRWLockLookupTable.h"

GpioRWLockLookupTable::GpioRWLockLookupTable() noexcept
{
	_rwLocks = new vector<std::shared_ptr<posixthreads::ReadWriteLock>>(gpio::MAX_PINS);
}

GpioRWLockLookupTable::~GpioRWLockLookupTable()
{
	for (int ii = 0; ii < gpio::MAX_PINS; ii++)
	{
		if (_rwLocks[ii].use_count() > 0)
		{
			_rwLocks[ii]->unlock();
		}
	}
	// vector clear resets the smart pointer
	_rwLocks->clear();
	delete _rwLocks;
}

std::shared_ptr<posixthreads::ReadWriteLock> GpioRWLockLookupTable::getReferenceToLock(gpio::PinNumber pinNumber) noexcept
{
	return _initLock(static_cast<size_t>(pinNumber));
}

std::shared_ptr<posixthreads::ReadWriteLock> GpioRWLockLookupTable::getReferenceToLock(gpio::GpioNumber gpioPinNumber) noexcept
{
	return _initLock(static_cast<size_t>(gpioPinNumber));
}

std::shared_ptr<posixthreads::ReadWriteLock> GpioRWLockLookupTable::_initLock(size_t pin)
{
	if (_rwLocks[pin].use_count == 0)
	{
		_rwLocks[pin] = std::make_shared<ReadWriteLock>();
	}
	
	return _rwLocks[pin];
}
