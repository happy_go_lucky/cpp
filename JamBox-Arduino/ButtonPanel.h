/**
 * The Button panel (4x4 keypad membrane) has 8 pins, each represnts the a row or a column. The way the button panel works is
 * that either the set of column pins or row pins is used for input, and the other set is used for the output. It is advised
 * to use pull up resistor with the input pins. The current is supplied from output to input pins and thier combination gives 
 * the button status
 *       C1  C2   C3  C4
 *      +---+---+---+---+
 *  R1  | 1 | 2 | 3 | A |
 *      +---+---+---+---+
 *  R2  | 4 | 5 | 6 | B |
 *      +---+---+---+---+
 *  R3  | 7 | 8 | 9 | C |
 *      +---+---+---+---+
 *  R4  | * | 0 | # | D |
 *      +---+---+---+---+
 *          ||||||||
 *          ||||||||
 *          ||||||||
 * Pin:     87654321
 * 
 *  Pin 1: C4
 *  Pin 2: C3
 *  Pin 3: C2
 *  Pin 4: C1
 *  Pin 5: R4
 *  Pin 6: R3
 *  Pin 7: R2
 *  Pin 8: R1
 *  
 *  Button 1: R1 & C1 ( Pin 8 & Pin 4 )
 *  Button 2: R1 & C2 ( Pin 8 & Pin 3 )
 *  Button 3: R1 & C3 ( Pin 8 & Pin 2 )
 *  Button 4: R2 & C1 ( Pin 7 & Pin 4 )
 *  Button 5: R2 & C2 ( Pin 7 & Pin 3 )
 *  Button 6: R2 & C3 ( Pin 7 & Pin 2 )
 *  Button 7: R3 & C1 ( Pin 6 & Pin 4 )
 *  Button 8: R3 & C2 ( Pin 6 & Pin 3 )
 *  Button 9: R3 & C3 ( Pin 6 & Pin 2 )
 *  Button 0: R4 & C2 ( Pin 5 & Pin 3 )
 *  Button A: R1 & C4 ( Pin 8 & Pin 1 )
 *  Button B: R2 & C4 ( Pin 7 & Pin 1 )
 *  Button C: R3 & C4 ( Pin 6 & Pin 1 )
 *  Button D: R4 & C4 ( Pin 5 & Pin 1 )
 *  Button *: R4 & C1 ( Pin 5 & Pin 4 )
 *  Button #: R4 & C3 ( Pin 5 & Pin 2 )
 *        
 */

#ifndef BUTTON_PANEL_H
#define BUTTON_PANEL_H

#include "Arduino.h"
#include "defines.h"

class ButtonPanel
{
  public:
    const static int NUM_EVENTS = 2;
    enum BUTTON_EVENTS { ON_DOWN = 0, ON_UP };
    
    const static int NUM_BUTTON_ROWS = 4;
    const static int NUM_BUTTON_COLS = 4;
    const static int NUM_PINS = 8;

    // PINS
    // input pins rows
    const static int PIN_8 = 30;  // R1
    const static int PIN_7 = 32;  // R2
    const static int PIN_6 = 34;  // R3
    const static int PIN_5 = 36;  // R4
    // output pins columns
    const static int PIN_4 = 39;  // C1
    const static int PIN_3 = 41;  // C2
    const static int PIN_2 = 43;  // C3
    const static int PIN_1 = 45;  // C4

    // Rows and columns defined in terms of order of pins from 0-8
    const static int C1 = 3;
    const static int C2 = 2;
    const static int C3 = 1;
    const static int C4 = 0;
    const static int R1 = 7;
    const static int R2 = 6;
    const static int R3 = 5;
    const static int R4 = 4;

    // BUTTONS
    const static char BUTTON_1 = '1';
    const static char BUTTON_2 = '2';
    const static char BUTTON_3 = '3';
    const static char BUTTON_4 = '4';
    const static char BUTTON_5 = '5';
    const static char BUTTON_6 = '6';
    const static char BUTTON_7 = '7';
    const static char BUTTON_8 = '8';
    const static char BUTTON_9 = '9';
    const static char BUTTON_0 = '0';
    const static char BUTTON_A = 'A';
    const static char BUTTON_B = 'B';
    const static char BUTTON_C = 'C';
    const static char BUTTON_D = 'D';
    const static char BUTTON_STAR = '*';
    const static char BUTTON_HASH = '#';
    const static char NULL_CHAR = '\0';

    const static int PIN_ARR[NUM_PINS];
    const static int ROW_PINS[NUM_BUTTON_ROWS];
    const static int COL_PINS[NUM_BUTTON_COLS];
    const static char BUTTON_ARRAY[NUM_BUTTON_ROWS][NUM_BUTTON_COLS];
    
    ButtonPanel();
    ~ButtonPanel();

    void loop();
    const bool* const * getButtonArray();
    bool addEventListener( const int, void ( *listenerFucn )( const char ) );
    bool removeEventListener( const int );
    
  private:
    bool** _buttonStatusMatrix;
    bool* _pinStatusArray;
    void ( *listenerFuncArray[NUM_EVENTS] )( const char );

    void _initPinMode();
    void _initButtons();
    void _allocButtonMatrix();
    void _resetButtonMatrix();
    void _updateButtonStatus();
    void _allocPinStatusArray();
    void _initPinStatusArray();
    #ifdef PRINT_DEBUG
    void _updatePinStatus();
    #endif
    void _deallocPinStatusArray();
    void _deallocButtonStatusMatrix();
    void _initListenerArray();
    char _checkButtonPress();
    void _onButtonDown();
    void _onButtonUp();
    void _printPinStatusArray();
    void _printButtonStatusMatrix();
};

#endif
