#include "../inc/GpioManager.h"
namespace gpio
{
	GpioManager::GpioManager() noexcept
	{
		#ifndef NDEBUG
		std::cout << "GpioManager::GpioManager()" << std::endl;
		#endif
	}

	GpioManager::~GpioManager()
	{
		#ifndef NDEBUG
		std::cout << "GpioManager::~GpioManager()" << std::endl;
		#endif
	}

	std::shared_ptr<GpioPin> GpioManager::reservePinByGpio(GpioNumber gpioNumber)
	{
		return std::shared_ptr<GpioPin>(new GpioPin(gpioNumber),
				GpioPin::Cleanup()
				);
	}
}
