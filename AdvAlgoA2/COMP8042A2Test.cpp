#include <iostream>
#include <vector>
#include <string>
#include <math.h>
#include "FSM_Helper.h"
#include "Expression.h"
#include "CircularQueue.h"
#include "CircularQueue.cpp"
#include "BinarySearchTree.h"
#include "BinarySearchTree.cpp"

using std::cout;
using std::cin;
using std::endl;
using std::vector;

string exprStr;
Expression expr;

void treeTestCase2();
void testBalancing();
void testBalancing_RR();
void testBalancing_LL();
void testBalancing_RL();
void testBalancing_LR();
void testRecursiveBalancing();

int main()
{
	cout << "***** Q1: Postfix expression" << endl;
	cout << "Input the expression as a one string ending with = e.g. 123 32 + = " << endl;
	bool validationResult;
	
	double evaluationResult = 0;
	for (int k=1; k<=4; k++)
	{
		cout << "Enter a postfix expression: " << endl;
		cin >> expr;
		cout << "evaluating postfix expression.." << endl;
		evaluationResult = expr.evaluate(ExpressionType::POSTFIX_EXPR);
		cout << "Result = " << evaluationResult << endl;
	}
	
	cout << "Enter a non-valid infix expression:" << endl;
	cin >> expr;
	cout << "validating the expression: " << expr << endl;
	validationResult = expr.isValid(ExpressionType::INFIX_EXPR);
	cout << "Result = " << validationResult << endl;
	
	cout << "Enter a non-valid postfix expression:" << endl;
	cin >> expr;
	cout << "validating the expression: " << expr << endl;
	validationResult = expr.isValid(ExpressionType::POSTFIX_EXPR);
	cout << "Result = " << validationResult << endl;
	
	cout << "Enter a non-valid prefix expression:" << endl;
	cin >> expr;
	cout << "validating the expression: " << expr << endl;
	validationResult = expr.isValid(ExpressionType::PREFIX_EXPR);
	cout << "Result = " << validationResult << endl;
	
	cout << endl << endl << "***** Q2: Queue using circular array" << endl;
	CircularQueue<int> Q(5);
	cout << "Empty Q: " << Q << endl;
	Q.enque(1);
	Q.enque(2);
	Q.enque(3);
	cout << "Full Q1: " << Q << endl;
	cout << Q.deque() << " " << Q.deque() << " " << Q.deque() << " " << endl;
	cout << Q.deque() << endl;
	Q.enque(10);
	Q.enque(20);
	Q.enque(30);
	Q.enque(40);
	Q.enque(50);
	cout << "Full Q2: " << Q << endl;
	cout << Q.deque() << endl;
	Q.enque(100);
	cout << "Full Q3: " << Q << endl;
	
	cout << "testing queue size incease" << endl;
	Q.enque(110);
	cout << "Full Q4: " << Q << endl;
	
	cout << Q.deque() << endl;
	cout << Q.deque() << endl;
	cout << Q.deque() << endl;
	cout << Q.deque() << endl;
	
	
	cout << endl << endl << "***** Q3: Binary Search Tree" << endl;
	BinarySearchTree<int> bst;
	
	cout << "Should be empty: " << bst.isEmpty() << endl;
	cout << "Tree (should be empty): ";
	bst.printTree();
	cout << endl;
	
	bst.insert(78);
	bst.insert(12);
	bst.insert(56);
	bst.insert(34);
	bst.insert(40);
	bst.remove(56);
	bst.insert(56);

	cout << "Tree (should have 5 elements): ";
	bst.printTree();
	cout << endl;
	
	cout << "Should not be empty: " << bst.isEmpty() << endl;
	int minVal = bst.findMin();
	int maxVal = bst.findMax();
	bool containsSuccess = bst.contains(12);
	bool containsFail = bst.contains(999);
	cout << "Min (12) = " << minVal << "; Max (78) = " << maxVal 
		<< "; Contains 12 (true): " << containsSuccess 
		<< "; Contains 999 (false): " << containsFail << endl;

	BinarySearchTree<int> bst2(bst);
	cout << "Tree2 (should have 5 elements): ";
	bst2.printTree();
	cout << endl;
	
	BinarySearchTree<int> bst3 = bst2;
	cout << "Tree3 (should have 5 elements): ";
	bst3.printTree();
	cout << endl;

	bst.makeEmpty();
	cout << "Tree (should be empty): ";
	bst.printTree();
	cout << endl;

	treeTestCase2();
	testBalancing_RR();
	testBalancing_LL();
	testBalancing_RL();
	testBalancing_LR();
	testRecursiveBalancing();
	return 0;
}

void treeTestCase2()
{
	cout << "Building tree [5, 10, 0, 1, -1, 9, 11]..." << endl;
	BinarySearchTree<int> tree;
	tree.insert(5);
	tree.insert(10);
	tree.insert(0);
	tree.insert(1);
	tree.insert(-1);
	tree.insert(9);
	tree.insert(11);

	cout << "Removing elements [1, 5, 10, 20, 11] from tree..." << endl;
	tree.remove(1);
	tree.remove(5);
	tree.remove(10);
	tree.remove(20);
	tree.remove(11);
	cout << "Printing tree with deleted nodes..." << endl;
	tree.printTree(cout, true);
	cout << endl;
	
	cout << "Testing removal of marked nodes..." << endl;	
	tree.removeLazyMarkedNodes();
	cout << "Printing tree after removal of marked nodes..." << endl;
	tree.printTree(cout, true);
}

void testBalancing_RR()
{
	
	cout << "Testing RR imbalance..................." << endl;
	BinarySearchTree<int> tree;
	
	tree.insert(10);
	tree.insert(12);
	tree.insert(13);
	
	cout << "Printing tree..." << endl;
	tree.printLevelOrder();
	cout << endl;
	
	cout << "Testing removal of marked nodes..." << endl;	
	tree.balance();
	cout << "Printing tree after balance..." << endl;
	tree.printLevelOrder();
}

void testBalancing_LL()
{
	cout << "Testing LL imbalance..................." << endl;
	BinarySearchTree<int> tree;
	
	tree.insert(10);
	tree.insert(5);
	tree.insert(2);
	
	cout << "Printing tree..." << endl;
	tree.printLevelOrder();
	cout << endl;
	
	cout << "Testing removal of marked nodes..." << endl;	
	tree.balance();
	cout << "Printing tree after balance..." << endl;
	tree.printLevelOrder();
}

void testBalancing_RL()
{
	cout << "Testing RL imbalance..................." << endl;
	BinarySearchTree<int> tree;
	tree.insert(10);
	tree.insert(15);
	tree.insert(12);
	
	cout << "Printing tree..." << endl;
	tree.printLevelOrder();
	cout << endl;
	
	cout << "Testing removal of marked nodes..." << endl;	
	tree.balance();
	cout << "Printing tree after balance..." << endl;
	tree.printLevelOrder();
}


void testBalancing_LR()
{
	cout << "Testing LR imbalance..................." << endl;
	BinarySearchTree<int> tree;
	tree.insert(10);
	tree.insert(5);
	tree.insert(7);
	
	cout << "Printing tree..." << endl;
	tree.printLevelOrder();
	cout << endl;
	
	cout << "Testing removal of marked nodes..." << endl;	
	tree.balance();
	cout << "Printing tree after balance..." << endl;
	tree.printLevelOrder();
}

void testRecursiveBalancing()
{
	cout << "Testing recrsive imbalance..................." << endl;
	BinarySearchTree<int> tree;
	
	tree.insert(20);
	tree.insert(10);
	tree.insert(30);
	tree.insert(24);
	tree.insert(35);
	tree.insert(40);
	
	cout << "Printing tree..." << endl;
	tree.printLevelOrder();
	cout << endl;
	
	cout << "Testing removal of marked nodes..." << endl;	
	tree.balance();
	cout << "Printing tree after balance..." << endl;
	tree.printLevelOrder();
}
