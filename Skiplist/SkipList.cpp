#include "SkipList.h"

template <typename T>
const string SkipList<T>::DEFAULT_DELIM = " \n";

template <typename T>
SkipList<T>::SkipList()
{
	cout << "SkipList<T>::SkipList()" << endl;
	heads = nullptr;
	_initHeadArray();
	delimiter = DEFAULT_DELIM;
	numComparisons = 0;
	numTraversals = 0;
	_setSeedForRandomInt();
}

template <typename T>
SkipList<T>::SkipList( const SkipList& otherList )
{
	heads = nullptr;
	_copyList( otherList );
	numComparisons = otherList.numComparisons;
	numTraversals = otherList.numTraversals;
	delimiter = otherList.delimiter;
}

template <typename T>
SkipList<T>::~SkipList()
{
	cout << "SkipList<T>::~SkipList()" << endl;
	_deleteSkipList();
	_resetRandomGeneratorToDefault();
}

template <typename T>
void SkipList<T>::insert( const T data )
{
	// iterate the list starting from top to bottom
	int curLvl = MAX_LEVEL - 1;
	Node<T>* listItr;	// list with a dummy node
	// data will be inserted at previous node, and we need to track the previous lists, so we need array of prev nodes
	Node<T>** prevNode = ( Node<T>** ) malloc( sizeof( Node<T>* ) * MAX_LEVEL );
	// initialize previous node array to nullptr
	int ii = 0;
	while ( ii < MAX_LEVEL )
	{
		prevNode[ii] = heads[ii];
		ii++;
	}
	
	while ( curLvl >= MAIN_LIST )	// iterate all lists
	{
		listItr = heads[curLvl]->getNext();	
		
		while ( listItr != nullptr )	// iterate all nodes of a list
		{
			numTraversals++;
			// if current node has data element smaller than data, then keep going to next
			// until we hit a node which has equal or greater value than the data to be insterted
			if ( listItr->getData() < data )
			{
				//~ cout << "moving sideways" << endl;
				prevNode[curLvl] = listItr;		// move sideways in list
				listItr = listItr->getNext();
				
				// we have reached end of the list, if there is any pevious node, move downwards
				if ( listItr == nullptr && prevNode[curLvl] != heads[curLvl]  )
				{
					if ( prevNode[curLvl]->getDown() != nullptr )
					{
						//~ cout << "moving downwards 1, level: " << curLvl << endl;
						listItr = prevNode[curLvl]->getDown();
						curLvl--;
					}
				}
			}
			else 	// we have found the node with greater value in one of the lists
			{
				// if there is link to bottom list follow that, if  not just break out
				if ( prevNode[curLvl] != heads[curLvl] && prevNode[curLvl]->getDown() != nullptr )
				{
					//~ cout << "moving downwards 2, level: " << curLvl << endl;
					listItr = prevNode[curLvl]->getDown();
					curLvl--;
				}
				else
				{
					// if we have reached here, we have encountered end of the list
					//~ goto insertNode;
					break;
				}
			}
		}
		
		numTraversals++;
		curLvl--;
	}
	//~ insertNode:
	// fix current level from previous loop
	if ( curLvl < MAIN_LIST )
	{
		curLvl = MAIN_LIST;
	}
	
	// insert in the main list
	Node<T>* aNode = new Node<T>( data, prevNode[curLvl]->getNext(), nullptr, nullptr );
	prevNode[curLvl]->setNext( aNode );
	curLvl++;
	
	while( curLvl < MAX_LEVEL && _coinFlip() == TAILS )
	{
		Node<T>* anotherNode = new Node<T>( data, prevNode[curLvl]->getNext(), nullptr, aNode );
		aNode->setUp( anotherNode );
		prevNode[curLvl]->setNext( anotherNode );
		curLvl++;
		numTraversals++;
	}
	
	delete[] prevNode;
}

// change /............................................
template <typename T>
bool SkipList<T>::found( const T data )
{
	numComparisons = 0;
	
	// iterate the list starting from top to bottom
	int curLvl = MAX_LEVEL - 1;
	Node<T>* listItr = heads[curLvl]->getNext();	// list with a dummy node
	// data will be inserted at previous node, and we need to track the previous lists, so we need array of prev nodes
	Node<T>** prevNode = ( Node<T>** ) malloc( sizeof( Node<T>* ) * MAX_LEVEL );
	// initialize previous node array to nullptr
	int ii = 0;
	while ( ii < MAX_LEVEL )
	{
		prevNode[ii] = heads[ii];
		ii++;
	}
	
	while ( curLvl >= MAIN_LIST )	// iterate all lists
	{
		listItr = heads[curLvl]->getNext();
		while ( listItr != nullptr )	// iterate all nodes of a list
		{
			numComparisons++;
			// if current node has data, it's a success
			if ( listItr->getData() == data )
			{
				delete[] prevNode;
				return true;
			}
			
			// if current node has data element smaller than data, then keep going to next
			// until we hit a node which has equal or greater value than the data to be insterted
			if ( listItr->getData() < data )
			{
				//~ cout << "moving sideways " << listItr->getData() << endl;
				prevNode[curLvl] = listItr;		// move sideways in list
				listItr = listItr->getNext();
				
				// we have reached end of the list, if there is any pevious node, move downwards
				if ( listItr == nullptr && prevNode[curLvl] != heads[curLvl] )
				{
					if ( prevNode[curLvl]->getDown() != nullptr )
					{
						//~ cout << listItr->getData()  << "moving downwards 1 from level: " << curLvl << endl;
						listItr = prevNode[curLvl]->getDown();
						curLvl--;
					}
				}
			}
			else 	// we have found the node with greater value in one of the lists
			{
				// if there is a link to bottom list, go that way 
				// else no more downward route return false
				if ( prevNode[curLvl] != heads[curLvl] && prevNode[curLvl]->getDown() != nullptr )
				{
					//~ cout << listItr->getData()  << "moving downwards 2 from level: " << curLvl << endl;
					listItr = prevNode[curLvl]->getDown();
					curLvl--;
				}
				else
				{
					delete[] prevNode;
					return false;
				}
			}
		}
		
		//~ cout << "moving downwards 3 from level: " << curLvl << endl;
		numComparisons++;
		curLvl--;
	}
	
	// if we are here, then data is not found
	delete[] prevNode;
	return false;
}

template <typename T>
void SkipList<T>::printAllInOrder() const
{
	_print( MAIN_LIST, delimiter );
}

template <typename T>
void SkipList<T>::setOutputDelimiter( string delimiter )
{
	this->delimiter = delimiter;
}

template <typename T>
int SkipList<T>::numberOfElements() const
{
	int len = 0;
	Node<T>* listItr = heads[MAIN_LIST]->getNext();
	while ( listItr != nullptr )
	{
		len++;
		listItr = listItr->getNext();
	}
	
	return len;
}

template <typename T>
void SkipList<T>::reset()
{
	_resetStats();
	_resetSkipList();
}

template <typename T>
bool SkipList<T>::isEmpty() const
{
	return heads[MAIN_LIST]->getNext() == nullptr;
}

template <typename T>
int SkipList<T>::getTraversals() const
{
	return numTraversals;
}

template <typename T>
int SkipList<T>::getComparisons() const
{
	return numComparisons;
}

template <typename T>
SkipList<T>& SkipList<T>::operator= ( const SkipList<T>& otherList )
{
	if ( this != &otherList )
	{
		_copyList( otherList );
		numComparisons = otherList.numComparisons;
		numTraversals = otherList.numTraversals;
		delimiter = otherList.delimiter;
	}
	
	return *this;
}

template <typename T>
void SkipList<T>::_deleteSkipList()
{
	int ii = 0;
	while ( ii < MAX_LEVEL )
	{
		_deleteList( heads[ii] );
		heads[ii] = nullptr;
		ii++;
	}
}

template <typename T>
void SkipList<T>::_resetSkipList()
{
	int ii = 0;
	while ( ii < MAX_LEVEL )
	{
		_deleteList( heads[ii]->getNext() );
		heads[ii]->setNext( nullptr );
		ii++;
	}
}

template <typename T>
void SkipList<T>::_deleteList( Node<T>* node )
{
	if ( node == nullptr )
	{
		return;
	}
	
	_deleteList( node->getNext() );
	delete node;
}

template <typename T>
void SkipList<T>::_resetStats()
{
	numTraversals = 0;
	numComparisons = 0;
}

template <typename T>
void SkipList<T>::_copyList( const SkipList<T>& otherList )
{
	// it starts from the bottom list first node and builds bottom up connections first, and saves those to prev node array
	// prev node array is used to store last element of each list, so in next iteration, they can used to connect to next nodes
	// then when one link from bottom to top is done, it moves to next of main bottom list and repeat until all elements are done
	if ( heads != nullptr )
	{
		_deleteSkipList();
		heads = nullptr;
	}
	_initHeadArray();
	
	// we need a reference to prev nodes from bottom to top
	Node<T>** prevNodeList = ( Node<T>** ) malloc( sizeof( Node<T>* ) * MAX_LEVEL );
	int ii = MAIN_LIST;
	while ( ii < MAX_LEVEL )
	{
		prevNodeList[ii] = nullptr;
		ii++;
	}
	
	Node<T>* oListItr_L = otherList.heads[MAIN_LIST]->getNext();	// list has a dummy node
	Node<T>* oListItr_U;
	Node<T>* cListItr_L = heads[MAIN_LIST];
	int nodecount = 0;
	while ( oListItr_L != nullptr )
	{
		nodecount++;
		ii = MAIN_LIST;
		cListItr_L->setNext( new Node<T>( oListItr_L->getData() ) );
		cListItr_L = cListItr_L->getNext();
		prevNodeList[ii++] = cListItr_L;
		oListItr_U = oListItr_L->getUp();
		
		while( oListItr_U != nullptr )	// it will only get in this loop if up node exists 
		{
			nodecount++;
			Node<T>* node = new Node<T>( oListItr_U->getData(), nullptr, nullptr, cListItr_L );
			cListItr_L->setUp( node );
			if ( prevNodeList[ii] != nullptr )
			{
				prevNodeList[ii]->setNext( node );
			}
			else
			{
				// this list is empty, point its head
				heads[ii]->setNext( node );
			}
			
			cListItr_L = cListItr_L->getUp();	// move current list ptr to up
			prevNodeList[ii++] = cListItr_L;	// set new node to prev for next iteration
			oListItr_U = oListItr_U->getUp();	// move other list ptr to up
		}
		// once this while loop is done, we will have one tower ready and previousNodeList is pointing to this tower
		
		cListItr_L = prevNodeList[MAIN_LIST];
		oListItr_L = oListItr_L->getNext();	// move to next node in the bottom list of other
	}
	cout << "copied " << nodecount++ << " nodes" << endl;
	delete[] prevNodeList;
	delimiter = otherList.delimiter;
}

template <typename T>
void SkipList<T>::_print( int listNum, string delimiter ) const
{
	if ( heads[listNum]->getNext() == nullptr )
	{
		cout << "[  ]" << endl;
		return;
	}
	
	Node<T>* listItr = heads[listNum]->getNext();
	while ( listItr != nullptr )
	{
		cout << listItr->getData() << delimiter;
		listItr = listItr->getNext();
	}
	cout << endl;
}

template <typename T>
void SkipList<T>::dumpTopList()
{
	_print( MAX_LEVEL - 1, delimiter );
}

template <typename T>
void SkipList<T>::_initHeadArray()
{
	if ( heads == nullptr )
	{
		heads = ( Node<T>** ) malloc( sizeof( Node<T>* ) * MAX_LEVEL );
		int ii = 0;
		while( ii < MAX_LEVEL )
		{
			heads[ii] = new Node<T>( T() );	// dummy node
			ii++;
		}
	}
}

template <typename T>
int SkipList<T>::_coinFlip()
{
	return rand() % NUM_COIN_SIDES;
}

template <typename T>
void SkipList<T>::_setSeedForRandomInt()
{
	srand( time( NULL ) );
}

template <typename T>
void SkipList<T>::_resetRandomGeneratorToDefault()
{
	unsigned int defaultSeed = 1;
	srand( defaultSeed );
}
