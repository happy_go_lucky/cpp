#ifndef GIS_DATA_H
#define GIS_DATA_H

#include <string>
#include <iostream>
#include <iomanip>
#include "DDCoord.h"
#include "DMSCoord.h"
#include "Date.h"

using namespace std;

struct GISData
{
	static const char PRINT_WIDTH = 18;
	static const int INVALID_NUMERIC = -1;
	static const constexpr char* INVALID_STR = "";
	
	enum DataOrder
	{
		FEATURE_ID = 0,
		FEATURE_NAME,
		FEATURE_CLASS,
		STATE_ALPHA,
		STATE_NUMERIC,
		COUNTY_NAME,
		COUNTY_NUMERIC,
		PRIM_LAT_DMS,
		PRIM_LONG_DMS,
		PRIM_LAT_DEC,
		PRIM_LONG_DEC,
		SOURCE_LAT_DMS,
		SOURCE_LONG_DMS,
		SOURCE_LAT_DEC,
		SOURCE_LONG_DEC,
		ELEV_IN_M,
		ELEV_IN_FT,
		MAP_NAME,
		DATE_CREATED,
		DATE_EDITED
	};
	
	unsigned int feature_id;
	string feature_name;
	string feature_class;
	string state_alpha;
	short state_numeric;
	string county_name;
	short county_numeric;
	DMSCoord primary_lat_dms;
	DMSCoord primary_long_dms;
	DDCoord primary_lat_dec;
	DDCoord primary_long_dec;
	DMSCoord source_lat_dms;
	DMSCoord source_long_dms;
	DDCoord source_lat_dec;
	DDCoord source_long_dec;
	unsigned int elev_in_m;
	unsigned int elev_in_ft;
	string map_name;
	Date date_created;
	Date date_edited;
	
	friend ostream& operator <<(ostream& out, const GISData& data);
};

#endif
