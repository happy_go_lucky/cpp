#include <iostream>
#include <cstdlib>
#include <ctime>

#include "../include/BetterVector.h"
#include "../src/BetterVector.cpp"

void fillVector(BetterVector<int>& vec);
void printStats(const BetterVector<int>& vec);

int main()
{
	BetterVector<int> vec(size_t(20));
	fillVector(vec);
	
	cout << "original vector:" << endl;
	printStats(vec);
	
	BetterVector<int> vec2 = vec;
	
	cout << "testing copy constructor" << endl;
	assert(vec == vec2);
	
	BetterVector<int> vec3;
	cout << "testing assignment" << endl;
	vec3 = vec;
	assert(vec == vec3);
	
	cout << "testing equality" << endl;
	assert(vec == vec2);
	
	cout << "testing [] operator" << endl;
	vec2[2] = 10;
	assert(vec2[2] == 10);
	
	cout << "testing inequality" << endl;
	assert(vec != vec2);
	
	cout << "testing invariance" << endl;
	vec.checkValidity();
	
	cout << "testing intializer_list" << endl;
	BetterVector<int> vec4({1, 2, 3, 4});
	printStats(vec4);
	
	cout << "testing move assignment" << endl;
	BetterVector<int> vec5 = move(vec4);
	cout << "source vector" << endl;
	printStats(vec4);
	cout << "destination vector" << endl;
	printStats(vec5);
	
	cout << "testing move constructor" << endl;
	BetterVector<int> vec6(move(vec3));
	cout << "source vector" << endl;
	printStats(vec3);
	cout << "destination vector" << endl;
	printStats(vec6);
	
	cout << "testing out of bound exception" << endl;
	try
	{
		cout << vec5[vec5.size()] << endl;
	}
	catch(const exception& e)
	{
		cout << e.what() << endl;
	}
	
	cout << "testing clear" << endl;
	vec6.clear();
	assert(vec6.empty());
	
	cout << "testing erase of one element" << endl;
	cout << "original vector" << endl;
	printStats(vec5);
	vec5.erase(vec5.begin() + 1);
	cout << "vector after removing 2nd element" << endl;
	printStats(vec5);
	
	cout << "testing erase of range of elements" << endl;
	cout << "original vector" << endl;
	printStats(vec2);
	vec2.erase(vec2.begin() + 1, vec2.begin() + 4);
	cout << "vector after removing 2nd element to 5th element" << endl;
	printStats(vec2);
	
	cout << "testing insert lval" << endl;
	int val = 11;
	vec2.insertAt(3, val);
	cout << "vector after inserting at 3rd index a value 11" << endl;
	printStats(vec2);
	
	cout << "testing insert rval" << endl;
	vec2.insertAt(4, 111);
	cout << "vector after inserting at 4rd index a value 111" << endl;
	printStats(vec2);
	
	cout << "testing push_back lval" << endl;
	val = 11;
	vec2.pushBack(val);
	cout << "vector after push_back 11" << endl;
	printStats(vec2);
	
	cout << "testing push_back rval" << endl;
	vec2.pushBack(111);
	cout << "vector after push_back 111" << endl;
	printStats(vec2);
	
	cout << "testing pop_back" << endl;
	vec2.popBack();
	cout << "vector after pop_back" << endl;
	printStats(vec2);
	
	cout << "testing resize" << endl;
	vec2.resize(10);
	cout << "vector after resize to size 10" << endl;
	printStats(vec2);
	
	cout << "testing shrink" << endl;
	vec2.shrink();
	cout << "vector after shrink" << endl;
	printStats(vec2);
	
	cout << "testing front element: " << vec2.front() << endl;
	cout << "testing back element: " << vec2.back() << endl;
	
	return EXIT_SUCCESS;
}

void fillVector(BetterVector<int>& vec)
{
	srand(0);
	
	for (size_t ii = 0; ii < vec.size(); ii++)
	{
		vec[ii] = rand();
	}
}

void printStats(const BetterVector<int>& vec)
{
	cout << "vector size: " << vec.size() << endl;
	cout << "vector capacity: " << vec.capacity() << endl;
	cout << vec;
}
