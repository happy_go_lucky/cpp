/**
 * This class represent a thread pool task.
 * This a wrapper around the std::function so that it appears more
 * meaniingful.
 * 
 * */

#ifndef THREAD_POOL_TASK_H
#define THREAD_POOL_TASK_H

#include <iostream>
#include <functional>

namespace posixthreads
{
	class ThreadPoolTask
	{
		public:
			ThreadPoolTask();
			~ThreadPoolTask();
			
			void setTask(std::function<void()> task) noexcept;
			void isValid() const noexcept;
			void execute();
			void getResult();
		
		private:
			std::function<void()> _threadFunction;
			
	};
}

#endif
