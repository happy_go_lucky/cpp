#include "../inc/ThreadPool.h"

ThreadPool::ThreadPool(posixthreads::ThreadPoolSizeType sizeType, unsigned short size) noexcept
{
	_updateNumThreads(sizeType, size);
}

ThreadPool::~ThreadPool()
{
	
}

unsigned int ThreadPool::capacity() const noexcept
{
	return _numTotalThreads;
}

unsigned int ThreadPool::size() const noexcept
{
	return _numActiveThreads;
}

void ThreadPool::_updateNumThreads(posixthreads::ThreadPoolSizeType sizeType, unsigned short size) noexcept
{
	if (sizeType == posixthreads::ThreadPoolSizeType::SYSTEM_BALANCED_SIZE)
	{
		_numTotalThreads = _getDefaultPoolSize();
	}
	else
	{
		_numTotalThreads = size;
	}
}

int ThreadPool::_getDefaultPoolSize() const noexcept
{
	return get_nprocs() + POOL_SIZE_ALLOWED_OVERLOAD;
}

void ThreadPool::addTask(ThreadTask* task)
{
	_taskUpdateLock->lock();
	_tasksPendingQueue.enqueue(task);
	_taskUpdateLock0->unlock();
}

void ThreadPool::monitorTaskQueue()
{
	
}
