#include "FSM_Transition.h"

FSM_Transition::FSM_Transition() noexcept
{
	_targetStateType = ExprStates::STATE_NONE;
	_exitConditionFunc = nullptr;
}

FSM_Transition::FSM_Transition(ExprStates targetStateType, Base_TC* conditionFunc) noexcept
{
	_targetStateType = targetStateType;
	_exitConditionFunc = conditionFunc;
}

FSM_Transition::~FSM_Transition()
{
	delete _exitConditionFunc;
}

void FSM_Transition::setTargetStateType(ExprStates stateType)
{
	_targetStateType = stateType;
}

void FSM_Transition::setTransitionFunction(Base_TC* func)
{
	_exitConditionFunc = func;
}

ExprStates FSM_Transition::getTargetStateType() const
{
	return _targetStateType;
}

bool FSM_Transition::execute(ExprElement* inputElement, const GlobalVariables::StateActionParams& actionParams) const
{
	return (*_exitConditionFunc)(inputElement, actionParams);
}


void FSM_Transition::debugPrint()
{
	#ifndef NDEBUG
	cout << "target state type: " << static_cast<int>(_targetStateType) << endl;
	#endif
}
