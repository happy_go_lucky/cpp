#ifndef RECT_AREA_IMPL_ARGS_H
#define RECT_AREA_IMPL_ARGS_H

#include "Rectangle.h"

struct RectAreaImplArgs
{
	int numRects;
	int numThreads;
	int threadNumber;
	double deltaX;
	double* totalArea;
	Rectangle* rectArray;
};

#endif
