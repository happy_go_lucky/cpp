#ifndef PR_QUAD_NODE_H
#define PR_QUAD_NODE_H

#include <iostream>
#include <vector>
#include "QuadTreeRecord.h"
#include "PRQuadData.h"

using std::cout;
using std::endl;
using std::ostream;

/**
 * each node has either 4 children or none. i.e. is a leaf node.
 * */

class PRQuadNode
{
	public:
		// define friendship
		friend class QuadTree;
	
		PRQuadNode() noexcept;
		PRQuadNode(const PRQuadNode& node);
		PRQuadNode(PRQuadNode&& node) noexcept;
		PRQuadNode(const QuadTreeRecord& record);
		PRQuadNode(const PRQuadData& record);
		~PRQuadNode();
		
		PRQuadNode& operator =(const PRQuadNode& node);
		PRQuadNode& operator =(PRQuadNode&& node) noexcept;
		
		friend ostream& operator <<(ostream& out, const PRQuadNode& node) noexcept;
		
		inline bool isLeaf() const noexcept
		{
			return _nextNW == nullptr && _nextNE == nullptr &&
					_nextSW == nullptr && _nextSE == nullptr;
		}
		
		bool isEmpty() const noexcept;
		bool isSubDivided() const noexcept;
		
		// if the node is subdivided, it has chilldren
		// else it has data we are looking for
		bool hasSubDivided_NW() const noexcept;
		bool hasSubDivided_NE() const noexcept;
		bool hasSubDivided_SW() const noexcept;
		bool hasSubDivided_SE() const noexcept;
		
		bool isEmpty_NW() const noexcept;
		bool isEmpty_NE() const noexcept;
		bool isEmpty_SW() const noexcept;
		bool isEmpty_SE() const noexcept;
		
		void setDataNW(const PRQuadData& data);
		void setDataNE(const PRQuadData& data);
		void setDataSW(const PRQuadData& data);
		void setDataSE(const PRQuadData& data);
		
		void setDataNW(const QuadTreeRecord& data);
		void setDataNE(const QuadTreeRecord& data);
		void setDataSW(const QuadTreeRecord& data);
		void setDataSE(const QuadTreeRecord& data);
		
		void updateOffsetNW(const QuadTreeRecord& data) noexcept;
		void updateOffsetNE(const QuadTreeRecord& data) noexcept;
		void updateOffsetSW(const QuadTreeRecord& data) noexcept;
		void updateOffsetSE(const QuadTreeRecord& data) noexcept;
		
		void updateOffsetNW(const PRQuadData& data) noexcept;
		void updateOffsetNE(const PRQuadData& data) noexcept;
		void updateOffsetSW(const PRQuadData& data) noexcept;
		void updateOffsetSE(const PRQuadData& data) noexcept;
		
		void setLinkNW(PRQuadNode* node) noexcept;
		void setLinkNE(PRQuadNode* node) noexcept;
		void setLinkSW(PRQuadNode* node) noexcept;
		void setLinkSE(PRQuadNode* node) noexcept;
		
		void breakLinkNE() noexcept;
		void breakLinkNW() noexcept;
		void breakLinkSE() noexcept;
		void breakLinkSW() noexcept;
		
		PRQuadNode* getNextNE() const noexcept;
		PRQuadNode* getNextNW() const noexcept;
		PRQuadNode* getNextSE() const noexcept;
		PRQuadNode* getNextSW() const noexcept;
		
		PRQuadData getDataNW() const noexcept;
		PRQuadData getDataNE() const noexcept;
		PRQuadData getDataSW() const noexcept;
		PRQuadData getDataSE() const noexcept;
		
	private:
		// the child nodes are based on the quadrants
		PRQuadNode* _nextNW;	// North West
		PRQuadNode* _nextNE;	// North East
		PRQuadNode* _nextSW;	// South West
		PRQuadNode* _nextSE;	// South East
		
		PRQuadData _dataNW;
		PRQuadData _dataNE;
		PRQuadData _dataSW;
		PRQuadData _dataSE;
};

#endif
