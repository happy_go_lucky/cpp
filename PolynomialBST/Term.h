#ifndef TERM_H
#define TERM_H

/**
 * a univariate polynomial term
 * 	______________
 * 	|   coeff    |
 * 	|____________|
 * 	|  degree x  |
 * 	|____________|
 * 
 * */

#include "PolyException.h"
#include <iostream>

using std::ostream;
using std::cout;
using std::endl;

class Term
{
	public:
		Term( int = 0, int = 0 );
		Term( const Term& );
		~Term();
		
		int getCoeff() const;
		int getExponent_x() const;
		
		void setCoeff( int );
		void setExponent_x( int );
		
		// adds polynomial two nodes(terms) of same degree
		// throws a PolyException exception if trying to add different degree terms 
		friend Term operator + ( const Term&, const Term& );
		const Term& operator = ( const Term& );
		friend bool operator < ( const Term&, const Term& );
		friend bool operator > ( const Term&, const Term& );
		friend bool operator == ( const Term&, const Term& );
		friend ostream& operator << ( ostream&, const Term& );
		
	private:
		int _coeff;
		int _exponent_x;
};

#endif
