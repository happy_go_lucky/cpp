#include "TimerHelper.h"

void initTimer( struct itimerval* intervalTimer, const int timerType )
{
	//it_value : will show time remaining when timer is running
	// the value will be 0 when disabled
	getitimer( timerType, intervalTimer );
}

// make sure timers are initialized before this
void armTimer( struct itimerval* intervalTimer, const int timerType, time_t timerLimit_seconds, suseconds_t timerLimit_microseconds, time_t timerInterval_seconds, suseconds_t timerInterval_microseconds )
{
	// timerLimit_seconds defines the timer run time which is decremented
	// when reaches 0, if the Interval is defined, it is reset to interval
	// when SIGALRM signal is received, the timer should have been reset to new value. 
	// And time reamining (it_value.tv_usec) will be > 0 if interval is set to be > 0
	( intervalTimer->it_interval ).tv_sec = timerInterval_seconds;
	( intervalTimer->it_interval ).tv_usec = timerInterval_microseconds;
	( intervalTimer->it_value ).tv_sec = timerLimit_seconds;
	( intervalTimer->it_value ).tv_usec = timerLimit_microseconds;
	
	setitimer( timerType, intervalTimer, NULL );
}

void disarmTimer( struct itimerval* intervalTimer, const int timerType )
{
	( intervalTimer->it_value ).tv_sec = 0;
	( intervalTimer->it_value ).tv_usec = 0;
	
	setitimer( timerType, intervalTimer, NULL );
}

void print_itimerval_struct( struct itimerval* aTimer )
{
	printf( "interval seconds: %lu\n", ( aTimer->it_interval ).tv_sec );
	printf( "interval microseconds: %lu\n", ( aTimer->it_interval ).tv_usec );
	printf( "time period seconds: %lu\n", ( aTimer->it_value ).tv_sec );
	printf( "time period microseconds: %lu\n", ( aTimer->it_value ).tv_usec );
}
