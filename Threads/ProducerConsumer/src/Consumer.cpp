#include "../inc/Consumer.h"

Consumer::Consumer()
{
	
}

Consumer::~Consumer()
{
	
}

void Consumer::init(SharedMemory* shMem)
{
	std::cout << "Consumer with pid: " << getpid() 
			<< " reading from shared memory" << std::endl;
	std::cout << *(int*)(shMem->getPointerToMemory()) << std::endl;
}

