#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <windows.h>

#include "ParseProgramList.h"

void launchProgram( const char* programPath );

int main( int argCount, char** argArray )
{
	if ( argCount != 2 )
	{
		fprintf( stderr, "please supply a file name to open the file from command line\n" );
		return EXIT_FAILURE;
	}
	
	int fileReadPosition = 0;
	char* lineRead;
	while ( true )
	{
		lineRead = readNextLine( argArray[1], '\n', &fileReadPosition );
		launchProgram( lineRead );
		if ( lineRead == NULL ) break;
		free( lineRead );
	}
	
	return EXIT_SUCCESS;
}

void launchProgram( const char* programPath )
{
	STARTUPINFO si;
	PROCESS_INFORMATION pi;
	ZeroMemory( &si, sizeof( si ) );
	si.cb = sizeof( si );
	ZeroMemory( &pi, sizeof( pi ) );
	
	if ( CreateProcess( NULL, programPath, NULL, NULL, false, CREATE_NEW_CONSOLE, NULL, NULL, &si, &pi ) )
	{
		printf("successfully created process\n");
	}
	else
	{
		printf("unable to create process\n");
		return;
	}
	
	// wait until child process exits
	WaitForSingleObject( pi.hProcess, INFINITE );
	
	// close process and thread handles
	CloseHandle( pi.hProcess );
	CloseHandle( pi.hThread );
}