#include "MatrixTraversor.h"

MatrixTraversor::MatrixTraversor( TraversorType type, unsigned numMatrixRows, unsigned numMatrixCols )
{
	_traversorType = type;
	_matrixRows = numMatrixRows;
	_matrixColumns = numMatrixCols;
	_rowTraversor = MIN_INDEX;
	_columnTraversor = MIN_INDEX;
}

MatrixTraversor::~MatrixTraversor()
{
	
}

unsigned int MatrixTraversor::getRowPosition() const
{
	return _rowTraversor;
}

unsigned int MatrixTraversor::getColumnPosition() const
{
	return _columnTraversor;
}

void MatrixTraversor::resetTraversor( unsigned int value )
{
	_rowTraversor = ( _traversorType == ROW && value > INVALID_INDEX && value <= _matrixRows ) ? value : MIN_INDEX;
	_columnTraversor = ( _traversorType == COLUMN && value > INVALID_INDEX && value <= _matrixColumns ) ? value : MIN_INDEX;
}

void MatrixTraversor::traverse()
{
	if ( _traversorType == ROW )	// traverses elements in each row, row by row
	{
		if ( _columnTraversor < _matrixColumns )
		{
			_columnTraversor++;
		}
		else
		{
			_columnTraversor = MIN_INDEX;
			_rowTraversor = _rowTraversor < _matrixRows ? _rowTraversor + 1 : MIN_INDEX;
		}
	}
	else	// traverses elements in each column, column by column
	{
		if ( _rowTraversor < _matrixRows )
		{
			_rowTraversor++;
		}
		else
		{
			_rowTraversor = MIN_INDEX;
			_columnTraversor = _columnTraversor < _matrixColumns ? _columnTraversor + 1 : MIN_INDEX;
		}
	}
}

MatrixTraversor::TraversorType MatrixTraversor::getType() const
{
	return _traversorType;
}

void MatrixTraversor::setType( const TraversorType type )
{
	_traversorType = type;
	resetTraversor();
}