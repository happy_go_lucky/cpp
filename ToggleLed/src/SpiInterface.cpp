#include "../inc/SpiInterface.h"

namespace gpio
{
	SpiInterface::SpiInterface(unsigned int bus, unsigned int device)
	{
		_deviceFilename.reserve(SPI_PATH_LENGTH);
		_deviceFilename.append(SPI_PATH);
		_deviceFilename.append(bus - '0');
		_deviceFilename.append(".");
		_deviceFilename.append(device - '0');
		
		_currentMode = SpiMode::MODE_3;
		_speed = 500'000;
		_delay = 0;
		_bitsPerWord = 8;
		_deviceFD = -1;
	}
	
	SpiInterface::~SpiInterface()
	{
		close();
	}
	
	void SpiInterface::init()
	{
		
	}
	
	int SpiInterface::open()
	{
		#ifndef NDEBUG
		std::cout << "opening file for SPI" << std::endl;
		#endif
		if ((_deviceFD = open(_deviceFilename.c_str(), O_RDWR)) < 0)
		{
			std::cerr << "SPI can't open device" << std::endl;
			return EXIT_FAILURE;
		}
		
		if (setMode(_currentMode) == EXIT_FAILURE)
			return EXIT_FAILURE;
		if (setSpeed(_speed) == EXIT_FAILURE)
			return EXIT_FAILURE;
		if (setBitsPerWord(_bitsPerWord) == EXIT_FAILURE)
			return EXIT_FAILURE;
		return EXIT_SUCCESS;
	}
	
	uByte SpiInterface::readRegister(unsigned int registerAddress)
	{
		uByte send[2], receive[2];
		memset(send, 0, sizeof(send));
		memset(receive, 0, sizeof(receive));
		send[0] = 0x80 | registerAddress;
		transfer(send, receive, 2);
		
		return receive[1];
	}
	
	uByte SpiInterface::readRegisters(unsigned int fromRegisterAddress, unsigned int numRegisters)
	{
		uByte* data = new uByte[numRegisters];
		uByte send[numRegisters + 1], receive[numRegisters + 1];
		memset(send, 0, sizeof(send));
		send[0] = 0x80 | 0x40 | fromRegisterAddress;
		transfer(send, receive, numRegisters + 1);
		memcpy(data, receive + 1, number);
		return data;
	}
	
	int SpiInterface::writeRegister(unsigned int registerAddress, uByte value)
	{
		uByte send[2], receive[2];
		memset(receive, 0, sizeof(receive));
		send[0] = registerAddress;
		send[1] = value;
		transfer(send, receive, 2);
		
		return EXIT_SUCCESS;
	}
	
	int SpiInterface::write(uByte value)
	{
		uByte null_return = 0x00;
		transfer(&value, &null_return, 1);
		
		return EXIT_SUCCESS;
	}
	
	int SpiInterface::write(uByte value, int length)
	{
		uByte null_return = 0x00;
		transfer(&value, &null_return, length);
		
		return EXIT_SUCCESS;
	}
	
	int SpiInterface::setSpeed(uDoubleWord speed)
	{
		_speed = speed;
		return ioctl(_deviceFD, SPI_IOC_WR_MAX_SPEED_HZ, &speed);
	}
	
	int SpiInterface::getCurrentSpeed() const
	{
		return ioctl(_deviceFD, SPI_IOC_RD_MAX_SPEED_HZ, &speed);
	}
	
	int SpiInterface::setMode(SpiMode mode)
	{
		_currentMode = mode;
		
		if (ioctl(_deviceFD, SPI_IOC_WR_MODE, &_currentMode) == EXIT_FAILURE)
		{
			return EXIT_FAILURE;
		}
		if (ioctl(_deviceFD, SPI_IOC_RD_MODE, &_currentMode) == EXIT_FAILURE)	
		{
			return EXIT_FAILURE;
		}
		
		return EXIT_SUCCESS;
	}
	
	void SpiInterface::close()
	{
		close(_deviceFD);
		_deviceFD = -1;
	}
	
	int SpiInterface::transfer(uByte dataRead, uByte dataWrtie, int length)
	{
		struct spi_ioc_transfer spiTransfer;
		spiTransfer.tx_buf = send;
		spiTransfer.rx_buf = receive;
		spiTransfer.len = length;
		spiTransfer.speed_hz = _speed;
		spiTransfer.bits_per_word = _bitsPerWord;
		spiTransfer.delay_usec = _delay;
		spiTransfer.pad = 0;
		
		int status = ioctl(_deviceFD, SPI_IOC_MESSAGE(1), &spiTransfer);
		
		if (status < 0)
		{
			std::cerr << "SPI_IOC_MESSAGE failed" << std::endl;
			return EXIT_FAILURE;
		}
		return status;
	}
	
	void SpiInterface::debugDumpRegister(unsigned int numRegisters)
	{
		std::cout << "SPI mode: " << _currentMode << std::endl;
		std::cout << "Bits per word: " << _bitsPerWord << std::endl;
		std::cout << "Max speed: " << _speed << std::endl;
		std::cout << "registers: " << std::endl;
		uByte* registers = readRegisters(numRegisters);
		for (int ii = 0; ii < numRegisters; ii++)
		{
			std::cout << *(registers + ii) << " ";
			if (ii % 16 == 15)
				std::cout << endl;
		}
	}
	
	void SpiInterface::_printHex(int value) const
	{
		std::cout << "0x" << std::hex << value << std::endl;
	}
	
	void SpiInterface::_printBinary(int value) const
	{
		std::bitset<8> bits(value);
		std::cout << bits << std::endl;
	}
}
