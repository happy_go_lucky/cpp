#include "Button.h"

Button::Button( const int type, const int pin )
{
  _pin = pin;
  _buttonId = type;
  _clickEngaged = false;
  _state = BUTTON_UP;

  pinMode( _pin, INPUT );
  digitalWrite( _pin, LOW );

  _initListenerArray();
}

Button::~Button()
{
  _initListenerArray();
}

char Button::getButtonId() const
{
  return _buttonId;
}

void Button::_onButtonHold()
{
  if ( listenerFuncArray[ON_HOLD] != nullptr && ( _state == BUTTON_DOWN ) )
  {
    //_isPressed = true;
    ( *listenerFuncArray[ON_HOLD] )();
  }
}

void Button::_onButtonClick()
{
  if ( listenerFuncArray[ON_CLICK] != nullptr && ( _state == BUTTON_UP ) && ( _clickEngaged == true ) )
  {
    _clickEngaged = false;
    ( *listenerFuncArray[ON_CLICK] )();
  }
}

void Button::_initListenerArray()
{
  for ( int ii = 0; ii < NUM_BUTTON_EVENTS; ii++ )
  {
    listenerFuncArray[ii] = nullptr;
  }
}


bool Button::addEventListener( const int eventType, void (*listenerFunc)() )
{
  #ifdef PRINT_DEBUG
  Serial.println("ButtonPanel::addEventListener");
  #endif
  if ( eventType < ON_HOLD || eventType >= NUM_BUTTON_EVENTS )
  {
    return false;
  }
  else
  {
    listenerFuncArray[eventType] = listenerFunc;
    return true;
  }
}

bool Button::removeEventListener( const int eventType )
{
  #ifdef PRINT_DEBUG
  Serial.println("ButtonPanel::removeEventListener");
  #endif
  if ( eventType < ON_HOLD || eventType >= NUM_BUTTON_EVENTS )
  {
    return false;
  }
  else
  {
    listenerFuncArray[eventType] = nullptr;
    return true;
  }
}

void Button::loop()
{
  _checkState();
  _onButtonHold();
  _onButtonClick();
}

void Button::_checkState()
{
  if ( digitalRead( _pin ) == LOW )
  {
    _state = BUTTON_UP;
  }
  else
  {
    _state = BUTTON_DOWN;
    _clickEngaged = true;
  }
}

