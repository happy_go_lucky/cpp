#ifndef NODE_H
#define NODE_H

/**
 * Node for a double ended list
 * 
 * 	__________
 * 	|  data  |
 * 	|________|
 * 	|  next  |
 * 	|________|
 * 	|  prev  |
 * 	|________|
 * 
 * */

template <class TYPE>
class Node
{
	public:
		Node( TYPE, Node<TYPE>* = nullptr, Node<TYPE>* = nullptr );
		~Node();
		
		TYPE getData() const;
		void setData( TYPE );
		Node<TYPE>* getNext() const;
		Node<TYPE>* getPrev() const;
		void setNext( Node<TYPE>* );
		void setPrev( Node<TYPE>* );
		
	private:
		TYPE data;
		Node<TYPE>* next;
		Node<TYPE>* prev;
};

#endif
