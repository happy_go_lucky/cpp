#ifndef RECTANGLE_AREA_IMPL_H
#define RECTANGLE_AREA_IMPL_H

#include <posixthreads/inc/ThreadDefs.h>
#include <posixthreads/inc/Thread.h>
#include "Rectangle.h"
#include <posixthreads/inc/MutexLock.h>
#include "RectAreaImplArgs.h"

class RectangleArea_Impl : public posixthreads::Thread
{
	public:
		RectangleArea_Impl();
		~RectangleArea_Impl();
		
		void* threadRun() override;
	
	private:
		posixthreads::MutexLock* _mutexLock;
	
};

#endif
