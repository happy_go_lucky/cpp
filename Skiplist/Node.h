#ifndef NODE_H
#define NODE_H

/*
 * 	___________
 * 	|   up    |
 * 	|_________|
 * 	|  next   |
 * 	|_________|
 * 	|  data   |
 * 	|_________|
 * 	|  down   |
 * 	|_________|
 * 
 * */

template <class T>
class Node
{
	public:
		Node( const T&, Node<T>* = nullptr, Node<T>* = nullptr, Node<T>* = nullptr );
		~Node();
		
		void setData( const T& );
		void setNext( Node<T>* );
		void setUp( Node<T>* );
		void setDown( Node<T>* );
		
		T getData() const;
		Node<T>* getNext() const;
		Node<T>* getUp() const;
		Node<T>* getDown() const;
		
	private:
		T data;
		Node<T>* next;
		Node<T>* upLevel;
		Node<T>* downLevel;
};

#endif
