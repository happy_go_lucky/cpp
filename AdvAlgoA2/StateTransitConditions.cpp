#include "StateTransitConditions.h"

bool EvalOperatorInfix_TC::operator ()(ExprElement* elem, 
								const GlobalVariables::StateActionParams& actionParams)
{
	return (isOperator(elem) && 
			getOperatorType(elem) == globalVars.EVAL_OPERATOR) &&
			actionParams.bracketCount == 0 &&
			actionParams.avgOpCount == 0;
}

bool OperandInfix_TC::operator ()(ExprElement* elem, 
							const GlobalVariables::StateActionParams& actionParams)
{
	return areParamsValid(actionParams) && !isOperator(elem);
}

bool OperatorInfix_TC::operator ()(ExprElement* elem, 
							const GlobalVariables::StateActionParams& actionParams)
{
	return areParamsValid(actionParams) && isOperator(elem);
}

bool OperandPrefix_TC::operator ()(ExprElement* elem, 
							const GlobalVariables::StateActionParams& actionParams)
{
	return areParamsValid(actionParams) && !isOperator(elem);
}

bool OperatorPrefix_TC::operator ()(ExprElement* elem, 
							const GlobalVariables::StateActionParams& actionParams)
{
	return (actionParams.operatorCount >= actionParams.operandCount &&
			areParamsValid(actionParams) && isOperator(elem));
}

bool OperandPostfix_TC::operator ()(ExprElement* elem, 
							const GlobalVariables::StateActionParams& actionParams)
{
	return (actionParams.operandCount >= actionParams.operatorCount &&
			areParamsValid(actionParams) && !isOperator(elem));
}

bool OperatorPostfix_TC::operator ()(ExprElement* elem, 
							const GlobalVariables::StateActionParams& actionParams)
{
	return areParamsValid(actionParams) && isOperator(elem);
}

bool BracketOpen_TC::operator ()(ExprElement* elem, 
								const GlobalVariables::StateActionParams& actionParams)
{
	return (areParamsValid(actionParams) &&
			isOperator(elem) && 
			getOperatorType(elem) == globalVars.PARENTHESIS_OPEN_OPERATOR);
}

bool BracketClose_TC::operator ()(ExprElement* elem, 
								const GlobalVariables::StateActionParams& actionParams)
{
	return (areParamsValid(actionParams) &&
			actionParams.bracketCount >= 0 &&
			isOperator(elem) && 
			getOperatorType(elem) == globalVars.PARENTHESIS_CLOSE_OPERATOR);
}

bool EvalOperatorPnP_TC::operator ()(ExprElement* elem, 
									const GlobalVariables::StateActionParams& actionParams)
{
	return (areParamsValid(actionParams) &&
			isOperator(elem) && 
			getOperatorType(elem) == globalVars.EVAL_OPERATOR) &&
			actionParams.operandCount - actionParams.operatorCount == 1;
}

bool AvgEndInfix_TC::operator ()(ExprElement* elem, 
							const GlobalVariables::StateActionParams& actionParams)
{
	return (areParamsValid(actionParams) &&
			!isOperator(elem) &&
			actionParams.avgOpCount > 0);
}

bool AvgBeginInfix_TC::operator ()(ExprElement* elem, 
							const GlobalVariables::StateActionParams& actionParams)
{
	return (areParamsValid(actionParams) &&
			isOperator(elem) && 
			getOperatorType(elem) == globalVars.AVG_OPERATOR);
}


bool BracketCloseToOpen_TC::operator ()(ExprElement* elem, 
							const GlobalVariables::StateActionParams& actionParams)
{
	return (areParamsValid(actionParams) &&
			isOperator(elem) && 
			actionParams.avgOpCount > 0 &&
			getOperatorType(elem) == globalVars.PARENTHESIS_CLOSE_OPERATOR);
}

bool BracketCloseToOperand_TC::operator ()(ExprElement* elem, 
							const GlobalVariables::StateActionParams& actionParams)
{
	return (areParamsValid(actionParams) &&
			isOperator(elem) && 
			actionParams.avgOpCount > 0 &&
			getOperatorType(elem) == globalVars.PARENTHESIS_CLOSE_OPERATOR);
}

bool AvgEndLoop_TC::operator ()(ExprElement* elem, 
							const GlobalVariables::StateActionParams& actionParams)
{
	return (areParamsValid(actionParams) &&
			!isOperator(elem) && 
			actionParams.avgOpCount > 0 &&
			actionParams.operandCount >= actionParams.operatorCount);
}

bool ToAvgPostfix_TC::operator ()(ExprElement* elem, 
							const GlobalVariables::StateActionParams& actionParams)
{
	return (areParamsValid(actionParams) &&
			isOperator(elem) && 
			getOperatorType(elem) == globalVars.AVG_OPERATOR &&
			actionParams.operandCount >= 3 &&
			actionParams.operandCount >= actionParams.operatorCount);
}

bool ToAvgPrefix_TC::operator ()(ExprElement* elem, 
							const GlobalVariables::StateActionParams& actionParams)
{
	return (areParamsValid(actionParams) &&
			isOperator(elem) && 
			getOperatorType(elem) == globalVars.AVG_OPERATOR);
}
