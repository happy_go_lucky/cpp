
#include <iostream>
#include <cstdlib>
#include <string>
#include <fstream>
#include "HashTable.h"

using namespace std;

const int HASH_TABLE_SIZE = 500;
const char* FILE_TO_READ = "words.txt";
const char* FILE_TO_CHECK = "gettysburg_address.txt";
const char* STORAGE_FILE_500 = "results_500.txt";
const char* STORAGE_FILE_1000 = "results_1000.txt";

int hashIndexCollisions = 0;
int totalWordCount = 0;
int misspelledWordCount = 0;
int correctWordCount = 0;
int numOps = 0;
int numOpsMisspelled = 0;
int numOpsCorrect = 0;

void readInDictionaryFile( HashTable& );
void readInAndSpellCheckFile( HashTable& );
void writeCollisionDataToFile( HashTable&, const char*, const int );
void readInAndSpellCheckFile( HashTable&, const char* );
void writeSpellingData( ofstream& );

int main() 
{
	// test 1
	HashTable hashMap = HashTable( HASH_TABLE_SIZE * 2 );
	readInDictionaryFile( hashMap );
	writeCollisionDataToFile( hashMap, STORAGE_FILE_1000, HASH_TABLE_SIZE * 2 );
	readInAndSpellCheckFile( hashMap, STORAGE_FILE_1000 );
	
	// test 2
	HashTable hashMap2 = HashTable( HASH_TABLE_SIZE );
	readInDictionaryFile( hashMap2 );
	writeCollisionDataToFile( hashMap2, STORAGE_FILE_500, HASH_TABLE_SIZE );
	readInAndSpellCheckFile( hashMap, STORAGE_FILE_500 );
	
	system( "pause" );
	return EXIT_SUCCESS;
}

void readInDictionaryFile( HashTable& hash )
{
	ifstream fileIn;
	fileIn.open( FILE_TO_READ );
	
	if ( !fileIn || fileIn.fail() )
	{
		cout << "error opening file " << FILE_TO_READ << endl;
		exit( EXIT_FAILURE );
	}
	
	string input;
	
	while ( !fileIn.eof() )
	{
		fileIn >> input;
		
		hash.addValueToTable( input );
	}
	
	fileIn.close();
}

void writeCollisionDataToFile( HashTable& hash, const char* filename, const int hashSize )
{
	ofstream fileOut;
	fileOut.open( filename );
	
	if ( !fileOut || fileOut.fail() )
	{
		cout << "error opening file " << filename << endl;
		exit( EXIT_FAILURE );
	}
	
	int count = 0;
	int* collision = hash.getCollisionsArray();
	fileOut << "collisions: \n";
	for ( int ii = 0; ii < hashSize; ii++ )
	{
		count++;
		fileOut << "cell " << ii << ": "<< *( collision + ii );
		
		if (count % 20 == 0)
		{
			fileOut << "\n";
		}
		else
		{
			fileOut << "\t";
		}
	}
	
	fileOut.close();
}

void readInAndSpellCheckFile( HashTable& hash, const char* fileToWrite )
{
	ifstream fileIn;
	fileIn.open( FILE_TO_CHECK );
	
	if ( !fileIn || fileIn.fail() )
	{
		cout << "error opening file " << FILE_TO_CHECK << endl;
		exit( EXIT_FAILURE );
	}
	
	ofstream fileOut;
	fileOut.open( fileToWrite, ios::app );
	
	if ( !fileOut || fileOut.fail() )
	{
		cout << "error opening file " << fileToWrite << endl;
		exit( EXIT_FAILURE );
	}
	
	string input;
	
	while ( !fileIn.eof() )
	{
		fileIn >> input;
		totalWordCount++;
		numOps = 0;
		if ( hash.constainsValue( input, numOps ) )
		{
			correctWordCount++;
			numOpsCorrect += numOps;
		}
		else
		{
			misspelledWordCount++;
			numOpsMisspelled += numOps;
		}
		
		fileOut << "\n num ops for word \""<< input << "\" is: " << numOps << "\n";
	}
	
	writeSpellingData( fileOut );
	
	fileIn.close();
	fileOut.close();
}

void writeSpellingData( ofstream& fileOut )
{
	fileOut << "\n spelling data: \n";
	fileOut << "\n number of words read: " << totalWordCount << "\n";
	fileOut << "\n number of correct words: " << correctWordCount << "\n";
	fileOut << "\n number of misspelled words: " << misspelledWordCount << "\n";
	fileOut << "\n avg. number of ops for misspelled words: " << numOpsMisspelled / misspelledWordCount << "\n";
	fileOut << "\n avg. number of ops for correct words: " << numOpsCorrect / correctWordCount << "\n";
}
