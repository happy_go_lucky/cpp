#include "../inc/RectAreaNonMutex_Impl.h"

RectAreaNonMutex_Impl::RectAreaNonMutex_Impl() : 
		posixthreads::Thread(posixthreads::DetachType::COOPERATIVE)
{
	
}

RectAreaNonMutex_Impl::~RectAreaNonMutex_Impl()
{
	
}

void* RectAreaNonMutex_Impl::threadRun()
{
	RectAreaImplArgs threadArgs = *((RectAreaImplArgs*) _threadArgs);
	
	double mid = 0.0;
	double area = 0.0;
	
	for (int ii = threadArgs.threadNumber; ii < threadArgs.numRects; ii += threadArgs.numThreads)
	{
		threadArgs.rectArray[ii].setWidth(threadArgs.deltaX); 
		mid = (ii + 0.5) * threadArgs.rectArray[ii].getWidth();
		threadArgs.rectArray[ii].setHeight(4.0 / (1.0 + mid * mid));
		area += threadArgs.rectArray[ii].getArea();
	}
	
	*((double*) _returnVal) = area;
	
	return EXIT_SUCCESS;
}

