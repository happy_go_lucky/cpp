#include <iostream>
#include <cstdlib>
#include <ctime>
#include <assert.h>
#include <utility>

#include "../include/CircularLinkedList.h"
#include "../src/CircularLinkedList.cpp"

int main()
{
	std::cout << "testing list of size 5" << std::endl;
	CircularLinkedList<int> list(5);
	assert(!list.isEmpty());
	std::cout << list;
	
	std::cout << "testing lval copy cons" << std::endl;
	CircularLinkedList<int> list2(list);
	std::cout << list2;
	
	std::cout << "testing lval equality" << std::endl;
	assert(list == list2);
	
	std::cout << "testing rval copy cons" << std::endl;
	CircularLinkedList<int> list3(std::move(list2));
	std::cout << "new list" << std::endl;
	std::cout << list3;
	std::cout << "empty list" << std::endl;
	std::cout << list2;
	
	std::cout << "testing rval equality" << std::endl;
	assert(list == std::move(list3));
	
	std::cout << "testing default cons" << std::endl;
	CircularLinkedList<int> list4;
	std::cout << list4;
	assert(list4.isEmpty());
	
	std::cout << "testing push_front: 3" << std::endl;
	list4.push_front(3);
	std::cout << list4;
	assert(!list4.isEmpty());
	assert(list4.front() == 3);
	
	std::cout << "testing push_back: 5" << std::endl;
	list4.push_back(5);
	std::cout << list4;
	assert(list4.front() == 3);
	assert(list4.back() == 5);
	
	std::cout << "testing pop_front" << std::endl;
	list4.pop_front();
	std::cout << list4;
	assert(list4.front() == 5);
	assert(list4.back() == 5);
	
	std::cout << "testing pop_back" << std::endl;
	list4.pop_back();
	std::cout << list4;
	assert(list4.isEmpty());
	
	std::cout << "testing initializer list" << std::endl;
	list4 = {1, 2, 3, 5};
	std::cout << list4;
	assert(!list4.isEmpty());
	assert(list4.front() == 1);
	assert(list4.back() == 5);
	
	std::cout << "testing reverse list" << std::endl;
	list4.reverseList();
	std::cout << list4;
	assert(list4.front() == 5);
	assert(list4.back() == 1);
	
	std::cout << "testing inserting after 2" << std::endl;
	list4.insertAfter(list4.begin()->getNextNode()->getNextNode(), 4);
	std::cout << list4;
	assert(list4.front() == 5);
	assert(list4.back() == 1);
	
	std::cout << "testing search for value 4" << std::endl;
	assert(list4.search(4));
	
	std::cout << "testing search node for value 3" << std::endl;
	assert(list4.searchNode(3)->getData() == 3);
	
	std::cout << "testing remove node for value 3" << std::endl;
	list4.remove(3);
	std::cout << list4;
	
	std::cout << "testing remove node by address. remove 3rd node" << std::endl;
	list4.remove(list4.begin()->getNextNode()->getNextNode());
	std::cout << list4;
	
	std::cout << "testing clear" << std::endl;
	list4.clear();
	std::cout << list4;
	assert(list4.isEmpty());
	
	return EXIT_SUCCESS;
}
