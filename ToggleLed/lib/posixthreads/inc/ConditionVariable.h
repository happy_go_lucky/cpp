#ifndef CONDITION_VARIABLE_H
#define CONDITION_VARIABLE_H

#include <pthread.h>
#include <ctime>
#include <string.h>
#include <functional>
#include "MutexLock.h"
#include "HelperFxns.h"
#include "ThreadException.h"
#include "ThreadDefs.h"

using namespace std;

namespace posixthreads
{
	class ConditionVariable
	{
		public:
			ConditionVariable(MutexLock& mutex) noexcept;
			ConditionVariable(const ConditionVariable& cv) = delete;
			ConditionVariable(ConditionVariable&& cv) = delete;
			~ConditionVariable();
			
			ConditionVariable& operator =(const ConditionVariable& cv) = delete;
			ConditionVariable& operator =(ConditionVariable&& cv) = delete;
			
			void initialize(ProcessAccessType accessType);
			void cleanup();
			
			/**
			 * unblocks one of the waiting threads on the condition variable
			 * the conditionUpdate function is used to set the value
			 * once the value is updated, the signal is issued
			 * */
			template <typename Type>
			void signal(std::function<void (Type)> condUpdate, 
					Type value)
			{
				#ifndef NDEBUG
				std::cout << "ConditionVariable::signal" << std::endl;
				#endif
				
				int state = EXIT_FAILURE;
				condUpdate(value);
				state = pthread_cond_signal(_cv);
				
				#ifndef NDEBUG
				if (state != EXIT_SUCCESS)
				{
					std::cout << strerror(state) << std::endl;
					throw ThreadException((char*) "unable to signal the thread waiting on condition variable");
				}
				#endif
			}
			
			/**
			 * ----------------------------important note-----------------------
			 * being a shared library class, the templated code has to be added to
			 * the header file, otherwise linker won't be able to see it.
			 * */
			
			/**
			 * if more than one threads are being blocked on the condition variable
			 * this will unblock all of them
			 * */
			template <typename Type>
			void signalAll(std::function<void (Type)> condUpdate, 
					Type value)
			{
				#ifndef NDEBUG
				std::cout << "ConditionVariable::signalAll" << std::endl;
				#endif
				
				int state = EXIT_FAILURE;
				condUpdate(value);
				state = pthread_cond_broadcast(_cv);
				#ifndef NDEBUG
				if (state != EXIT_SUCCESS)
				{
					std::cout << strerror(state) << std::endl;
					throw ThreadException((char*) "unable broadcast the threads waiting on condition variable");
				}
				#endif
			}
			
			/**
			 * The calling thread will wait on the condition, and blocked.
			 * It will also unlock the mutex variable specified.
			 * The specified mutex variable has to be locked by the caller thread before waiting
			 * Once the condition is met, the mutex is returned to the caller thread.
			 * 
			 * variable Type has to have == and != operators defined
			 * */
			template <typename Type>
			void waitForSignal(std::function<bool (Type)> condPredicate, 
					Type expectedValue)
			{
				#ifndef NDEBUG
				std::cout << "ConditionVariable::waitForSignal" << std::endl;
				#endif
		
				// wait call expects the mutex to be locked before it can wait
				int state = EXIT_FAILURE;
				while (!condPredicate(expectedValue))
				{
					std::cout << "waiting for condition. Putting thread to sleep" << std::endl;
					// the calling thread is put to sleep and the mutex lock is released
					// once the calling thread is woken up, the mutex is locked again
					state = pthread_cond_wait(_cv, _mutexLock._mutex);
				}
				// mutex is supposed to be unlocked once the condition is valid and wait is over
				std::cout << "condition met. waking up thread" << std::endl;

				#ifndef NDEBUG
				if (state != EXIT_SUCCESS)
				{
					std::cout << strerror(state) << std::endl;
					throw ThreadException((char*) "unable to wait on a condition variable");
				}
				#endif
			}
			
			/**
			 * block the calling thread for a given time and release the mutex
			 * also stops the wait if signal or broadcast is recieved
			 * */
			template <typename Type>
			void timedWait(int waitTimeInSeconds, 
					std::function<bool (Type)> condPredicate, Type expectedValue)
			{
				#ifndef NDEBUG
				std::cout << "ConditionVariable::timedWait" << std::endl;
				#endif
				
				struct timespec time;
				int state = EXIT_FAILURE;
				
				helperfxns::getCurrentTimeSpec(time);
				// also look into the following if above doesn't work
				// clock_gettime(CLOCK_MONOTONIC, &time);
				time.tv_sec += waitTimeInSeconds;
				
				while (!condPredicate(expectedValue))
				{
					state = pthread_cond_timedwait(_cv, _mutexLock._mutex, &time);
				}
				
				#ifndef NDEBUG
				if (state != EXIT_SUCCESS)
				{
					std::cout << strerror(state) << std::endl;
					throw ThreadException((char*) "unable to set timed wait on condition variable");
				}
				#endif
			}
			
			ProcessAccessType getAccessType() const noexcept;
			// TODO: look into pthread_condattr_setclock if needed for timed wait
		
		private:
			pthread_cond_t* _cv;
			pthread_condattr_t* _cvAttrib;
			// mutex is external because it is used by the caller thread to
			// set and unset the condition that will be used for the while check 
			MutexLock& _mutexLock;
			
			void _setAccessType(ProcessAccessType accessType);
	};
}
#endif
