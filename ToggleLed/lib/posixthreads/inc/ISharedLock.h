#ifndef I_SHARED_LOCK
#define I_SHARED_LOCK

namespace posixthreads
{
	class ISharedLock
	{
		public:
			ISharedLock() = default;
			~ISharedLock() = default;
			
			ISharedLock(const ISharedLock& lock) = delete;
			ISharedLock(ISharedLock&& lock) = delete;
			
			ISharedLock& operator =(const ISharedLock& lock) = delete;
			ISharedLock& operator =(ISharedLock&& lock) = delete;
		
		protected:
			virtual void initResources() = 0;
			virtual void lock() = 0;
			virtual void unlock() = 0;
			virtual void cleanupResources() = 0;
	};
}
#endif
