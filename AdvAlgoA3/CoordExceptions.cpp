#include "CoordExceptions.h"

CoordCompException::CoordCompException( char* message )
{
	_message = message;
}

const char* CoordCompException::what() const throw()
{
	return _message;
}
