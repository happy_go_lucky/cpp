#include "QuadNode.h"

QuadNode::QuadNode() noexcept
{
	_nodeType = UNINITIALISED;
}

QuadNode::QuadNode(NodeType type) noexcept
{
	_nodeType = type;
}

QuadNode::~QuadNode()
{
	
}

bool QuadNode::isData() const noexcept
{
	return _nodeType == DATA_NODE;
}

bool QuadNode::isSubDivided() const noexcept
{
	return _nodeType == SUBDIVIDED_NODE;
}

QuadNode::NodeType QuadNode::getType() const noexcept
{
	return _nodeType;
}
