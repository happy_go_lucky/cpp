#include <iostream>
#include <memory>
#include <cassert>

class Test
{
	public:
		Test() noexcept
		{
			std::cout << "Test constructor" << std::endl;
			_privacy = new int(8);
			_privacy_sp = std::make_shared<int>(10);
		}
		
		Test(const Test& test) noexcept
		{
			std::cout << "Test copy constructor" << std::endl;
			_privacy = new int(*(test._privacy));
			_privacy_sp = std::make_shared<int>(*(test._privacy_sp));
		}
		
		Test(Test&& test) noexcept
		{
			std::cout << "Test move constructor" << std::endl;
			_privacy = std::move(test._privacy);
			_privacy_sp = std::move(test._privacy_sp);
		}
		
		Test& operator =(const Test& test) noexcept
		{
			std::cout << "Test copy assignment" << std::endl;
			if (this != &test)
			{
				_privacy = new int(*(test._privacy));
				_privacy_sp = std::make_shared<int>(*(test._privacy_sp));
			}
			return *this;
		}
		
		Test& operator =(Test&& test) noexcept
		{
			std::cout << "Test move assignment" << std::endl;
			*_privacy = std::move(*(test._privacy));
			_privacy_sp = std::move(test._privacy_sp);
			
			test._privacy = nullptr;
			
			return *this;
		}
		
		~Test()
		{
			std::cout << "~Test()" << std::endl;
			delete _privacy;
		}
		
		void print() noexcept
		{
			std::cout << "test print" << std::endl;
		}
		
		int* privacyKiller() noexcept
		{
			return _privacy;
		}
		
		std::weak_ptr<int> getAccess_wp() noexcept
		{
			return _privacy_sp;
		}
		
		std::shared_ptr<int> getAccess_sp() noexcept
		{
			return _privacy_sp;
		}
		
	private:
		int* _privacy;
		std::shared_ptr<int> _privacy_sp;
};

void testSharing(std::shared_ptr<Test> sp)
{
	std::cout << "testing shared" << std::endl;
	std::cout << "use count: " << sp.use_count() << std::endl;
	sp->print();
	
	std::cout << "testing privacy.." << std::endl;
	std::cout << "raw ptr: " << sp->privacyKiller() << std::endl;
	
	std::shared_ptr<int> testptr = (sp->getAccess_wp()).lock();
	
	std::cout << "weak ptr: " << *testptr << std::endl;
	std::cout << "shared ptr: " << sp->getAccess_sp() << std::endl;
}

void testWeak(std::weak_ptr<Test> wp)
{
	std::cout << "testing weak" << std::endl;
	std::cout << "use count: " << wp.use_count() << std::endl;
	std::shared_ptr<Test> sp = wp.lock();
	sp->print();
	
	std::cout << "testing privacy.." << std::endl;
	std::cout << "raw ptr: " << sp->privacyKiller() << std::endl;
	
	std::shared_ptr<int> testptr = (sp->getAccess_wp()).lock();
	
	std::cout << "weak ptr: " << *testptr << std::endl;
	std::cout << "shared ptr: " << sp->getAccess_sp() << std::endl;
}

std::unique_ptr<Test> testFactory()
{
	// unique pointers allow moving, and the ownership is transferred to callee
	return std::make_unique<Test>();
}

int main()
{
	std::cout << "Testing raw pointers" << std::endl;
	Test* testRaw = new Test();
	
	std::cout << "Testing copy" << std::endl;
	Test* testRawCopy = new Test();
	*testRawCopy = *testRaw;
	
	std::cout << "Testing move" << std::endl;
	Test* testRawMove = new Test();
	*testRawMove = std::move(*testRaw);
	
	assert(testRaw == nullptr);
	std::cout << std::endl;
	
	std::cout << "Testing smart pointers ptr" << std::endl;

	std::cout << "Creating unique pointer using factory" << std::endl;
	std::unique_ptr<Test> uniquePtr = testFactory();
	uniquePtr->print();
	std::cout << "moving unique pointer to new pointer" << std::endl;
	std::unique_ptr<Test> uniqueMove = std::move(uniquePtr);
	uniqueMove->print();
	
	assert(uniquePtr == nullptr);
	
	std::cout << std::endl;
	
	std::cout << "Creating make shared ptr.." << std::endl;
	std::shared_ptr<Test> autoPtr = std::make_shared<Test>();
	
	testSharing(autoPtr);
	testWeak(autoPtr);
	
	std::cout << std::endl;
	
	std::cout << "creating new shared ptr.." << std::endl;
	std::shared_ptr<Test> manualPtr(new Test());
	
	// sometimes needed, like in the case of friends.
	std::cout << "testing custom constructor and destructor" << std::endl;
	std::shared_ptr<Test> manualSharedPtr(new Test(), 
								[](Test* p) { delete p; } );
	
	testSharing(manualSharedPtr);
	testWeak(manualPtr);

	return 0;
}
