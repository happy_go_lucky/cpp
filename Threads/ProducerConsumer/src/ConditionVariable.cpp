#include "../inc/ConditionVariable.h"

ConditionVariable::ConditionVariable(ProcessAccessType accessType) noexcept
{
	_cv = nullptr;
	_cvAttrib = nullptr;
	_accessType = accessType;
}

ConditionVariable::~ConditionVariable()
{
	if (_cv != nullptr) delete _cv;
	if (_cvAttrib != nullptr) delete _cvAttrib;
}

void ConditionVariable::initialize(MutexLock* mutex)
{
	_cv = new pthread_cond_t();
	_cvAttrib = new pthread_condattr_t();
	
	pthread_condattr_init(_cvAttrib);
	_setAccessType(_accessType);
	_mutexLock = mutex;
	
	int state = pthread_cond_init(_cv, NULL);
	if (state != EXIT_SUCCESS)
	{
		std::cout << strerror(state) << std::endl;
		throw ThreadException((char*) "unable to initialize condition variable");
	}
}

void ConditionVariable::cleanup()
{
	int stateCV = pthread_cond_destroy(_cv);
	int stateAtrrib = pthread_condattr_destroy(_cvAttrib);
	
	if (stateCV != EXIT_SUCCESS || stateAtrrib != EXIT_SUCCESS)
	{
		std::cout << strerror(stateCV) << std::endl;
		std::cout << strerror(stateAtrrib) << std::endl;
		throw ThreadException((char*) "unable to destroy condition variable");
	}
	
	delete _cv;
	delete _cvAttrib;
	_cv = nullptr;
	_cvAttrib = nullptr;
}

/**
 * unblocks one of the waiting threads on the condition variable
 * */
void ConditionVariable::signal()
{
	int state = pthread_cond_signal(_cv);
	if (state != EXIT_SUCCESS)
	{
		std::cout << strerror(state) << std::endl;
		throw ThreadException((char*) "unable to signal the thread waiting on condition variable");
	}
}

/**
 * if more than one threads are being blocked on the condition variable
 * this will unblock all of them
 * */
void ConditionVariable::signalAll()
{
	int state = pthread_cond_broadcast(_cv);
	if (state != EXIT_SUCCESS)
	{
		std::cout << strerror(state) << std::endl;
		throw ThreadException((char*) "unable broadcast the threads waiting on condition variable");
	}
}

/**
 * The calling thread will wait on the condition, and blocked.
 * It will also unlocks the mutex variable specified.
 * The specified mutex variable has to be locked by the caller thread before waiting
 * Once the condition is met, the mutex is returned to the caller thread.
 * */
void ConditionVariable::waitForSignal()
{
	int state = pthread_cond_wait(_cv, _mutexLock->_mutex);
	if (state != EXIT_SUCCESS)
	{
		std::cout << strerror(state) << std::endl;
		throw ThreadException((char*) "unable to wait on a condition variable");
	}
}

/**
 * block the calling thread for a given time and release the mutex
 * also stops the wait if signal or broadcast is recieved
 * */
void ConditionVariable::timedWait(int waitTimeInSeconds)
{
	struct timespec time;
	helperfxns::getCurrentTimeSpec(time);
	// also look into the following if above doesn't work
	// clock_gettime(CLOCK_MONOTONIC, &time);
	time.tv_sec += waitTimeInSeconds;
	int state = pthread_cond_timedwait(_cv, _mutexLock->_mutex, &time);
	if (state != EXIT_SUCCESS)
	{
		std::cout << strerror(state) << std::endl;
		throw ThreadException((char*) "unable to set timed wait on condition variable");
	}
}

/**
 * returns access type
 * */
ConditionVariable::ProcessAccessType ConditionVariable::getAccessType() const noexcept
{
	int accessType = -1;
	int retVal = pthread_condattr_getpshared(_cvAttrib, &accessType);
	
	if (retVal == EXIT_SUCCESS)
	{
		if (accessType == PTHREAD_PROCESS_SHARED)
			return ProcessAccessType::SHARED;
		else
			return ProcessAccessType::PRIVATE;
	}
	else
	{
		return ProcessAccessType::INVALID;
	}
}

void ConditionVariable::_setAccessType(ProcessAccessType accessType)
{
	if (accessType == ProcessAccessType::SHARED)
		pthread_condattr_setpshared(_cvAttrib, PTHREAD_PROCESS_SHARED);
	else
		pthread_condattr_setpshared(_cvAttrib, PTHREAD_PROCESS_PRIVATE);
}
