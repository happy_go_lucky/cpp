#include "SubDividedNode.h"

SubDividedNode::SubDividedNode() noexcept : QuadNode(QuadNode::SUBDIVIDED_NODE)
{
	_nw = nullptr;
	_ne = nullptr;
	_sw = nullptr;
	_se = nullptr;
}

SubDividedNode::SubDividedNode(const SubDividedNode& node) noexcept
{
	_nw = node._nw;
	_ne = node._ne;
	_sw = node._sw;
	_se = node._se;
}

SubDividedNode::~SubDividedNode() noexcept
{
	delete _nw;
	delete _ne;
	delete _sw;
	delete _se;
}

void SubDividedNode::set_ne(QuadNode* node)
{
	_ne = node;
}

void SubDividedNode::set_nw(QuadNode* node)
{
	_nw = node;
}

void SubDividedNode::set_se(QuadNode* node)
{
	_se = node;
}

void SubDividedNode::set_sw(QuadNode* node)
{
	_sw = node;
}

void SubDividedNode::setEmpty() noexcept
{
	_nw = nullptr;
	_ne = nullptr;
	_sw = nullptr;
	_se = nullptr;
}

bool SubDividedNode::has_ne() const noexcept
{
	return _ne != nullptr;
}

bool SubDividedNode::has_nw() const noexcept
{
	return _nw != nullptr;
}

bool SubDividedNode::has_se() const noexcept
{
	return _se != nullptr;
}

bool SubDividedNode::has_sw() const noexcept
{
	return _sw != nullptr;
}

QuadNode* SubDividedNode::get_ne() const noexcept
{
	return _ne;
}

QuadNode* SubDividedNode::get_nw() const noexcept
{
	return _nw;
}

QuadNode* SubDividedNode::get_se() const noexcept
{
	return _se;
}

QuadNode* SubDividedNode::get_sw() const noexcept
{
	return _sw;
}
