#include <iostream>
#include <string>
#include <queue>
#include <strings.h>
#include "../include/Rectangle.h"
#include "../include/Matrix.h"
#include "Matrix.cpp"
#include <include/BetterVector.h>
#include <src/BetterVector.cpp>

#include "../include/Josephus.h"

//#define NDEBUG // disable all asserts

using namespace std;

template <typename Object, typename Comparator>
const Object& findMax(const BetterVector<Object>& arr, Comparator isLessThan);
const Rectangle& findMaxByArea(const BetterVector<Rectangle>& rec_vec);
const Rectangle& findMaxByPerim(const BetterVector<Rectangle>& rec_vec);

bool isLessThanArea(const Rectangle& rect1, const Rectangle& rect2);
bool isLessThanPerim(const Rectangle& rect1, const Rectangle& rect2);

int main()
{
	BetterVector<Rectangle> recVec
	{
		Rectangle(1, 3),
		Rectangle(4, 4),
		Rectangle(4, 2),
		Rectangle(7, 2),
		Rectangle(1, 2),
		Rectangle(2, 2),
		Rectangle(6, 2)
	};
	
	cout << "***** Q1: Rectangle class" << endl;
	
	// this for loop is inefficient in comaprison to classic, hence removed and replaced
/*	for (Rectangle r : recVec)
	{
		cout << r << endl;
	}
*/
	for (size_t ii = 0; ii < recVec.size(); ii++)
	{
		cout << recVec[ii] << endl;
	}
	
	cout << endl;
	
	cout << "Highest Area: " << findMaxByArea(recVec) << endl;
	cout << "Highest Perimeter: " << findMaxByPerim(recVec) << endl << endl;


	cout << endl << endl << "***** Q2: matrix class" << endl;
	Matrix<int> mat = Matrix<int>();
	cout << "Zero-parameter matrix (rows,cols) = (" << mat.numrows() << "," << mat.numcols() << ")" << endl;
	
	mat.resize(4, 3);
	cout << "Resized matrix to 4x3" << endl;
	cout << mat << endl;
	mat[2][1] = 12;
	cout << "Modified (2,1)" << endl;
	cout << mat << endl;


	cout << endl << endl << "***** Q3: Josephus problem" << endl;
	cout << "Elimination order (0, 5): ";
	Josephus josephusProblem;
	
	int winner = josephusProblem.getWinner(0, 5);
	cout << "+++WINNER: " << winner << endl;
	cout << "Elimination order (1, 5): ";
	winner = josephusProblem.getWinner(1, 5);
	cout << "+++WINNER: " << winner << endl;
	cout << "Elimination order (3, 7): ";
	winner = josephusProblem.getWinner(3, 7);
	cout << "+++WINNER: " << winner << endl;

	return EXIT_SUCCESS;
}

const Rectangle& findMaxByArea(const BetterVector<Rectangle>& rec_vec)
{
	return findMax(rec_vec, isLessThanArea);
}

const Rectangle& findMaxByPerim(const BetterVector<Rectangle>& rec_vec)
{
	return findMax(rec_vec, isLessThanPerim);
}

template <typename Object, typename Comparator>
const Object& findMax(const BetterVector<Object>& arr, Comparator isLessThan)
{
	size_t maxIndex = 0;
	for (size_t ii = 1; ii < arr.size(); ++ii)
	{
		if (isLessThan(arr[maxIndex], arr[ii]))
		{
			maxIndex = ii;
		}
	}
	return arr[maxIndex];
}

bool isLessThanArea(const Rectangle& rect1, const Rectangle& rect2)
{
	return rect1.areaCompare(rect2);
}

bool isLessThanPerim(const Rectangle& rect1, const Rectangle& rect2)
{
	return rect1.perimeterCompare(rect2);
}
