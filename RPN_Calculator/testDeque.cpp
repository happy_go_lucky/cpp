#include <iostream>
#include <string>
#include "Deque.h"
#include "Deque.cpp"

using std::cout;
using std::cin;
using std::cerr;
using std::endl;
using std::string;

template <typename TYPE>
void printQueue( Deque<TYPE>& );

int main()
{
	Deque<string> deque;
	
	// insert
	printQueue( deque );
	deque.pushBack( "333" );
	printQueue( deque );
	deque.pushFront( "222" );
	printQueue( deque );
	deque.pushFront( "444" );
	printQueue( deque );
	deque.pushFront( "555" );
	printQueue( deque );
	
	// copy
	cout << "copying list" << endl;
	Deque<string> anotherDeque = deque;
	cout << "copied deque: " << endl;
	printQueue( anotherDeque );
	
	// clear
	cout << "clearing list" << endl;
	deque.clear();
	printQueue( deque );
	
	// delete
	try
	{
		anotherDeque.popBack();
		printQueue( anotherDeque );
		anotherDeque.popFront();
		printQueue( anotherDeque );
		anotherDeque.popBack();
		printQueue( anotherDeque );
		anotherDeque.popFront();
		printQueue( anotherDeque );
		anotherDeque.popFront();
		printQueue( anotherDeque );
	}
	catch( Effedup& excep )
	{
		cout << excep.what() << endl;
	}
	return EXIT_SUCCESS;
}

template <typename TYPE>
void printQueue( Deque<TYPE>& deque )
{
	cout << "is deque empty? " << ( deque.isEmpty() ? "true" : "false" ) << endl;
	#ifdef Q_DEBUG
	deque.print();
	#endif
}
