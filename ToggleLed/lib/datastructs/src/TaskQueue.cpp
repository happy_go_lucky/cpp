#ifndef TASK_QUEUE_CPP
#define TASK_QUEUE_CPP

#include "../inc/TaskQueue.h"
#include "DoublyList.cpp"
#include "DoublyListIterator.cpp"

namespace datastructs
{
	template <typename Type>
	TaskQueue<Type>::TaskQueue() noexcept
	{
		// nothing to do here
	}

	template <typename Type>
	TaskQueue<Type>::~TaskQueue()
	{
		// nothing to do here
	}

	template <typename Type>
	TaskQueue<Type>::TaskQueue(const TaskQueue<Type>& taskQueue) noexcept
	{
		_taskList = taskQueue._taskList;
	}

	template <typename Type>
	TaskQueue<Type>& TaskQueue<Type>::operator =(const TaskQueue<Type>& taskQueue) & noexcept
	{
		if (taskQueue != *this)
		{
			_taskList = taskQueue._taskList;
		}
		
		return *this;
	}

	template <typename Type>
	void TaskQueue<Type>::addTask(const Type& task) noexcept
	{
		_taskList.appendNode(task);
	}
	
	template <typename Type>
	void TaskQueue<Type>::addTask(Type&& task) noexcept
	{
		_taskList.appendNode(task);
	}

	template <typename Type>
	void TaskQueue<Type>::removeTask(const Type& task) noexcept
	{
		DoublyListIterator<Type> itr = _taskList.findNode(task);
		if (itr.isNodeValid())
			_taskList.deleteNode(itr);
	}

	template <typename Type>
	void TaskQueue<Type>::emptyList() noexcept
	{
		_taskList.emptyList();
	}

	template <typename Type>
	int TaskQueue<Type>::size() const noexcept
	{
		return _taskList.size();
	}
	
	template <typename Type>
	Type TaskQueue<Type>::getTask() noexcept
	{
		DoublyListIterator<Type> itr = _taskList.head();
		Type task = *itr;
		return task;
	}
}

#endif
