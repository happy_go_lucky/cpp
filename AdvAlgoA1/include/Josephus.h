#ifndef JOSEPHUS_H
#define JOSEPHUS_H

#include <iostream>
#include <list>
#include <include/CircularLinkedList.h>
#include <src/CircularLinkedList.cpp>

using namespace std;

// The Josephus problem is the following game:
//  N people, numbered 1 to N, are sitting in a circle.
//  Starting at person 1, a hot potato is passed.
//  After M passes, the person holding the hot potato is eliminated,
//	  the circle closes ranks, and the game continues
//	  with the person who was sitting after the eliminated person
//	  picking up the hot potato.
//  The last remaining person wins.
//  Thus, if M = 0 and N = 5, players are eliminated in order, and player 5 wins.
//	  If M = 1 and N = 5, the order of elimination is 2, 4, 1, 5.
// m is number of people
// n is number of passes before elimination

class Josephus
{
	public:
		Josephus() = default;
		~Josephus() = default;
		
		int getWinner(int numPlayers, int numPassesToElimination);
};

#endif
