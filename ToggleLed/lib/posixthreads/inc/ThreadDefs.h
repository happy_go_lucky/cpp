#ifndef THREAD_DEFS_H
#define THREAD_DEFS_H

#include <pthread.h>

namespace posixthreads
{
	enum class DetachType : unsigned int
	{
		COOPERATIVE = 0,
		INDEPENDENT
	};
	
	enum class CancelState : unsigned int
	{
		CANCELLABLE = PTHREAD_CANCEL_ENABLE,
		NON_CANCELLABLE = PTHREAD_CANCEL_DISABLE
	};
	
	enum class CancelType : unsigned int
	{
		DEFFERRED = PTHREAD_CANCEL_DEFERRED,
		IMMEDIATE = PTHREAD_CANCEL_ASYNCHRONOUS
	};
	
	enum class ProcessAccessType : int
	{
		INVALID = -1,
		SHARED,
		PRIVATE
	};
	
	enum class ThreadCondition : unsigned int
	{
		UNKNOWN = 0,
		WAIT,
		RUN
	};
}

#endif
