#ifndef FSM_HELPER_H
#define FSM_HELPER_H

enum class ExpressionType
{
	PREFIX_EXPR = 0,
	INFIX_EXPR,
	POSTFIX_EXPR
};

enum class ElementType
{
	TYPE_OPERATOR = 0,
	TYPE_OPERAND
};

// END state should be the last in the enum
// enum should start at 0
enum class ExprStates
{
	STATE_NONE = -1,	// default/invalid state
	STATE_START,	// 0
	STATE_OPERAND,
	STATE_OPERATOR,
	STATE_BRACKET_OPEN,	// 3
	STATE_BRACKET_CLOSE,
	STATE_AVG_OP_BEGIN,	// 5
	STATE_AVG_OP_END,
	STATE_END
};

#endif
