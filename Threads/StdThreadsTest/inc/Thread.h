#ifndef THREAD_H
#define THREAD_H

#include <iostream>
#include <thread>

using namespace std;

class Thread
{
	public:
		Thread();
		~Thread();
		
		void start();
		void stop();
		void join();
		void detach();
	
	private:
		thread* _pThread;
		bool _isStopped;
		void _run();
};

#endif
