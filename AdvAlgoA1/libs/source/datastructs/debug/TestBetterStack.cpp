#include <iostream>
#include <cstdlib>
#include <ctime>

#include "../include/BetterStack.h"
#include "../src/BetterStack.cpp"

int main()
{
	BetterStack<int> aStack;
	std::cout << aStack;
	assert(aStack.empty());
	BetterStack<int> aStack2(5);
	std::cout << aStack2;
	
	std::cout << "testing equality on empty" << std::endl;
	assert(aStack == aStack2);
	
	std::cout << "testing empty pop" << std::endl;
	std::cout << aStack.pop() << std::endl;
	
	std::cout << "testing empty top" << std::endl;
	std::cout << aStack.top() << std::endl;
	
	std::cout << "testing push rval" << std::endl;
	aStack.push(1);
	std::cout << aStack;
	
	std::cout << "testing push lval" << std::endl;
	int val = 1;
	aStack2.push(val);
	std::cout << aStack2;
	
	std::cout << "testing equality on non-empty" << std::endl;
	assert(aStack == aStack2);
	
	std::cout << "testing copy cons lval" << std::endl;
	BetterStack<int> aStack3(aStack);
	std::cout << aStack3;
	
	std::cout << "testing assignment rval" << std::endl;
	BetterStack<int> aStack4 = BetterStack<int>({1, 2, 3, 4});
	std::cout << aStack4;
	
	std::cout << "testing initializer list" << std::endl;
	BetterStack<int> aStack5({1, 2, 3, 4});
	std::cout << aStack5;
	
	std::cout << "testing pop empty" << std::endl;
	while (!aStack5.empty())
	{
		std::cout << "poped: " << aStack5.pop() << std::endl;
	}
	
	return EXIT_SUCCESS;
}
