#include "../inc/SemaphoreUnnamed.h"

SemaphoreUnnamed::SemaphoreUnnamed(SemaphoreDefs::SharingType sharingType)
{
	_semaphore = nullptr;
	_sharedMemory = nullptr;
	_sharingType = sharingType;
	/*
	_sharingType = sharingType == PROCESS_SHARED ? 
				SharingType::PROCESS : SharingType::THREADS;*/
}

SemaphoreUnnamed::~SemaphoreUnnamed()
{
		
}

// for thread shared
void SemaphoreUnnamed::initThreadSharing(int semaphoreCount)
{	
	if (_sharingType != SemaphoreDefs::SharingType::THREADS)
	{
		std::cerr << "invalid initialization for semaphore" << std::endl;
		return;
	}
	_semaphoreCount = semaphoreCount;
	
	#ifndef NDEBUG
	int ret = sem_init(_semaphore, 0, _semaphoreCount);
	if (ret != EXIT_SUCCESS)
	{
		int err = errno;
		std::cout << strerror(err) << std::endl;
		throw ThreadException((char*) "unable to create semaphore");
	}
	#else
	sem_init(_semaphore, 0, _semaphoreCount);
	#endif
}

// for process shared
void SemaphoreUnnamed::initProcessSharing(int semaphoreCount)
{
	if (_sharingType != SemaphoreDefs::SharingType::PROCESS)
	{
		std::cerr << "invalid initialization for semaphore" << std::endl;
		return;
	}
	_semaphoreCount = semaphoreCount;
	
	/**
	 *  we have to initialize the semaphore in the shared memory, therefore memory
	 * has to be created before sem_init and semaphore has to be initialized within
	 * the shared memory
	 * */
	_sharedMemory = new SharedMemory(SharedMemoryDefs::VisibilityType::ANONYMOUS,
								SharedMemoryDefs::SharingType::SHARED);
	_sharedMemory->initAnonymousMemory();
	_sharedMemory->createSharedMemory(NULL, sizeof(sem_t),
				helperfxns::AccessPermissions::RW, 0, 0);
	//~ _sharedMemory->updateData(sizeof(sem_t), sharedData.storage);
	_semaphore = (sem_t*) _sharedMemory->getPointerToMemory();
	
	#ifndef NDEBUG
	int ret = sem_init(_semaphore, 1, _semaphoreCount);
	if (ret != EXIT_SUCCESS)
	{
		int err = errno;
		std::cerr << strerror(err) << std::endl;
		throw ThreadException((char*) "unable to create semaphore");
	}
	#else
	sem_init(_semaphore, 1, _semaphoreCount);
	#endif
}

void SemaphoreUnnamed::wait()
{
	sem_wait(_semaphore);
}

void SemaphoreUnnamed::post()
{
	sem_post(_semaphore);
}

void SemaphoreUnnamed::cleanup()
{
	#ifndef NDEBUG
	int ret = sem_destroy(_semaphore);
	if (ret == -1)
	{
		int err = errno;
		std::cerr << "Unable to close a unnamed semaphore" << std::endl;
		std::cerr << "error code: " << err << " => " << strerror(err) << std::endl;
	}
	#else
	sem_destroy(_semaphore);
	#endif
	
	if (_sharingType == SemaphoreDefs::SharingType::PROCESS)
	{
		_sharedMemory->cleanupResources();
		delete _sharedMemory;
	}
}
