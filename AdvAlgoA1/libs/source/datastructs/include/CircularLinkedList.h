#ifndef CIRCULAR_LINKED_LIST_H
#define CIRCULAR_LINKED_LIST_H

#include "ListNode.h"
#include "../src/ListNode.cpp"
#include "BetterStack.h"
#include "../src/BetterStack.cpp"

/**
 * This list is using a dummy node.
 * It makes life a lot easier with recursion to have a dummy node.
 * 
 * */

template <class T>
class CircularLinkedList
{
	public:
		CircularLinkedList() noexcept;
		CircularLinkedList(size_t size);
		//explicit CircularLinkedList(double size) = delete; // causes issues
		//explicit CircularLinkedList(float size) = delete; // causes issues
		CircularLinkedList(std::initializer_list<T> list);
		CircularLinkedList(const CircularLinkedList<T>& list);
		CircularLinkedList(CircularLinkedList<T>&& list) noexcept;
		
		~CircularLinkedList();
		
		inline bool isEmpty() const
		{
			return _head->getNextNode() == _head;
		}
		
		inline size_t size() const
		{
			return _size;
		}
		
		inline const T& front() const noexcept
		{
			return _head->getNextNode()->getData();
		}
		
		inline const T& back() const noexcept
		{
			return _tail->getData();
		}
		
		inline ListNode<T>* begin() const noexcept
		{
			return _head->getNextNode();
		}
		
		inline ListNode<T>* end() const noexcept
		{
			return _tail;
		}
		
		void push_front(const T& val);
		void push_front(T&& val);
		void push_back(const T& val);
		void push_back(T&& val);
		void insertAfter(ListNode<T>* targetNode, const T& val);
		void insertAfter(ListNode<T>* targetNode, T&& val);
		void remove(const T& val) noexcept;
		void remove(ListNode<T>* targetNode, ListNode<T>* parentNode = nullptr) noexcept;
		void pop_front() noexcept;
		void pop_back() noexcept;
		bool search(const T& val) const noexcept;
		const ListNode<T>* searchNode(const T& val) const noexcept;
		void reverseList() noexcept;
		void clear();
		void resize(size_t size);	// TODO: implement it if needed
		ListNode<T>* getNextNode(ListNode<T>* node) const noexcept;
		
		template <typename U>
		friend std::ostream& operator << (std::ostream& out, const CircularLinkedList<U>& list) noexcept;
		CircularLinkedList<T>& operator = (const CircularLinkedList<T>& list);
		CircularLinkedList<T>& operator = (CircularLinkedList<T>&& list);
		bool operator == (const CircularLinkedList<T>& list) noexcept;
		bool operator == (const CircularLinkedList<T>&& list) noexcept;
		
		void checkValidity();
	
	private:
		ListNode<T>* _head;
		ListNode<T>* _tail;
		size_t _size;
		
		void destroyList();
		void copyList(const CircularLinkedList<T>& list);
		void createEmptyList();
};

#endif
