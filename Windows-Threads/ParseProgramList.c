#include "ParseProgramList.h"

static const char* READ_MODE = "r";
static const char* WRITE_MODE = "w";

int _lineReadBufferLength = 10;
char* _lineRead = NULL;

char* readNextLine( const char* fileName, const char delimiter, int* filePos )
{
	#ifdef DEBUG_PRINT
	printf( "reading file %s\n", fileName );
	#endif
	FILE* fileReadPointer;
	
	fileReadPointer = fopen( fileName, READ_MODE );
	
	if ( fileReadPointer == NULL )
	{
		fprintf( stderr, "unable to open file %s for reading.\n", fileName );
		return NULL;
	}
	
	if ( *filePos > 0 )
	{
		fseek( fileReadPointer, *filePos + 1, SEEK_SET );
		#ifdef DEBUG_PRINT
		printf( "successfully seek file for reading\n" );
		#endif
	}
	_initBuffer();
	char readBuffer;
	int numCharsRead = 0;
	int charsRead = 0;
	while ( fscanf( fileReadPointer, "%c%n", &readBuffer, &charsRead ) != EOF )
	{
		numCharsRead += charsRead;
		
		#ifdef DEBUG_PRINT
		printf( "read: %c\n", readBuffer );
		#endif
		
		if ( readBuffer == delimiter )
		{
			*( _lineRead + numCharsRead - 1 ) = '\0';
			break;
		}
		
		*( _lineRead + numCharsRead - 1 ) = readBuffer;
		
		if ( numCharsRead == _lineReadBufferLength )
		{
			_increaseBuffer();
		}
	}
	
	fclose( fileReadPointer );
	
	*filePos += numCharsRead;
	if ( numCharsRead == 0 )
	{
			#ifdef DEBUG_PRINT
			printf( "end of file\n" );
			#endif
			free( _lineRead );
			return NULL;
	}
	
	return _lineRead;
}

void _initBuffer()
{
	#ifdef DEBUG_PRINT
	printf ( "_initBuffer\n" );
	#endif
	_lineReadBufferLength = 10;
	_lineRead = ( char* ) malloc( sizeof( char ) * _lineReadBufferLength );
}

void _increaseBuffer()
{
	#ifdef DEBUG_PRINT
	printf ( "buffer limit reached, increasing buffer\n" );
	#endif
	
	char* temporaryBuffer = ( char* ) malloc( sizeof( char ) * ( _lineReadBufferLength *= 2 ) );
	_copyBuffer( _lineRead, temporaryBuffer, _lineReadBufferLength / 2 );
	free( _lineRead );
	_lineRead = temporaryBuffer;
}

void _copyBuffer( const char* source, char* destination, const int sourceLength )
{
	int ii = 0;
	while (  ii < sourceLength )
	{
		*( destination + ii ) = *( source + ii );
		ii++;
	}
}