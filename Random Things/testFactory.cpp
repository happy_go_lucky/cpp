
#include <iostream>
#include <memory> // for smart ptrs

using namespace std;

class TestClass
{
	public:
		TestClass() = default;
		~TestClass() = default;
		
		TestClass(int value)
		{
			_member = value;
		}
		
		TestClass(const TestClass& obj) = delete;
		TestClass(TestClass&& obj)
		{
			std::cout << "TestClass::TestClass&&" << std::endl;
			_member = std::move(obj._member);
		}
		
		TestClass& operator =(const TestClass& obj) = delete;
		TestClass& operator =(TestClass&& obj)
		{
			std::cout << "TestClass move assignment" << std::endl;
			_member = std::move(obj._member);
			return *this;
		}
		
		void print() const noexcept
		{
			std::cout << "The spanshot of object" << std::endl;
			std::cout << "address: " << this << std::endl;
			std::cout << "member: " << _member << std::endl;
		}
		
		std::unique_ptr<int> getObject()
		{
			return std::make_unique<int>(5);
		}
		
		std::unique_ptr<TestClass> getInstance()
		{
			return std::make_unique<TestClass>(5);
		}
	
	private:
		int _member;
};

int main()
{
	TestClass obj(7);
	
	std::cout << "unique ptr factory test: " << std::endl;
	std::unique_ptr<int> objnew = obj.getObject();
	std::cout << "unique ptr factory: " << *objnew << std::endl;
	std::unique_ptr<TestClass> newInstance = obj.getInstance();
	std::cout << "factory instance move: " << std::endl;
	newInstance->print();

	return 0;
}
