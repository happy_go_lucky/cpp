ver = c++11

obj_dir = obj
src_dir = src
inc_dir = include
dep_dir = deps
helper_staticlib = libdatastructs.a
helper_staticlib_path = ../../

#specify all the include directories here
vpath %.cpp $(src_dir)
vpath %.h $(inc_dir)
vpath %.d $(dep_dir)
vpath %.o $(obj_dir)

#0-3 >
optimize_lvl = 0
#0: no info, 3 max info.
debug_lvl = 3

c_flags = -O$(optimize_lvl) -pedantic -Wpedantic -Wall \
	-Wextra -pthread -std=$(ver) -g$(debug_lvl) -ggdb$(debug_lvl)

#-MF $(dep_dir)/$*.d: set the dependency file to depname.d
#MT is target
#MF is the filename where the dependencies are saved to
gen_deps = -MT $(@:.d=.o) -MM -MF $(dep_dir)/$*.d

lib_files = BetterVector.cpp\
	BetterStack.cpp\
	ListNode.cpp\
	CircularLinkedList.cpp

archive_files = $(lib_files:.cpp=.o)
ar_dep_files = $(lib_files:.cpp=.d)
archive_file_loc = $(addprefix $(obj_dir)/,$(archive_files))

all: $(helper_staticlib)

$(helper_staticlib): $(archive_files)
	ar rcs $(helper_staticlib_path)$@ $(archive_file_loc)

%.o: %.cpp
	$(CXX) $(c_flags) -c $< -o $(obj_dir)/$@

# run setup before running the rule
%.d: %.cpp | setup
	$(CXX) $(c_flags) $< $(gen_deps)

#include must not precede the default target
include $(ar_dep_files)

.PHONY: clean
.PHONY: all
.PHONY: setup
.PHONY: debug

clean:
	@rm -f $(obj_dir)/*.o
	@rm -f *.o
	@rm -f $(dep_dir)/*.d
	@rm -f *.d
	@rm -f $(helper_staticlib_path)$(helper_staticlib)
	@echo cleaned all temporary files

setup:
	@mkdir -p $(obj_dir)
	@mkdir -p $(dep_dir)

debug:
	nm $(archive_files)
