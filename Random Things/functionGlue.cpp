/**
 * This class deals with examples for std::function, std::bind, and std;:parameters
 * */

#include <functional>
#include <iostream>
#include <string>

class TestClass2
{
    public:
        TestClass2() = default;
        ~TestClass2() = default;
        
        template <typename Type>
        void testCallback(std::function<bool (const Type&)> predicateFunc, Type value)
        {
            if (predicateFunc(value))
                std::cout << "predicate is true" << std::endl;
            else
                std::cout << "predicate is false" << std::endl;
        }
};

class TestClass
{
	public:
		TestClass() = default;
		~TestClass() = default;
		
		inline void printSomething() const noexcept
		{
			std::cout << "TestClass::printSomething()" << std::endl;
		}
		
		inline int printArgs(const int& param1, const int& param2, const int& param3) const noexcept
		{
			std::cout << "TestClass::printArgs()" << std::endl;
			std::cout << "argument 1: " << param1 << std::endl;
			std::cout << "argument 2: " << param2 << std::endl;
			std::cout << "argument 3: " << param3 << std::endl;
			std::cout << std::string(20, '-') << std::endl;
			return 3;
		}
		
		inline void testFunctionPassing(std::function<bool(const bool& value)> func, bool val)
		{
			if (func(val))
				std::cout << "the function returns true" << std::endl;
			else
				std::cout << "the function returns false" << std::endl;
		}
		
		inline void testCallback()
		{
		    TestClass2 testCall;
        	std::function <bool (const bool&)> func = 
        	    std::bind(&TestClass::predicate, this, std::placeholders::_1);
        	testCall.testCallback(func, false);
		}
		
	private:
	    bool aValue = false;
	    
        inline bool predicate(bool value)
        {
            return aValue == value;
        }
};

void printSomething();
int printArgs(const int& param1, const int& param2, const int& param3);
void testFunctionPointer();

int main(int argC, char* argV[])
{
	testFunctionPointer();
	
	return EXIT_SUCCESS;
}

void printSomething()
{
	std::cout << "running local scope function" << std::endl;
}

int printArgs(const int& param1, const int& param2, const int& param3)
{
	std::cout << "printArgs()" << std::endl;
	std::cout << "argument 1: " << param1 << std::endl;
	std::cout << "argument 2: " << param2 << std::endl;
	std::cout << "argument 3: " << param3 << std::endl;
	std::cout << std::string(20, '-') << std::endl;
	
	return 3;
}

void passFunctionNonBound(std::function<void(TestClass&)> func)
{
	TestClass* anInstance = new TestClass();
	func(*anInstance);
}

template <typename Type>
void passFunctionBound(std::function<int(const Type&, const Type&, const Type&)> func)
{
	func(9, 8, 7);
}

bool aMethod(bool value)
{
	return value;
}

bool aValue = false;
bool predicate(bool value)
{
    return aValue == value;
}

void testFunctionPointer()
{
	std::cout << "testing function pointer" << std::endl;
	
	// std::function is a wrapper for function pointers
	// it can also be used for the lambdas 
	std::function<void()> simpleFunc;
	std::function<int(const int&, const int&, const int&)> complexFunc;
	std::function<void(const TestClass&)> simpleFuncExt;
	std::function<int(const TestClass&, const int&, const int&, const int&)> complexFuncExt;
	std::function<int(const int&, const int&, const int&)> complexFuncExtBound;
	
	std::cout << "calling local scope" << std::endl;
	// testing local void function pointer
	simpleFunc = printSomething;
	simpleFunc();
	// testing local non-void function pointer
	complexFunc = printArgs;
	complexFunc(1, 2, 3);
	
	std::cout << "calling external scope" << std::endl;
	// testing external void function pointer
	simpleFuncExt = &TestClass::printSomething;
	// testing external non-void function pointer
	complexFuncExt = &TestClass::printArgs;
	
	// this class instance is needed for the external function pointers
	TestClass* anInstance = new TestClass();
	simpleFuncExt(*anInstance);
	
	// binding: is used to bind the aguments to a function (pointer)
	// placeholders are the ones which will be dynamic arguments
	// notice the difference in complexFuncExt and complexFuncExtBound 
	// we are binding the first agrument, and rest are dynamics
	complexFuncExtBound = std::bind(&TestClass::printArgs, *anInstance, 
					std::placeholders::_1, std::placeholders::_2, std::placeholders::_3);
	
	std::cout << "passing function around" << std::endl;
	passFunctionNonBound(simpleFuncExt);
	passFunctionBound(complexFuncExtBound);
	
	anInstance->testFunctionPassing(aMethod, true);
	
	anInstance->testCallback();
	
	std::cout << "deleting the instance of external function" << std::endl;
	delete anInstance;
	anInstance = nullptr;
	
	simpleFuncExt(*anInstance);
	
	complexFuncExt(*anInstance, 1, 2, 3);
	complexFuncExtBound(5, 6, 7);
}


