#include "../inc/MutexLock.h"

namespace posixthreads
{
	MutexLock::MutexLock() noexcept : ISharedLock()
	{
		_mutex = new pthread_mutex_t();
	}

	MutexLock::~MutexLock()
	{
		unlock();
		cleanupResources();
		delete _mutex;
	}

	void MutexLock::initResources()
	{
		pthread_mutex_init(_mutex, NULL);
	}

	void MutexLock::lock()
	{
		#ifndef NDEBUG
		std::cout << "Mutex::lock" << std::endl;
		#endif
		pthread_mutex_lock(_mutex);
	}

	void MutexLock::unlock()
	{
		#ifndef NDEBUG
		std::cout << "Mutex::unlock" << std::endl;
		#endif
		pthread_mutex_unlock(_mutex);
	}

	void MutexLock::cleanupResources()
	{
		pthread_mutex_destroy(_mutex);
	}
}
