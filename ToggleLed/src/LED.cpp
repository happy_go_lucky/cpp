#include "../inc/LED.h"

LED::LED(std::shared_ptr<gpio::GpioPin> gpioPin) : _gpioPin(gpioPin)
{
	#ifndef NDEBUG
	std::cout << "LED::LED at gpio pin: " << static_cast<int> (gpioPin->getPinNumber()) << std::endl;
	#endif
	
	_state = State::OFF;
	_gpioPin->setMode(gpio::SignalMode::OUTPUT);
}

LED::~LED()
{
	#ifndef NDEBUG
	cout << "LED::~LED at gpio pin: " << static_cast<int> (_gpioPin->getPinNumber()) << std::endl;
	#endif
}

void LED::turnOn()
{
	_gpioPin->setValue(gpio::SignalValue::HIGH);
	_state = State::ON;
}

void LED::turnOff()
{
	_gpioPin->setValue(gpio::SignalValue::LOW);
	_state = State::OFF;
}

LED::State LED::getState() const
{
	if (_gpioPin->getValue() == gpio::SignalValue::HIGH)
	{
		return LED::State::ON;
	}
	return LED::State::OFF;
}

void LED::toggleOnOff()
{
	if (_state == State::ON)
	{
		turnOff();
	}
	else
	{
		turnOn();
	}
}
