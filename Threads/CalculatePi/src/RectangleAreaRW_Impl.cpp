#include "../inc/RectangleAreaRW_Impl.h"

RectangleAreaRW_Impl::RectangleAreaRW_Impl() : 
			posixthreads::Thread(posixthreads::DetachType::COOPERATIVE)
{
	_mutexLock = new posixthreads::MutexLock();
	
	_mutexLock->initResources();
}

RectangleAreaRW_Impl::~RectangleAreaRW_Impl()
{
	_mutexLock->cleanupResources();
	delete _mutexLock;
}

void* RectangleAreaRW_Impl::threadRun()
{
	RectAreaImplArgs threadArgs = *((RectAreaImplArgs*) _threadArgs);
	
	double mid = 0.0;
	double area = 0.0;
	
	for (int ii = threadArgs.threadNumber; ii < threadArgs.numRects; ii += threadArgs.numThreads)
	{
		threadArgs.rectArray[ii].setWidth(threadArgs.deltaX); 
		mid = (ii + 0.5) * threadArgs.rectArray[ii].getWidth();
		threadArgs.rectArray[ii].setHeight(4.0 / (1.0 + mid * mid));
		area += threadArgs.rectArray[ii].getArea();
	}
	
	_mutexLock->lock();
	*(threadArgs.totalArea) += area;
	_mutexLock->unlock();
	
	return EXIT_SUCCESS;
}

