#include "../inc/ThreadPool.h"

namespace posixthreads
{
	ThreadPool::ThreadPool(ThreadPoolSizeType sizeType, unsigned short size) :
			Thread(DetachType::COOPERATIVE)
	{
		_updateNumThreads(sizeType, size);
		_initLocks();
		_initThreadVector();
	}

	ThreadPool::~ThreadPool()
	{
		_pendingTasks.emptyList();
		_currentTasks.emptyList();
		
		_cleanupLocks();
		_cleanupThreadVector();
	}

	unsigned int ThreadPool::numPendingTaks() const noexcept
	{
		return _pendingTasks.size();
	}

	unsigned int ThreadPool::numActiveTasks() const noexcept
	{
		return _currentTasks.size();
	}

	void ThreadPool::addTask(std::weak_ptr<ThreadPoolTask> task)
	{
		_taskUpdateLock->lock();
		
		_pendingTasks.addTask(task);
		
		_taskUpdateLock->unlock();
	}

	void ThreadPool::_grabTaskForProcessing()
	{
		_taskUpdateLock->lock();
		
		if (_pendingTasks.size() > 0)
		{
			// remove the task from the beginning of the queue
			ThreadPoolTask task = _pendingTasks.head()->getData();
			_pendingTasks->deleteNodeAtFront();
			
			// add the task to the end of current queue
			_currentTasks.addTask(task);
		}
		
		_taskUpdateLock->unlock();
	}
	
	void* ThreadPool::threadRun()
	{
		/*
		 * this thread should loop over all the tasks in the current task queue
		 * and assign them to a specific threads of the pools
		 * 
		 * once the task is complete it should remove that task from the pool
		 * 
		 * */
		
		while (true)
		{
			if ()
		}
		
		return EXIT_SUCCESS;
	}

	void ThreadPool::_updateNumThreads(ThreadPoolSizeType sizeType, unsigned short size) noexcept
	{
		if (sizeType == ThreadPoolSizeType::SYSTEM_BALANCED_SIZE)
		{
			_numThreads = _getDefaultPoolSize();
		}
		else
		{
			_numThreads = size;
		}
	}

	int ThreadPool::_getDefaultPoolSize() const noexcept
	{
		return get_nprocs() + POOL_SIZE_ALLOWED_OVERLOAD;
	}

	void ThreadPool::addTask(ThreadTask* task)
	{
		_taskUpdateLock->lock();
		_pendingTasks.push(task);
		_taskUpdateLock->unlock();
	}

	void ThreadPool::monitorTaskQueues()
	{
		_grabTaskForProcessing();
		
	}

	void ThreadPool::_shunTaskInProcessQueue()
	{
		_taskUpdateLock->lock();
		
		size_t& size = _taskInProcessQueue.size();
		for (size_t ii = 0; ii < size; ii++)
		{
			std::weak_ptr<ThreadPoolTask> ptrToTask = _taskInProcessQueue.front();
			if (!ptrToTask.expired())
			{
				std::shared_ptr<ThreadPoolTask> taskPtr = ptrToTask.lock();
				if (taskPtr->isFinished())
				{
					_taskInProcess.pop();
				}
			}
		}
		
		_taskUpdateLock->unlock();
	}

	void ThreadPool::_initThreadVector() noexcept
	{
		short ii = 0;
		while (ii < _numThreads)
		{
			_threadVector[ii] = new Thread(DetachType::COOPERATIVE);
			_threadVector[ii].start();
			ii++;
		}
	}

	void ThreadPool::_cleanupThreadVector() noexcept
	{
		int ii = 0;
		while (ii < _numThreads)
		{
			if (_threadVector[ii].join() != EXIT_SUCCESS)
			{
				_threadVector[ii].terminateThread(true);
			}
			ii++;
		}
	}

	void ThreadPool::_initLocks()
	{
		_taskUpdateLock = new MutexLock();
		_taskUpdateLock->initResources();
	}

	void ThreadPool::_cleanupLocks()
	{
		_taskUpdateLock->cleanupResources();
		delete _taskUpdateLock;
	}
}
