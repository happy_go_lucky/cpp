/**
 * This file implements time profiling for a process using kernel timers
 * Gives you both active (CPU time) and passive time (non CPU time) of process
 * 
 * */

#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <signal.h>
#include <unistd.h>
#include <stdbool.h>
#include <stdint.h>
#include <sys/types.h>
#include <sys/wait.h>

#include "TimerHelper.h"

#include <string.h>

long int secondsElapsed = 0;
long int totalProcessTimeInMillis = 0;
long int processActiveInMillis = 0;

void sigHandler( int signalNumber );
void enableSignalHandlers( struct sigaction* signalAction, void handlerFunction(int) );
void disableSignalHandlers( struct sigaction* signalAction );
void getTimerOfDay( int activeTimeInSeconds, int passiveTimeInSeconds );
void printProfilingResults();
static inline long int secondsToMillis( long int seconds );

int main( int agrCount, char** argList )
{
	int activeTimeInSeconds = 3;
	int passiveTimeInSeconds = 4;
	getTimerOfDay( activeTimeInSeconds, passiveTimeInSeconds );
	return EXIT_SUCCESS;
}

void sigHandler( int signalNumber )
{
	switch ( signalNumber )
	{
		case SIGALRM:
			//printf( "SIGALRM timer signal\n" );
			secondsElapsed++;
		break;
		
		case SIGVTALRM:
			//printf( "SIGVTALRM timer signal\n" );
			processActiveInMillis++;
		break;
		
		case SIGPROF:
			//printf( "SIGPROF timer signal\n" );
			totalProcessTimeInMillis++;
		break;
		
		default:
			fprintf( stderr, "unknown signal with signal number: %d\n", signalNumber );
		break;
	}
}

void enableSignalHandlers( struct sigaction* signalAction, void handlerFunction(int) )
{
	#ifdef DEBUG_PRINT
	printf( "enabling signal handlers for process\n" );
	#endif
	
	signalAction->sa_handler = handlerFunction;
	
	if ( sigaction( SIGALRM, signalAction, NULL ) == -1 )
	{
		fprintf( stderr, "unable to create SIGALRM signal handler\n" );
	}
	if ( sigaction( SIGVTALRM, signalAction, NULL ) == -1 )
	{
		fprintf( stderr, "unable to create SIGVTALRM signal handler\n" );
	}
	if ( sigaction( SIGPROF, signalAction, NULL ) == -1 )
	{
		fprintf( stderr, "unable to create SIGPROF signal handler\n" );
	}
}

void disableSignalHandlers( struct sigaction* signalAction )
{
	#ifdef DEBUG_PRINT
	printf( "disabling signal handlers\n" );
	#endif
	signalAction->sa_handler = SIG_DFL;
	sigaction( SIGALRM, signalAction, NULL );
	sigaction( SIGVTALRM, signalAction, NULL );
	sigaction( SIGPROF, signalAction, NULL );
}

void getTimerOfDay( int activeTimeInSeconds, int passiveTimeInSeconds )
{
	struct sigaction signalAction;
	struct itimerval realTimer;
	struct itimerval virtualTimer;
	struct itimerval profTimer;
	processActiveInMillis = 0;
	totalProcessTimeInMillis = 0;
	memset( &signalAction, 0, sizeof( signalAction ) );
	
	initTimer( &realTimer, ITIMER_REAL );
	initTimer( &virtualTimer, ITIMER_VIRTUAL );
	initTimer( &profTimer, ITIMER_PROF );
	enableSignalHandlers( &signalAction, &sigHandler );
	
	// set run time interval
	armTimer( &realTimer, ITIMER_REAL, 1, 0, 1, 0 );
	// set milliseconds intervals 
	armTimer( &virtualTimer, ITIMER_VIRTUAL, 0, 1000, 0, 1000 );
	armTimer( &profTimer, ITIMER_PROF, 0, 1000, 0, 1000 );
	
	// active time
	while ( processActiveInMillis < secondsToMillis( activeTimeInSeconds ) );
	
	pid_t pid = fork();
	if ( pid == 0 ) // child
	{
		printf( "child is doing its work\n" );
		sleep( passiveTimeInSeconds );
		exit(1);
	}
	
	// passive time
	int status = -1;
	do 
	{
		waitpid( pid, &status, 0 );
	} while ( !WIFEXITED( status ) );
	
	disarmTimer( &realTimer, ITIMER_REAL );
	disarmTimer( &virtualTimer, ITIMER_VIRTUAL );
	disarmTimer( &profTimer, ITIMER_PROF );
	
	disableSignalHandlers( &signalAction );
	printProfilingResults();
}

void printProfilingResults()
{
	#ifdef DEBUG_PRINT
	printf( "CPU time: %lu, Total time: %lu\n", processActiveInMillis, totalProcessTimeInMillis );
	#endif
	printf( "num seconds elapsed: %lu\n", secondsElapsed );
	printf( "CPU time consumed by process is: %lu seconds and %lu miliseconds\n", ( processActiveInMillis / 1000 ), ( processActiveInMillis % 1000 )  );
	printf( "Total time consumed by process is: %lu seconds and %lu miliseconds\n", ( totalProcessTimeInMillis / 1000 ), ( totalProcessTimeInMillis % 1000 ) );
}

static inline long int secondsToMillis( long int seconds )
{
	return seconds * 1000;
}
