#ifndef DMS_COORD_H
#define DMS_COORD_H

#include <iostream>
#include <string>
#include <climits>
#include <cstring>
#include "CoordExceptions.h"

using namespace std;

class DMSCoord
{
	public:
		static const char NORTH = 'N';
		static const char EAST = 'E';
		static const char WEST = 'W';
		static const char SOUTH = 'S';
		static const char NULL_DIR = '\0';
		static const constexpr char* NORTH_STR = "North";
		static const constexpr char* EAST_STR = "East";
		static const constexpr char* WEST_STR = "West";
		static const constexpr char* SOUTH_STR = "South";
	
		DMSCoord() noexcept;
		DMSCoord(short deg, short min, short sec, char dir);
		DMSCoord(char* coordStr);
		DMSCoord(const DMSCoord& coords);
		DMSCoord(DMSCoord&& coords) noexcept;
		~DMSCoord();
		
		friend ostream& operator <<(ostream& out, const DMSCoord& coords);
		DMSCoord& operator =(const DMSCoord& coords);
		DMSCoord& operator =(char* coordStr);
		DMSCoord& operator =(DMSCoord&& coord);
		bool operator <(const DMSCoord& coord);
		bool operator >(const DMSCoord& coord);
		bool operator <=(const DMSCoord& coord);
		bool operator >=(const DMSCoord& coord);
		bool operator ==(const DMSCoord& coord);
		
		void setDegrees(short value);
		void setMinutes(short value);
		void setSeconds(short value);
		void setDirection(char value);
		void setValue(const string& coordStr);
		
		short getDegrees() const noexcept;
		short getMinutes() const noexcept;
		short getSeconds() const noexcept;
		char getDirection() const noexcept;
		int getTotalSeconds() const noexcept;
		const char* getDirName(char dir) const;
		
		void parseLatitudeString(const string& str);
		void parseLongitudeString(const string& str);
		bool isValid() const noexcept;
	
	private:
		short _degrees;
		short _minutes;
		short _seconds;
		char _direction;
		int _totalSeconds;
		
		void convertFromString(char* coordStr);
		void computeTotalSeconds();
};

#endif
