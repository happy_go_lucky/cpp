/**
 * A thread that polls on a GPIO pin for edge detection
 * The pin has to be set to INPUT mode for it to function because
 * the signal data has to be read by the pin for it to monitor
 * 
 * the thread will continue polling for a value on the pin, hence blocking
 * the thread. Once the value change is triggered, this thread will execute
 * a callback function which will notify the caller thread about the change.
 * 
 * Ideally we should thread pool for this so that this thread can utilized'
 * when free.
 * */

#ifndef GPIO_WAIT_FOR_EDGE_WORKER_H
#define GPIO_WAIT_FOR_EDGE_WORKER_H

#include <iostream>
#include <functional>
#include <cstring>
#include <sys/epoll.h>
#include <string>
#include <fcntl.h>
#include <signal.h>
#include <unistd.h>  // for the microsecond sleep function
#include <cstdlib>
//~ #include <bitset>

#include <posixthreads/inc/Thread.h>
#include <posixthreads/inc/MutexLock.h>
#include <posixthreads/inc/ThreadDefs.h>
#include <posixthreads/inc/ConditionVariable.h>

#include "GpioDefines.h"

namespace gpio
{
	class GpioWaitForEdgeWorker : public posixthreads::Thread
	{
		public:
			friend class GpioPin;
			
			const static int INVALID = -1;
			
			enum class ExecutionCommand : unsigned int
			{
				WAIT_FOR_EDGE = 0,
				EXIT_THREAD,
				WAIT_FOR_COMMAND
			};
		
			GpioWaitForEdgeWorker(std::string pinPath, int debounceTimeMillis = 0, int timeout = -1);
			~GpioWaitForEdgeWorker();
			
			GpioWaitForEdgeWorker(const GpioWaitForEdgeWorker& worker) = delete;
			GpioWaitForEdgeWorker(GpioWaitForEdgeWorker&& worker) = delete;
			GpioWaitForEdgeWorker& operator =(const GpioWaitForEdgeWorker& worker) = delete;
			GpioWaitForEdgeWorker& operator =(GpioWaitForEdgeWorker&& worker) = delete;
			
			void* threadRun() override;
			void signalExit();
			void signalWaitForEdge(std::function<void()> callbackFunction);
			inline bool isWaiting() const
			{
				return _isWaiting;
			}
			
			void setDebounceTime(int timeInMillis);
			bool conditionPredicate(posixthreads::ThreadCondition value);
			void conditionUpdate(posixthreads::ThreadCondition value);
		
		private:
			std::string _pinPath;
			int _timeout;	// timeout -1 infinite. otherwise in miliseconds
			bool _isWaiting;
			int _debounceTime;
			posixthreads::MutexLock _waitMutex;
			posixthreads::ConditionVariable* _waitCV;
			posixthreads::ThreadCondition _condition;
			std::function<void()> _callbackFunction;
			ExecutionCommand _command;
			std::function<bool (posixthreads::ThreadCondition)> _predicateFuncWrapper;
			
			int _waitForEdge();
			static void _sigHandler(int signalNumber, siginfo_t* info, void* ucontext);
	};
}
#endif
