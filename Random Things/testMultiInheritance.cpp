#include <iostream>

class ClassA
{
	public:
		ClassA()
		{
			std::cout << "ClassA::ClassA" << std::endl;
		}
		~ClassA()
		{
			std::cout << "ClassA::~ClassA" << std::endl;
		}
		virtual void override_A(int param) = 0;
		static void funct_A(void* scope)
		{
			reinterpret_cast<ClassA* >(scope)->override_A(1);
		}
};

class ClassB
{
	public:
		ClassB()
		{
			std::cout << "ClassB::ClassB" << std::endl;
		}
		~ClassB()
		{
			std::cout << "ClassB::~ClassB" << std::endl;
		}
		
		virtual void override_B() = 0;
		static void funct_B(void* scope)
		{
			reinterpret_cast<ClassB* >(scope)->override_B();
		}
};

class TestClass : public ClassB, public ClassA
{
	public:
		TestClass() = default;
		~TestClass() = default;
		
		void override_B() override
		{
			std::cout << "TestClass::override_B" << std::endl;
		}
		
		void override_A(int param) override
		{
			std::cout << "TestClass::override_A:  " << param << std::endl;
		}
		
		void testMultiInheritance()
		{
			funct_A(this);
			funct_B(this);
		}
	private:
};

int main ()
{
	std::cout << "testing multiple inheritance" << std::endl;

	TestClass test;
	test.testMultiInheritance();
	
	return 0;
}
