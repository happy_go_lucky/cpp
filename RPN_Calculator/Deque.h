#ifndef DEQUE_H
#define DEQUE_H

/**
 * double ended queue
 * can insert and delete at both head and tail
 * 
 *	head -->__________ 	  __________ 	__________ 	  __________<-- tail
 *			|  data  | 	  |  data  | 	|  data  | 	  |  data  |
 *			|________| 	  |________| 	|________| 	  |________|
 *			|  next  |--->|  next  |--->|  next  |--->|  null  |
 *			|________| 	  |________| 	|________| 	  |________|
 *			|  null  |<---|  prev  |<---|  prev  |<---|  prev  |
 *			|________| 	  |________| 	|________| 	  |________|
 * 
 * */


#include "Node.h"
#include "Node.cpp"
#include <iostream>
#include "Effedup.h"

using std::cout;
using std::endl;

template <class TYPE>
class Deque
{
	public:
		Deque();
		Deque( const Deque<TYPE>& );
		~Deque();
		
		bool isEmpty() const;
		void pushBack( TYPE );
		void pushFront( TYPE );
		TYPE popFront();
		TYPE popBack();
		TYPE peekFront() const;
		TYPE peekBack() const;
		void clear();
		int size() const;
		
		Deque<TYPE>& operator = ( const Deque<TYPE>& );
		
		#ifdef Q_DEBUG
		void print() const;	// prints the queue, for testing
		#endif
		
	private:
		Node<TYPE>* _head;
		Node<TYPE>* _tail;
		int _size;
		
		void _copyDeque( Node<TYPE>* );
};

#endif
