#include "../inc/GpioReaderMutexTable.h"

GpioReaderMutexTable::GpioReaderMutexTable() noexcept
{
	_readMutexes = new vector<std::shared_ptr<MutexLock>>(gpio::MAX_PINS);
}

GpioReaderMutexTable::~GpioReaderMutexTable()
{
	for (int ii = 0; ii < gpio::MAX_PINS; ii++)
	{
		if (_readMutexes[ii].use_count() > 0)
		{
			_readMutexes[ii]->unlock();
			_readMutexes[ii]->cleanupResources();
		}
	}
	// vector clear resets the smart pointer
	_readMutexes->clear();
	delete _readMutexes;
}

std::shared_ptr<MutexLock> GpioReaderMutexTable::getReferenceToLock(gpio::PinNumber pinNumber) noexcept
{
	return _initLock(static_cast<size_t>(pinNumber));
}

std::shared_ptr<MutexLock> GpioReaderMutexTable::getReferenceToLock(gpio::GpioNumber gpioPinNumber) noexcept
{
	return _initLock(static_cast<size_t>(gpioPinNumber));
}

std::shared_ptr<MutexLock> GpioReaderMutexTable::_initLock(size_t pin)
{
	if (_readMutexes[pin].use_count() == 0)
	{
		_readMutexes[pin] = std::make_shared<MutexLock>();
		_readMutexes[pin].initResources();
	}
	
	return _readMutexes[pin];
}
