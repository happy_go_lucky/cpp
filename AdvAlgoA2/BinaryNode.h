#ifndef BINARY_NODE_H
#define BINARY_NODE_H

#include <iostream>

using std::ostream;
using std::cout;
using std::endl;

template <class T>
class BinaryNode
{
	public:
		BinaryNode() noexcept;
		BinaryNode(const T& theElement, BinaryNode<T>* lt = nullptr, BinaryNode<T>* rt = nullptr);
		BinaryNode(T&& theElement, BinaryNode<T>* lt = nullptr, BinaryNode<T>* rt = nullptr) noexcept;
		
		BinaryNode(const BinaryNode<T>& node);
		BinaryNode(BinaryNode<T>&& node) noexcept;
		
		~BinaryNode();
		
		BinaryNode<T>& operator =(const BinaryNode<T>& node);
		BinaryNode<T>& operator =(BinaryNode<T>&& node) noexcept;
		
		bool operator ==(const BinaryNode<T>& node);
		bool operator ==(BinaryNode<T>&& node) noexcept;
		
		template <typename U>
		friend ostream& operator <<(ostream& out, const BinaryNode<U>& node) noexcept;
		template <typename U>
		friend ostream& operator <<(ostream& out, BinaryNode<U>&& node) noexcept;
		
		inline BinaryNode<T>* getLeft() const noexcept
		{
			return _left;
		}
		
		inline BinaryNode<T>* getRight() const noexcept
		{
			return _right;
		}
		
		inline const T& getData() const noexcept
		{
			return _element;
		}
		
		inline void setRight(BinaryNode<T>* right) noexcept
		{
			_right = right;
		}
		
		inline void setLeft(BinaryNode<T>* left) noexcept
		{
			_left = left;
		}
		
		inline void setData(const T& data)
		{
			_element = data;
		}
		
		inline void setData(T&& data) noexcept
		{
			_element = std::move(data);
		}
		
		inline void markDeleted() noexcept
		{
			_markedForDeletion = true;
		}
		
		inline void unmarkDeleted() noexcept
		{
			_markedForDeletion = false;
		}
		
		inline bool isMarkedForDeletion() const noexcept
		{
			return _markedForDeletion;
		}
		
		inline int getLeftHeight() const noexcept
		{
			return _leftHeight;
		}
		
		inline void setLeftHeight(int height) noexcept
		{
			_leftHeight = height;
		}
		
		inline int getRightHeight() const noexcept
		{
			return _rightHeight;
		}
		
		inline void setRightHeight(int height) noexcept
		{
			_rightHeight = height;
		}
		
		void debugPrint() const noexcept;
		
		inline BinaryNode<T>** getLeftRef()
		{
			return &_left;
		}
		
		inline BinaryNode<T>** getRightRef()
		{
			return &_right;
		}
		
		inline bool isLeafNode() const noexcept
		{
			return _left == nullptr && _right == nullptr;
		}
		
		inline bool hasLeft() const noexcept
		{
			return _left != nullptr;
		}
		
		inline bool hasRight() const noexcept
		{
			return _right != nullptr;
		}
		
		inline bool isLeftImbalanced(int balanceFactor) const noexcept
		{
			return _leftHeight - _rightHeight >= balanceFactor;
		}
		
		inline bool isRightImbalanced(int balanceFactor) const noexcept
		{
			return _rightHeight - _leftHeight >= balanceFactor;
		}
		
		inline int getHighestHeight() const noexcept
		{
			if (_leftHeight > _rightHeight)
				return _leftHeight;
			else
				return _rightHeight;
		}
		
		inline void breakBothLinks()
		{
			_right = _left = nullptr;
		}
		
		inline void breakLeftLink()
		{
			_left = nullptr;
		}
		
		inline void breakRightLink()
		{
			_right = nullptr;
		}
	
	private:
		T _element;
		int _leftHeight;
		int _rightHeight;
		BinaryNode<T>* _left;
		BinaryNode<T>* _right;
		bool _markedForDeletion;
};

#endif
