#include "Calculator.h"

Calculator::Calculator()
{
	_isBuffered = false;
	_isValidExpr = true;
	_buffer = 0;
	_validatorIndex = 0;
	_quitReading = false;
}

Calculator::Calculator( const Calculator& otherCalc )
{
	_copyCalcMemory( otherCalc );
}

Calculator::~Calculator()
{
	
}

void Calculator::loadFile( const char* fileName )
{
	ifstream fileIn;
	
	if ( _loadInputFile( fileIn, fileName ) )
	{
		_parseInputFileForCommands( fileIn );
	}
}
		
bool Calculator::input( const char charInput )
{
	#ifndef Q_DEBUG
	cout << charInput;
	#endif
	
	if ( _evalQuitFlag( charInput ) )
	{
		_clearCalcMemory();
		return true;
	}
	
	if ( !_isValidExpr && charInput != CMD_EVAL )
	{
		return false;
	}
	
	switch ( charInput )
	{
		case CMD_EVAL:
			if ( _isBuffered )
			{
				_addToExprQueue( _buffer );
			}
			_eval();
			break;
		
		case CMD_ADD:
			if ( _isBuffered )
			{
				_addToExprQueue( _buffer );
			}
			_isValidExpr = _add();
			break;
		
		case CMD_SUB:
			if ( _isBuffered )
			{
				_addToExprQueue( _buffer );
			}
			_isValidExpr = _subtract();
			break;
		
		case CMD_DIV:
			if ( _isBuffered )
			{
				_addToExprQueue( _buffer );
			}
			try
			{
				_isValidExpr = _divide();
			}
			catch ( Effedup& except )
			{
				cout << endl << "exception: " << except.what() << endl;
				_isValidExpr = false;
			}
			break;
		
		case CMD_EXP:
			if ( _isBuffered )
			{
				_addToExprQueue( _buffer );
			}
			_isValidExpr = _exponent();
			break;
		
		case CMD_TIMES:
			if ( _isBuffered )
			{
				_addToExprQueue( _buffer );
			}
			_isValidExpr = _multiply();
			break;
		
		default:
			// to parse mulitple digit ints from string, keep buffering each digit for consequtive digits
			// once we encounter anything which is not a valid digit between 0 - 9, push to expression queue
			if ( _isDigit( charInput ) )
			{
				_isBuffered = true;
				_buffer = ( _buffer * BASE ) + _charToInt_0_9( charInput );
			}
			else
			{
				if ( _isBuffered )
				{
					_addToExprQueue( _buffer );
				}
			}
			break;
	}
	
	return false;
}

void Calculator::_eval()
{
	#ifndef Q_DEBUG
	cout << endl;
	#endif
	if ( _exprDeque.isEmpty() )
	{
		cout << "invalid expression" << endl;
		_isValidExpr = true;
		_exprDeque.clear();
		return;
	}
	
	_buffer = _exprDeque.popBack();
	
	if ( _exprDeque.isEmpty() && _isValidExpr )
	{
		cout << "The evluated result is: " << _buffer << endl;
	}
	else
	{
		cout << "invalid expression" << endl;
		_isValidExpr = true;
		_exprDeque.clear();
	}
	_buffer = 0;
	#ifdef Q_DEBUG
	_exprDeque.print();
	#endif
}

bool Calculator::_add()
{
	int op1, op2;
	if ( _exprDeque.isEmpty() )
	{
		return false;
	}
	op1 = _exprDeque.popBack();
	
	if ( _exprDeque.isEmpty() )
	{
		return false;
	}
	op2 = _exprDeque.popBack();
	
	#ifdef Q_DEBUG
	_printEvaluationSteps( op2, op1, CMD_ADD );
	#endif
	_exprDeque.pushBack( op2 + op1 );
	return true;
}

bool Calculator::_subtract()
{
	int op1, op2;
	if ( _exprDeque.isEmpty() )
	{
		return false;
	}
	op1 = _exprDeque.popBack();
	
	if ( _exprDeque.isEmpty() )
	{
		return false;
	}
	op2 = _exprDeque.popBack();
	
	#ifdef Q_DEBUG
	_printEvaluationSteps( op2, op1, CMD_SUB );
	#endif
	_exprDeque.pushBack( op2 - op1 );
	return true;
}

bool Calculator::_divide()
{
	int op1, op2;
	if ( _exprDeque.isEmpty() )
	{
		return false;
	}
	op1 = _exprDeque.popBack();
	
	if ( op1 == 0 )
	{
		throw Effedup( ( char* ) "trying to divide by 0" );
		return false;
	}
	
	if ( _exprDeque.isEmpty() )
	{
		return false;
	}
	op2 = _exprDeque.popBack();
	
	#ifdef Q_DEBUG
	_printEvaluationSteps( op2, op1, CMD_DIV );
	#endif
	_exprDeque.pushBack( op2 / op1 );
	return true;
}

bool Calculator::_multiply()
{
	int op1, op2;
	if ( _exprDeque.isEmpty() )
	{
		return false;
	}
	op1 = _exprDeque.popBack();
	
	if ( _exprDeque.isEmpty() )
	{
		return false;
	}
	op2 = _exprDeque.popBack();
	
	#ifdef Q_DEBUG
	_printEvaluationSteps( op2, op1, CMD_TIMES );
	#endif
	_exprDeque.pushBack( op2 * op1 );
	return true;
}

bool Calculator::_exponent()
{
	int op1, op2;
	if ( _exprDeque.isEmpty() )
	{
		return false;
	}
	op1 = _exprDeque.popBack();
	
	if ( _exprDeque.isEmpty() )
	{
		return false;
	}
	op2 = _exprDeque.popBack();
	
	#ifdef Q_DEBUG
	_printEvaluationSteps( op2, op1, CMD_EXP );
	#endif
	_exprDeque.pushBack( _power( op2, op1 ) );
	return true;
}

void Calculator::_addToExprQueue( int val )
{
	_exprDeque.pushBack( val );
	_resetBuffer();
}

void Calculator::_resetBuffer()
{
	_buffer = 0;
	_isBuffered = false;
}

bool Calculator::_isDigit( const char val )
{
	return ( val >= ASCII_0 and val <= ASCII_0 + BASE - 1 );
}

int Calculator::_charToInt_0_9( const char val )
{
	return val - ASCII_0;
}

int Calculator::_power( const int val, int exponent )
{
	int result = val;
	if ( exponent == 0 )
	{
		return 1;
	}
	while ( exponent > 1 )
	{
		result *= val;
		exponent--;
	}
	return result;
}

void Calculator::_printEvaluationSteps( int op1, int op2, char oper )
{
	cout << "evaluating " << op1 << oper << op2 << endl;
}

// precondition: expects a well formed c-string ending with a terminating null char
int Calculator::_c_string_len( const char* str )
{
	int len = 0;
	
	while ( *( str + len ) != '\0' )
	{
		len++;
	}
	
	return len;
}

bool Calculator::_loadInputFile( ifstream& inputFileStream, const char* filename )
{
	inputFileStream.open( filename );
	if ( !inputFileStream || inputFileStream.fail() )
	{
		cerr << "no input file found (expected " << filename << " file), enter correct file name" << endl;
		return false;
	}
	else
	{
		cerr << "input file found, commands will be parsed from the input file" << endl;
		return true;
	}
}

bool Calculator::_evalQuitFlag( const char command )
{
	//~ check for "quit" string
	if ( command == CMD_QUIT[_validatorIndex] )
	{
		_validatorIndex++;
		
		if ( _c_string_len( CMD_QUIT ) == _validatorIndex )
		{
			_quitReading = true;
			return true;
		}
		return false;
	}
	else 	// if no consequitive characters match, reset the validatorIndex 
	{
		_validatorIndex = 0;
		return false;
	}
}

void Calculator::_parseInputFileForCommands( ifstream& inputFile )
{
	char contents;
	while ( !inputFile.eof() && !_quitReading )
	{
		inputFile >> noskipws >> contents;
		input( contents );
	}
	
	inputFile.close();
}

Calculator& Calculator::operator=( const Calculator& otherCalc )
{
	if ( this != &otherCalc )
	{
		_clearCalcMemory();
		_copyCalcMemory( otherCalc );
	}
	return *this; 
}

void Calculator::_clearCalcMemory()
{
	_buffer = 0;
	_exprDeque.clear();
	_isBuffered = false;
	_isValidExpr = false;
	_quitReading = false;
	_validatorIndex = 0;
}

void Calculator::_copyCalcMemory( const Calculator& otherCalc )
{
	_buffer = otherCalc._buffer;
	_exprDeque = otherCalc._exprDeque;
	_isBuffered = otherCalc._isBuffered;
	_isValidExpr = otherCalc._isValidExpr;
	_quitReading = otherCalc._quitReading;
	_validatorIndex = otherCalc._validatorIndex;
}
