#ifndef CONSUMER_H
#define CONSUMER_H

#include <iostream>
#include "SharedMemory.h"

class Consumer
{
	public:
		Consumer();
		~Consumer();
	
		void init(SharedMemory* shMem);
	
	private:
};

#endif
