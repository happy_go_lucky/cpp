#include "DMSCoord.h"

DMSCoord::DMSCoord() noexcept
{
	_degrees = SHRT_MAX;
	_minutes = SHRT_MAX;
	_seconds = SHRT_MAX;
	_direction = NULL_DIR;
	computeTotalSeconds();
}

DMSCoord::DMSCoord(short deg, short min, short sec, char dir)
{
	_degrees = deg;
	_minutes = min;
	_seconds = sec;
	_direction = dir;
	computeTotalSeconds();
}

// lat 370731N
// long 1080008W
DMSCoord::DMSCoord(char* coordStr)
{
	convertFromString(coordStr);
	computeTotalSeconds();
}

DMSCoord::DMSCoord(const DMSCoord& coord)
{
	_degrees = coord._degrees;
	_minutes = coord._minutes;
	_seconds = coord._seconds;
	_direction = coord._direction;
	computeTotalSeconds();
}

DMSCoord::DMSCoord(DMSCoord&& coord) noexcept
{
	_degrees = move(coord._degrees);
	_minutes = move(coord._minutes);
	_seconds = move(coord._seconds);
	_direction = move(coord._direction);
	computeTotalSeconds();
}

DMSCoord::~DMSCoord()
{
	
}

ostream& operator <<(ostream& out, const DMSCoord& coord)
{
	// 107d 40m 21s West
	
	out << coord._degrees << "d " << coord._minutes << "m " << coord._seconds << "s " 
		<< coord.getDirName(coord._direction);
	
	return out;
}

DMSCoord& DMSCoord::operator =(const DMSCoord& coord)
{
	if (this != &coord)
	{
		_degrees = coord._degrees;
		_minutes = coord._minutes;
		_seconds = coord._seconds;
		_direction = coord._direction;
		computeTotalSeconds();
	}
	
	return *this;
}

DMSCoord& DMSCoord::operator =(DMSCoord&& coord)
{
	_degrees = move(coord._degrees);
	_minutes = move(coord._minutes);
	_seconds = move(coord._seconds);
	_direction = move(coord._direction);
	computeTotalSeconds();
	return *this;
}

DMSCoord& DMSCoord::operator =(char* coordStr)
{
	convertFromString(coordStr);
	computeTotalSeconds();
	return *this;
}

bool DMSCoord::operator <(const DMSCoord& coord)
{
	if (_direction == coord._direction)
	{
		if (_degrees < coord._degrees ||
			_minutes < coord._minutes ||
			_seconds < coord._seconds)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	else if (_direction == WEST && coord._direction == EAST)
	{
		return true;
	}
	else if (_direction == SOUTH && coord._direction == NORTH)
	{
		return true;
	} 
	else
	{
		throw CoordCompException((char*) "incorrect comparion <. Only compare lat with lat and long with long");
		return false;
	}
}

bool DMSCoord::operator >(const DMSCoord& coord)
{
	if (_direction == coord._direction)
	{
		if (_degrees > coord._degrees ||
			_minutes > coord._minutes ||
			_seconds > coord._seconds)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	else if (_direction == EAST && coord._direction == WEST)
	{
		return true;
	}
	else if (_direction == NORTH && coord._direction == SOUTH)
	{
		return true;
	} 
	else
	{
		throw CoordCompException((char*) "incorrect comparion >. Only compare lat with lat and long with long");
		return false;
	}
}

bool DMSCoord::operator <=(const DMSCoord& coord)
{
	return _totalSeconds <= coord._totalSeconds;
}

bool DMSCoord::operator >=(const DMSCoord& coord)
{
	return _totalSeconds >= coord._totalSeconds;
}

bool DMSCoord::operator == (const DMSCoord& coord)
{
	return _totalSeconds >= coord._totalSeconds;
}

void DMSCoord::setDegrees(short value)
{
	_degrees = value;
	computeTotalSeconds();
}

void DMSCoord::setMinutes(short value)
{
	_minutes = value;
	computeTotalSeconds();
}

void DMSCoord::setSeconds(short value)
{
	_seconds = value;
	computeTotalSeconds();
}

void DMSCoord::setDirection(char value)
{
	_direction = value;
	computeTotalSeconds();
}

short DMSCoord::getDegrees() const noexcept
{
	return _degrees;
}

short DMSCoord::getMinutes() const noexcept
{
	return _minutes;
}

short DMSCoord::getSeconds() const noexcept
{
	return _seconds;
}

char DMSCoord::getDirection() const noexcept
{
	return _direction;
}

const char* DMSCoord::getDirName(char dir) const
{
	switch (dir)
	{
		case NORTH:
		return NORTH_STR;
		
		case SOUTH:
		return SOUTH_STR;
		
		case WEST:
		return WEST_STR;

		default:
		case EAST:
		return EAST_STR;
	}
}

void DMSCoord::parseLatitudeString(const string& str)
{
	// 370731N
	_degrees = stoi(str.substr(0, 2));
	_minutes = stoi(str.substr(2, 2));
	_seconds = stoi(str.substr(4, 2));
	_direction = str.substr(6, 1).at(0);
}

void DMSCoord::parseLongitudeString(const string& str)
{
	// 1080008W
	_degrees = stoi(str.substr(0, 3));
	_minutes = stoi(str.substr(3, 2));
	_seconds = stoi(str.substr(5, 2));
	_direction = str.substr(7, 1).at(0);
}

bool DMSCoord::isValid() const noexcept
{
	return _degrees != SHRT_MAX && _minutes != SHRT_MAX && 
			_seconds != SHRT_MAX && _direction != NULL_DIR;
}

void DMSCoord::convertFromString(char* coordStr)
{
	if (coordStr == nullptr || *coordStr == '\0')
	{
		_degrees = SHRT_MAX;
		_minutes = SHRT_MAX;
		_seconds = SHRT_MAX;
		_direction = NULL_DIR;
	}
	else
	{
		if (strlen(coordStr) == 7)
		{
			// it is latitude
			parseLatitudeString(string(coordStr));
		}
		else
		{
			// it is longitude
			parseLongitudeString(string(coordStr));
		}
	}
}

void DMSCoord::computeTotalSeconds()
{
	_totalSeconds = _degrees * 3600 + _minutes * 60 + _seconds;
	if (_direction == WEST || _direction == SOUTH)
		_totalSeconds *= -1;
}

int DMSCoord::getTotalSeconds() const noexcept
{
	return _totalSeconds;
}
