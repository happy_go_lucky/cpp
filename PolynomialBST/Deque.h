#ifndef DEQUE_H
#define DEQUE_H

/**
 * double ended queue
 * can insert and delete at both head and tail
 * 
 *	head -->__________ 	  __________ 	__________ 	  __________<-- tail
 *			|  data  | 	  |  data  | 	|  data  | 	  |  data  |
 *			|________| 	  |________| 	|________| 	  |________|
 *			|  next  |--->|  next  |--->|  next  |--->|  null  |
 *			|________| 	  |________| 	|________| 	  |________|
 *			|  null  |<---|  prev  |<---|  prev  |<---|  prev  |
 *			|________| 	  |________| 	|________| 	  |________|
 * 
 * */


#include "DequeNode.h"
#include "DequeNode.cpp"
#include <iostream>

using std::cout;
using std::endl;

template <class TYPE>
class Deque
{
	public:
		Deque();
		Deque( const Deque<TYPE>& );
		~Deque();
		
		bool isEmpty() const;
		void pushBack( TYPE );
		void pushFront( TYPE );
		TYPE popFront();
		TYPE popBack();
		TYPE peekFront() const;
		TYPE peekBack() const;
		void clear();
		int size() const;
		
		Deque<TYPE>& operator = ( const Deque<TYPE>& );
		
		void print() const;	// prints the queue, for testing
		
	private:
		DequeNode<TYPE>* _head;
		DequeNode<TYPE>* _tail;
		int _size;
		
		void _copyDeque( DequeNode<TYPE>* );
};

#endif
