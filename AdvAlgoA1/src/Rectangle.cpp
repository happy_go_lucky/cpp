#include "../include/Rectangle.h"

Rectangle::Rectangle() noexcept
{
	//cout << "Rectangle::Rectangle()" << endl;
	_width = 0;
	_length = 0;
	updateAreaPerimeter();
}

Rectangle::Rectangle(int width, int length) noexcept
{
	//cout << "Rectangle::Rectangle(x, x)" << endl;
	_width = width;
	_length = length;
	
	updateAreaPerimeter();
}

Rectangle::~Rectangle()
{
	//cout << "~Rectangle()" << endl;
}

Rectangle::Rectangle(const Rectangle& rec)
{
	_width = rec._width;
	_length = rec._length;
	_area = rec._area;
	_perimeter = rec._perimeter;
}

Rectangle::Rectangle(Rectangle&& rec)
{
	_width = std::move(rec._width);
	_length = std::move(rec._length);
	_area = std::move(rec._area);
	_perimeter = std::move(rec._perimeter);
}

ostream& operator << (ostream& s, const Rectangle& r)
{
	s << "Rectangle (WxL): " << r.getLength() << "x" << r.getWidth();
	return s;
}

Rectangle& Rectangle::operator= (const Rectangle& rec)
{
	if (this != &rec)
	{
		_width = rec._width;
		_length = rec._length;
		_area = rec._area;
		_perimeter = rec._perimeter;
	}
	
	return *this;
}

Rectangle& Rectangle::operator= (Rectangle&& rec) noexcept
{
	_width = std::move(rec._width);
	_length = std::move(rec._length);
	_area = std::move(rec._area);
	_perimeter = std::move(rec._perimeter);
	
	return *this;
}

bool Rectangle::operator== (const Rectangle& rec) const noexcept
{
	return _width == rec._width && _length == rec._length;
}

bool Rectangle::operator== (const Rectangle&& rec) const noexcept
{
	return _width == rec._width && _length == rec._length;
}

bool Rectangle::areaCompare(const Rectangle& rect) const
{
	return getArea() < rect.getArea();
}

bool Rectangle::perimeterCompare(const Rectangle& rect) const
{
	return getPerimeter() < rect.getPerimeter();
}

void Rectangle::setLength(int length)
{
	_length = length;
	
	updateAreaPerimeter();
	checkValidity();
}

void Rectangle::setWidth(int width)
{
	_width = width;
	
	updateAreaPerimeter();
	checkValidity();
}

void Rectangle::updateAreaPerimeter()
{
	_area = _length * _width;
	_perimeter = 2 * (_length + _width);
}

void Rectangle::checkValidity() const
{
	assert(_width >= 0);
	assert(_length >= 0);
	assert(_area >= 0);
	assert(_perimeter >= 0);
}
