#ifndef SHARED_DATA_H
#define SHARED_DATA_H

#include <iostream>
//~ #include "SemaphoreUnnamed.h"

struct SharedData
{
	static const int RESOURCE_STORAGE_SIZE = 10;
	//~ SemaphoreUnnamed* maxBoundSem;
	//~ SemaphoreUnnamed* minBoundSem;
	int storage[RESOURCE_STORAGE_SIZE];
	int target;
	
	inline friend ostream& operator <<(ostream& out, const SharedData& sharedData)
	{
		out << "printing shraed memory" << std::endl;
		out << "storage: " << std::endl;
		for (int ii = 0; ii < sharedData.RESOURCE_STORAGE_SIZE; ii++)
		{
			out << sharedData.storage[ii] << ", ";
		}
		out << std::endl;
		return out;
	}
};

#endif
