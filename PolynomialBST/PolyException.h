#ifndef POLY_EXCEPTION_H
#define POLY_EXCEPTION_H

#include <exception>

class PolyException:public std::exception
{
	public:
		PolyException( char* = ( char* ) "Polynomial Exception!" );
		~PolyException();
		virtual const char* what() const throw();
		
	private:
		char* _message;
};

#endif
