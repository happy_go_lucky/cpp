#include "../inc/Rectangle.h"

Rectangle::Rectangle() noexcept
{
	_width = 0;
	_height = 0;
	
	updateAreaPerimeter();
}

Rectangle::Rectangle(double width, double height) noexcept
{
	_width = width;
	_height = height;
	
	updateAreaPerimeter();
}

Rectangle::~Rectangle()
{
	//cout << "~Rectangle()" << endl;
}

Rectangle::Rectangle(const Rectangle& rec)
{
	_width = rec._width;
	_height = rec._height;
	_area = rec._area;
	_perimeter = rec._perimeter;
}

Rectangle::Rectangle(Rectangle&& rec)
{
	_width = std::move(rec._width);
	_height = std::move(rec._height);
	_area = std::move(rec._area);
	_perimeter = std::move(rec._perimeter);
}

ostream& operator << (ostream& s, const Rectangle& r)
{
	s << "Rectangle (WxL): " << r.getHeight() << "x" << r.getWidth();
	return s;
}

Rectangle& Rectangle::operator= (const Rectangle& rec)
{
	if (this != &rec)
	{
		_width = rec._width;
		_height = rec._height;
		_area = rec._area;
		_perimeter = rec._perimeter;
	}
	
	return *this;
}

Rectangle& Rectangle::operator= (Rectangle&& rec) noexcept
{
	_width = std::move(rec._width);
	_height = std::move(rec._height);
	_area = std::move(rec._area);
	_perimeter = std::move(rec._perimeter);
	
	return *this;
}

bool Rectangle::operator== (const Rectangle& rec) const noexcept
{
	return _width == rec._width && _height == rec._height;
}

bool Rectangle::operator== (const Rectangle&& rec) const noexcept
{
	return _width == rec._width && _height == rec._height;
}

bool Rectangle::hasEqualArea(const Rectangle& rect) const
{
	return _area == rect.getArea();
}

bool Rectangle::hasEqualPerimeter(const Rectangle& rect) const
{
	return _perimeter == rect.getPerimeter();
}

void Rectangle::setHeight(double height)
{
	_height = height;
	
	updateAreaPerimeter();
	checkValidity();
}

void Rectangle::setWidth(double width)
{
	_width = width;
	
	updateAreaPerimeter();
	checkValidity();
}

void Rectangle::updateAreaPerimeter()  noexcept
{
	_area = _height * _width;
	_perimeter = 2 * (_height + _width);
}

void Rectangle::checkValidity() const
{
	assert(_width >= 0);
	assert(_height >= 0);
	assert(_area >= 0);
	assert(_perimeter >= 0);
}
