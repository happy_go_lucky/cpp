#include "PRQuadNode.h"

PRQuadNode::PRQuadNode() noexcept
{
	_nextNE = nullptr;
	_nextNW = nullptr;
	_nextSE = nullptr;
	_nextSW = nullptr;
}

PRQuadNode::PRQuadNode(const PRQuadNode& node)
{
	_dataNE = node._dataNE;
	_dataNW = node._dataNW;
	_dataSE = node._dataSE;
	_dataSW = node._dataSW;
	
	_nextNE = nullptr;
	_nextNW = nullptr;
	_nextSE = nullptr;
	_nextSW = nullptr;
}

PRQuadNode::PRQuadNode(PRQuadNode&& node) noexcept
{
	_dataNE = std::move(node._dataNE);
	_dataNW = std::move(node._dataNW);
	_dataSE = std::move(node._dataSE);
	_dataSW = std::move(node._dataSW);
	
	_nextNE = std::move(node._nextNE);
	_nextNW = std::move(node._nextNW);
	_nextSE = std::move(node._nextSE);
	_nextSW = std::move(node._nextSW);
	
	node._nextNE = nullptr;
	node._nextNW = nullptr;
	node._nextSE = nullptr;
	node._nextSW = nullptr;
}

PRQuadNode::PRQuadNode(const QuadTreeRecord& record)
{
	_dataNE = record;
	
	_nextNE = nullptr;
	_nextNW = nullptr;
	_nextSE = nullptr;
	_nextSW = nullptr;
}

PRQuadNode::PRQuadNode(const PRQuadData& record)
{
	_dataNE = record;
	
	_nextNE = nullptr;
	_nextNW = nullptr;
	_nextSE = nullptr;
	_nextSW = nullptr;
}

PRQuadNode::~PRQuadNode()
{
	// nothing to do here
}

PRQuadNode& PRQuadNode::operator =(const PRQuadNode& node)
{
	if (this != &node)
	{
		_dataNE = node._dataNE;
		_dataNW = node._dataNW;
		_dataSE = node._dataSE;
		_dataSW = node._dataSW;
		
		_nextNE = nullptr;
		_nextNW = nullptr;
		_nextSE = nullptr;
		_nextSW = nullptr;
	}
	
	return *this;
}

PRQuadNode& PRQuadNode::operator =(PRQuadNode&& node) noexcept
{
	_dataNE = std::move(node._dataNE);
	_dataNW = std::move(node._dataNW);
	_dataSE = std::move(node._dataSE);
	_dataSW = std::move(node._dataSW);
	
	_nextNE = std::move(node._nextNE);
	_nextNW = std::move(node._nextNW);
	_nextSE = std::move(node._nextSE);
	_nextSW = std::move(node._nextSW);
	
	node._nextNE = nullptr;
	node._nextNW = nullptr;
	node._nextSE = nullptr;
	node._nextSW = nullptr;
	
	return *this;
}

ostream& operator <<(ostream& out, const PRQuadNode& node) noexcept
{
	if (node.hasSubDivided_NW())
	{
		out << "@";
	}
	else
	{
		cout << node._dataNW << ", ";		
	}
	
	if (node.hasSubDivided_NE())
	{
		out << "@";
	}
	else
	{
		cout << node._dataNE << ", ";		
	}
	
	if (node.hasSubDivided_SE())
	{
		out << "@";
	}
	else
	{
		cout << node._dataSE << ", ";		
	}
	
	if (node.hasSubDivided_SW())
	{
		out << "@";
	}
	else
	{
		cout << node._dataSW << ", ";		
	}
	return out;
}

void PRQuadNode::setDataNW(const PRQuadData& data)
{
	_dataNW = data;
}

void PRQuadNode::setDataNE(const PRQuadData& data)
{
	_dataNE = data;
}

void PRQuadNode::setDataSW(const PRQuadData& data)
{
	_dataSW = data;
}

void PRQuadNode::setDataSE(const PRQuadData& data)
{
	_dataSE = data;
}

void PRQuadNode::setDataNW(const QuadTreeRecord& data)
{
	_dataNW = data;
}

void PRQuadNode::setDataNE(const QuadTreeRecord& data)
{
	_dataNE = data;
}

void PRQuadNode::setDataSW(const QuadTreeRecord& data)
{
	_dataSW = data;
}

void PRQuadNode::setDataSE(const QuadTreeRecord& data)
{
	_dataSE = data;
}

void PRQuadNode::setLinkNW(PRQuadNode* node) noexcept
{
	_nextNW = node;
}

void PRQuadNode::setLinkNE(PRQuadNode* node) noexcept
{
	_nextNE = node;
}

void PRQuadNode::setLinkSW(PRQuadNode* node) noexcept
{
	_nextSW = node;
}

void PRQuadNode::setLinkSE(PRQuadNode* node) noexcept
{
	_nextSE = node;
}

void PRQuadNode::breakLinkNE() noexcept
{
	_nextNE = nullptr;
}

void PRQuadNode::breakLinkNW() noexcept
{
	_nextNW = nullptr;
}

void PRQuadNode::breakLinkSE() noexcept
{
	_nextSE = nullptr;
}

void PRQuadNode::breakLinkSW() noexcept
{
	_nextSW = nullptr;
}

bool PRQuadNode::hasSubDivided_NW() const noexcept
{
	return _nextNW != nullptr;
}

bool PRQuadNode::hasSubDivided_NE() const noexcept
{
	return _nextNE != nullptr;
}

bool PRQuadNode::hasSubDivided_SW() const noexcept
{
	return _nextSW != nullptr;
}

bool PRQuadNode::hasSubDivided_SE() const noexcept
{
	return _nextSE != nullptr;
}

PRQuadNode* PRQuadNode::getNextNE() const noexcept
{
	return _nextNE;
}

PRQuadNode* PRQuadNode::getNextNW() const noexcept
{
	return _nextNW;
}

PRQuadNode* PRQuadNode::getNextSE() const noexcept
{
	return _nextSE;
}

PRQuadNode* PRQuadNode::getNextSW() const noexcept
{
	return _nextSW;
}

PRQuadData PRQuadNode::getDataNW() const noexcept
{
	return _dataNW;
}

PRQuadData PRQuadNode::getDataNE() const noexcept
{
	return _dataNE;
}

PRQuadData PRQuadNode::getDataSW() const noexcept
{
	return _dataSW;
}

PRQuadData PRQuadNode::getDataSE() const noexcept
{
	return _dataSE;
}

bool PRQuadNode::isEmpty() const noexcept
{
	return _dataNW.isNull() && _dataNE.isNull() && _dataSE.isNull() && _dataSW.isNull();
}

bool PRQuadNode::isEmpty_NW() const noexcept
{
	return _dataNW.isNull();
}

bool PRQuadNode::isEmpty_NE() const noexcept
{
	return _dataNE.isNull();
}

bool PRQuadNode::isEmpty_SW() const noexcept
{
	return _dataSW.isNull();
}

bool PRQuadNode::isEmpty_SE() const noexcept
{
	return _dataSE.isNull();
}

void PRQuadNode::updateOffsetNW(const QuadTreeRecord& data) noexcept
{
	_dataNW.updateOffset(data.recordByteOffset);
}

void PRQuadNode::updateOffsetNE(const QuadTreeRecord& data) noexcept
{
	_dataNE.updateOffset(data.recordByteOffset);
}

void PRQuadNode::updateOffsetSW(const QuadTreeRecord& data) noexcept
{
	_dataSW.updateOffset(data.recordByteOffset);
}

void PRQuadNode::updateOffsetSE(const QuadTreeRecord& data) noexcept
{
	_dataSE.updateOffset(data.recordByteOffset);
}

void PRQuadNode::updateOffsetNW(const PRQuadData& data) noexcept
{
	_dataNW.updateOffset(data.getOffsetList());
}

void PRQuadNode::updateOffsetNE(const PRQuadData& data) noexcept
{
	_dataNE.updateOffset(data.getOffsetList());
}

void PRQuadNode::updateOffsetSW(const PRQuadData& data) noexcept
{
	_dataSW.updateOffset(data.getOffsetList());
}

void PRQuadNode::updateOffsetSE(const PRQuadData& data) noexcept
{
	_dataSE.updateOffset(data.getOffsetList());
}

bool PRQuadNode::isSubDivided() const noexcept
{
	return _nextNW != nullptr || _nextNE != nullptr ||
			_nextSW != nullptr || _nextSE != nullptr;
}
