#include "MatrixException.h"

MatrixException::MatrixException( char* message )
{
	_message = message;
}

MatrixException::~MatrixException()
{
	// use try catch
}

const char* MatrixException::what() const throw()
{
	return _message;
}
