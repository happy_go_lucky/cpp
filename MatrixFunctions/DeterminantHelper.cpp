#include "DeterminantHelper.h"

DeterminantHelper::DeterminantHelper( const unsigned rows, const unsigned cols )
{
    _rows = rows;
    _cols = cols;
    _rowMap = new unsigned[rows];
    _columnMap = new unsigned[cols];
    _resetMap();
}

DeterminantHelper::DeterminantHelper( const DeterminantHelper& aDeterminantHelper )
{
    _rows = aDeterminantHelper.getRows();
    _cols = aDeterminantHelper.getCols();
    _rowMap = new unsigned[_rows];
    _columnMap = new unsigned[_cols];
    _copyMap( aDeterminantHelper.getRowMap(), aDeterminantHelper.getColMap(), _rows );
}

DeterminantHelper::~DeterminantHelper()
{
    delete _rowMap;
    delete _columnMap;
}

unsigned* DeterminantHelper::getRowMap() const
{
    return _rowMap;
}

unsigned* DeterminantHelper::getColMap() const
{
    return _columnMap;
}

unsigned DeterminantHelper::getRows() const
{
    return _rows;
}

unsigned DeterminantHelper::getCols() const
{
    return _cols;
}

void DeterminantHelper::setInvalidRowAndCol( const unsigned row, const unsigned col )
{
    _rowMap[row - 1] = INVALID_ENTRY;
    _columnMap[col - 1] = INVALID_ENTRY;
}

void DeterminantHelper::setValidRowAndCol( const unsigned row, const unsigned col )
{
    _rowMap[row - 1] = VALID_ENTRY;
    _columnMap[col - 1] = VALID_ENTRY;
}

unsigned DeterminantHelper::getFirstRow() const
{
    unsigned ii = 0;
    while ( ii < _rows && _rowMap[ii] == INVALID_ENTRY )
    {
        ii++;
    }
    return ii + 1;
}

unsigned DeterminantHelper::getFirstCol() const
{
    unsigned ii = 0;
    while ( ii < _cols && _columnMap[ii] == INVALID_ENTRY )
    {
        ii++;
    }
    return ii + 1;
}

unsigned DeterminantHelper::getLastRow_2x2() const
{
    unsigned ii = _rows - 1;
    while ( ii > 0 && _rowMap[ii] == INVALID_ENTRY )
    {
        ii--;
    }
    return ii + 1;
}

unsigned DeterminantHelper::getLastCol_2x2() const
{
    unsigned ii = _cols - 1;
    while ( ii > 0 && _columnMap[ii] == INVALID_ENTRY )
    {
        ii--;
    }
    return ii + 1;
}

void DeterminantHelper::_resetMap()
{
    unsigned ii = 0;
    while ( ii < _rows )
    {
        _rowMap[ii] = VALID_ENTRY;
        _columnMap[ii] = VALID_ENTRY;
        ii++;
    }
}

void DeterminantHelper::_copyMap( unsigned* const rowArr, unsigned* const colArr, unsigned len )
{
    unsigned ii = 0;
    while ( ii < len )
    {
        _rowMap[ii] = *( rowArr + ii );
        _columnMap[ii] = *( colArr + ii );
        ii++;
    }
}

void DeterminantHelper::printData() const
{
    unsigned ii = 0;
    cout << "Determinant Helper row map\n[ " << endl;
    while ( ii < _rows )
    {
        cout << _rowMap[ii] << ", ";
        ii++;
    }
    cout << " ]\nDeterminant Helper column map\n[ " << endl;
    ii = 0;
    while ( ii < _cols )
    {
        cout << _columnMap[ii] << ", ";
        ii++;
    }
    cout << " ]" << endl;
}