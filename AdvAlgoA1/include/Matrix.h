#ifndef MATRIX_H
#define MATRIX_H

#include <iostream>
#include <initializer_list>
#include <include/BetterVector.h>
#include <src/BetterVector.cpp>

using namespace std;

template <class Object>
class Matrix
{

public:
	// this is used as default constructor
	Matrix() noexcept;
	Matrix(size_t rows, size_t cols) noexcept;
	Matrix(double rows, double cols) = delete;
	Matrix(float rows, float cols) = delete;
	// a rule used to create and pass a vector/array in a shorthand initialization way
	// iniializer_list is controlled via compiler and automatically instantiate things as list
	// e.g. Matrix({1, 2, 3}) will create the lst = {1, 2, 3} for following
	Matrix(initializer_list<BetterVector<Object>> lst);
	// copy constructor
	Matrix(const BetterVector<BetterVector<Object>>& v);
	// move constructor
	Matrix(BetterVector<BetterVector<Object>>&& v);
		
	~Matrix();
	
	template <typename T>
	friend ostream& operator << (ostream& s, const Matrix<T>& m);
	const BetterVector<Object>& operator[](size_t row) const;
	BetterVector<Object>& operator[](size_t row);
	
	size_t numrows() const noexcept;
	size_t numcols() const noexcept;

	// *** Provide resize method here
	void resize(size_t rows, size_t cols);

private:
	BetterVector<BetterVector<Object>> _array;
};

#endif
