#ifndef EXPRESSION_H
#define EXPRESSION_H

#include <iostream>
#include <string>
#include <stack>
#include <vector>
#include "FSM_Helper.h"
#include "ElementOperator.h"
#include "ElementOperand.h"
#include "ExprFSM.h"
#include "GlobalVariables.h"

using namespace std;

class Expression
{
	public:
		const static size_t MAX_EXPR_SIZE = 500;

		Expression() = default;
		Expression(const char* expr);
		Expression(const string& expr);
		Expression(const string&& expr) noexcept;
		
		Expression(Expression& expr);
		Expression(Expression&& expr);
		
		~Expression() noexcept;
		
		friend ostream& operator <<(ostream& out, const Expression& expr);
		friend istream& operator >>(istream& in, Expression& expr);
		Expression& operator =(const Expression& expr) noexcept;
		Expression& operator =(Expression&& expr) noexcept;
		
		double evaluate(ExpressionType expressionType) const;
		bool isValid(ExpressionType expressionType) const;
		
		inline bool isEmpty() const
		{
			return _expression.length() == 0;
		}
		
	private:
		string _expression;
		vector<ExprElement*> _exprElements;
		bool _hasValidElements;
		ExprFSM* _fsmInfix;
		ExprFSM* _fsmPrefix;
		ExprFSM* _fsmPostfix;
		GlobalVariables _globalVars;
	
		void buildExpression();
		bool isValid_infix() const;
		bool isValid_prefix() const;
		bool isValid_postfix() const;
		bool isValidOperator(char op) const;
		void parseForElements(const string* str, vector<string>* values, const char* whitespace) const;
		bool validateAndBuildExpr(vector<string>& values);
		void printElementVector() const;
		bool isWhitespace(char c) const;
		void cleanupElementVector();
		void initFSM();
		void cleanupStateMachines();
		double evalPreFix() const;
		double evalPostFix() const;
		double evalInFix() const;
		// precondition: the expression is validated before it's computed
		template<typename T>
		T computePostfix(stack<T>& data, char oper) const;
		template<typename T>
		T add(T leftOperand, T rightOperand) const;
		template<typename T>
		T subtract(T leftOperand, T rightOperand) const;
		template<typename T>
		T divide(T leftOperand, T rightOperand) const;
		template<typename T>
		T multiply(T leftOperand, T rightOperand) const;
		template<typename T>
		T exponent(T leftOperand, T rightOperand) const;
		template<typename T>
		T average(T operandA, T operandB, T operandC) const;
};

#endif
