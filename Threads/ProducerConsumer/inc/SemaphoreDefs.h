#ifndef SEMAPHORE_DEFS_H
#define SEMAPHORE_DEFS_H

namespace SemaphoreDefs
{
	enum SharingType
	{
		PROCESS = 0,
		THREADS
	};
}

#endif
