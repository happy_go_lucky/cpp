#include <iostream>
#include <string>
#include <vector>
#include "Polynomial.h"

// used for checking if the file redirection has been used
// since this is an os dependent function, therefore, check if using unix or windows os
#ifdef __unix__
#include <unistd.h>
#else
#include <io.h>
#endif

using std::cout;
using std::cin;
using std::endl;
using std::string;
using std::noskipws;
using std::vector;

const char* INPUT_FILE = "input.txt";

const int STDIN_NOT_TERMINAL = 0;	// isatty() returns 0 if the std input is not a terminal and 1 otherwise
const int STDIN_TERMINAL = 1;
const int ASCII_CODE_0 = 48;
const int BASE = 10;
const int ASCII_CODE_MINUS = 45;
const int NUM_INDETERMINATES = 1;
const int NUM_COEFFICIENT = 1;

// reads file specified in the command line, charcter by character, including whitespaces
void parseInput( vector<Polynomial>& );
// checks whether the a char is a valid integer between 0-9
bool isIneteger( const char );
// gets integer value from the characters, if the intgere is greater than 9 (more than 1 digit)
// and stores them in a vector of polynomials
void parseIntFromChars( const char, int& );
// prints the polynomials read from the text input file
void printReadPolynomials( vector<Polynomial>& );
// tries to differentiates the polynomials in a vector of polynomials
void testDifferentiation( vector<Polynomial>& );
// tries to add all polynomails in the vector of polynomials
void testAddition( vector<Polynomial>& );
// just a test test function start testing various polynomial operations
void testPolynomials();
void testEvaluation( vector<Polynomial>& );
//void interfaceTest();

int main()
{
	// to check if file input is redirected
	bool isFileRedirected = false;
	#ifdef __unix__
	isFileRedirected = isatty( STDIN_FILENO ) == STDIN_NOT_TERMINAL;
	#else
	isFileRedirected = _isatty( _fileno( stdin ) ) == STDIN_NOT_TERMINAL;
	#endif
	
	if ( isFileRedirected )
	{
		// in case of build errors, comment everything in main except following function call, this is all we need to test the program
		// if you want to test copy constructor/assignment operator on calculator, try calling testCalculatorWithCopyCons(). They produce the same output though
		testPolynomials();
	}
	else
	{
		cout << "No input file is provided in the command line." << endl;
		cout << "Please use the program in command line like following:" << endl;
		cout << "./runme < " << INPUT_FILE << " > output.txt" << endl;
	}
	
	return EXIT_SUCCESS;
}

void testPolynomials()
{
	vector<Polynomial> polyArray;
	parseInput( polyArray );
	printReadPolynomials( polyArray );
	testEvaluation( polyArray );
	
	testDifferentiation( polyArray );
	
	testAddition( polyArray );
}

// precondition expected all intergers
void parseInput( vector<Polynomial>& polyArray )
{
	char buffer;
	int termElements[2];
	const int COEFF = 1;
	const int EXPONENT_INDETERMINATE_X = 0;
	int numTermsToRead = 0;
	int numTermElements = NUM_COEFFICIENT + NUM_INDETERMINATES;
	bool isSigned = false;
	int intVal = 0;
	
	while ( !cin.eof() )
	{
		cin >> noskipws >> buffer;
		//~ cout << "read: " << buffer << endl;
		if ( isIneteger( buffer ) )
		{
			parseIntFromChars( buffer, intVal );
		}
		else if ( buffer == ASCII_CODE_MINUS )
		{
			isSigned = true;
		}
		else
		{
			if ( isSigned )
			{
				intVal = intVal * -1;
				isSigned = false;
			}
			
			if ( numTermsToRead == 0 && intVal > 0 )
			{
				numTermsToRead = intVal;
				polyArray.push_back( Polynomial() );
				//~ cout << "num of terms to read: " << numTermsToRead << endl;
			}
			else
			{
				termElements[ numTermElements - 1 ] = intVal;
				numTermElements--;
				if ( numTermElements == 0 )
				{
					numTermsToRead--;
					//~ printf( "adding: %d, %d, %d\n", termElements[COEFF], termElements[EXPONENT_INDETERMINATE_X],  );
					polyArray[polyArray.size() - 1].insertTerm( termElements[COEFF], termElements[EXPONENT_INDETERMINATE_X] );
					numTermElements = NUM_COEFFICIENT + NUM_INDETERMINATES;
				}
			}
			
			intVal = 0;
		}
	}
}

// convers a series of characters to an integer, when each character is passed one after another
// buffer is used to store intermediate integer value.
void parseIntFromChars( const char digit, int& buffer )
{
	buffer = ( digit - ASCII_CODE_0 ) + ( BASE * buffer );
}

bool isIneteger( const char val )
{
	return ( val >= ASCII_CODE_0 && val <= ASCII_CODE_0 + 9 );
}

void printReadPolynomials( vector<Polynomial>& polyArray )
{
	unsigned int ii = 0;
	while ( ii < polyArray.size() )
	{
		cout << "polynomial " << ii + 1 << " read from the file:" << endl;
		cout << polyArray[ii] << endl;
		cout << "plynomial " << ii + 1 << " degree is: " << polyArray[ii].degree() << endl;
		ii++;
	}
	cout << endl;
}

void testEvaluation( vector<Polynomial>& polyArray )
{
	unsigned int ii = 0;
	int evaluationVal = 5;
	while ( ii < polyArray.size() )
	{
		cout << "evaluating polynomial " << ii + 1 << endl;
		cout << polyArray[ii] << endl;
		cout << "result of evaluation with x = " << evaluationVal << " is: " << polyArray[ii].evalulate( evaluationVal ) << endl;
		ii++;
	}
	cout << endl;
}

void testDifferentiation( vector<Polynomial>& polyArray )
{
	cout << "differentiating polynomials" << endl;
	unsigned int ii = 0;
	while ( ii < polyArray.size() )
	{
		cout << "dx ( ";
		cout << polyArray[ii];
		cout << " )" << endl;
		polyArray[ii].diff_x();
		cout << polyArray[ii] << endl;
		cout << "plynomial degree is: " << polyArray[ii].degree() << endl;
		ii++;
	}
	cout << endl;
}

void testAddition( vector<Polynomial>& polyArray )
{
	cout << "testing addition" << endl;
	unsigned int ii = 0;
	Polynomial temp;
	while ( ii < polyArray.size() )
	{
		cout << endl << "adding " << endl;
		cout << temp << endl;
		cout << " + " << endl;
		cout << polyArray[ii] << endl;
		cout << "---------------------------------------" << endl;
		try
		{
			temp = temp + polyArray[ii];
		}
		catch ( PolyException& except )
		{
			cout << endl << "exception: " << except.what() << endl;
		}
		cout << temp << endl;
		
		ii++;
	}
}
