#ifndef FSM_STATE_H
#define FSM_STATE_H

#include <iostream>
#include <vector>
#include "GlobalVariables.h"
#include "FSM_Helper.h"
#include "StateActions.h"
#include "FSM_Transition.h"

using std::vector;
using std::cout;
using std::endl;

class FSM_State
{
	public:
		FSM_State() noexcept;
		explicit FSM_State(ExprStates state) noexcept;
		~FSM_State();
		
		void setExitAction(BaseAction* action);
		void setEntryAction(BaseAction* action);
		void setStateType(ExprStates state);
		void addUpdateAction(BaseAction* action);
		void addTransition(FSM_Transition* transition);
		
		BaseAction* getEntryAction() const;
		BaseAction* getExitAction() const;
		ExprStates getStateType() const;
		
		// iterate through the actions
		void execute(GlobalVariables::StateActionParams& actionParams);
		// update method checks for the state input
		// iterate through the transition conditions
		// return target state of based on transition
		ExprStates update(ExprElement* inputElement, 
						GlobalVariables::StateActionParams& actionParams, 
						bool& validity);
		void debugPrint();
		
	private:
		vector<FSM_Transition*> _transitions;
		vector<BaseAction*> _actions;
		BaseAction* _entryAction;
		BaseAction* _exitAction;
		ExprStates _stateType;
		
		template <typename T>
		void clearVector(vector<T>& vec);
};

#endif
