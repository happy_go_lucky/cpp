/* signal example */
#include <stdio.h>      /* printf */
#include <csignal>     /* signal, raise, sig_atomic_t */
#include <iostream>
#include <functional>

class TestSignal
{
	public:
		TestSignal() = default;
		~TestSignal() = default;
		
		void testSignal()
		{
			_aVar = 9;
			_prev_handler = signal (SIGINT, handler);
			raise(SIGINT);
		}
		
		static void handler(int param)
		{
			switch (param)
			{
				case SIGINT:
				std::cout << "Test::SIGINT" << std::endl;
				break;

				case SIGUSR1:
				std::cout << "Test::SIGUSR1" << std::endl;
				break;

				default:
				std::cout << "Test::unkown signal: " << param << std::endl;
				break;
			}
			std::cout << "printing member: " << _aVar << std::endl;
		}
		
		
	private:
		void (*_prev_handler)(int);
		int _aVar;
};

sig_atomic_t signaled = 0;

void my_handler (int param)
{
	switch (param)
	{
		case SIGINT:
		std::cout << "SIGINT" << std::endl;
		break;

		case SIGUSR1:
		std::cout << "SIGUSR1" << std::endl;
		break;

		default:
		std::cout << "unkown signal: " << param << std::endl;
		break;
	}
}

int main ()
{
	void (*prev_handler)(int);

	prev_handler = signal (SIGINT, my_handler);
	raise(SIGINT);

	prev_handler = signal (SIGUSR1, my_handler);
	raise(SIGUSR1);

	prev_handler = signal (SIGUSR2, my_handler);
	raise(SIGUSR2);

	printf ("signaled is %d.\n",signaled);

	TestSignal test;
	test.testSignal();
	
	return 0;
}
