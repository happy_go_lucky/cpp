#ifndef CONDITION_VARIABLE_H
#define CONDITION_VARIABLE_H

#include <pthread.h>
#include <ctime>
#include <string.h>
#include "MutexLock.h"
#include "HelperFxns.h"
#include "ThreadException.h"
#include "ThreadDefs.h"

using namespace std;

namespace posixthreads
{
	class ConditionVariable
	{
		public:
			ConditionVariable() noexcept;
			ConditionVariable(const ConditionVariable& cv) = delete;
			ConditionVariable(ConditionVariable&& cv) = delete;
			~ConditionVariable();
			
			ConditionVariable& operator =(const ConditionVariable& cv) = delete;
			ConditionVariable& operator =(ConditionVariable&& cv) = delete;
			
			void initialize(MutexLock* mutex, ProcessAccessType accessType);
			void cleanup();
			void signal(ThreadEventCondition& threadEventCondition);
			void signalAll(ThreadEventCondition& threadEventCondition);
			void waitForSignal(ThreadEventCondition& threadEventCondition);
			void timedWait(int waitTimeInSeconds);
			
			ProcessAccessType getAccessType() const noexcept;
			// TODO: look into pthread_condattr_setclock if needed for timed wait
		
		private:
			pthread_cond_t* _cv;
			pthread_condattr_t* _cvAttrib;
			// mutex is external because it is used by the caller thread to
			// set and unset the condition that will be used for the while check 
			MutexLock* _mutexLock;
			
			void _setAccessType(ProcessAccessType accessType);
	};
}
#endif
