#ifndef DOUBLY_LIST_CPP
#define DOUBLY_LIST_CPP

#include "../inc/DoublyList.h"
#include "DoublyListNode.cpp"
#include "DoublyListIterator.cpp"

namespace datastructs
{
	template <typename Type>
	DoublyList<Type>::DoublyList() noexcept
	{
		_headPtr = _tailPtr = nullptr;
		_size = 0;
	}

	template <typename Type>
	DoublyList<Type>::~DoublyList() noexcept
	{
		// delete the list here
		emptyList();
	}

	template <typename Type>
	DoublyList<Type>::DoublyList(const DoublyList<Type>& list) noexcept
	{
		_headPtr = _tailPtr = nullptr;
		_size = 0;
		
		emptyList();
		DoublyListIterator<Type> itr1 = _headPtr;
		DoublyListIterator<Type> itr2 = list._headPtr;
		// first node to empty list has to entered separately, and then 2nd iterator can lead 1st iterator
		insertNode(*itr2, itr1, DoublyList::InsertPosition::AFTER);
		itr2++;
		while (itr2.isNodeValid())
		{
			insertNode(*itr2, itr1, DoublyList::InsertPosition::AFTER);
			itr1++;
			itr2++;
		}
	}

	template <typename Type>
	DoublyList<Type>& DoublyList<Type>::operator =(const DoublyList<Type>& list) & noexcept
	{
		if (*this != list)
		{
			emptyList();
			DoublyListIterator<Type> itr1 = _headPtr;
			DoublyListIterator<Type> itr2 = list._headPtr;
			// first node to empty list has to entered separately, and then 2nd iterator can lead 1st iterator
			insertNode(*itr2, itr1, DoublyList::InsertPosition::AFTER);
			itr2++;
			while (itr2.isNodeValid())
			{
				insertNode(*itr2, itr1, DoublyList::InsertPosition::AFTER);
				itr1++;
				itr2++;
			}
		}
		return *this;
	}

	template <typename Type>
	bool DoublyList<Type>::operator ==(const DoublyList<Type>& list) const noexcept
	{
		if (list._size != _size)
		{
			return false;
		}
		DoublyListIterator<Type> itr1 = _headPtr;
		DoublyListIterator<Type> itr2 = list._headPtr;
		int count = 0;
		while (itr2.isNodeValid() && itr1.isNodeValid())
		{
			if (*itr1 == *itr2)
			{
				itr1++;
				itr2++;
				count++;
				continue;
			}
			else
			{
				return false;
			}
		}
		return count == _size;
	}

	template <typename Type>
	bool DoublyList<Type>::operator !=(const DoublyList<Type>& list) const noexcept
	{
		if (list._size != _size)
		{
			return true;
		}
		DoublyListIterator<Type> itr1 = _headPtr;
		DoublyListIterator<Type> itr2 = list._headPtr;
		int count = 0;
		while (itr2.isNodeValid() && itr1.isNodeValid())
		{
			if (*itr1 != *itr2)
			{
				itr1++;
				itr2++;
				count++;
				continue;
			}
			else
			{
				return false;
			}
		}
		return count == _size;
	}

	template <typename U>
	std::ostream& operator <<(std::ostream& out, 
			const DoublyList<U>& list) noexcept
	{
		DoublyListIterator<U> itr = list._headPtr;
		while (itr.isNodeValid())
		{
			std::cout << *itr << ", ";
			itr++;
		}
		return out;
	}

	template <typename Type>
	void DoublyList<Type>::appendNode(const Type& data) noexcept
	{
		DoublyListNode<Type>* node = new DoublyListNode<Type>(data, nullptr, _tailPtr);
		_tailPtr = node;
		if (_headPtr == nullptr) _headPtr = _tailPtr;
		_size++;
	}

	template <typename Type>
	void DoublyList<Type>::appendNode(Type&& data) noexcept
	{
		DoublyListNode<Type>* node = new DoublyListNode<Type>(data, nullptr, _tailPtr);
		_tailPtr = node;
		if (_headPtr == nullptr) _headPtr = _tailPtr;
		_size++;
	}

	template <typename Type>
	void DoublyList<Type>::prependNode(const Type& data) noexcept
	{
		DoublyListNode<Type>* node = new DoublyListNode<Type>(data, _headPtr, nullptr);
		_headPtr = node;
		if (_tailPtr == nullptr) _tailPtr = _headPtr;
		_size++;
	}

	template <typename Type>
	void DoublyList<Type>::prependNode(Type&& data) noexcept
	{
		DoublyListNode<Type>* node = new DoublyListNode<Type>(data, _headPtr, nullptr);
		_headPtr = node;
		if (_tailPtr == nullptr) _tailPtr = _headPtr;
		_size++;
	}

	// TODO: do the move version
	template <typename Type>
	void DoublyList<Type>::insertNode(const Type& data, DoublyListIterator<Type>& itr, 
			DoublyList::InsertPosition position) noexcept
	{
		DoublyListNode<Type>* node = new DoublyListNode<Type>(data, nullptr, nullptr);
		
		if (itr.isNodeValid())
		{
			DoublyListNode<Type>* nextNode = itr._nodePtr->_nextNode;
			DoublyListNode<Type>* prevNode = itr._nodePtr->_prevNode;
			if (position == DoublyList::InsertPosition::AFTER)
			{
				node->_nextNode = nextNode;
				node->_prevNode = itr._nodePtr;
				itr._nodePtr->_nextNode = node;
				
				if (nextNode != nullptr)
				{
					nextNode->_prevNode = node;
				}
			}
			else
			{
				node->_prevNode = prevNode;
				node->_nextNode = itr._nodePtr;
				itr._nodePtr->_prevNode = node;
				
				if (prevNode != nullptr)
				{
					prevNode->_nextNode = node;
				}
			}
		}
		else
		{
			// list is empty, hence update head and tail
			itr._nodePtr = _headPtr = _tailPtr = node;
		}
		_size++;
	}

	template <typename Type>
	void DoublyList<Type>::deleteNode(const DoublyListIterator<Type>& itr) noexcept
	{
		_deleteNode(itr);
	}

	template <typename Type>
	void DoublyList<Type>::deleteNodeAtBack() noexcept
	{
		_deleteNode(_tailPtr);
	}

	template <typename Type>
	void DoublyList<Type>::deleteNodeAtFront() noexcept
	{
		_deleteNode(_headPtr);
	}

	template <typename Type>
	DoublyListIterator<Type> DoublyList<Type>::findNode(const Type& value) const noexcept
	{
		DoublyListIterator<Type> itr = _headPtr;
		
		while (itr.isNodeValid())
		{
			if (*itr == value)
			{
				return itr;
			}
			itr++;
		}
		
		return DoublyListIterator<Type>(nullptr);
	}

	template <typename Type>
	bool DoublyList<Type>::search(const Type& value) const noexcept
	{
		DoublyListIterator<Type> itr = _headPtr;
		while (itr.isNodeValid())
		{
			if (*itr == value)
			{
				return true;
			}
			itr++;
		}
		
		return false;
	}

	template <typename Type>
	void DoublyList<Type>::emptyList() & noexcept
	{
		if (isEmpty())
		{
			return;
		}
		
		DoublyListIterator<Type> itr = _headPtr;
		while (itr.isNodeValid())
		{
			_deleteNode(itr);
			// delete node moves the iterator to next node, hence the iterator
			// doesn't need to be advanced here
		}
	}

	template <typename Type>
	DoublyListIterator<Type> DoublyList<Type>::head() const
	{
		return DoublyListIterator<Type>(_headPtr);
	}

	template <typename Type>
	DoublyListIterator<Type> DoublyList<Type>::tail() const
	{
		return DoublyListIterator<Type>(_tailPtr);
	}

	template <typename Type>
	void DoublyList<Type>::_deleteNode(DoublyListNode<Type>* node)
	{
		if (node == nullptr)
		{
			return;
		}
		
		DoublyListNode<Type>* nextNode = node->_nextNode;
		DoublyListNode<Type>* prevNode = node->_prevNode;
		
		delete node;
		_size--;
		if (prevNode != nullptr)	// not a head node
		{
			prevNode->_nextNode = nextNode;
		}
		else
		{
			_headPtr = nextNode;
		}
		
		if (nextNode != nullptr)	// not a tail node
		{
			nextNode->_prevNode = prevNode;
		}
		else
		{
			_tailPtr = prevNode;
		}
	}

	template <typename Type>
	void DoublyList<Type>::_deleteNode(DoublyListIterator<Type>& itr)
	{
		if (itr.isNodeValid())
		{
			DoublyListNode<Type>* node = itr._nodePtr;
			itr++;
			_deleteNode(node);
		}
	}
}
#endif
