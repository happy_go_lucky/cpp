#include "../inc/SharedMemory.h"

SharedMemory::SharedMemory(SharedMemoryDefs::VisibilityType visibility,
				SharedMemoryDefs::SharingType sharingType)
{
	_shmVisibilityType = visibility;
	_shmName = nullptr;
	_shmAddress = nullptr;
	_setSharingType(sharingType);
}

SharedMemory::~SharedMemory()
{
	
}

/**
 * for named shared memory call this first
 * @arg name has to begin with '/' character
 * */
void SharedMemory::initIdentifiedMemory(const char* nameStr, 
				SharedMemoryDefs::AccessType accessType,
				mode_t accessPermissions, bool createNew, bool exclusive,
				bool truncate)
{
	assert(_shmVisibilityType == SharedMemoryDefs::VisibilityType::NAMED);
	
	_shmName = new char[MAX_NAME_LENGTH];
	
	if (nameStr == nullptr)
	{
		sprintf(_shmName, "%c%p", NAME_PREFIX, (void*) this);
	}
	else
	{
		sprintf(_shmName, "%c%s", NAME_PREFIX, nameStr);
	}
	
	int accessFlags;
	if (accessType == SharedMemoryDefs::AccessType::READ_ONLY)
	{
		accessFlags = O_RDONLY;
		if (truncate)
		{
			std::cerr << "Truncate operation on shared memory needs write permissions"
			<< std::endl;
		}
	}
	else
	{
		accessFlags = O_RDWR;
	}
	
	if (createNew)
	{
		accessFlags |= O_CREAT;
	}
	if (exclusive)
	{
		accessFlags |= O_EXCL;
	}
	
	// create the shared memory
	_shmFd = shm_open(_shmName, accessFlags, accessPermissions);
	
	#ifndef NDEBUG
	if (_shmFd == -1)
	{
		int errorNo = errno;
		std::cerr << "shm_open error number: " << errorNo << 
					", " << strerror(errorNo) << std::endl;
		
		//~ std::cout << "name: " << _shmName << ", access flags: " << accessFlags <<
			//~ ", access permission: " << accessPermissions << std::endl;
		throw ThreadException((char*) "unable to allocate shared memory");
	}
	#endif
}

void SharedMemory::initAnonymousMemory()
{
	assert(_shmVisibilityType == SharedMemoryDefs::VisibilityType::ANONYMOUS);
	
	_shmFd = -1;
	_shmAllocationType |= MAP_ANONYMOUS;
}

/**
 * for unnamed shared memory, directly call this one
 * @param startAddress if null, it is auto managed by kernel 
 * @param sharingType @see man page of mmap for details
 * @param offsetFromFDStart should be ideally 0. but customizable @see man page of mmap for details
 * */
void SharedMemory::createSharedMemory(void* startAddress, size_t sizeInBytes,
				helperfxns::AccessPermissions perm, int allocationType, 
				off_t offsetFromFDStart)
{
	int protectionPermissions = PROT_NONE;
	if (perm == helperfxns::AccessPermissions::R)
	{
		protectionPermissions = PROT_READ;
	}
	if (perm == helperfxns::AccessPermissions::W)
	{
		protectionPermissions = PROT_WRITE;
	}
	if (perm == helperfxns::AccessPermissions::X)
	{
		protectionPermissions = PROT_EXEC;
	}
	if (perm == helperfxns::AccessPermissions::RW)
	{
		protectionPermissions = PROT_READ | PROT_WRITE;
	}
	if (perm == helperfxns::AccessPermissions::RX)
	{
		protectionPermissions = PROT_READ | PROT_EXEC;
	}
	if (perm == helperfxns::AccessPermissions::WX)
	{
		protectionPermissions = PROT_WRITE | PROT_EXEC;
	}
	if (perm == helperfxns::AccessPermissions::WRX)
	{
		protectionPermissions = PROT_READ | PROT_WRITE | PROT_EXEC;
	}
	
	_shmAllocationType |= allocationType;
	_shmSize = sizeInBytes;
	int errorNo = 0;
	
	// truncate the "named" memory to exact size
	if (_shmName != nullptr)
	{
		int truncateStatus = ftruncate(_shmFd, sizeInBytes);
		
		#ifndef NDEBUG
		if (truncateStatus == -1)
		{
			errorNo = errno;
			std::cerr << "shared memory trucate error: " << errorNo << 
				", " << strerror(errorNo) << std::endl;
		}
		#endif
	}
	
	// NULL startAddress will let the kernel take over the control of memory space
	// fileDescriptor the shared memory file handle. ifgnored mostly, but if MAP_ANONYMOUS
	// is defined, it should be -1
	_shmAddress = mmap(startAddress, sizeInBytes, protectionPermissions, 
				_shmAllocationType, _shmFd, offsetFromFDStart);
	
	#ifndef NDEBUG
	errorNo = errno;
	if (_shmAddress == MAP_FAILED)
	{
		std::cerr << "mmap error number: " << errorNo << 
					", " << strerror(errorNo) << std::endl;
		std::cerr << "start address: " << startAddress << std::endl;
		std::cerr << "size in bytes: " << sizeInBytes << std::endl;
		std::cerr << "permissions: " << protectionPermissions << std::endl;
		std::cerr << "allocation type: " << _shmAllocationType << " expected: " 
				<< MAP_PRIVATE << "," << MAP_SHARED << "," << MAP_SHARED_VALIDATE << std::endl;
		std::cerr << "fd: " << _shmFd << std::endl;
		std::cerr << "offset: " << offsetFromFDStart << std::endl;
	}
	#endif
}

void SharedMemory::cleanupResources()
{
	if (_shmAddress == nullptr) return;
	
	munmap(_shmAddress, _shmSize);
	close(_shmFd);
	
	if (_shmName != nullptr)
	{
		shm_unlink(_shmName);
		delete[] _shmName;
	}
}

void SharedMemory::_setSharingType(SharedMemoryDefs::SharingType sharingType)
{
	if (sharingType == SharedMemoryDefs::SharingType::SHARED)
	{
		_shmAllocationType = MAP_SHARED;
	}
	else
	{
		_shmAllocationType = MAP_PRIVATE;
	}
}

void* SharedMemory::getPointerToMemory()
{
	return _shmAddress;
}

void SharedMemory::updateData(size_t numBytes, void* data)
{
	#ifndef NDEBUG
	std::cout << "updating shared memory with " <<  numBytes << " bytes" << std::endl;
	if (_shmAddress == nullptr)
	{
		std::cout << "shared is null" << std::endl;
	}
	if (data == nullptr)
	{
		std::cout << "data is null" << std::endl;
	}
	std::cout << "data: " << *(int*) data << std::endl;
	#endif
	memcpy(_shmAddress, data, numBytes);
}
