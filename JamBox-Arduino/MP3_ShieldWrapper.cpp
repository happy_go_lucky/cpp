#include "MP3_ShieldWrapper.h"

int MP3_ShieldWrapper::_songNumber = INIT_SONG_NUMBER;

MP3_ShieldWrapper::MP3_ShieldWrapper()
{
  _songNumber = 1;
  
  player.keyDisable();            //disable the button on the MP3 shield
  player.digitalControlEnable();  //enable to control with digital pin
  player.analogControlEnable();   //enable to control with analog pin
  player.begin();                 //start and initialize the player(MP3 shield)
  
  player.attachAnalogOperation( A4, adjustVolume );                       //attach an interrupt to control the volume with a potentialmeter
  player.setPlayMode( PM_NORMAL_PLAY );                                   //set the play mode of the player
  player.scanAndPlayAll();                                                //add all the audio files that was stored in the SD card to the playlist
  player.playOne( ( char* ) "001.MP3" );                                  //make sure the player starts playing from this file
  player.opPause();
}

MP3_ShieldWrapper::~MP3_ShieldWrapper()
{
  
}

void MP3_ShieldWrapper::loop()
{
  player.play();
}

//adjust the volume with a potentialmeter
void MP3_ShieldWrapper::adjustVolume( void )
{
  unsigned int vol_temp = analogRead( A4 );
  unsigned char volume = vol_temp / 12;
  if ( volume == 0x55 ) volume = MAXVOL; //MAXVOL = 0xfe;
  player.setVolume( volume );
}

int MP3_ShieldWrapper::getTrackNumber() const
{
  return _songNumber;
}

void MP3_ShieldWrapper::play()
{
  player.opPlay();
}

void MP3_ShieldWrapper::pause()
{
  player.opPause();
}

void MP3_ShieldWrapper::next()
{
  player.opNextSong();
  _songNumber++;
  
  if( _songNumber > NUM_TRACKS )
  {
    _songNumber = 1;
  }
  
  //printNameOfSong( _songNumber );
  player.play();
}

void MP3_ShieldWrapper::prev()
{
  player.opPreviousSong();
  _songNumber--;
  
  if( _songNumber < 1 )
  {
    _songNumber = NUM_TRACKS;
  }
  
  //printNameOfSong( _songNumber );
  player.play();
}


