#ifndef MATRIX_IMPL_H
#define MATRIX_IMPL_H

#include <iostream>
#include "MatrixTraversor.h"
#include "DeterminantHelper.h"
#include "MatrixException.h"
#include <thread>

#include <sys/sysinfo.h>	//sysconf can also be used

using std::ostream;
using std::cout;
using std::endl;

struct DeterminantArgs
{
	unsigned int sizeOfSquareMatrix;
	DeterminantHelper* detHelper;
};

template <class T>
class Matrix
{
	public:
		Matrix( const unsigned int, const unsigned int );
		Matrix( const Matrix& );
		~Matrix();
		
		// implementing using friend functions doesn't really help in this case.
		Matrix operator* ( const Matrix& ) const;
		Matrix operator* ( const double ) const;
		template <class F>
		friend Matrix<F> operator* ( const double, const Matrix<F>& );
		Matrix operator+ ( const Matrix& ) const;
		Matrix operator- ( const Matrix& ) const;
		const Matrix& operator= ( const Matrix& );
		bool operator== ( const Matrix& ) const;
		
		bool isMultiplicationPossible( const Matrix& ) const; // this times the other Matrix is possible
		Matrix transpose() const;
		void printMatrix() const;
		void printMatrixTranspose() const;
		bool isSquareMatrix() const;
		bool isSameDimension( const Matrix& ) const;
		unsigned int numRows() const;
		unsigned int numCols() const;
		double determinant() const;
		Matrix inverse() const;
		T getDataAt( const unsigned int, const unsigned int ) const;
		void setDataAt( const T, const unsigned int, const unsigned int );
		Matrix getCofactorMatrix() const;
		Matrix getMinorMatrix() const;
		Matrix getAdjugateMatrix() const;
		void* threadHelper( void* ) const;
		
	private:
		unsigned int _numRows;
		unsigned int _numCols;
		T** _matrix;
		
		void _copyMatrix( const Matrix& );
		void _allocMatrix( T**&, unsigned int, unsigned int );
		void _deallocMatrix( T**&, unsigned int );
		double _computeDeterminant( unsigned, DeterminantHelper* ) const;
		void _printMatrix( T**, unsigned, unsigned, MatrixTraversor& ) const;
};

#endif