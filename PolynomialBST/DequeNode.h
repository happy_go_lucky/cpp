#ifndef DEQUE_NODE_H
#define DEQUE_NODE_H

/**
 * Node for a double ended list
 * 
 * 	__________
 * 	|  data  |
 * 	|________|
 * 	|  next  |
 * 	|________|
 * 	|  prev  |
 * 	|________|
 * 
 * */

template <class TYPE>
class DequeNode
{
	public:
		DequeNode( TYPE, DequeNode<TYPE>* = nullptr, DequeNode<TYPE>* = nullptr );
		~DequeNode();
		
		TYPE getData() const;
		void setData( TYPE );
		DequeNode<TYPE>* getNext() const;
		DequeNode<TYPE>* getPrev() const;
		void setNext( DequeNode<TYPE>* );
		void setPrev( DequeNode<TYPE>* );
		
	private:
		TYPE data;
		DequeNode<TYPE>* next;
		DequeNode<TYPE>* prev;
};

#endif
