#include "../include/Josephus.h"

int Josephus::getWinner(int numPassesToElimination, int numPlayers)
{
	// Hint for making the algorithm efficient: think carefully how to
	// determine next player to eliminate
	int winner = -1;
	if (numPlayers > 0)
	{	
		CircularLinkedList<int> playerList(numPlayers);
		
		ListNode<int>* itr = playerList.begin();
		for (int ii = 0; ii < numPlayers; ii++)
		{
			itr->setData(ii + 1);
			itr = itr->getNextNode();
		}
		
		int numPasses = 0;
		itr = playerList.begin();
		
		while (playerList.size() > 1)
		{
			if (numPasses < numPassesToElimination)
			{
				itr = playerList.getNextNode(itr);
				numPasses++;
			}
			else
			{
				numPasses = 0;
				ListNode<int>* temp = itr;
				itr = playerList.getNextNode(itr);
				winner = temp->getData();
				playerList.remove(temp);
				std::cout << winner;
				if (playerList.size() > 1)
				{
					std::cout << ", ";
				}
			}
		}
		winner = itr->getData();
	}
	std::cout << std::endl;
	// Be sure to use cout to print out each player as they are eliminated
	return winner;
}
