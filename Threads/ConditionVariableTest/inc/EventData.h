#ifndef EVENT_DATA_H
#define EVENT_DATA_H

#include <iostream>
#include <posixthreads/inc/ThreadDefs.h>
#include <posixthreads/inc/MutexLock.h>
#include <posixthreads/inc/ConditionVariable.h>

struct EventData
{
	EventData() = default;
	~EventData() = default;
	
	EventData(const EventData& data) = delete;
	EventData(EventData&& data) = delete;
	std::ostream& operator =(ostream& out, const EventData& data) = delete;
	std::ostream& operator =(ostream& out, EventData&& data) = delete;
	
	MutexLock serverUpdateLock;
	MutexLock clientUpdateLock;
	ConditionVariable event_requestIssued;
	ConditionVariable event_requestProcessed;
	posixthreads::ThreadEventCondition requestIssuedCondition;
	posixthreads::ThreadEventCondition requestProcessedCondition;
	posixthreads::ThreadTaskCondition taskCondition;
};

#endif
