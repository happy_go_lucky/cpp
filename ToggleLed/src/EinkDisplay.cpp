#include "../inc/EinkDisplay.h"

EinkDisplay::EinkDisplay()
{
    _gpioManager = new gpio::GpioManager();
    _resetPin = gpioManager->reservePinByGpio(RST_PIN);
	_dataCmdControlPin = gpioManager->reservePinByGpio(DC_PIN);
	_spiChipSelectPin = gpioManager->reservePinByGpio(CS_PIN);
	_busyPin = gpioManager->reservePinByGpio(BUSY_PIN);
    
    
}

EinkDisplay::~EinkDisplay()
{
    delete _gpioManager;
}

/**
 * Software reset
**/
void EinkDisplay::reset()
{
    _resetPin->setValue(gpio::SignalValue::HIGH);
	gpio::sleepForMillis(200);
    _resetPin->setValue(gpio::SignalValue::LOW);
	gpio::sleepForMillis(10);
	_resetPin->setValue(gpio::SignalValue::HIGH);
	gpio::sleepForMillis(200);
}

/**
 * send command
**/
void EinkDisplay::sendCommand(gpio::uByte cmdRegister)
{
    _dataCmdControlPin->setValue(gpio::SignalValue::LOW);
    _spiChipSelectPin->setValue(gpio::SignalValue::LOW);
    DEV_SPI_WriteByte(cmdRegister);
    _spiChipSelectPin->setValue(gpio::SignalValue::HIGH);
}

/**
 * send data
**/
void EinkDisplay::sendData(gpio::uByte dataToWrite)
{
	_dataCmdControlPin->setValue(gpio::SignalValue::HIGH);
	_spiChipSelectPin->setValue(gpio::SignalValue::LOW);
	DEV_SPI_WriteByte(dataToWrite);
	_spiChipSelectPin->setValue(gpio::SignalValue::HIGH);
}

/**
 * Wait until the busy_pin goes LOW
**/
void EinkDisplay::readBusy()
{
	// gpio::uByte busy;
	// do {
		// sendCommand(0x71);
		// busy = _busyPin->getValue();
		// busy =!(busy & 0x01);
	// } while(busy);
	// gpio::sleepForMillis(200);
    while(_busyPin->getValue() == gpio::SignalValue::HIGH)
	{	  //LOW: idle, HIGH: busy
		gpio::sleepForMillis(100);
	
}

/**
 * Turn On Display full
**/
void EinkDisplay::turnOnDisplay()
{
	sendCommand(0x22);
	sendData(0xF7);
	sendCommand(0x20);
	readBusy();
}

/**
 * Turn On Display part
**/
void EinkDisplay::turnOnDisplayPart()
{
	sendCommand(0x22);
	sendData(0xFF);
	sendCommand(0x20);
	readBusy();
}

/**
 * Initialize the e-Paper register
**/
void EinkDisplay::init()
{
	reset();

	readBusy();
	sendCommand(0x12);  //SWRESET
	readBusy();

	sendCommand(0x01); //Driver output control
	sendData(0xC7);
	sendData(0x00);
	sendData(0x01);

	sendCommand(0x11); //data entry mode
	sendData(0x01);

	sendCommand(0x44); //set Ram-X address start/end position
	sendData(0x00);
	sendData(0x18);	//0x0C-->(18+1)*8=200

	sendCommand(0x45); //set Ram-Y address start/end position
	sendData(0xC7);   //0xC7-->(199+1)=200
	sendData(0x00);
	sendData(0x00);
	sendData(0x00);

	sendCommand(0x3C); //BorderWavefrom
	sendData(0x01);

	sendCommand(0x18);
	sendData(0x80);

	sendCommand(0x22); // //Load Temperature and waveform setting.
	sendData(0XB1);
	sendCommand(0x20);

	sendCommand(0x4E);   // set RAM x address count to 0;
	sendData(0x00);
	sendCommand(0x4F);   // set RAM y address count to 0X199;
	sendData(0xC7);
	sendData(0x00);
	readBusy();
}

void EinkDisplay::clear()
{
	gpio::uWord width, height;
	width = (SCREEN_WIDTH % 8 == 0) ? (SCREEN_WIDTH / 8 ) : (SCREEN_WIDTH / 8 + 1);
	height = SCREEN_HEIGHT;

	sendCommand(0x24);
	for (gpio::uWord jj = 0; jj < height; jj++)
	{
		for (gpio::uWord ii = 0; ii < width; ii++)
		{
			sendData(0XFF);
		}
	}
	turnOnDisplay();
}

/**
 * sends the image buffer in RAM to e-Paper and displays
**/
void EinkDisplay::display(gpio::uByte* image)
{
	gpio::uWord width, height;
	width = (SCREEN_WIDTH % 8 == 0) ? (SCREEN_WIDTH / 8 ) : (SCREEN_WIDTH / 8 + 1);
	height = SCREEN_HEIGHT;

	gpio::uDoubleWord Addr = 0;
	sendCommand(0x24);
	for (gpio::uWord jj = 0; jj < height; jj++)
	{
		for (gpio::uWord ii = 0; ii < width; ii++)
		{
			Addr = ii + jj * width;
			sendData(image[Addr]);
		}
	}
	turnOnDisplay();
}

/**
 * The image of the previous frame must be uploaded, otherwise the first few seconds
 * will display an exception
**/
void EinkDisplay::displayPartBaseImage(gpio::uByte* image)
{
	gpio::uWord width, height;
	width = (SCREEN_WIDTH % 8 == 0) ? (SCREEN_WIDTH / 8 ) : (SCREEN_WIDTH / 8 + 1);
	height = SCREEN_HEIGHT;

	gpio::uDoubleWord Addr = 0;
	sendCommand(0x24);
	for (gpio::uWord jj = 0; jj < height; jj++)
	{
		for (gpio::uWord ii = 0; ii < width; ii++)
		{
			Addr = ii + jj * width;
			sendData(image[Addr]);
		}
	}
	sendCommand(0x26);
	for (gpio::uWord jj = 0; jj < height; jj++)
	{
		for (gpio::uWord ii = 0; ii < width; ii++)
		{
			Addr = ii + jj * width;
			sendData(image[Addr]);
		}
	}
	turnOnDisplayPart();
}

/**
 * Sends the image buffer in RAM to e-Paper and displays
**/
void EinkDisplay::displayPart(gpio::uByte* image)
{
	gpio::uWord width, height;
	width = (SCREEN_WIDTH % 8 == 0) ? (SCREEN_WIDTH / 8 ) : (SCREEN_WIDTH / 8 + 1);
	height = SCREEN_HEIGHT;

	gpio::uDoubleWord Addr = 0;
	sendCommand(0x24);
	for (gpio::uWord jj = 0; jj < height; jj++)
	{
		for (gpio::uWord ii = 0; ii < width; ii++)
		{
			Addr = ii + jj * width;
			sendData(image[Addr]);
		}
	}
	turnOnDisplayPart();
}

void EinkDisplay::sleep()
{
	sendCommand(0x10); //enter deep sleep
	sendData(0x01);
	gpio::sleepForMillis(100);
}
