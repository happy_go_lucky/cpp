#ifndef C_STRING_FXNS_H
#define C_STRING_FXNS_H

#include <stdbool.h>	// for bool
#include <stdio.h>
#include <stdlib.h>

char* cStringConcatinate( const char* strA, const char* strB );
int strLen( const char* str );
bool cStringCompare( const char* strA, const char* strB );
bool cStringCompareChar( const char* strA, const char strB );

#endif
