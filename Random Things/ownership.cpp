/**
 * This is to test the ownership of the pointers which are passed around as the
 * and stored globally..
 * */

#include <iostream>
using namespace std;

class MyClass
{
    private:
    const int* x;
    const int* const y;
    
    public:
    MyClass(const int* a, const int* const b) : x(a), y(b)
    {
        //constructor
    }

    void show_values()
    {
        cout << "Value of constant ptr x: " << *x << endl;
        cout << "Value of constant ptr to const y: " << *y << endl;
    }
};

int main()
{
    int* value1 = new int(100);
    int* value2 = new int(200);
    
    MyClass ob1(value1, value2);
    ob1.show_values();
    
    delete value1;
    delete value2;
}
