#ifndef BINARY_SEARCH_TREE_CPP
#define BINARY_SEARCH_TREE_CPP

#include "BinarySearchTree.h"

template <typename T>
BinarySearchTree<T>::BinarySearchTree() noexcept
{
	_root = nullptr;
	_numDeleted = 0;
	_numNodes = 0;
}

template <typename T>
BinarySearchTree<T>::BinarySearchTree(const BinarySearchTree<T>& bst)
{
	_numDeleted = bst._numDeleted;
	_numNodes = bst._numNodes;
	_root = clone(bst._root);
}

template <typename T>
BinarySearchTree<T>::BinarySearchTree(BinarySearchTree<T>&& bst) noexcept
{
	_root = bst._root;
	_numDeleted = bst._numDeleted;
	_numNodes = bst._numNodes;
	bst._root = nullptr;
}

template <typename T>
BinarySearchTree<T>::~BinarySearchTree()
{
	makeEmpty();
	_root = nullptr;
}

template <typename T>
BinarySearchTree<T>& BinarySearchTree<T>::operator =(const BinarySearchTree<T>& bst) noexcept
{
	BinarySearchTree<T> copy = bst;
	std::swap(*this, copy);
	return *this;
}

template <typename T>
BinarySearchTree<T>& BinarySearchTree<T>::operator =(BinarySearchTree<T>&& bst) noexcept
{
	std::swap(_root, bst._root);
	return *this;
}

// Find the smallest item in the tree.
// Throw UnderflowException if empty.
template <typename T>
const T& BinarySearchTree<T>::findMin() const
{
	if(isEmpty())
	{
		throw UnderflowException((char*) "trying to find a value in empty tree");
	}
	return findMin(_root)->getData();
}

// Find the largest item in the tree.
// Throw UnderflowException if empty.
template <typename T>
const T& BinarySearchTree<T>::findMax() const
{
	if(isEmpty())
	{
		throw UnderflowException((char*) "trying to find a value in empty tree");
	}
	return findMax(_root)->getData();
}

template <typename T>
BinaryNode<T>** BinarySearchTree<T>::findMinRef()
{
	return findMinRef(&_root);
}

// the function which tests for the branch heights and assures the tree is
// an AVL tree (balanced)
template <typename T>
void BinarySearchTree<T>::balance()
{
	if (_root == nullptr)
	{
		return;
	}
	// TODO fill in this function
	removeLazyMarkedNodes();
	balanceTreeOnAVL();
}

template <typename T>
void BinarySearchTree<T>::removeLazyMarkedNodes()
{
	BetterStack<BinaryNode<T>**> traversalStack;
	BinaryNode<T>** currNodePtrPtr = &_root;
	traversalStack.push(currNodePtrPtr);
	vector<BinaryNode<T>**> nodeListForTraversal;
	
	while (!traversalStack.empty())
	{
		currNodePtrPtr = traversalStack.pop();
		nodeListForTraversal.push_back(currNodePtrPtr);
		
		if ((*currNodePtrPtr)->getLeft() != nullptr)
		{
			traversalStack.push((*currNodePtrPtr)->getLeftRef());
		}
		if ((*currNodePtrPtr)->getRight() != nullptr)
		{
			traversalStack.push((*currNodePtrPtr)->getRightRef());
		}
	}
	
	while (!nodeListForTraversal.empty())
	{
		currNodePtrPtr = nodeListForTraversal.back();
		nodeListForTraversal.pop_back();
		
		//~ cout << "checking node: " << (*currNodePtrPtr)->getData() << endl;
		//~ printTree(cout, true);
		
		if ((*currNodePtrPtr)->isMarkedForDeletion())
		{
			// start the deletion process
			if ((*currNodePtrPtr)->isLeafNode())
			{
				// this the leaf node, with no child, and it is marked for deletion.
				// delete it and set it to null
				delete *currNodePtrPtr;
				*currNodePtrPtr = nullptr;
				_numDeleted--;
				_numNodes--;
			}
			else if (!(*currNodePtrPtr)->hasLeft())
			{
				// this node has a right child, and it is marked for deletion
				// set it's right child to itself and delete this ptr
				BinaryNode<T>** rightChild = (*currNodePtrPtr)->getRightRef();
				std::swap(*currNodePtrPtr, *rightChild);
				delete *rightChild;
				*rightChild = nullptr;
				_numDeleted--;
				_numNodes--;
			}
			else if (!(*currNodePtrPtr)->hasRight())
			{
				// this node has a left child, and it is marked for deletion
				// set it's left child to itself and delete this ptr
				BinaryNode<T>** leftChild = (*currNodePtrPtr)->getLeftRef();
				std::swap(*currNodePtrPtr, *leftChild);
				delete *leftChild;
				*leftChild = nullptr;
				_numDeleted--;
				_numNodes--;
			}
			else
			{
				// this node has two children, and is marked for deletion
				// goto the min of right subtree and swap it with the deleted node
				// there's a chance that this child has right subtree therefore
				// save the curr right
				// set the curr left to min left
				// set the curr right to min right 
				// push the right subtree one level up before deleting this node
				// set the min to saved curr right
				BinaryNode<T>** rightSubtreeMinNode = findMinRef((*currNodePtrPtr)->getRightRef());
				
				// we need to check if the returned min is the immediate child
				// if it is, then only fix left connections
				bool isImmediateChild = false;
				if (*rightSubtreeMinNode == (*currNodePtrPtr)->getRight())
				{
					isImmediateChild = true;
				}
				
				// swap to be deleted node with min node
				std::swap(*currNodePtrPtr, *rightSubtreeMinNode);
				
				// fix new parent node's connections
				(*currNodePtrPtr)->setLeft((*rightSubtreeMinNode)->getLeft());
				
				if (isImmediateChild)
				{
					// just set left connection and delete the node
					delete *rightSubtreeMinNode;
					*rightSubtreeMinNode = nullptr;
				}
				else
				{
					// save right child of node new parent
					BinaryNode<T>* rightOfMin = (*currNodePtrPtr)->getRight();
					// save target node to delete to a temp location
					// it's pointer will modified
					BinaryNode<T>* nodeToDelete = *rightSubtreeMinNode;
					
					// set the target node's pointer to it's right child
					// in other words push the right subtree one level up
					*rightSubtreeMinNode = rightOfMin;
					(*currNodePtrPtr)->setRight(nodeToDelete->getRight());
					delete nodeToDelete;
				}
				
				_numDeleted--;
				_numNodes--;
			}
		}
	}
}

// Returns true if x is found in the tree.
template <typename T>
bool BinarySearchTree<T>::contains(const T& x) const
{
	return contains(x, _root);
}

// Print the tree contents in sorted order.
template <typename T>
void BinarySearchTree<T>::printTree(ostream& out, bool printDeleted) const
{
	if(isEmpty())
	{
		out << "Empty tree" << endl;
	}
	else
	{
		out << "NUM DELETED = " << _numDeleted << endl;
		out << "NUM TOTAL NODES = " << _numNodes << endl;
		out << "NUM VALID NODES = " << _numNodes - _numDeleted << endl;
		printTree(_root, out, printDeleted);
	}
}

// it is O(n) operation because all nodes will have to be checked
template <typename T>
bool BinarySearchTree<T>::isBalancedOnAVL()
{
	return true;
}

template <typename T>
void BinarySearchTree<T>::balanceTreeOnAVL()
{
	computeHeightForAVL();
	applyAVL();
}

template <typename T>
void BinarySearchTree<T>::applyAVL()
{
	queue<BinaryNode<T>**> traversalQueue;
	BinaryNode<T>** currNodePtrPtr = &_root;
	traversalQueue.push(currNodePtrPtr);
	vector<BinaryNode<T>**> nodeListForTraversal;
	
	while (!traversalQueue.empty())
	{
		currNodePtrPtr = traversalQueue.front();
		traversalQueue.pop();
		nodeListForTraversal.push_back(currNodePtrPtr);
		
		if ((*currNodePtrPtr)->hasLeft())
		{
			traversalQueue.push((*currNodePtrPtr)->getLeftRef());
		}
		if ((*currNodePtrPtr)->hasRight())
		{
			traversalQueue.push((*currNodePtrPtr)->getRightRef());
		}
	}
	
	while (!nodeListForTraversal.empty())
	{
		currNodePtrPtr = nodeListForTraversal.back();
		nodeListForTraversal.pop_back();
		
		cout << "[ ";
		cout << (*currNodePtrPtr)->getData() << ": ";
		cout << (*currNodePtrPtr)->getLeftHeight() << ", ";
		cout << (*currNodePtrPtr)->getRightHeight();
		cout << " ]" << endl;
		
		checkapplyAVLRotations(currNodePtrPtr);
	}
}

template <typename T>
void BinarySearchTree<T>::computeHeightForAVL()
{
	queue<BinaryNode<T>**> traversalQueue;
	BinaryNode<T>** currNodePtrPtr = &_root;
	traversalQueue.push(currNodePtrPtr);
	vector<BinaryNode<T>**> nodeListForTraversal;
	
	while (!traversalQueue.empty())
	{
		currNodePtrPtr = traversalQueue.front();
		traversalQueue.pop();
		nodeListForTraversal.push_back(currNodePtrPtr);
		
		if ((*currNodePtrPtr)->hasLeft())
		{
			traversalQueue.push((*currNodePtrPtr)->getLeftRef());
		}
		if ((*currNodePtrPtr)->hasRight())
		{
			traversalQueue.push((*currNodePtrPtr)->getRightRef());
		}
	}
	
	cout << "[ ";
	for (int ii = 0; ii < nodeListForTraversal.size(); ii++)
	{
		cout << (*nodeListForTraversal[ii])->getData() << ", ";
	}
	cout << " ]" << endl;
	
	while (!nodeListForTraversal.empty())
	{
		currNodePtrPtr = nodeListForTraversal.back();
		nodeListForTraversal.pop_back();
		
		if ((*currNodePtrPtr)->isLeafNode())
		{
			(*currNodePtrPtr)->setLeftHeight(0);
			(*currNodePtrPtr)->setRightHeight(0);
		}
		else if (!(*currNodePtrPtr)->hasLeft())
		{
			(*currNodePtrPtr)->setLeftHeight(0);
			(*currNodePtrPtr)->setRightHeight((*currNodePtrPtr)->getRight()->getHighestHeight() + 1);
		}
		else if (!(*currNodePtrPtr)->hasRight())
		{
			(*currNodePtrPtr)->setLeftHeight((*currNodePtrPtr)->getLeft()->getHighestHeight() + 1);
			(*currNodePtrPtr)->setRightHeight(0);
		}
		else
		{
			(*currNodePtrPtr)->setLeftHeight((*currNodePtrPtr)->getLeft()->getHighestHeight() + 1);
			(*currNodePtrPtr)->setRightHeight((*currNodePtrPtr)->getRight()->getHighestHeight() + 1);
		}
	}
}

template <typename T>
void BinarySearchTree<T>::checkapplyAVLRotations(BinaryNode<T>** node)
{
	BinaryNode<T>** child;
	BinaryNode<T>** grandChild;
	if (is_LR_imbalanced(*node))
	{
		child = (*node)->getLeftRef();
		grandChild = (*child)->getRightRef();
		avl_LR_rotation(node, child, grandChild);
	}
	else if (is_RL_imbalanced(*node))
	{
		child = (*node)->getRightRef();
		grandChild = (*child)->getLeftRef();
		avl_RL_rotation(node, child, grandChild);
	}
	else if (is_L_imbalanced(*node))
	{
		child = (*node)->getLeftRef();
		grandChild = (*child)->getLeftRef();
		avl_R_rotation(node, child, grandChild);
	}
	else if (is_R_imbalanced(*node))
	{
		child = (*node)->getRightRef();
		grandChild = (*child)->getRightRef();
		avl_L_rotation(node, child, grandChild);
	}
	else
	{
		// not imbalanced
	}
}

template <typename T>
bool BinarySearchTree<T>::is_LR_imbalanced(BinaryNode<T>* node)
{
	if (node->isLeftImbalanced(AVL_BALANCE_FACTOR))
	{
		return node->hasLeft() && node->getLeft()->hasRight();
	}
	
	return false;
}

template <typename T>
bool BinarySearchTree<T>::is_RL_imbalanced(BinaryNode<T>* node)
{
	if (node->isRightImbalanced(AVL_BALANCE_FACTOR))
	{
		return node->hasRight() && node->getRight()->hasLeft();
	}
	
	return false;
}

template <typename T>
bool BinarySearchTree<T>::is_L_imbalanced(BinaryNode<T>* node)
{
	if (node->isLeftImbalanced(AVL_BALANCE_FACTOR))
	{
		return node->hasLeft() && !node->getLeft()->hasRight();
	}
	
	return false;
}

template <typename T>
bool BinarySearchTree<T>::is_R_imbalanced(BinaryNode<T>* node)
{
	if (node->isRightImbalanced(AVL_BALANCE_FACTOR))
	{
		return node->hasRight() && !node->getRight()->hasLeft();
	}
	
	return false;
}

template <typename T>
void BinarySearchTree<T>::rotationL(BinaryNode<T>** pivot)
{
	cout << "performing rotation L" << endl;
	BinaryNode<T>* originalPivot = *pivot;
	
	*pivot = (*pivot)->getRight();	// pivot becomes parent
	BinaryNode<T>* leftSubtreeOfNewPivot = (*pivot)->getLeft();
	(*pivot)->setLeft(originalPivot);	// pull becomes right child of pivot
	// now we want to attach right subtree of child to left of right child
	(*pivot)->getLeft()->setRight(leftSubtreeOfNewPivot);
}

template <typename T>
void BinarySearchTree<T>::rotationR(BinaryNode<T>** pivot)
{
	cout << "performing rotation R" << endl;
	BinaryNode<T>* originalPivot = *pivot;
	
	*pivot = (*pivot)->getLeft();	// pivot becomes parent
	BinaryNode<T>* rightSubtreeOfNewPivot = (*pivot)->getRight();
	(*pivot)->setRight(originalPivot);	// pull becomes right child of pivot
	// now we want to attach right subtree of child to left of right child
	(*pivot)->getRight()->setLeft(rightSubtreeOfNewPivot);
}

template <typename T>
void BinarySearchTree<T>::avl_LR_rotation(BinaryNode<T>** parent, 
											BinaryNode<T>** child, 
											BinaryNode<T>** grandChild)
{
	cout << "performing avl_LR_rotation" << endl;
	rotationL(child);
	computeHeightForAVL();
	rotationR(parent);
	computeHeightForAVL();
}

template <typename T>
void BinarySearchTree<T>::avl_RL_rotation(BinaryNode<T>** parent, 
											BinaryNode<T>** child, 
											BinaryNode<T>** grandChild)
{
	cout << "performing avl_RL_rotation" << endl;
	rotationR(child);
	computeHeightForAVL();
	rotationL(parent);
	computeHeightForAVL();
}

template <typename T>
void BinarySearchTree<T>::avl_L_rotation(BinaryNode<T>** parent, 
											BinaryNode<T>** child, 
											BinaryNode<T>** grandChild)
{
	cout << "performing avl_L_rotation" << endl;
	rotationL(parent);
	computeHeightForAVL();
}

template <typename T>
void BinarySearchTree<T>::avl_R_rotation(BinaryNode<T>** parent, 
											BinaryNode<T>** child, 
											BinaryNode<T>** grandChild)
{
	cout << "performing avl_R_rotation" << endl;
	rotationR(parent);
	computeHeightForAVL();
}

// Make the tree logically empty.
template <typename T>
void BinarySearchTree<T>::makeEmpty()
{
	makeEmpty(_root);
	_root = nullptr;
}

// Insert x into the tree; duplicates are ignored.
template <typename T>
void BinarySearchTree<T>::insert(const T& x)
{
	if (isEmpty())
	{
		_root = new BinaryNode<T>(x);
		_numNodes++;
	}
	else
	{
		insert(x, _root);
	}
}

// Insert x into the tree; duplicates are ignored.
template <typename T>
void BinarySearchTree<T>::insert(T&& x)
{
	if (isEmpty())
	{
		_root = new BinaryNode<T>(std::move(x));
		_numNodes++;
	}
	else
	{
		insert(std::move(x), _root);
	}
}

// Remove x from the tree. Nothing is done if x is not found.
template <typename T>
void BinarySearchTree<T>::remove(const T& x)
{
	remove(x, _root);
}

/*
	Internal methods to insert into a subtree.
	x is the item to insert.
	t is the node that roots the subtree.
	Set the new _root of the subtree.
 */
template <typename T>
void BinarySearchTree<T>::insert(const T& x, BinaryNode<T>* t)
{
	if(t == nullptr)
	{
		return;
	}
	else if(x < t->getData())
	{
		if (t->hasLeft())
		{
			insert(x, t->getLeft());
		}
		else
		{
			t->setLeft(new BinaryNode<T>(x));
			_numNodes++;
		}
	}
	else if(t->getData() < x)
	{
		if (t->hasRight())
		{
			insert(x, t->getRight());
		}
		else
		{
			t->setRight(new BinaryNode<T>(x));
			_numNodes++;
		}
	}
	else
	{
		// Duplicate; flip the flag
		t->unmarkDeleted();
		_numDeleted--;
	}
}

template <typename T>
void BinarySearchTree<T>::insert(T&& x, BinaryNode<T>* t)
{
	if(t == nullptr)
	{
		return;
	}
	else if(x < t->getData())
	{
		if (t->hasLeft())
		{
			insert(std::move(x), t->getLeft());
		}
		else
		{
			t->setLeft(new BinaryNode<T>(std::move(x)));
			_numNodes++;
		}
	}
	else if(t->getData() < x)
	{
		if (t->hasRight())
		{
			insert(std::move(x), t->getRight());
		}
		else
		{
			t->setRight(new BinaryNode<T>(std::move(x)));
			_numNodes++;
		}
	}
	else
	{
		// Duplicate; flip the flag
		t->unmarkDeleted();
		_numDeleted--;
	}
}

/*
	Internal method to remove from a subtree.
	x is the item to remove.
	t is the node that roots the subtree.
	Set the new root of the subtree.
*/
template <typename T>
void BinarySearchTree<T>::remove(const T& x, BinaryNode<T>* t)
{
	if(t == nullptr)
	{
		return;
	}
	else if(x < t->getData())
	{
		return remove(x, t->getLeft());
	}
	else if(t->getData() < x)
	{
		return remove(x, t->getRight());
	}
	else // Match
	{
		t->markDeleted();
		_numDeleted++;
		return;
	}
}

/*
	Internal method to find the smallest item in a subtree t.
	Return node containing the smallest item.
*/
//TODO:: check for the right node
template <typename T>
BinaryNode<T>* BinarySearchTree<T>::findMin(BinaryNode<T>* t) const
{
	BinaryNode<T>* minNode = nullptr;
	if (t == nullptr)
	{
		return minNode;
	}
	
	// check the left side first
	// min will be most likely to be found here unless it's empty
	if (t->hasLeft())
	{
		minNode = findMin(t->getLeft());
		
		if (minNode != nullptr)
			return minNode;
	}
	
	// since min is not on left, it could be this node
	// if it is not marked for deletion, min is this node
	// everything on the right will be bigger than this, this is min.
	if (!t->isMarkedForDeletion())
	{
		minNode = t;
		
		if (minNode != nullptr)
			return minNode;
	}
	
	// if min exists it has to be the right side now
	if (t->hasRight())
	{
		minNode = findMin(t->getRight());
		
		if (minNode != nullptr)
			return minNode;
	}
	
	return nullptr;
}

// this is BST delete helper
template <typename T>
BinaryNode<T>** BinarySearchTree<T>::findMinRef(BinaryNode<T>** t)
{
	if (t == nullptr || *t == nullptr)
	{
		return nullptr;
	}
	
	if (!(*t)->hasLeft())
	{
		// since left is null, this node will be the min. node
		// everything on the right will be bigger than this.
		
		// we don't need check if this is the deleted node because we
		// recursively applied the deletion from bottom most node, therefore
		// eveything below has been corrected, and there are no deleted nodes
		return t;
	}
	else
	{
		// left child has children, and keep going to left until we have found
		// a node with no more children on left
		return findMinRef((*t)->getLeftRef());
	}
}

/*
	Internal method to find the largest item in a subtree t.
	Return node containing the largest item.
*/
template <typename T>
BinaryNode<T>* BinarySearchTree<T>::findMax(BinaryNode<T>* t) const
{
	BinaryNode<T>* maxNode = nullptr;
	if (t == nullptr)
	{
		return maxNode;
	}
	
	// check the right side first
	// maxn will be most likely to be found here unless it's empty
	if (t->hasRight())
	{
		maxNode = findMax(t->getRight());
		
		if (maxNode != nullptr)
			return maxNode;
	}
	
	// since max is not on right, it could be this node
	// if it is not marked for deletion, max is this node
	// everything on the left will be smaller than this, this is max.
	if (!t->isMarkedForDeletion())
	{
		maxNode = t;
		
		if (maxNode != nullptr)
			return maxNode;
	}
	
	// if max exists it has to be the left side now
	if (t->hasLeft())
	{
		maxNode = findMax(t->getLeft());
		
		if (maxNode != nullptr)
			return maxNode;
	}
	
	return nullptr;
}

/*
	Internal method to test if an item is in a subtree.
	x is item to search for.
	t is the node that roots the subtree.
*/
template <typename T>
bool BinarySearchTree<T>::contains(const T& x, BinaryNode<T>* t) const
{
	if(t == nullptr)
	{
		return false;
	}
	else if(x < t->getData())
	{
		return contains(x, t->getLeft());
	}
	else if(t->getData() < x)
	{
		return contains(x, t->getRight());
	}
	else
	{
		return !t->isMarkedForDeletion();	// Match
	}
}

// Internal method to make subtree empty.
template <typename T>
void BinarySearchTree<T>::makeEmpty(BinaryNode<T>* t)
{
	if(t != nullptr)
	{
		makeEmpty(t->getLeft());
		makeEmpty(t->getRight());
		delete t;
	}
}

// Internal method to print a subtree rooted at t in sorted order.
template <typename T>
void BinarySearchTree<T>::printTree(BinaryNode<T>* t, ostream& out, bool printDeleted) const
{
	if(t != nullptr)
	{
		//~ t->debugPrint();
		printTree(t->getLeft(), out, printDeleted);
		
		if (t->isMarkedForDeletion())
		{
			if (printDeleted)
			{
				out << t->getData() << " --- X" << endl;
			}
		}
		else
		{
			out << t->getData() << endl;
		}
		printTree(t->getRight(), out, printDeleted);
	}
}

// Internal method to clone subtree.
template <typename T>
BinaryNode<T>* BinarySearchTree<T>::clone(BinaryNode<T>* t) const
{
	if(t == nullptr)
	{
		return nullptr;
	}
	else
	{
		return new BinaryNode<T>(t->getData(), clone(t->getLeft()), clone(t->getRight()));
	}
}

// Test if the tree is logically empty.
// Return true if empty, false otherwise.
template <typename T>
bool BinarySearchTree<T>::isEmpty() const noexcept
{
	if (_root == nullptr)
	{
		return true;
	}
	else
	{
		return _numNodes == _numDeleted;
	}
}

template <typename T>
void BinarySearchTree<T>::printLevelOrder()
{
	queue<BinaryNode<T>*> que;
	que.push(_root);
	BinaryNode<T>* node;
	
	cout << "printing level order" << endl;
	cout << "[ ";
	while(!que.empty())
	{
		node = que.front();
		que.pop();
		
		cout << node->getData() << ", ";
		if (node->hasLeft()) que.push(node->getLeft());
		if (node->hasRight()) que.push(node->getRight());
	}
	cout << " ]" << endl;
}

#endif
