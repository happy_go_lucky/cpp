#ifndef EXPR_ELEMENT_H
#define EXPR_ELEMENT_H

#include <iostream>
#include "FSM_Helper.h"

using std::ostream;

class ExprElement
{
	public:
		ExprElement() = delete;
		ExprElement(ElementType type) noexcept;
		ExprElement(const ExprElement& val) = default;
		
		virtual ~ExprElement() = default;
		
		ExprElement& operator =(const ExprElement& val) = default;
		
		inline ElementType getType() const
		{
			return _type;
		}
		
		friend ostream& operator <<(ostream& out, const ExprElement& elem);
		
	private:
		ElementType _type;
};

#endif
