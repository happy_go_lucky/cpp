#include "c_string_fxns.h"

int strLen( const char* str )
{
	int ii = 0;
	while ( *( str + ii ) != '\0' )
	{
		ii++;
	}
	return ii;
}

/**
 * returns a string strA + strB
 * */
char* cStringConcatinate( const char* strA, const char* strB )
{
	char* newStr = malloc( sizeof(char) * ( strLen( strA ) + strLen( strB ) + 1 ) );
	int ii = 0;
	while ( *(strA + ii ) != '\0' )
	{
		*( newStr + ii ) = *( strA + ii );
		ii++;
	}
	int jj = 0;
	while ( *(strB + jj ) != '\0' )
	{
		*( newStr + ii ) = *( strB + jj );
		ii++;
		jj++;
	}
	*( newStr + ii ) = '\0';
	return newStr;
}

bool cStringCompare( const char* strA, const char* strB )
{
	int lenStrA = strLen( strA );
	int lenStrB = strLen( strB );
	
	if ( lenStrA != lenStrB )
	{
		return false;
	}
	
	int ii = 0;
	while ( *( strA + ii ) != '\0' )
	{
		if ( *( strA + ii ) != *( strB + ii ) )
			return false;
		ii++;
	}
	
	return true;
}

bool cStringCompareChar( const char* strA, const char strB )
{
	return ( ( strLen( strA ) == 1 ) && ( *strA == strB ) );
}
