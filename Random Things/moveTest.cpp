
#include <iostream>
#include <memory> // for smart ptrs

using namespace std;

class TestClass
{
	public:
		TestClass() = default;
		~TestClass() = default;
		
		TestClass(int value)
		{
			_member = value;
		}
		
		TestClass(const TestClass& obj) = delete;
		TestClass(TestClass&& obj)
		{
			_member = std::move(obj._member);
		}
		
		TestClass& operator =(const TestClass& obj) = delete;
		TestClass& operator =(TestClass&& obj)
		{
			_member = std::move(obj._member);
            return *this;
		}
		
		void print() const noexcept
		{
			std::cout << "The spanshot of object" << std::endl;
			std::cout << "address: " << this << std::endl;
			std::cout << "member: " << _member << std::endl;
		}
	
	private:
		int _member;
};

int main()
{
	int value1 = 2;
	int& valueRef1 = value1;
	const int& valueRef2 = std::move(int(10));
	const int& valueRef3 = std::move(*(new int(12)));
	
	std::cout << "value1: " << value1 << std::endl;
	std::cout << "valueRef1: " << valueRef1 << std::endl;
	std::cout << "valueRef2: " << valueRef2 << std::endl;
	std::cout << "valueRef3: " << valueRef3 << std::endl;
	
	TestClass obj(7);
    TestClass objCopy;
    obj.print();
    
    objCopy = std::move(obj);
    objCopy.print();

	return 0;
}
