#include "LCD.h"

const int LCD::LCD_BLOCK[NUM_ROWS * NUM_COLS] = { 
      VAL_BLOCK_1_1, VAL_BLOCK_1_2, VAL_BLOCK_1_3, VAL_BLOCK_1_4, VAL_BLOCK_1_5, VAL_BLOCK_1_6, VAL_BLOCK_1_7, VAL_BLOCK_1_8,
      VAL_BLOCK_1_9, VAL_BLOCK_1_10, VAL_BLOCK_1_11, VAL_BLOCK_1_12, VAL_BLOCK_1_13, VAL_BLOCK_1_14, VAL_BLOCK_1_15, VAL_BLOCK_1_16,
      VAL_BLOCK_2_1, VAL_BLOCK_2_2, VAL_BLOCK_2_3, VAL_BLOCK_2_4, VAL_BLOCK_2_5, VAL_BLOCK_2_6, VAL_BLOCK_2_7, VAL_BLOCK_2_8, 
      VAL_BLOCK_2_9, VAL_BLOCK_2_10, VAL_BLOCK_2_11, VAL_BLOCK_2_12, VAL_BLOCK_2_13, VAL_BLOCK_2_14, VAL_BLOCK_2_15, VAL_BLOCK_2_16
    };

LCD::LCD( const int pin, const int serialBaudRate )
{
  _pin = pin;
  _serialBaudRate = serialBaudRate;

  // Attach the serial display's RX line to digital pin 2
  _lcd = new SoftwareSerial(3, _pin); // pin 2 = TX, pin 3 = RX (unused)
  _lcd->begin( _serialBaudRate );  // set up serial port for serial baud rate
  //writeString( "JAMBOX INIT...", BLOCK_1_1 );
}

LCD::~LCD()
{
  //delete _lcd;
}

void LCD::writeString( const char* str, const int cursorPos ) const
{
  _lcd->write( 254 ); // cursor to beginning of first line
  _lcd->write( LCD_BLOCK[cursorPos] );
  _copyStr(str, _stringToWrite);
  _lcd->write( _stringToWrite );
}

void LCD::clearLCD() const
{
  //moveCursorTo( BLOCK_1_1 );
  //writeString( "                                " );  // NUM_ROWS * NUM_COLS number of spaces 
}

void LCD::_copyStr( const char* src, char* dest )
{
  int ii = 0;
  while ( *( src + ii ) != '\0' )
  {
    *( dest + ii ) = *( src + ii );
    ii++;
  }
  
  while ( ii < ( NUM_ROWS * NUM_COLS ) )
  {
    *( dest + ii ) = ' ';
    ii++;
  }

  *( dest + ii ) = '\0';
}

