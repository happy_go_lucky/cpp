#ifndef DETERMINANT_HELPER_H
#define DETERMINANT_HELPER_H

#include <iostream>

using std::cout;
using std::endl;

class DeterminantHelper
{
    public:
        const unsigned INVALID_ENTRY = 1;
        const unsigned VALID_ENTRY = 0;
        DeterminantHelper( const unsigned, const unsigned );
        DeterminantHelper( const DeterminantHelper& );
        ~DeterminantHelper();

        void _resetMap();
        unsigned* getRowMap() const;
        unsigned* getColMap() const;
        unsigned getRows() const;
        unsigned getCols() const;
        void setInvalidRowAndCol( const unsigned, const unsigned );
        void setValidRowAndCol( const unsigned, const unsigned );
        unsigned getFirstRow() const;
        unsigned getFirstCol() const;
        unsigned getLastRow_2x2() const;
        unsigned getLastCol_2x2() const;
        void printData() const;
    
    private:
        unsigned* _rowMap;
        unsigned* _columnMap;

        unsigned _rows;
        unsigned _cols;

        void _copyMap( unsigned* const, unsigned* const, unsigned );
};

#endif