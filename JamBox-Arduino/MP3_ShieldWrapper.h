#ifndef MP3_SHILED_WRAPPER_H
#define MP3_SHILED_WRAPPER_H

#include "defines.h"
#include <SD.h>
#include <SPI.h>
#include "Arduino.h"
#include <MusicPlayer.h>

class MP3_ShieldWrapper
{
  public:
    const static int INIT_SONG_NUMBER = 0;
    const static int TRACK_NAME_LENGTH = 9;
    const static int NUM_TRACKS = 3;
    
    MP3_ShieldWrapper();
    ~MP3_ShieldWrapper();
    
    static void loop();
    static void adjustVolume( void );
    int getTrackNumber() const;
    static void play();
    static void pause();
    static void next();
    static void prev();
    
  private:
    static int _songNumber;
};

#endif
