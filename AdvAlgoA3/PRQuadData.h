#ifndef PR_QUAD_DATA_H
#define PR_QUAD_DATA_H

#include <vector>
#include "QuadTreeRecord.h"
#include "HelperFunctions.h"

using namespace std;

class PRQuadData
{
	public:
		// define friendship
		friend class QuadTree;
	
		PRQuadData() noexcept;
		PRQuadData(const QuadTreeRecord& record);
		PRQuadData(const PRQuadData& node);
		~PRQuadData();
		
		PRQuadData& operator =(const PRQuadData& node);
		PRQuadData& operator =(const QuadTreeRecord& record);
		bool operator ==(const PRQuadData& node);
		bool operator ==(const QuadTreeRecord& record);
		friend ostream& operator <<(ostream& out, const PRQuadData& node);
		
		void setValue(const QuadTreeRecord& record);
		bool isNull() const noexcept;
		void updateOffset(size_t value);
		void updateOffset(const vector<size_t>& listOffset);
		DMSCoord getLat() const noexcept;
		DMSCoord getLong() const noexcept;
		const vector<size_t>& getOffsetList() const noexcept;
		bool isSameOffset(const PRQuadData& record) const;
		bool isSameCoords(const PRQuadData& record) const;
		
	private:
		vector<size_t> _offsetList;
		DMSCoord _lat;
		DMSCoord _long;
};

#endif
