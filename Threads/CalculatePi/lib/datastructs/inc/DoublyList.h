#ifndef DOUBLY_LIST_H
#define DOUBLY_LIST_H

#include <iostream>
#include <memory>
#include "DoublyListNode.h"
#include "DoublyListIterator.h"

namespace datastructs
{
	template <class Type>
	class DoublyList
	{
		public:
			enum class InsertPosition : int
			{
				BEFORE = 0,
				AFTER
			};
		
			DoublyList() noexcept;
			~DoublyList() noexcept;
			
			DoublyList(const DoublyList<Type>& list) noexcept;
			
			DoublyList<Type>& operator =(const DoublyList<Type>& list) & noexcept;
			
			bool operator ==(const DoublyList<Type>& list) const noexcept;
			bool operator !=(const DoublyList<Type>& list) const noexcept;
			
			template <typename U>
			friend std::ostream& operator <<(std::ostream& out, 
					const DoublyList<U>& list) noexcept;
			
			void appendNode(const Type& data) noexcept;
			void appendNode(Type&& data) noexcept;
			void prependNode(const Type& data) noexcept;
			void prependNode(Type&& data) noexcept;
			void insertNode(const Type& data, DoublyListIterator<Type>& itr, 
					DoublyList::InsertPosition position) noexcept;
			
			void deleteNode(const DoublyListIterator<Type>& itr) noexcept;
			void deleteNodeAtBack() noexcept;
			void deleteNodeAtFront() noexcept;
			
			// check for iterator's validity if the node is not found
			DoublyListIterator<Type> findNode(const Type& value) const noexcept;
			bool search(const Type& value) const noexcept;
			void emptyList() & noexcept;
			
			inline bool isEmpty() const noexcept
			{
				return _headPtr == nullptr && _headPtr == _tailPtr;
			}
			
			inline int size() const noexcept
			{
				return _size;
			}
			
			DoublyListIterator<Type> head() const;
			DoublyListIterator<Type> tail() const;
		
		private:
			DoublyListNode<Type>* _headPtr;
			DoublyListNode<Type>* _tailPtr;
			int _size;
			
			void _deleteNode(DoublyListNode<Type>* node);
			void _deleteNode(DoublyListIterator<Type>& itr);
	};
}
#endif
