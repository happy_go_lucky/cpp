#ifndef GPIO_DEFINES_H
#define GPIO_DEFINES_H

#include <stdint.h>
#include <unistd.h>

namespace gpio
{
	typedef uint8_t uByte;
	typedef uint16_t uWord;
	typedef uint32_t uDoubleWord;
	
	static const short MAX_PINS = 40;
	static const short NUM_INVALID_PINS = 12;
	// an ugly performance optiomization by limiting buffer size
	static const int GPIO_COMMAND_BUFFER_LEN = 100;
	static const int GPIO_EXPORT_TIME = 100'000;
	
	// these commands are representative of files in unix
	// the followiing is the full path 
	// GPIO_PATH + GPIO_PIN_NAME_STR + CMD
	static constexpr const char* GPIO_EXPORT_CMD = "export";
	static constexpr const char* GPIO_UNEXPORT_CMD = "unexport";
	
	// the followiing is the full path 
	// GPIO_PATH + GPIO_PIN_NAME_STR + _pinNumber + OS_DIRECTORY_CHAR + CMD
	static constexpr const char* GPIO_MODE_CMD = "direction";
	static constexpr const char* GPIO_VALUE_CMD = "value";
	static constexpr const char* GPIO_EDGE_CMD = "edge";
	static constexpr const char* GPIO_ACTIVE_LOW_CMD = "active_low";
	
	static constexpr const char* GPIO_MODE_IN = "in";
	static constexpr const char* GPIO_MODE_OUT = "out";
	static constexpr const char* GPIO_EDGE_NONE = "none";
	static constexpr const char* GPIO_EDGE_RISING = "rising";
	static constexpr const char* GPIO_EDGE_FALLING = "falling";
	static constexpr const char* GPIO_EDGE_BOTH = "both";
	
	static constexpr const char* VALUE_LOW_STR = "0";
	static constexpr const char* VALUE_HIGH_STR = "1";
	
	enum class InvalidPins : unsigned char
	{
		PIN_1 = 1,		// 3.3V
		PIN_2 = 2,		// 5V
		PIN_4 = 4,		// 5V
		PIN_6 = 6,		// GND
		PIN_9 = 9,		// GND
		PIN_14 = 14,	// GND
		PIN_17 = 17,	// 3.3V
		PIN_20 = 20,	// GND
		PIN_25 = 25,	// GND
		PIN_30 = 30,	// GND
		PIN_34 = 34,	// GND
		PIN_39 = 39		// GND
	};

	enum class PinNumber : unsigned char
	{
		PIN_INVALID = 0,
		//~ PIN_1,	// 3.3V
		//~ PIN_2,	// 5V
		PIN_3 = 3,
		//~ PIN_4,	// 5V
		PIN_5 = 5,
		//~ PIN_6,	// GND
		PIN_7 = 7,
		PIN_8,
		//~ PIN_9,	// GND
		PIN_10 = 10,
		PIN_11,
		PIN_12,
		PIN_13,
		//~ PIN_14,	// GND
		PIN_15 = 15,
		PIN_16,
		//~ PIN_17,	// 3.3V
		PIN_18 = 18,
		PIN_19,
		//~ PIN_20,	// GND
		PIN_21 = 21,
		PIN_22,
		PIN_23,
		PIN_24,
		//~ PIN_25,	// GND
		PIN_26 = 26,
		PIN_27,	// ID_SD/Do not connect
		PIN_28,	// ID_SC/Do not connect
		PIN_29,
		//~ PIN_30,	// GND
		PIN_31 = 31,
		PIN_32,
		PIN_33,
		//~ PIN_34,	// GND
		PIN_35 = 35,
		PIN_36,
		PIN_37,
		PIN_38,
		//~ PIN_39,	// GND
		PIN_40 = 40
	};
	
	enum class GpioNumber : unsigned char
	{
		GPIO_INVALID = 0,
		GPIO_2 = 2,	// SDA
		GPIO_3,	// SCL
		GPIO_4,	// GPCLK0
		GPIO_5,
		GPIO_6,
		GPIO_7,	// SPIO CE1
		GPIO_8,	// SPIO CE0
		GPIO_9,	// SPIO MISO
		GPIO_10,	// SPIO MOSI
		GPIO_11,	// SPIO SCLK
		GPIO_12,	// PWM0
		GPIO_13,	// PWM1
		GPIO_14,	// TXD
		GPIO_15,	// RXD
		GPIO_16,	// SPI1 CE2
		GPIO_17,	// SPI1 CE1
		GPIO_18,	// SPI1 CE0
		GPIO_19,	// SPI1 MISO
		GPIO_20,	// SPI1 MOSI
		GPIO_21,	// SPI1 SCLK
		GPIO_22,
		GPIO_23,
		GPIO_24,
		GPIO_25,
		GPIO_26,
		GPIO_27
	};

	enum class GpioToPinNumber : unsigned char
	{
		GPIO_INVALID = 0,
		GPIO_2 = 3,
		GPIO_3 = 5,
		GPIO_4 = 7,
		GPIO_5 = 29,
		GPIO_6 = 31,
		GPIO_7 = 26,
		GPIO_8 = 24,
		GPIO_9 = 21,
		GPIO_10 = 19,
		GPIO_11 = 23,
		GPIO_12 = 32,
		GPIO_13 = 33,
		GPIO_14 = 8,
		GPIO_15 = 10,
		GPIO_16 = 36,
		GPIO_17 = 11,
		GPIO_18 = 12,
		GPIO_19 = 35,
		GPIO_20 = 38,
		GPIO_21 = 40,
		GPIO_22 = 15,
		GPIO_23 = 16,
		GPIO_24 = 18,
		GPIO_25 = 22,
		GPIO_26 = 37,
		GPIO_27 = 13
	};
	
	enum class GpioOperationPin : unsigned char
	{
		GPIO_4 = 4,
		GPIO_5,
		GPIO_6,
		GPIO_12 = 12,
		GPIO_13,
		GPIO_16 = 16,
		GPIO_17,
		GPIO_18,
		GPIO_22 = 22,
		GPIO_23,
		GPIO_24,
		GPIO_25,
		GPIO_26,
		GPIO_27
	};
	
	enum class SpiOperationPin : unsigned char
	{
		SPI0_MOSI = 10,
		SPI0_MISO = 9,
		SPI0_SCLK = 11,
		SPI0_CE0 = 8,
		SPI0_CE1 = 7,
		SPI1_MISO = 19,
		SPI1_MOSI = 20,
		SPI1_CLK = 21
	};
	
	enum class I2COperationPin : unsigned char
	{
		SDA = 2,
		SCL = 4
	};
	
	enum class UARTOperationPin : unsigned char
	{
		TXD = 14,
		RXD = 15
	};
	
	const InvalidPins ARR_INVALID_PINS[] = 
	{
		InvalidPins::PIN_1,
		InvalidPins::PIN_2,
		InvalidPins::PIN_4,
		InvalidPins::PIN_6,
		InvalidPins::PIN_9,
		InvalidPins::PIN_14,
		InvalidPins::PIN_17,
		InvalidPins::PIN_20,
		InvalidPins::PIN_25,
		InvalidPins::PIN_30,
		InvalidPins::PIN_34,
		InvalidPins::PIN_39
	};

	enum class SignalMode : unsigned char
	{
		INPUT = 0,
		OUTPUT
	};
	
	enum class SignalEdge : unsigned char
	{
		RISING,
		FALLING,
		BOTH,
		NONE
	};
	
	enum class SignalValue : unsigned char
	{
		LOW = 0,
		HIGH
	};
	
	enum class GpioCommand : unsigned char
	{
		EXPORT_PIN,
		UNEXPORT_PIN,
		SIGNAL_MODE,
		SIGNAL_EDGE,
		SIGNAL_VALUE,
		ACTIVE_LOW
	};
	
	enum class SpiMode : unsigned char
	{
		MODE_0,
		MODE_1,
		MODE_2,
		MODE_3
	};

	inline const char* getCommandStr(GpioCommand command)
	{
		switch (command)
		{
			case GpioCommand::EXPORT_PIN:
			return GPIO_EXPORT_CMD;
			
			case GpioCommand::UNEXPORT_PIN:
			return GPIO_UNEXPORT_CMD;
			
			case GpioCommand::SIGNAL_MODE:
			return GPIO_MODE_CMD;
			
			case GpioCommand::SIGNAL_EDGE:
			return GPIO_EDGE_CMD;
			
			case GpioCommand::SIGNAL_VALUE:
			return GPIO_VALUE_CMD;
			
			case GpioCommand::ACTIVE_LOW:
			return GPIO_ACTIVE_LOW_CMD;
			
			default:
			return nullptr;
		}
	}

	inline const char* getSignalValueStr(SignalValue value)
	{
		switch(value)
		{
			case SignalValue::LOW:
			return VALUE_LOW_STR;
			
			case SignalValue::HIGH:
			return VALUE_HIGH_STR;
			
			default:
			return nullptr;
		}
	}

	inline const char* getSignalModeStr(SignalMode value)
	{
		switch (value)
		{
			case SignalMode::INPUT:
			return GPIO_MODE_IN;
			
			case SignalMode::OUTPUT:
			return GPIO_MODE_OUT;
			
			default:
			return nullptr;
		}
	}

	inline const char* getSignalEdgeStr(SignalEdge value)
	{
		switch (value)
		{
			case SignalEdge::RISING:
			return GPIO_EDGE_RISING;
			
			case SignalEdge::FALLING:
			return GPIO_EDGE_FALLING;
			
			case SignalEdge::BOTH:
			return GPIO_EDGE_BOTH;
			
			case SignalEdge::NONE:
			default:
			return GPIO_EDGE_NONE;
		}
	}
	
	inline bool isValidPinNumber(PinNumber pinNumber) noexcept
	{
		if (pinNumber < PinNumber::PIN_3 || pinNumber > PinNumber::PIN_40)
		{
			return false;
		}
		
		for (short ii = 0; ii < NUM_INVALID_PINS; ii++)
		{
			if (pinNumber == (PinNumber) ARR_INVALID_PINS[ii])
			{
				return false;
			}
		}
		
		return true;
	}
	
	inline bool isValidGpioNumber(GpioNumber gpioNumber) noexcept
	{
		return isValidPinNumber((PinNumber) gpioNumber);
	}
	
	inline void sleepForMillis(unsigned int time)
	{
		ulseep(time * 1000);
	}
}

#endif
