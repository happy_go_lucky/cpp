#ifndef CIRCULAR_QUEUE_H
#define CIRCULAR_QUEUE_H

#include <iostream>
#include <cstring>

using namespace std;

template <class Object>
class CircularQueue
{
	public:
		CircularQueue(size_t s);
		CircularQueue(const CircularQueue<Object>& aQueue);
		CircularQueue(const CircularQueue<Object>&& aQueue);
		CircularQueue(initializer_list<Object> list);
		~CircularQueue();
		
		CircularQueue& operator =(const CircularQueue<Object>& aQueue);
		CircularQueue& operator =(const CircularQueue<Object>&& aQueue) noexcept;
		template <typename T>
		friend ostream& operator <<(ostream& out, const CircularQueue<T>& aQueue) noexcept;
		template <typename T>
		friend ostream& operator <<(ostream& out, const CircularQueue<T>&& aQueue) noexcept;
		
		void enque(const Object& obj);
		void enque(Object&& obj);
		Object deque();
		
		inline Object* front() const noexcept
		{
			return &_values[_front];
		}
		
		inline Object* back() const noexcept
		{
			return &_values[_back];
		}
		
		inline bool empty() const noexcept
		{
			return _front == INVALID_INDEX;
		}
		
		inline bool full() const noexcept
		{
			return size() == _capacity;
		}
		
		size_t size() const noexcept;
		
		inline size_t capacity() noexcept
		{
			return _capacity;
		}
		
		void print() noexcept;
		void debugPrint() const noexcept;
		
	private:
		const static size_t INVALID_INDEX = -1;
		Object* _values;
		size_t _front;
		size_t _back;
		size_t _capacity;
		
		void increaseCapacity();
};

#endif
