#ifndef HELPER_FUNCTIONS_H
#define HELPER_FUNCTIONS_H

#include <iostream>
#include <fstream>
#include <vector>
#include "DDCoord.h"
#include "DMSCoord.h"

using namespace std;

namespace helpfxns
{
	inline DDCoord toDecimal(const DMSCoord& coord)
	{
		float value = coord.getDegrees() +
					((float) coord.getMinutes() / (float) 60) +
					((float) coord.getSeconds() / (float) 3600);

		// fix direction
		if (coord.getDirection() == DMSCoord::SOUTH ||
			coord.getDirection() == DMSCoord::WEST)
		{
			value = value * -1;
		}
		
		DDCoord alias(value);
		return alias;
	}
	
	template <typename T>
	inline void printArray(T* arr, size_t size)
	{
		for (size_t ii = 0; ii < size; ii++)
		{
			cout << arr[ii] << ", " << endl;
		}
	}
	
	template <typename T>
	inline void printVector(const vector<T>& vec)
	{
		for (size_t ii = 0; ii < vec.size(); ii++)
		{
			cout << vec[ii] << ", " << endl;
		}
	}
	
	template <typename T>
	inline void printVector(const vector<T>& vec, fstream* logFile)
	{
		for (size_t ii = 0; ii < vec.size(); ii++)
		{
			(*logFile) << vec[ii] << ", " << endl;
		}
	}
	
	inline bool isDirty_eof_bit(fstream* streamObj)
	{
		return (streamObj->rdstate() & ifstream::eofbit) != 0;
	}
	
	inline bool isDirty_bad_bit(fstream* streamObj)
	{
		return (streamObj->rdstate() & ifstream::badbit) != 0;
	}
	
	inline bool isDirty_good_bit(fstream* streamObj)
	{
		return (streamObj->rdstate() & ifstream::goodbit) != 0;
	}
	
	inline bool isDirty_fail_bit(fstream* streamObj)
	{
		return (streamObj->rdstate() & ifstream::failbit) != 0;
	}
	
	inline void checkStreamErrors(fstream* streamObj)
	{
		if (isDirty_fail_bit(streamObj))
		{
			std::cerr << "stream error: failbit" << endl;
		}
		
		if (isDirty_good_bit(streamObj))
		{
			std::cerr << "stream error: goodbit" << endl;
		}
		
		if (isDirty_eof_bit(streamObj))
		{
			std::cerr << "stream error: eofbit" << endl;
		}
		
		if (isDirty_bad_bit(streamObj))
		{
			std::cerr << "stream error: badbit" << endl;
		}
	}
	
	/*
	inline DMSCoord toDMS(const DDCoord& coord)
	{
		// compute latitude
		short degrees = (unsigned short) coord.getDegrees();
		float minutes = (coord.getDegrees() - degrees) * 60;
		float seconds = (minutes - (short) minutes) * 60;
		// TODO: fix direction
	}
	* */
	}

#endif
