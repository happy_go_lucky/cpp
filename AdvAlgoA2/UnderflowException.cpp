#include "UnderflowException.h"

UnderflowException::UnderflowException( char* message )
{
	_message = message;
}

const char* UnderflowException::what() const throw()
{
	return _message;
}
