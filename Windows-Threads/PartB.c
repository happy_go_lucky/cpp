#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <windows.h>
#include <time.h>

#define INCREMENT_MAX 3
#define MAX_SLEEP_TIME 200
#define DECIMAL_BASE 10

HANDLE* threadArray;
int* threadUpdateArray;
bool signalThreadCondition = false;

struct ThreadArguments {
	DWORD threadId;
	int threadNumber;
	int threadIncrement;
	int threadSleepTime;
	long int threadIncrementLimit;
};

struct ThreadArguments* threadArgs;

void setupThreadVariables( int numThreads );
void createThreads( int numThreads, long int limit );
DWORD WINAPI threadFunction( LPVOID params );
void startThreadRace( int numThreads );
void freeThreads( int numThreads );
int getIntegerInRange( int range );
int stringToInt( char* value );
long int stringToLongInt( char* value );
bool isDecimalValue( char value );

int main( int argCount, char** argArray )
{
	if ( argCount < 3 )
	{
		fprintf( stderr, "please supply two params for number of threads and limit\n" );
		return EXIT_FAILURE;
	}
	
	int numThreads = stringToInt( argArray[1] );
	long int limit = stringToLongInt( argArray[2] );
	
	#ifdef DEBUG_PRINT
	printf( "creating %d threads and with limit of %d\n", numThreads, limit );
	#endif
	
	srand( limit + numThreads );
	
	setupThreadVariables( numThreads );
	createThreads( numThreads, limit );
	startThreadRace( numThreads );
	
	#ifdef DEBUG_PRINT
	printf( "waiting for threads to finish\n" );
	#endif
	WaitForMultipleObjects( numThreads, threadArray, true, INFINITE );
	
	freeThreads( numThreads );
	srand( 1 );
	
	return EXIT_SUCCESS;
}

void setupThreadVariables( int numThreads )
{
	threadArray = ( HANDLE* ) malloc( sizeof( HANDLE ) * numThreads );
	threadUpdateArray = ( int* ) malloc( sizeof( int ) * numThreads );
	
	// initialize thread variables
	while ( numThreads > 0 )
	{
		threadUpdateArray[numThreads - 1] = 0;
		numThreads--;
	}
}

void createThreads( int numThreads, long int limit )
{
	if ( numThreads < 1 )
	{
		fprintf( stderr, "number of threads cannot be less than 1\n" );
		return;
	}
	
	threadArgs = ( struct ThreadArguments* ) malloc( sizeof( struct ThreadArguments ) * numThreads );
	int ii = 0;
	while ( ii < numThreads )
	{
		
		threadArgs[ii].threadNumber = ii;
		threadArgs[ii].threadIncrement = getIntegerInRange( INCREMENT_MAX );
		threadArgs[ii].threadSleepTime = getIntegerInRange( MAX_SLEEP_TIME );
		threadArgs[ii].threadIncrementLimit = limit;
		
		HANDLE threadHandle = CreateThread( NULL, 0, threadFunction, &threadArgs[ii], CREATE_SUSPENDED, &( threadArgs[ii].threadId ) );
		if ( threadHandle == NULL )
		{
			fprintf( stderr, "failed to create thread\n" );
			ExitProcess( ii );
		}
		else
		{
			threadArray[ii] = threadHandle;
			#ifdef DEBUG_PRINT
			printf( "created thread with id = %lu, and handle = %lu\n", threadArgs[ii].threadId, threadHandle );
			#endif
		}
		ii++;
	}
}

// param is a void pointer. cast it to appropriate type when needed
DWORD WINAPI threadFunction( LPVOID params )
{
	struct ThreadArguments* args = ( struct ThreadArguments* ) params;
	#ifdef DEBUG_PRINT
	printf( "running thread function with id: %lu\n", args->threadNumber );
	#endif
	
	while ( threadUpdateArray[ args->threadNumber ] < args->threadIncrementLimit )
	{
		if ( signalThreadCondition ) ExitThread( EXIT_FAILURE );
		threadUpdateArray[ args->threadNumber ] += args->threadIncrement;
		Sleep( args->threadSleepTime );
	}
	
	signalThreadCondition = true;
	printf( "winner thread: %lu\n", args->threadId );
	ExitThread( EXIT_SUCCESS );
}

void startThreadRace( int numThreads )
{
	#ifdef DEBUG_PRINT
	printf( "starting threads\n" );
	#endif
	while ( numThreads > 0 )
	{
		ResumeThread( threadArray[numThreads - 1] );
		numThreads--;
	}
}

void freeThreads( int numThreads )
{
	#ifdef DEBUG_PRINT
	printf( "freeing threads\n" );
	#endif
	while ( numThreads > 0 )
	{
		CloseHandle( threadArray[numThreads - 1] );
		numThreads--;
	}
	
	free( threadArray );
	free( threadUpdateArray );
	free( threadArgs );
}

int getIntegerInRange( int range )
{
	return ( rand() % range ) + 1;
}

int stringToInt( char* value )
{
	int returnValue = 0;
	while ( *value != '\0' )
	{
		if ( isDecimalValue( *value ) )
		{
			returnValue = ( returnValue * DECIMAL_BASE ) + *value - '0';
			value++;
		}
		else
		{
			return -1;
		}
	}
	
	return returnValue;
}

bool isDecimalValue( char value )
{
	return ( value >= '0' && value <= '9' );
}

long int stringToLongInt( char* value )
{
	long int returnValue = 0;
	while ( *value != '\0' )
	{
		if ( isDecimalValue( *value ) )
		{
			returnValue = ( returnValue * DECIMAL_BASE ) + *value - '0';
			value++;
		}
		else
		{
			return -1;
		}
	}
	
	return returnValue;
}