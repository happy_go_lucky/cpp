#include "../inc/SemaphoreNamed.h"

SemaphoreNamed::SemaphoreNamed(const char* nameStr)
{
	_semaphore = nullptr;
	_semaphoreName = new char[MAX_NAME_LENGTH];
	
	if (nameStr == nullptr)
	{
		sprintf(_semaphoreName, "%c%p", NAME_PREFIX, (void*) this);
	}
	else
	{
		sprintf(_semaphoreName, "%c%s", NAME_PREFIX, nameStr);
	}
}

SemaphoreNamed::~SemaphoreNamed()
{
	delete[] _semaphoreName;
	if (_semaphore != nullptr)
	{
		cleanup();
	}
}

void SemaphoreNamed::init(int semaphoreCount, mode_t accessPermissions)
{
	_semaphoreCount = semaphoreCount;
	
	// named semaphore, begins with a "/" + one or more characters
	_semaphore = sem_open(_semaphoreName, O_CREAT, accessPermissions, _semaphoreCount);
	
	#ifndef NDEBUG
	if (_semaphore == SEM_FAILED)
	{
		int err = errno;
		std::cerr << "Unable to open a named semaphore" << std::endl;
		std::cerr << "error code: " << err << " => " << strerror(err) << std::endl;
	}
	#endif
}

void SemaphoreNamed::wait()
{
	sem_wait(_semaphore);
}

void SemaphoreNamed::post()
{
	sem_post(_semaphore);
}

void SemaphoreNamed::timedWait(int waitTimeInSeconds)
{
	struct timespec time;
	helperfxns::getCurrentTimeSpec(time);
	time.tv_sec += waitTimeInSeconds;
	int state = sem_timedwait(_semaphore, &time);
	if (state != EXIT_SUCCESS)
	{
		std::cout << strerror(state) << std::endl;
		throw ThreadException((char*) "unable to set timed wait on semaphore");
	}
}

const char* SemaphoreNamed::getName() const
{
	return _semaphoreName;
}

void SemaphoreNamed::cleanup()
{
	if (_semaphore == nullptr)
	{
		return;
	}
	
	// unlink ensures that the semaphore is destroyed once all processes are done with it 
	int ret = sem_unlink(_semaphoreName);
	int err;
	#ifndef NDEBUG
	if (ret == -1)
	{
		err = errno;
		std::cerr << "Unable to close a named semaphore" << std::endl;
		std::cerr << "error code: " << err << " => " << strerror(err) << std::endl;
	}
	#endif
	
	ret = sem_close(_semaphore);
	#ifndef NDEBUG
	if (ret == -1)
	{
		err = errno;
		std::cerr << "Unable to close a named semaphore" << std::endl;
		std::cerr << "error code: " << err << " => " << strerror(err) << std::endl;
	}
	#endif
}
