#ifndef BETTER_VECTOR_CPP
#define BETTER_VECTOR_CPP

#include "../include/BetterVector.h"

template <typename T>
BetterVector<T>::BetterVector() noexcept
{
	_size = 0;
	_capacity = DEFAULT_CAPACITY;
	_array = new T[_capacity];
}

template <typename T>
BetterVector<T>::BetterVector(size_t size)
{
	if (size < 1)
	{
		_array = nullptr;
		_size = 0;
		_capacity = 0;
	}
	else
	{
		_size = size;
		_capacity = _size * 2;
		_array = new T[_capacity]();	// equivalent of calloc
	}
}

template <typename T>
BetterVector<T>::BetterVector(const BetterVector<T>& otherVec)
{
	_size = otherVec.size();
	_capacity = otherVec.capacity();
	_array = new T[_capacity];
	for (size_t ii = 0; ii < _size; ii++)
	{
		_array[ii] = otherVec[ii];
	}
}

template <typename T>
BetterVector<T>::BetterVector(BetterVector<T>&& otherVec) noexcept
{
	_size = move(otherVec.size());
	_capacity = move(otherVec.capacity());
	_array = move(otherVec._array);
	
	otherVec._array = nullptr;
	otherVec._size = 0;
	otherVec._capacity = 0;
}

template <typename T>
BetterVector<T>::BetterVector(initializer_list<T> list)
{
	_size = list.size();
	_capacity = _size * 2;
	_array = new T[_capacity];
	for (size_t ii = 0; ii < _size; ii++)
	{
		_array[ii] = *(list.begin() + ii);
	}
}

template <typename T>
BetterVector<T>::~BetterVector()
{
	delete[] _array;
}

template <typename T>
BetterVector<T>& BetterVector<T>::operator = (const BetterVector<T>& otherVec)
{
	if (this != &otherVec)
	{
		delete[] _array;
		
		_size = otherVec.size();
		_capacity = otherVec.capacity();
		_array = new T[_capacity];
		
		for (size_t ii = 0; ii < _size; ii++)
		{
			_array[ii] = otherVec[ii];
		}
	}
	return *this;
}

template <typename T>
BetterVector<T>& BetterVector<T>::operator = (BetterVector<T>&& otherVec) noexcept
{
	if (this != &otherVec)
	{
		delete[] _array;
		
		_size = move(otherVec._size);
		_capacity = move(otherVec._capacity);
		_array = move(otherVec._array);
		
		otherVec._array = nullptr;
		otherVec._size = 0;
		otherVec._capacity = 0;
	}
	return *this;
}

template <typename T>
const T& BetterVector<T>::operator [] (size_t index) const
{
	if (index >= _size)
	{
		throw std::out_of_range("vector index is out of range");
	}
	return _array[index];
}

template <typename T>
T& BetterVector<T>::operator [] (size_t index)
{
	if (index >= _size)
	{
		throw std::out_of_range("vector index is out of range");
	}
	return _array[index];
}

template <typename U>
ostream& operator << (ostream& stream, const BetterVector<U>& vec)
{
	const size_t& size = vec.size();
	stream << "{ ";
	for (size_t ii = 0; ii < size; ii++)
	{
		stream << vec[ii];
		if (ii < size - 1)
			stream << ", ";
	}
	stream << " }" << endl;
	
	return stream;
}

template <typename T>
bool BetterVector<T>::operator == (const BetterVector<T>& otherVec) const noexcept
{
	if (otherVec.size() != _size)
		return false;
	
	for (size_t ii = 0; ii < _size; ii++)
	{
		if (_array[ii] == otherVec[ii])
		{
			continue;
		}
		
		return false;
	}
	
	return true;
}

template <typename T>
bool BetterVector<T>::operator == (const BetterVector<T>&& otherVec) const noexcept
{
	if (otherVec.size() != _size)
		return false;
	
	for (size_t ii = 0; ii < _size; ii++)
	{
		if (_array[ii] == otherVec[ii])
		{
			continue;
		}
		
		return false;
	}
	
	return true;
}

template <typename T>
bool BetterVector<T>::operator != (const BetterVector<T>& otherVec) const noexcept
{
	if (otherVec.size() != _size)
		return false;
	
	for (size_t ii = 0; ii < _size; ii++)
	{
		if (_array[ii] == otherVec[ii])
		{
			return false;
		}
	}
	
	return true;
}

template <typename T>
bool BetterVector<T>::operator != (const BetterVector<T>&& otherVec) const noexcept
{
	if (otherVec.size() != _size)
		return false;
	
	for (size_t ii = 0; ii < _size; ii++)
	{
		if (_array[ii] == otherVec[ii])
		{
			return false;
		}
	}
	
	return true;
}

template <typename T>
void BetterVector<T>::checkValidity() const
{
	assert(_size <= _capacity);
}

template <typename T>
void BetterVector<T>::clear() noexcept
{
	_size = 0;
}

template <typename T>
void BetterVector<T>::erase(T* element) noexcept
{
	erase(element, element);
}

template <typename T>
void BetterVector<T>::erase(T* rangeBegin, T* rangeEnd, bool inclusive) noexcept
{
	size_t range = rangeEnd - rangeBegin + inclusive;
	if (rangeBegin >= begin() && rangeBegin < end() && range > 0 && range <= _size)
	{
		memmove(rangeBegin, rangeEnd + 1, sizeof(T) * (end() + 1 - rangeEnd));
		_size -= range;
	}
}

template <typename T>
void BetterVector<T>::insertAt(size_t index, const T& value)
{
	if (index >= 0 && index < _size)
	{
		_size++;
		manageCapacity();
		
		memmove(_array + index + 1, _array + index, sizeof(T) * (_size - index));
		_array[index] = value;
	}
}

template <typename T>
void BetterVector<T>::insertAt(size_t index, T&& value)
{
	if (index >= 0 && index < _size)
	{
		_size++;
		manageCapacity();
		
		memmove(_array + index + 1, _array + index, sizeof(T) * (_size - index));
		_array[index] = move(value);
	}
}

template <typename T>
void BetterVector<T>::pushBack(const T& value)
{
	_size++;
	manageCapacity();
	_array[_size - 1] = value;
}

template <typename T>
void BetterVector<T>::pushBack(T&& value)
{
	_size++;
	manageCapacity();
	_array[_size - 1] = move(value);
}

template <typename T>
void BetterVector<T>::popBack()
{
	if (_size > 0)
		_size--;
}

template <typename T>
void BetterVector<T>::resize(size_t newSize)
{
	if (newSize <= _capacity)
		_size = newSize;
	else
	{
		manageCapacity();
	}
}

template <typename T>
void BetterVector<T>::shrink()
{
	if (_size < _capacity)
	{
		T* temp = new T[_size];
		for (size_t ii = 0; ii < _size; ii++)
		{
			temp[ii] = _array[ii];
		}
		
		delete[] _array;
		_array = temp;
		_capacity = _size;
	}
}

template <typename T>
void BetterVector<T>::manageCapacity()
{
	if (_size > _capacity)
	{
		size_t newCapacity = _capacity * 2;
		T* temp = new T[newCapacity];
		
		for (size_t ii = 0; ii < _capacity; ii++)
		{
			temp[ii] = _array[ii];
		}
		
		delete[] _array;
		_array = temp;
		_capacity = newCapacity;
	}
}

#endif
