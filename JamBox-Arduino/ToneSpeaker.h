#ifndef TONE_SPEAKER_H
#define TONE_SPEAKER_H

#include "Arduino.h"
#include "defines.h"

class ToneSpeaker
{
  public:
    const static int MIN_TONE_VOLUME = 0;
    const static int MAX_TONE_VOLUME = 255;

    const static int MIN_TONE_VAL = 0;
    const static int MAX_TONE_VAL = 255;

    //const static int TONE_DURATION = 200;
    const static int TONE_DURATION = 1000;
    const static int TONE_VOLUME = 10;
    
    const static int NUM_NOTES = 7;
    const static int NUM_SCALES = 6;

    enum NOTE_DIFF_VALUE { NOTE_A_DIFF = 0, NOTE_B_DIFF = NOTE_A_DIFF + 7, NOTE_C_DIFF = NOTE_B_DIFF + 4,
                  NOTE_D_DIFF = NOTE_C_DIFF + 7, NOTE_E_DIFF = NOTE_D_DIFF + 10, NOTE_F_DIFF = NOTE_E_DIFF + 5,
                  NOTE_G_DIFF = NOTE_F_DIFF + 10 };
    
    enum NOTES { NOTE_A = 1, NOTE_B, NOTE_C, NOTE_D, NOTE_E, NOTE_F, NOTE_G };
    enum SCALE { SCALE_1 = 1, SCALE_2, SCALE_3, SCALE_4, SCALE_5, SCALE_6 };

    enum MELODY_SCALE_1 { NOTE_A_1 = 55, NOTE_B_1 = NOTE_A_1 + NOTE_B_DIFF, NOTE_C_1 = NOTE_B_1 + NOTE_C_DIFF,
                  NOTE_D_1 = NOTE_C_1 + NOTE_D_DIFF, NOTE_E_1 = NOTE_D_1 + NOTE_E_DIFF, NOTE_F_1 = NOTE_E_1 + NOTE_F_DIFF,
                  NOTE_G_1 = NOTE_F_1 + NOTE_G_DIFF };

    enum MELODY_SCALE_2 { NOTE_A_2 = 110, NOTE_B_2 = NOTE_A_2 + ( NOTE_B_DIFF * 2 ), NOTE_C_2 = NOTE_B_2 + ( NOTE_C_DIFF * 2 ),
                  NOTE_D_2 = NOTE_C_2 + ( NOTE_D_DIFF * 2 ), NOTE_E_2 = NOTE_D_2 + ( NOTE_E_DIFF * 2 ), NOTE_F_2 = NOTE_E_2 + ( NOTE_F_DIFF * 2 ),
                  NOTE_G_2 = NOTE_F_2 + ( NOTE_G_DIFF * 2 ) };

    enum MELODY_SCALE_3 { NOTE_A_3 = 220, NOTE_B_3 = NOTE_A_3 + ( NOTE_B_DIFF * 3 ), NOTE_C_3 = NOTE_B_3 + ( NOTE_C_DIFF * 3 ),
                  NOTE_D_3 = NOTE_C_3 + ( NOTE_D_DIFF * 3 ), NOTE_E_3 = NOTE_D_3 + ( NOTE_E_DIFF * 3 ), NOTE_F_3 = NOTE_E_3 + ( NOTE_F_DIFF * 3 ),
                  NOTE_G_3 = NOTE_F_3 + ( NOTE_G_DIFF * 3 ) };

    enum MELODY_SCALE_4 { NOTE_A_4 = 440, NOTE_B_4 = NOTE_A_4 + ( NOTE_B_DIFF * 4 ), NOTE_C_4 = NOTE_B_4 + ( NOTE_C_DIFF * 4 ),
                  NOTE_D_4 = NOTE_C_4 + ( NOTE_D_DIFF * 4 ), NOTE_E_4 = NOTE_D_4 + ( NOTE_E_DIFF * 4 ), NOTE_F_4 = NOTE_E_4 + ( NOTE_F_DIFF * 4 ),
                  NOTE_G_4 = NOTE_F_4 + ( NOTE_G_DIFF * 4 ) };

    enum MELODY_SCALE_5 { NOTE_A_5 = 880, NOTE_B_5 = NOTE_A_5 + ( NOTE_B_DIFF * 5 ), NOTE_C_5 = NOTE_B_5 + ( NOTE_C_DIFF * 5 ),
                  NOTE_D_5 = NOTE_C_5 + ( NOTE_D_DIFF * 5 ), NOTE_E_5 = NOTE_D_5 + ( NOTE_E_DIFF * 5 ), NOTE_F_5 = NOTE_E_5 + ( NOTE_F_DIFF * 5 ),
                  NOTE_G_5 = NOTE_F_5 + ( NOTE_G_DIFF * 5 ) };

    enum MELODY_SCALE_6 { NOTE_A_6 = 1760, NOTE_B_6 = NOTE_A_6 + ( NOTE_B_DIFF * 6 ), NOTE_C_6 = NOTE_B_6 + ( NOTE_C_DIFF * 6 ),
                  NOTE_D_6 = NOTE_C_6 + ( NOTE_D_DIFF * 6 ), NOTE_E_6 = NOTE_D_6 + ( NOTE_E_DIFF * 6 ), NOTE_F_6 = NOTE_E_6 + ( NOTE_F_DIFF * 6 ),
                  NOTE_G_6 = NOTE_F_6 + ( NOTE_G_DIFF * 6 ) };

    const int NOTES_SCALE_MATRIX[NUM_SCALES][NUM_NOTES] = { { NOTE_A_1, NOTE_B_1, NOTE_C_1, NOTE_D_1, NOTE_E_1, NOTE_F_1, NOTE_G_1 },
                                                 { NOTE_A_2, NOTE_B_2, NOTE_C_2, NOTE_D_2, NOTE_E_2, NOTE_F_2, NOTE_G_2 },
                                                 { NOTE_A_3, NOTE_B_3, NOTE_C_3, NOTE_D_3, NOTE_E_3, NOTE_F_3, NOTE_G_3 },
                                                 { NOTE_A_4, NOTE_B_4, NOTE_C_4, NOTE_D_4, NOTE_E_4, NOTE_F_4, NOTE_G_4 },
                                                 { NOTE_A_5, NOTE_B_5, NOTE_C_5, NOTE_D_5, NOTE_E_5, NOTE_F_5, NOTE_G_5 },
                                                 { NOTE_A_6, NOTE_B_6, NOTE_C_6, NOTE_D_6, NOTE_E_6, NOTE_F_6, NOTE_G_6 }
                                                };
    
    ToneSpeaker( const int );
    ~ToneSpeaker();

    void playTone( const int );
    void stopTone();
    void setScale( const int );

  private:
    int _speakerPin;
    int _scale;
};

#endif
