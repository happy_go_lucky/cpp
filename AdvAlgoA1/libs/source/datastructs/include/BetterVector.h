#ifndef BETTER_VECTOR_H
#define BETTER_VECTOR_H

#include <cstdlib>
#include <cstring>
#include <iostream>
#include <stdexcept>
#include <assert.h>
#include <initializer_list>

using namespace std;

template <class T>
class BetterVector
{
	public:
		const short DEFAULT_CAPACITY = 10;
	
		BetterVector() noexcept;
		BetterVector(size_t size);
		BetterVector(const BetterVector<T>& otherVec);
		BetterVector(BetterVector<T>&& otherVec) noexcept;
		BetterVector(initializer_list<T> list);
		~BetterVector();
		
		inline size_t size() const
		{
			return _size;
		}
		
		inline size_t capacity() const
		{
			return _capacity;
		}
		
		inline T* begin() const
		{
			return _array;
		}
		
		inline T* end() const
		{
			return _array + _size;	// address of elment past the last element
		}
		
		inline bool empty() const
		{
			return begin() == end();
		}
		
		inline T& front()
		{
			return *_array;
		}
		
		inline const T& front() const
		{
			return *_array;
		}
		
		inline T& back()
		{
			return *(_array + _size - 1);
		}
		
		inline const T& back() const
		{
			return *(_array + _size - 1);
		}
		
		void clear() noexcept;
		void erase(T* element) noexcept;
		void erase(T* rangeBegin, T* rangeEnd, bool inlcusive = true) noexcept;
		void insertAt(size_t index, const T& value);
		void insertAt(size_t index, T&& value);
		void pushBack(const T& value);
		void pushBack(T&& value);
		void popBack();
		void resize(size_t newSize);
		void shrink();
		
		BetterVector<T>& operator = (const BetterVector<T>& otherVec);
		BetterVector<T>& operator = (BetterVector<T>&& otherVec) noexcept;
		const T& operator [] (size_t index) const;
		T& operator [] (size_t index);
		
		template <typename U>
		friend ostream& operator << (ostream& s, const BetterVector<U>& vec);
		bool operator == (const BetterVector<T>& otherVec) const noexcept;
		bool operator == (const BetterVector<T>&& otherVec) const noexcept;
		bool operator != (const BetterVector<T>& otherVec) const noexcept;
		bool operator != (const BetterVector<T>&& otherVec) const noexcept;
		
		void checkValidity() const; // invariant

	private:
		T* _array;
		size_t _size;
		size_t _capacity;
		
		void manageCapacity();
};

#endif
