#ifndef COORD_EXCEPTIONS_H
#define COORD_EXCEPTIONS_H

#include <exception>

using std::exception;

class CoordCompException : public exception
{
	public:
		CoordCompException(char* = 
		( char* ) "Exception!");
		virtual const char* what() const throw();
		
	private:
		char* _message;
};

#endif
