#ifndef EXPR_FSM_H
#define EXPR_FSM_H

/**
 * this class is represntative of FSM for solving/validating an infix expression
 * */

#include <iostream>
#include <vector>
#include "ExprElement.h"
#include "GlobalVariables.h"
#include "FSM_Helper.h"
#include "FSM_State.h"
#include "FSM_Transition.h"

using std::vector;
using std::cout;
using std::endl;

class ExprFSM
{
	public:
		ExprFSM() noexcept;
		explicit ExprFSM(ExpressionType expressionType, ExprStates startStateType) noexcept;
		~ExprFSM();
		
		void setCurrentState(ExprStates stateType);
		void setExprType(ExpressionType expressionType);
		bool update(ExprElement* inputElement);
		FSM_State* getCurrentState() const;
		void debugPrint();
	
	private:
		FSM_State* _currentState;
		vector<FSM_State*> _allStateVector;
		ExpressionType _expressionType;
		GlobalVariables::StateActionParams _actionParams;
		
		void initStateVector();
		void cleanupStateVector();
		void updateCurrentState(ExprStates stateType);
		void setupInfixStates(ExprStates stateType, FSM_State* state);
		void setupPostfixStates(ExprStates stateType, FSM_State* state);
		void setupPrefixStates(ExprStates stateType, FSM_State* state);
};

#endif
