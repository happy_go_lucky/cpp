#ifndef DATE_H
#define DATE_H

#include <iostream>
#include <string>
#include <cstring>

using namespace std;

class Date
{
	public:
		Date() noexcept;
		Date(short day, short month, short year);
		Date(char* dateStr);
		Date(const Date& date);
		~Date();
		
		friend ostream& operator <<(ostream& out, const Date& date);
		Date& operator =(const Date& date);
		Date& operator =(Date&& date);
		Date& operator =(char* dateStr);
		
		short getYear() const noexcept;
		short getMonth() const noexcept;
		short getDay() const noexcept;
		
		void setYear(short year);
		void setMonth(short month);
		void setDay(short day);
		
		bool isValid() const noexcept;
		string getDateString() const;
		
	private:
		short _year;
		short _month;
		short _day;
		
		void computeDateFromString(char* dateStr);
};

#endif
