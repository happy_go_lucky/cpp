#ifndef HASHTABLE_H
#define HASHTABLE_H

#define HASH_WEIGHT  13
#define LARGE_INT 1024*1024*1024

#include <string>
#include <iostream>
#include "LinkedList.h"
#include "LinkedList.cpp"

class HashTable
{
	public:
		const static int DEFAULT_SIZE = 10;
		HashTable( int = DEFAULT_SIZE );
		~HashTable();
		
		const int getSize() const;
		void addValueToTable( string );
		bool constainsValue( string, int& ) const;
		int* getCollisionsArray() const;
		
	private:
		int size;
		LinkedList<string>* hashTable;
		int* numCollisions;
		int computeHash( string ) const;
		void initCollisionsArray();
};

#endif