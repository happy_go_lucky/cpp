#ifndef SHARED_MEMORY_H
#define SHARED_MEMORY_H

#include <iostream>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <cstring>
#include <cstdio>
#include <assert.h>
#include <climits>
#include <type_traits>
#include "ThreadException.h"
#include "HelperFxns.h"
#include "SharedMemoryDefs.h"

namespace posixthreads
{
	class SharedMemory
	{
		public:
			static const char NAME_PREFIX = '/';
			static const char NULL_CHAR = '\0';
			static const int MAX_NAME_LENGTH = NAME_MAX;
			static const int BASE_HEX = 16;
			
			SharedMemory() = delete;
			SharedMemory(SharedMemoryDefs::VisibilityType visibility,
						SharedMemoryDefs::SharingType sharingType);
			~SharedMemory();
			
			SharedMemory(const SharedMemory& sharedMem) = delete;
			SharedMemory(SharedMemory&& sharedMem) = delete;
			
			SharedMemory& operator =(const SharedMemory& sharedMem) = delete;
			SharedMemory& operator =(SharedMemory&& sharedMem) = delete;
			
			void initIdentifiedMemory(const char* nameStr, 
							SharedMemoryDefs::AccessType accessType,
							mode_t accessPermissions, bool createNew, bool exclusive,
							bool truncate);
			void initAnonymousMemory();
			void createSharedMemory(void* startAddress, size_t sizeInBytes,
							helperfxns::AccessPermissions perm, int sharingType, 
							off_t offsetFromFDStart);
			void cleanupResources();
			void* getPointerToMemory();
			void updateData(size_t numBytes, void* data);
		
		private:
			SharedMemoryDefs::VisibilityType _shmVisibilityType;
			int _shmFd;
			char* _shmName;
			int _shmAllocationType;
			void* _shmAddress;
			int _shmSize;
			
			void _setSharingType(SharedMemoryDefs::SharingType sharingType);
	};
}

#endif
