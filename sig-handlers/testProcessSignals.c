#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <signal.h>
#include <unistd.h>
#include <stdbool.h>
#include <stdint.h>
#include <sys/types.h>
#include <sys/wait.h>

//static long p_realt_secs = 0, c1_realt_secs = 0, c2_realt_secs = 0;
//static long p_virtt_secs = 0, c1_virtt_secs = 0, c2_virtt_secs = 0;
//static long p_proft_secs = 0, c1_proft_secs = 0, c2_proft_secs = 0;
//static struct itimerval p_realt, c1_realt, c2_realt;	// real timer
//static struct itimerval p_virtt, c1_virtt, c2_virtt;	// processor time used by process
//static struct itimerval p_proft, c1_proft, c2_proft;	// profiling timer

//void getSystemTime( struct timeval* tv );

int main( int argc, char** argv )
{
	/*
	struct timeval timeValue;
	getSystemTime( &timeValue );
	
	long unsigned fib = 0;
	int child1_pid = -1, child2_pid = -1;
	unsigned int fibarg;
	int c1_status, c2_status;

	if ( argc != 2 )
	{
		fprintf( stderr, "please provide arguments for Fibonacci number calculation\n" );
		return EXIT_FAILURE;
	}
	// Get command line arugment, fibarg
	fibarg = atoi( argv[1] );
	
	// Initialize parent, child1, and child 2 timer values
	initTimer( &p_realt, ITIMER_REAL );
	initTimer( &p_virtt, ITIMER_VIRTUAL );
	initTimer( &p_proft, ITIMER_PROF );
	
	initTimer( &c1_realt, ITIMER_REAL );
	initTimer( &c1_virtt, ITIMER_VIRTUAL );
	initTimer( &c1_proft, ITIMER_PROF );
	
	initTimer( &c2_realt, ITIMER_REAL );
	initTimer( &c2_virtt, ITIMER_VIRTUAL );
	initTimer( &c2_proft, ITIMER_PROF );
	
	// Enable your signal handlers for the parent
	enableSignalHandlers( getpid() );

	// Set the parent's itimers
	armTimer( &p_realt, ITIMER_REAL, 1, 0, 0, 0 );
	armTimer( &p_virtt, ITIMER_VIRTUAL, 1, 0, 1, 0 );
	armTimer( &p_proft, ITIMER_PROF, 1, 0, 1, 0 );
	
	child1_pid = fork();

	if( child1_pid == 0 )
	{
		// Enable child 1 signal handlers (disable parent handlers)
		enableSignalHandlers( getpid() );
		//disableSignalHandlers();
				
		// Set the child 1 itimers
		armTimer( &c1_realt, ITIMER_REAL, fibarg, 0, 1, 0 );
		armTimer( &c1_virtt, ITIMER_VIRTUAL, fibarg, 0, 1, 0 );
		armTimer( &c1_proft, ITIMER_PROF, fibarg, 0, 1, 0 );
		
		// Start child 1 on the Fibonacci program
		fib = fibonacci( fibarg );

		// Read the child 1 itimer values, and report them
		getitimer( ITIMER_PROF, &c1_realt );
		getitimer( ITIMER_REAL, &c1_virtt );
		getitimer( ITIMER_VIRTUAL, &c1_proft );
		printf("\n");
		printf("Child 1 fib = %ld, real time = %ld sec, %ld msec\n",
				fib, c1_realt_secs,
				elasped_usecs(c1_realt.it_value.tv_sec,
						c1_realt.it_value.tv_usec) / 1000);
		printf("Child 1 fib = %ld, cpu time = %ld sec, %ld msec\n",
				fib, c1_proft_secs,
				elapsed_usecs(c1_proft.it_value.tv_sec,
						c1_proft.it_value.tv_usec) / 1000);
		printf("Child 1 fib = =%ld, user time = %ld sec, %ld msec\n",
				fib, c1_virtt_secs,
				elapsed_usecs(c1_virtt.it_value.tv_sec,
						c1_virtt.it_value.tv_usec) / 1000);
		printf("Child 1 ib  = %ld, kernel time = %ld sec, %ld msec\n",
				fib, c1_proft_secs – c1_virtt_secs,
				(elaspsed_usecs(c1_proft.it_value.tv_sec,
						c1_proft.it_value.tv_usec) / 1000) -
						(elapsed_usecs(c1-virtt.it_value.tv_sec,
								c1_virtt.it_value.tv_usec) / 1000));
		fflush(stdout);
		
		exit(0);
	}
	else
	{
		while ( 0 == waitpid( child1_pid, &c1_status, WNOHANG ) )
		{
			//printf( "child is still running\n" );
			//sleep( 1 );
		}
		
		child2_pid = fork();
		if(child2_pid == 0) 
		{
			// Enable the child 2 signal handlers (disable parent handlers)
			...
			// Set the child 2 itimers
			...
			// Start the child 2 on the Fibonacci program
			fib = fibonacci(fibarg);
			// Read the child 2 itimer values and report them
			...
		}
		else
		{ //This is the parent
			//Start the parent on the Fibonacci program 
			fib = fibonacci(fibarg);

			// Wait for the children to terminate
			if ( child1_pid == waitpid( child1_pid, &c1_status, 0 ) )
			{
				printf( "child 1 has been terminated\n" );
			}
			if ( child2_pid == waitpid( child2_pid, &c2_status, 0 ) )
			{
				printf( "child 2 has been terminated\n" );
			}

			// Read the parent itimer values, and report them
			...
		}
		printf("this line should never be printed\n");
		
	}
	*/
	return EXIT_SUCCESS;
}

//void getSystemTime( struct timeval* tv )
//{
	//gettimeofday( tv, NULL ); // time since EPOCH (1 Jan 1970 00:00:00)
	//printf( "number of seconds since EPOCH: %ld\n", tv->tv_sec );
	//printf( "number of micro-seconds since EPOCH: %ld\n", tv->tv_usec );
	
	//struct timeStamp currentTime = getFormattedTimeSinceEpochFromSeconds( tv->tv_sec, tv->tv_usec );
	//printTimeStamp( currentTime );
//}
