#ifndef BINARY_NODE_CPP
#define BINARY_NODE_CPP

#include "BinaryNode.h"

template <typename T>
BinaryNode<T>::BinaryNode() noexcept
{
	_left = nullptr;
	_right = nullptr;
	_markedForDeletion = false;
}

template <typename T>
BinaryNode<T>::BinaryNode(const T& theElement, BinaryNode<T>* lt, BinaryNode<T>* rt)
{
	_element = theElement;
	_left = lt;
	_right = rt;
	_markedForDeletion = false;
}

template <typename T>
BinaryNode<T>::BinaryNode(T&& theElement, BinaryNode<T>* lt, BinaryNode<T>* rt) noexcept
{
	_element = std::move(theElement);
	_left = lt;
	_right = rt;
	_markedForDeletion = false;
}

template <typename T>
BinaryNode<T>::BinaryNode(const BinaryNode<T>& node)
{
	_element = node._element;
	_left = node._left;
	_right = node._right;
	_markedForDeletion = node._markedForDeletion;
}

template <typename T>
BinaryNode<T>::BinaryNode(BinaryNode<T>&& node) noexcept
{
	_element = std::move(_element);
	_left = node._left;
	_right = node._right;
	_markedForDeletion = node._markedForDeletion;
	node._right = nullptr;
	node._right = nullptr;
}

template <typename T>
BinaryNode<T>::~BinaryNode()
{
	// nothing to delete
}

template <typename T>
BinaryNode<T>& BinaryNode<T>::operator =(const BinaryNode<T>& node)
{
	if (*this != node)
	{
		_element = node._element;
		_left = node._left;
		_right = node._right;
		_markedForDeletion = node._markedForDeletion;
	}
	return this;
}

template <typename T>
BinaryNode<T>& BinaryNode<T>::operator =(BinaryNode<T>&& node) noexcept
{
	_element = std::move(node.getData());
	_left = node._left;
	_right = node._right;
	_markedForDeletion = node._markedForDeletion;
	node._right = nullptr;
	node._right = nullptr;

	return this;
}

template <typename T>
bool BinaryNode<T>::operator ==(const BinaryNode<T>& node)
{
	return _element == node.getData();
}

template <typename T>
bool BinaryNode<T>::operator ==(BinaryNode<T>&& node) noexcept
{
	return _element == node.getData();
}

template <typename U>
ostream& operator <<(ostream& out, const BinaryNode<U>& node) noexcept
{
	out << node.getData();
	return out;
}

template <typename U>
ostream& operator <<(ostream& out, BinaryNode<U>&& node) noexcept
{
	out << node.getData();
	return out;
}

template <typename T>
void BinaryNode<T>::debugPrint() const noexcept
{
	#ifndef NDEBUG
	cout << "data: " << _element << endl;
	cout << "left child: " << _left << endl;
	cout << "right child: " << _right << endl;
	if (_left) cout << "left data: " << _left->getData() << endl;
	if (_right) cout << "right data: " << _right->getData() << endl;
	cout << "marked deleted: ";
	_markedForDeletion ? cout << "true" << endl : cout << "false" << endl;
	#endif
}

#endif
