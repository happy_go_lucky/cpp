#ifndef BST_NODE_H
#define BST_NODE_H

template <class T>
class BST_Node
{
	public:
		BST_Node( const T&, BST_Node<T>* = nullptr, BST_Node<T>* = nullptr );
		BST_Node( const BST_Node<T>& );
		~BST_Node();
		
		T getData() const;
		BST_Node<T>* getLeft() const;
		BST_Node<T>* getRight() const;
		
		void setData( T );
		void setLeft( BST_Node<T>* );
		void setRight( BST_Node<T>* );
		
		//~ T operator = ( const BST_Node<T>& );
		
	private:
		T _data;
		BST_Node<T>* _left;
		BST_Node<T>* _right;
};

#endif
