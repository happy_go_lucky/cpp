#ifndef COMMAND_STRUCTS_H
#define COMMAND_STRUCTS_H

#include <iostream>
#include <string>
#include <fstream>
#include <cstring>
#include <map>
#include "DMSCoord.h"
#include "GISData.h"
#include "Date.h"
#include "DDCoord.h"
#include "HelperFunctions.h"
#include "HashTable.h"
#include "HashRecord.h"
#include "QuadTree.h"

using namespace std;

struct BaseCommand
{	
	int parameterType;
	char* className;
	fstream* dbFileStream;
	fstream* logFileStream;
	
	virtual void startBuild() = 0;
	virtual void build(char* paramString) = 0;
	virtual void finishBuild() = 0;
	virtual void execute() = 0;
};

struct WorldCommand : public BaseCommand
{
	enum ParamOrder
	{
		INVALID = 0,
		WEST_LONG,
		EAST_LONG,
		SOUTH_LONG,
		NORTH_LONG
	};
	
	DMSCoord westLongitude;
	DMSCoord eastLongitude;
	DMSCoord southLatitude;
	DMSCoord northLatitude;
	
	void startBuild();
	void build(char* paramString);
	void finishBuild();
	void execute();
	void printDMS();
	void printDD();
	void printBoundaries();
	
	friend ostream& operator <<(ostream& out, const WorldCommand& world);
};

struct ImportCommand : public BaseCommand
{
	static const short FILE_STREAM_BUFFER_SIZE = 500;
	static const char DATA_SEPARATOR = '|';
	static const char RECORD_DELIMITER = '\n';
	static const constexpr char* RECORD_DELIMITER_STR = "\n";
	
	enum ParamOrder
	{
		INVALID = 0,
		FILE_NAME
	};
	
	fstream dataFileStream;
	string gisPSVFileName;
	WorldCommand* worldCommand;
	HashTable* hash;
	QuadTree* tree;
	size_t importedRecords;
	size_t importedNodes;
	short totalRecords;
	
	void startBuild();
	void build(char* paramString);
	void finishBuild();
	void execute();
	
	void openFileStream();
	bool isFileStreamValid();
	void parseFileStream();
	void closeFileStream();
	void parseDataRecord(char* rawDataRecord, size_t& offset, size_t recordLength);
	char* tokenizecstr(char*& str, char delimiter);
	void updateHash(const GISData& dataRecord, size_t offset);
	void updateTree(const GISData& dataRecord, size_t offset);
	void updateDataBaseFile(char* rawDataRecord, size_t& offset, size_t recordLength);
	void filterDataAndUpdate(char* rawDataRecord, const GISData& dataRecord, 
							size_t& offset, size_t recordLength);
	void printHashStats();
	void printTreeStats();
};

struct DebugCommand : public BaseCommand
{
	static constexpr const char* DEBUG_QUADTREE = "quad";
	static constexpr const char* DEBUG_HASH = "hash";
	static constexpr const char* DEBUG_BUFFER_POOL = "pool";
	
	enum ParamOrder
	{
		INVALID = 0,
		DEBUG_TYPE
	};
	
	string debugType;
	HashTable* hash;
	QuadTree* tree;
	
	void startBuild();
	void build(char* paramString);
	void finishBuild();
	void execute();
};

struct WhatIsAtCommand : public BaseCommand
{
	static const short CACHE_SIZE = 15;
	
	enum ParamOrder
	{
		INVALID = 0,
		LAT_COORD,
		LONG_COORD
	};
	
	DMSCoord latCoord;
	DMSCoord longCoord;
	HashTable* hash;
	QuadTree* tree;
	map<string, size_t>* cacheHash;
	map<string, vector<size_t>>* cacheTree;
	
	void startBuild();
	void build(char* paramString);
	void finishBuild();
	void execute();
};

struct WhatIsInCommand : public BaseCommand
{
	static constexpr const char* MODIFIER_FILTER = "-filter";
	static constexpr const char* MODIFIER_LONG = "-long";
	static const short CACHE_SIZE = 15;
	
	enum ParamOrder
	{
		INVALID = 0,
		FLAG,
		FLAG_PARAM,
		LAT_COORD,
		LONG_COORD,
		LONG_HALF_WIDTH,
		LAT_HALF_HEIGHT
	};
	
	HashTable* hash;
	QuadTree* tree;
	map<string, size_t>* cacheHash;
	map<string, vector<size_t>>* cacheTree;
	
	DMSCoord latCoord;
	DMSCoord longCoord;
	short latHalfHeight;
	short longHalfWidth;
	string filter;
	bool isLong;
	bool isFilter;
	
	void startBuild();
	void build(char* paramString);
	void finishBuild();
	void execute();
};

struct WhatIsCommand : public BaseCommand
{
	static const short CACHE_SIZE = 15;
	
	enum ParamOrder
	{
		INVALID = 0,
		FEATURE_NAME,
		STATE_CODE
	};
	
	HashTable* hash;
	QuadTree* tree;
	map<string, size_t>* cacheHash;
	map<string, vector<size_t>>* cacheTree;
	
	string featureName;
	string stateCode;
	
	void startBuild();
	void build(char* paramString);
	void finishBuild();
	void execute();
};

struct QuitCommand : public BaseCommand
{
	void startBuild();
	void build(char* paramString);
	void finishBuild();
	void execute();
};

#endif
