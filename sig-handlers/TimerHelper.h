/***
This file deals with Unix timers
**/

#ifndef TIMER_HELPER_H
#define TIMER_HELPER_H

#include <sys/time.h>
#include <stdio.h>

void initTimer( struct itimerval* intervalTimer, const int timerType );
void print_itimerval_struct( struct itimerval* aTimer );
void armTimer( struct itimerval* intervalTimer, const int timerType, time_t timerLimit_seconds, 
					suseconds_t timerLimit_microseconds, time_t timerInterval_seconds, suseconds_t timerInterval_microseconds );
void disarmTimer( struct itimerval* timer, const int timerType );

#endif