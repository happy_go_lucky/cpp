typedef int bool;
#define true 1
#define false 0

#define BUFFER_SIZE 1024
#define MY_PORT 55555
#define MY_SERVERNAME "localhost"
#define MAX_CONNECTION_QUE 5

#define USER_CPU "Computer"
#define USER_CLIENT "Player"

#define WINNER_CPU "LOST"
#define WINNER_CLIENT "WON"
#define DRAW "DRAW"

#define USERDATA_PAPER "1"
#define USERDATA_SCISSORS "2"
#define USERDATA_ROCK "3"
#define USERDATA_STATS "4"
#define USERDATA_BAD "7"
#define TRAIN_TURNS 50

#define MESSAGE_GO "Go"
#define MESSAGE_DRAW "Draw"
#define DELIMITER "_"
#define INVALID -1

#define MAX_MOVES_FOR_NGRAM 100

enum Play_Options
{
	PAPER = 1,
	SCISSORS,
	ROCK,
	STATS,
	EXIT
};

enum NGram_Options
{
	MONOGRAM = 1,
	BIGRAM = 2,
	TRIGRAM = 3,
	QUADGRAM = 4,
	DECIGRAM = 5,
	RANDOM_NO_NGRAM = 0,
};

struct NGramStats
{
	int* pattern;
	int patternSize;
	int predictedMove;
	int predictionCount;
};

typedef struct NGramStats NGramStats;

bool checkMoveValidity(int move)
{
	if ( move < PAPER || move > ROCK)
	{
		printf ("\t choice entered by player is invalid\n\n");
		return false;
	}
	return true;
}

bool checkIfArrayEqual(int* arrA, int* arrB, int length)
{
	for (int ii = 0; ii < length; ii++)
	{
		if (arrA[ii] != arrB[ii])
		{
			return false;
		}
	}
	
	return true;
}

void copyArr(int* src, int* dst, int length)
{
	for (int ii = 0; ii < length; ii++)
	{
		dst[ii] = src[ii];
	}
}
