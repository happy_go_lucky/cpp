#ifndef STATE_ACTIONS_H
#define STATE_ACTIONS_H

//https://www.codeproject.com/Articles/1087619/State-Machine-Design-in-Cplusplus-2

/**
 * a set of actions defined to perform different state machine actions
 * */
 
#include "GlobalVariables.h"

class BaseAction
{
	public:
		BaseAction() = default;
		~BaseAction() = default;
		virtual void operator ()(GlobalVariables::StateActionParams& actionParams) = 0;
};

class StartAction : public BaseAction
{
	public:
		StartAction() = default;
		~StartAction() = default;
		void operator ()(GlobalVariables::StateActionParams& actionParams);
};

class EndAction : public BaseAction
{
	public:
		EndAction() = default;
		~EndAction() = default;
		void operator ()(GlobalVariables::StateActionParams& actionParams);
};

/**
 * verifies the '(' operator
 * */
class BracketOpenAction_Infix : public BaseAction
{
	public:
		BracketOpenAction_Infix() = default;
		~BracketOpenAction_Infix() = default;
		void operator ()(GlobalVariables::StateActionParams& actionParams);
};

/**
 * verifies the ')' operator
 * */
class BracketCloseAction_Infix : public BaseAction
{
	public:
		BracketCloseAction_Infix() = default;
		~BracketCloseAction_Infix() = default;
		void operator ()(GlobalVariables::StateActionParams& actionParams);
};

class AvgBeginAction_Infix : public BaseAction
{
	public:
		AvgBeginAction_Infix() = default;
		~AvgBeginAction_Infix() = default;
		void operator ()(GlobalVariables::StateActionParams& actionParams);
};

class AvgEndAction_Infix : public BaseAction
{
	public:
		AvgEndAction_Infix() = default;
		~AvgEndAction_Infix() = default;
		void operator ()(GlobalVariables::StateActionParams& actionParams);
};

class EndAction_PnP : public BaseAction
{
	public:
		EndAction_PnP() = default;
		~EndAction_PnP() = default;
		void operator ()(GlobalVariables::StateActionParams& actionParams);
};

class OperatorAction_PnP : public BaseAction
{
	public:
		OperatorAction_PnP() = default;
		~OperatorAction_PnP() = default;
		void operator ()(GlobalVariables::StateActionParams& actionParams);
};

class OperandAction_PnP : public BaseAction
{
	public:
		OperandAction_PnP() = default;
		~OperandAction_PnP() = default;
		void operator ()(GlobalVariables::StateActionParams& actionParams);
};

class AvgAction_PnP : public BaseAction
{
	public:
		AvgAction_PnP() = default;
		~AvgAction_PnP() = default;
		void operator ()(GlobalVariables::StateActionParams& actionParams);
};

#endif
