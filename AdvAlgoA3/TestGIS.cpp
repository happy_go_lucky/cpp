/**
 * 
 * build a data structure to store GIS information
 * data structure should be indexable for support following
 * import a new GIS record to db
 * search GIS record by geographic coordinates
 * search GIS record by feature name
 * search GIS record by state name
 * search GIS record by a rectangular geographic region
 * use buffer pool to assist searches. check below
 * display GIS information in a human readable format
 * 
 * files are passed as arguments
 * arg1: database filename
 * arg2: command script filename
 * arg3: log filename
 * 
 * database file is originally empty file, 
 * and if it already exists it should be recreated empty
 * 
 * program should report error if the command script is not found and exit
 * 
 * log file should be rewritten everytime the program is run
 * 
 * the entries in GIS file can have same coordinates for different locations
 * therefore it can't be the primary unique key
 * Use DMS format for coordinates
 * GIS file will be indexed by Feature Name and State (abbreviation) fields
 * 
 * Include a buffer pool as frontend for GIS database file to improve search speed
 * 
 * Store 
 * 
 **/

#include <iostream>
#include <fstream>
#include <cstring>
#include <vector>
#include <map>
#include "QuadTree.h"
#include "CommandStructs.h"
#include "HashRecord.h"
#include "HashTable.h"
#include "HelperFunctions.h"

using namespace std;

static const short NUM_REQUIRED_FILES = 3;
static const short FILE_STREAM_BUFFER_SIZE = 100;
static const char COMMAND_SCRIPT_COMMENT = ';';
static const char COMMAND_SCRIPT_DELIMITER = '\n';
static constexpr const char* TOKEN_SEPARATOR = "\t";

static constexpr const char* COMM_WORLD = "world";
static constexpr const char* COMM_IMPORT = "import";
static constexpr const char* COMM_DEBUG = "debug";
static constexpr const char* COMM_QUIT = "quit";
static constexpr const char* COMM_WHAT_IS_AT = "what_is_at";
static constexpr const char* COMM_WHAT_IS = "what_is";
static constexpr const char* COMM_WHAT_IS_IN = "what_is_in";

enum DataOrder
{
	FEATURE_ID = 0,
	FEATURE_NAME,
	FEATURE_CLASS,
	STATE_ALPHA,
	STATE_NUMERIC,
	COUNTY_NAME,
	COUNTY_NUMERIC,
	PRIMARY_LAT_DMS,
	PRIM_LONG_DMS,
	PRIM_LAT_DEC,
	PRIM_LONG_DEC,
	SOURCE_LAT_DMS,
	SOURCE_LONG_DMS,
	SOURCE_LAT_DEC,
	SOURCE_LONG_DEC,
	ELEV_IN_M,
	ELEV_IN_FT,
	MAP_NAME,
	DATE_CREATED,
	DATE_EDITED
};

enum FileOrder
{
	PROCESS_NAME = 0,
	DB_FILE,
	COMM_FILE,
	LOG_FILE
};

HashTable* dbHash;
QuadTree* dbTree;
map<string, size_t>* cacheLRU_hash;
map<string, vector<size_t>>* cacheLRU_tree;

char* dbFileName;
char* commScriptFileName;
char* logFileName;
WorldCommand* worldCommand;

fstream dbFileStream;
fstream commScriptFileStream;
fstream logFileStream;

vector<BaseCommand*> commandVector;

bool requiredFilesSpecified(int argCount);
void closeFileStreams();
void openFileStreams();
void getFileNames(char** argVector);
bool requiredFilesExist();
void parseCommandScript();
void parseBufferString(char* buffer);
void parseCommandType(char* commandStr, BaseCommand*& command);
void buildCommand(BaseCommand* command, char* param);
void clearCommandVector();
void cleanUp();

int main(int argCount, char** argVector)
{
	if (!requiredFilesSpecified(argCount))
	{
		return EXIT_FAILURE;
	}
	
	getFileNames(argVector);
	openFileStreams();
	if (!requiredFilesExist())
	{
		return EXIT_FAILURE;
	}
	
	dbHash = new HashTable();
	dbTree = new QuadTree();
	cacheLRU_hash = new map<string, size_t>();
	cacheLRU_tree = new map<string, vector<size_t>>();
	
	parseCommandScript();
	
	closeFileStreams();
	
	cleanUp();
	
	return EXIT_SUCCESS;
}

bool requiredFilesSpecified(int argCount)
{
	if (argCount < NUM_REQUIRED_FILES + 1)
	{
		cerr << "you'll need to specify " << NUM_REQUIRED_FILES << " files to run" << endl;
		cerr << "please run the program as follows: " << endl;
		cerr << "./GIS databaseFileName commandscriptFileName logFileName" << endl;
		return false;
	}
	return true;
}

bool requiredFilesExist()
{
	if (!commScriptFileStream || commScriptFileStream.fail())
	{
		cerr << "failed to find command script file." << endl;
		return false;
	}
	
	if (!logFileStream || logFileStream.fail())
	{
		cerr << "failed to find log file." << endl;
		return false;
	}
	
	if (!dbFileStream || dbFileStream.fail())
	{
		cerr << "failed to find database file." << endl;
		return false;
	}
	return true;
}

void openFileStreams()
{
	// trunc will also create new files if not exist 
	dbFileStream.open(dbFileName, ios::out | ios::in | ios::trunc);
	logFileStream.open(logFileName, ios::out | ios::in | ios::trunc);
	commScriptFileStream.open(commScriptFileName, ios::in);
}

void closeFileStreams()
{
	dbFileStream.close();
	commScriptFileStream.close();
	logFileStream.close();
}

void getFileNames(char** argVector)
{
	dbFileName = argVector[DB_FILE];
	commScriptFileName = argVector[COMM_FILE];
	logFileName = argVector[LOG_FILE];
}

void parseCommandScript()
{
	char lineBuffer[FILE_STREAM_BUFFER_SIZE];
	while (!commScriptFileStream.eof())
	{
		commScriptFileStream.getline(lineBuffer, FILE_STREAM_BUFFER_SIZE);
		parseBufferString(lineBuffer);
	}
}

void parseBufferString(char* buffer)
{
	if (buffer == nullptr)
	{
		return;
	}
	if (buffer[0] == COMMAND_SCRIPT_COMMENT)
	{
		return;
	}
	buffer = strtok(buffer, TOKEN_SEPARATOR);
	
	BaseCommand* command;
	
	if (buffer != NULL)
	{
		parseCommandType(buffer, command);
	}
	else
	{
		return;
	}
	if (command == nullptr)
	{
		return;
	}
	
	(*command).startBuild();
	bool isParsingData = true;
	while (isParsingData)
	{
		buffer = strtok(NULL, TOKEN_SEPARATOR);
		if (buffer != NULL)
		{
			buildCommand(command, buffer);
		}
		else
		{
			isParsingData = false;
		}
	}
	
	(*command).finishBuild();
	(*command).execute();
}

void parseCommandType(char* commandStr, BaseCommand*& command)
{
	if (strcmp(commandStr, COMM_WORLD) == 0)
	{
		command = worldCommand = new WorldCommand();
		command->logFileStream = &logFileStream;
	}
	else if (strcmp(commandStr, COMM_IMPORT) == 0)
	{
		ImportCommand* comm = new ImportCommand();
		comm->worldCommand = worldCommand;
		comm->hash = dbHash;
		comm->tree = dbTree;
		comm->dbFileStream = &dbFileStream;
		comm->logFileStream = &logFileStream;
		command = comm;
	}
	else if (strcmp(commandStr, COMM_DEBUG) == 0)
	{
		DebugCommand* comm = new DebugCommand();
		comm->hash = dbHash;
		comm->tree = dbTree;
		comm->dbFileStream = &dbFileStream;
		comm->logFileStream = &logFileStream;
		command = comm;
	}
	else if (strcmp(commandStr, COMM_QUIT) == 0)
	{
		command = new QuitCommand();
		command->dbFileStream = &dbFileStream;
		command->logFileStream = &logFileStream;
	}
	else if (strcmp(commandStr, COMM_WHAT_IS_AT) == 0)
	{
		WhatIsAtCommand* comm = new WhatIsAtCommand();
		comm->hash = dbHash;
		comm->tree = dbTree;
		comm->cacheHash = cacheLRU_hash;
		comm->cacheTree = cacheLRU_tree;
		comm->dbFileStream = &dbFileStream;
		comm->logFileStream = &logFileStream;
		command = comm;
	}
	else if (strcmp(commandStr, COMM_WHAT_IS) == 0)
	{
		WhatIsCommand* comm = new WhatIsCommand();
		comm->hash = dbHash;
		comm->tree = dbTree;
		comm->cacheHash = cacheLRU_hash;
		comm->cacheTree = cacheLRU_tree;
		comm->dbFileStream = &dbFileStream;
		comm->logFileStream = &logFileStream;
		command = comm;
	}
	else if (strcmp(commandStr, COMM_WHAT_IS_IN) == 0)
	{
		WhatIsInCommand* comm = new WhatIsInCommand();
		comm->hash = dbHash;
		comm->tree = dbTree;
		comm->cacheHash = cacheLRU_hash;
		comm->cacheTree = cacheLRU_tree;
		comm->dbFileStream = &dbFileStream;
		comm->logFileStream = &logFileStream;
		command = comm;
	}
	else
	{
		cerr << "invalid command " << commandStr << " found" << endl;
		command = nullptr;
		return;
	}
	
	commandVector.push_back(command);
}


void buildCommand(BaseCommand* command, char* param)
{
	(*command).build(param);
}

void clearCommandVector()
{
	const size_t& size = commandVector.size(); 
	for (int ii = 0; ii < size; ii++)
	{
		delete commandVector[ii];
	}
}

void cleanUp()
{
	clearCommandVector();
	delete dbHash;
	delete dbTree;
	delete cacheLRU_hash;
	delete cacheLRU_tree;
}
