#include <iostream>
#include <string>
#include "Calculator.h"

// used for checking if the file redirection has been used
// since this is an os dependent function, therefore, check if using unix or windows os
#ifdef __unix__
#include <unistd.h>
#else
#include <io.h>
#endif

using std::cout;
using std::cin;
using std::cerr;
using std::endl;
using std::string;

const char* INPUT_FILE = "input.txt";	// for safety, in case no filename is provided in the redirection 
const int STDIN_NOT_TERMINAL = 0;	// isatty() returns 0 if the std input is not a terminal and 1 otherwise
const int STDIN_TERMINAL = 1;


void testCalculator();
void testCalculatorWithCopyCons();
void testCalculatorWithLoadFile();

int main()
{
	bool isFileRedirected = false;
	#ifdef __unix__
	isFileRedirected = isatty( STDIN_FILENO ) == STDIN_NOT_TERMINAL;
	#else
	isFileRedirected = _isatty( _fileno( stdin ) ) == STDIN_NOT_TERMINAL;
	#endif
	
	if ( isFileRedirected )
	{
		// in case of build errors, comment everything in main except following function call, this is all we need to test the program
		// rest is just extra fanciness :)
		// if you want to test copy constructor/assignment operator on calculator, try calling testCalculatorWithCopyCons(). They produce the same output though
		testCalculator();
	}
	else
	{
		cout << "no input file is provided in the command line, trying to load hardcoded filename" << endl;
		testCalculatorWithLoadFile();
	}
	
	return EXIT_SUCCESS;
}

// loads the command line specified input file and prints to command line specifed output file
void testCalculator()
{
	Calculator rpn_calc;
	char contents = '\0';
	bool quit = false;
	
	do
	{
		cin >> noskipws >> contents;
		if ( contents == '\0' )
		{
			cout << "please specify input file" << endl;
			break;
		}
		
		quit = rpn_calc.input( contents );
	}
	while ( !quit );
}

// same as testCalculator() but also tests calculator copy
void testCalculatorWithCopyCons()
{
	Calculator rpn_calc;
	Calculator rpn_calc_copy;
	int count = 0;
	bool testCopy = false;
	char contents;
	bool quit = false;
	
	do
	{
		count++;
		if ( count == 10 )
		{
			testCopy = true;
			rpn_calc_copy = rpn_calc;
		}
		cin >> noskipws >> contents;
		if ( testCopy )
		{
			quit = rpn_calc_copy.input( contents );
		}
		else
		{
			quit = rpn_calc.input( contents );
		}
	}
	while ( !quit );
}

// load the hardcoded text file and print on console, just for fun
void testCalculatorWithLoadFile()
{
	Calculator rpn_calc;
	rpn_calc.loadFile( INPUT_FILE );
}
