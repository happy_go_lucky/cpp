void createInterrupt()
{
	sigset_t interruptSignal;	// used for interrupting wait
	sigemptyset(&interruptSignal);	// initialize and empty a signal set
	sigaddset(&interruptSignal, SIGUSR1);
	struct sigaction signalAction;
	signalAction.sa_handler = sigHandler;
	sigaction(SIGUSR1, &signalAction, NULL);
}
void sigHandler(int signalNumber)
{
	switch (signalNumber)
	{
		case SIGUSR1:
		std::cout << "caught SIGUSR1" << std::endl;
		break;
		
		default:
		std::cout << "unknown signal with signal number: " << signalNumber 
			<< " is received" << std::endl;
		break;
	}
}

void fireSignal()
{
	kill(getpid(), SIGUSR1);
}
