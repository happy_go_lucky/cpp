#ifndef LINKEDLIST_H
#define LINKEDLIST_H

#include "ListNode.h"
#include "ListNode.cpp"

template<class TYPE>
class LinkedList
{
	public:
		LinkedList();
		~LinkedList();
		void deleteNode( ListNode<TYPE>* );
		void deleteHeadNode();
		void addNodeAfter( const ListNode<TYPE>*, const TYPE& );
		void addNodeToHead( const TYPE& data );
		ListNode<TYPE>* searchData( const TYPE&, int& ) const;
		void addNodeToEnd( const TYPE& );
		void printList() const;
		int length() const;
		bool isEmpty() const;
		
	private:
		ListNode<TYPE>* head;
		ListNode<TYPE>* tempNode;
		void _cleanupAllNodes( const ListNode<TYPE>* );
};

#endif