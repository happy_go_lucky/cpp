#ifndef DD_COORD_H
#define DD_COORD_H

#include <iostream>
#include <cmath>
#include <string>

using namespace std;

class DDCoord
{
	public:
		static constexpr const char* NAN_VALUE = "1";
	
		DDCoord() noexcept;
		DDCoord(float value);
		DDCoord(char* coordStr);
		DDCoord(const DDCoord& value);
		DDCoord(DDCoord&& value) noexcept;
		~DDCoord();
		
		friend ostream& operator <<(ostream& out, const DDCoord& value);
		DDCoord& operator =(const DDCoord& coord);
		DDCoord& operator =(DDCoord&& coord);
		DDCoord& operator =(char* coordStr);
		
		void setDegrees(float value) noexcept;
		float getDegrees() const noexcept;
		bool isValid() const noexcept;
		
	private:
		float _degVal;
		void convertFromString(char* coordStr);
};

#endif
