/**
 * CS is set to LOW signal by the master singalling  the slave that the 
 * master is about to begin the transmission.
 * CLK turns on and the master starts square wave transmission. The first
 * bit is sometimes sampled to sync with the clock
 * When data transmissin is complete, CS is set to high by the master
 * 
 * */

#ifndef SPI_INTERFACE_H
#define SPI_INTERFACE_H

#include <string>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <linux/types.h>
#include <linux/spi/spidev.h>
#include <bitset>

#include "GpioDefines.h"

namespace gpio
{
	class SpiInterface
	{
		public:
			static constexpr const char* SPI_PATH = "/dev/spidev";
			static const int SPI_PATH_LENGTH = 15;
		
			SpiInterface();
			~SpiInterface();
			
			SpiInterface(const SpiInterface& spi) = delete;
			SpiInterface(SpiInterface&& spi) = delete;
			SpiInterface& operator =(const SpiInterface& spi) = delete;
			SpiInterface& operator =(SpiInterface&& spi) = delete;
			
			void init();
			void open();
			unsigned char readRegister(unsigned int registerAddress);
			unsigned char readRegisters(unsigned int fromRegisterAddress, unsigned int numRegisters);
			int write(uByte value);
			int write(uByte value, int length);
			int setSpeed(uDoubleWord speed);
			int setMode(SpiMode mode);
			void close();
			int transfer(uByte dataRead, uByte dataWrtie, int length);
		
		private:
			std::string _deviceFilename;
			int _deviceFD;
			SpiMode _currentMode;
			uDoubleWord _speed;
			unsigned int _delay;
			uByte _bitsPerWord;
			
			void _printHex(int value) const;
			void _printBinary(int value) const;
	};
}

#endif
