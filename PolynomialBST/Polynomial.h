#ifndef POLYNOMIAL_H
#define POLYNOMIAL_H

#include "BinarySearchTree.h"
#include "BinarySearchTree.cpp"
#include "Term.h"

class Polynomial
{
	public:
		Polynomial();
		Polynomial( const Polynomial& );
		~Polynomial();
		
		bool isZero() const;
		void print() const;
		int evalulate( int );
		int degree();
		int getCoeffOfDegree( int );
		bool insertTerm( int, int );
		void diff_x();
		
		friend ostream& operator << ( ostream&, const Polynomial& );
		Polynomial operator + ( const Polynomial& ) const;
		const Polynomial& operator = ( const Polynomial& );
		
	private:
		BinarySearchTree<Term> _poly;
};

// helper functors

template <class T>
class GetDegree: public BaseFunct<T>
{
	public:
		GetDegree();
		void operator () ( BST_Node<T>* );
		int getDegree() const;
	
	private:
		int _degree;
		// since degree can be negative, this fixes the issue with degree being below intial value of 0
		bool _isDegreeSet;
};

template <class T>
class Evaluate: public BaseFunct<T>
{
	public:
		Evaluate( int );
		void operator () ( BST_Node<T>* );
		int getResult() const;
		int computePower( int, int ) const;
	
	private:
		int _result;
		int _indeterminateValue;
};

template <class T>
class TreeToQueue: public BaseFunct<T>
{
	public:
		TreeToQueue();
		~TreeToQueue();
		void operator () ( BST_Node<T>* );
		Deque<T> getQueue() const;
	
	private:
		Deque<T> _deque;
};

template <class T>
class DifferentiatePoly: public BaseFunct<T>
{
	public:
		DifferentiatePoly();
		void operator () ( BST_Node<T>* );
};


#endif
