#include <iostream>
#include <cstdlib>
#include <fstream>

#include "Matrix.h"
#include "Matrix.cpp"

using std::cout;
using std::endl;
using std::ifstream;

void populateMatrix( Matrix<int>* const aMatrix );
void populateMatrix( Matrix<double>* const aMatrix );
double getRandomDouble( const int range );
int getRandomInt( const int range );
void test();

const int RAND_KEY = time( NULL );
const int DEFAULT_RAND_KEY = 1;

int main()
{
	srand( RAND_KEY );
	
	test();
	
	srand( DEFAULT_RAND_KEY );
	return EXIT_SUCCESS;
}

void test()
{
	Matrix<double>* squareMatrix = new Matrix<double>( 3, 3 );
	squareMatrix->setDataAt( 6, 1, 1 );
	squareMatrix->setDataAt( 3, 1, 2 );
	squareMatrix->setDataAt( 6, 1, 3 );
	squareMatrix->setDataAt( 5, 2, 1 );
	squareMatrix->setDataAt( 6, 2, 2 );
	squareMatrix->setDataAt( 8, 2, 3 );
	squareMatrix->setDataAt( 8, 3, 1 );
	squareMatrix->setDataAt( 9, 3, 2 );
	squareMatrix->setDataAt( 10, 3, 3 );
	
	Matrix<double>* matrixDouble = new Matrix<double>( 3, 3 );
	populateMatrix( matrixDouble );
	
	cout << "printing squareMatrix" << endl;
	squareMatrix->printMatrix();
	cout << "printing squareMatrix transpose" << endl;
	squareMatrix->printMatrixTranspose();
	
	cout << "determinant = " << squareMatrix->determinant() << endl;

	cout << "computing minor matrix for squareMatrix" << endl;
	Matrix<double> squareMatrixMinorMatrix = squareMatrix->getMinorMatrix();

	cout << "printing minor matrix of squareMatrix" << endl;
	squareMatrixMinorMatrix.printMatrix();

	cout << "computing cofactor matrix of squareMatrix" << endl;
	Matrix<double> squareMatrixCofactorMatrix = squareMatrix->getCofactorMatrix();

	cout << "printing cofactor matrix of squareMatrix" << endl;
	squareMatrixCofactorMatrix.printMatrix();

	cout << "computing adjugate matrix of squareMatrix" << endl;
	Matrix<double> squareMatrixAdjugateMatrix = squareMatrix->getAdjugateMatrix();

	cout << "printing adjugate matrix of squareMatrix" << endl;
	squareMatrixAdjugateMatrix.printMatrix();

	try
	{
		cout << "computing inverse matrix of squareMatrix" << endl;
		Matrix<double> squareMatrixInverse = squareMatrix->inverse();

		cout << "printing inverse matrix of squareMatrix" << endl;
		squareMatrixInverse.printMatrix();
	}
	catch ( MatrixException& except )
	{
		cout << endl << "exception: " << except.what() << endl;
	}

	cout << "printing double matrix" << endl;
	matrixDouble->printMatrix();
	
	cout << "testing square matrix multiplication" << endl;
	( *squareMatrix * *squareMatrix ).printMatrix();
	
	Matrix<double>* matrixCol = new Matrix<double>( 3, 1 );
	Matrix<double>* matrixRow = new Matrix<double>( 1, 3 );
	populateMatrix( matrixRow );
	populateMatrix( matrixCol );
	cout << "testing column matrix" << endl;
	matrixCol->printMatrix();

	cout << "testing row matrix" << endl;
	matrixRow->printMatrix();

	try
	{
		cout << "testing invalid matrix multiplication" << endl;
		Matrix<double> result = *matrixCol * *squareMatrix;
		result.printMatrix();
	}
	catch ( MatrixException& except )
	{
		cout << endl << "exception: " << except.what() << endl;
	}

	try
	{
		cout << "testing non square matrix multiplication" << endl;
		Matrix<double> result2 = *matrixRow * *squareMatrix;
		result2.printMatrix();
	}
	catch ( MatrixException& except )
	{
		cout << endl << "exception: " << except.what() << endl;
	}

	delete matrixRow;
	delete matrixCol;
	delete squareMatrix;
	delete matrixDouble;
}

void populateMatrix( Matrix<int>* const aMatrix )
{
	unsigned row = 1;
	unsigned col = 1;
	
	while ( row <= aMatrix->numRows() )
	{
		while ( col <= aMatrix->numCols() )
		{
			aMatrix->setDataAt( getRandomInt( 100 ), row, col );
			col++;
		}
		col = 1;
		row++;
	}
}

void populateMatrix( Matrix<double>* const aMatrix )
{
	unsigned row = 1;
	unsigned col = 1;
	
	while ( row <= aMatrix->numRows() )
	{
		while ( col <= aMatrix->numCols() )
		{
			aMatrix->setDataAt( getRandomDouble( 100 ), row, col );
			col++;
		}
		col = 1;
		row++;
	}
}

// postcondition: returns a random number between 0 and range
int getRandomInt( const int range )
{
	return rand() & range;
}

// postcondition: returns a random number between 0 and range
double getRandomDouble( const int range )
{
	return ( ( double )( rand() & range * 100 ) / 100 );
}