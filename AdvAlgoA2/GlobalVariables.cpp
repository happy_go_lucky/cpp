#include "GlobalVariables.h"

// from lowest to highest priority Left -> Right
const string GlobalVariables::LIST_OF_OPERATORS = "=@-+*/^()";
const string GlobalVariables::WHITESPACES = " \n\v\f\t\r";
const char GlobalVariables::NULL_TERM = '\0';
const char GlobalVariables::PARENTHESIS_OPEN_OPERATOR = '(';
const char GlobalVariables::PARENTHESIS_CLOSE_OPERATOR = ')';
const char GlobalVariables::AVG_OPERATOR = '@';
const char GlobalVariables::ADD_OPERATOR = '+';
const char GlobalVariables::SUB_OPERATOR = '-';
const char GlobalVariables::MUL_OPERATOR = '*';
const char GlobalVariables::DIV_OPERATOR = '/';
const char GlobalVariables::EXPO_OPERATOR = '^';
const char GlobalVariables::EVAL_OPERATOR = '=';
