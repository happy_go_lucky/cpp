#include "Node.h"

template <typename T>
Node<T>::Node( const T& data, Node<T>* next, Node<T>* upLevel, Node<T>* downLevel )
{
	this->data = data;
	this->next = next;
	this->upLevel = upLevel;
	this->downLevel = downLevel;
}

template <typename T>
Node<T>::~Node()
{
	
}

template <typename T>
void Node<T>::setData( const T& data )
{
	this->data = data;
}

template <typename T>
void Node<T>::setNext( Node<T>* next )
{
	this->next = next;
}

template <typename T>
void Node<T>::setUp( Node<T>* up )
{
	upLevel = up;
}

template <typename T>
void Node<T>::setDown( Node<T>* down )
{
	downLevel = down;
}

template <typename T>
T Node<T>::getData() const
{
	return data;
}

template <typename T>
Node<T>* Node<T>::getNext() const
{
	return next;
}

template <typename T>
Node<T>* Node<T>::getUp() const
{
	return upLevel;
}

template <typename T>
Node<T>* Node<T>::getDown() const
{
	return downLevel;
}
