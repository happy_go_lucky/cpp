#ifndef CONDITION_VARIABLE_H
#define CONDITION_VARIABLE_H

#include <pthread.h>
#include <ctime>
#include <string.h>
#include <functional>
#include "MutexLock.h"
#include "HelperFxns.h"
#include "ThreadException.h"
#include "ThreadDefs.h"

using namespace std;

namespace posixthreads
{
	class ConditionVariable
	{
		public:
			ConditionVariable(MutexLock& mutex) noexcept;
			ConditionVariable(const ConditionVariable& cv) = delete;
			ConditionVariable(ConditionVariable&& cv) = delete;
			~ConditionVariable();
			
			ConditionVariable& operator =(const ConditionVariable& cv) = delete;
			ConditionVariable& operator =(ConditionVariable&& cv) = delete;
			
			void initialize(ProcessAccessType accessType);
			void cleanup();
			
			template <typename Type>
			void signal(std::function<void(const Type&)> condUpdate, 
					const Type& value);
			
			template <typename Type>
			void signalAll(std::function<void(const Type&)> condUpdate, 
					const Type& value);
			
			template <typename Type>
			void waitForSignal(std::function<bool(const Type&)> condPredicate, 
					Type expectedValue);
			
			template <typename Type>
			void timedWait(int waitTimeInSeconds, 
					std::function<bool(const Type&)> condPredicate, Type expectedValue);
			
			ProcessAccessType getAccessType() const noexcept;
			// TODO: look into pthread_condattr_setclock if needed for timed wait
		
		private:
			pthread_cond_t* _cv;
			pthread_condattr_t* _cvAttrib;
			// mutex is external because it is used by the caller thread to
			// set and unset the condition that will be used for the while check 
			MutexLock& _mutexLock;
			
			void _setAccessType(ProcessAccessType accessType);
	};
}
#endif
