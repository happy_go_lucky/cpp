#ifndef GPIO_PIN_H
#define GPIO_PIN_H

#include <iostream>
#include <fstream>
#include <string>
#include <cstring>
#include <memory>
#include <unistd.h>	// for the microsecond sleep function
#include "GpioDefines.h"
#include "GpioWaitForEdgeWorker.h"

using namespace std;
namespace gpio
{
	class GpioPin
	{
		friend class GpioManager;
		
		public:
			static constexpr const char* GPIO_PATH = "/sys/class/gpio/";
			static constexpr const char* GPIO_PIN_PREFIX = "gpio";
			
			inline GpioNumber getPinNumber() const noexcept
			{
				return _pinNumber;
			}
			
			GpioPin() = delete;
			GpioPin(GpioPin&& gpioPin) = delete;
			GpioPin& operator =(GpioPin&& gpioPin) = delete;
			GpioPin(const GpioPin& gpioPin) = delete;
			GpioPin& operator =(const GpioPin& gpioPin) = delete;
			
			// General I/O settings
			int setMode(SignalMode signalMode);
			SignalMode getMode() const;
			int setValue(SignalValue pinValue);
			SignalValue getValue() const;
			void setDebounceTime(int timeInMillis);
			
			int setSignalEdge(SignalEdge value);
			SignalEdge getSignalEdge() const;
			
			bool waitForSignalEdge(SignalEdge value, std::function<void()> callbackFunction,
					bool freshStart = false);
			
			inline bool isWaitingForEdge() const
			{
				return _waitForEdgeWorker->isWaiting();
			}
			
			/*
			 * This struct is used as a wrapper for deletion for smart pointers
			 * since the constructor and destructor are private, smart pointers
			 * won't be able to invoke the private destructor.
			 * */
			struct Cleanup
			{
				void operator ()(GpioPin* pin) const
				{
					delete pin;
				}
			};
			
			inline pthread_t getEdgeWorkerId() const
			{
				return _waitForEdgeWorker->getId();
			}
			
			inline GpioWaitForEdgeWorker* getEdgeWorkerRef()
			{
				return _waitForEdgeWorker;
			}
		
		private:
			explicit GpioPin(GpioNumber gpioPinNumber);
			~GpioPin();
		
			GpioNumber _pinNumber;
			string _pinPath;
			GpioWaitForEdgeWorker* _waitForEdgeWorker;
			ofstream _bitStream;
			bool _isBitStreamOpen;
			
			int _write(const char* path, const char* gpioCmd, const char* value) const;
			std::string _read(const char* path, const char* gpioCmd) const;
			int _exportGPIO() const;
			int _unexportGPIO() const;
	};
}
#endif
