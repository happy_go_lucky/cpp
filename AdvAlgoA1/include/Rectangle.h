#ifndef RECTANGLE_H
#define RECTANGLE_H

#include <iostream>
#include <assert.h>

using namespace std;

class Rectangle
{
public:
	Rectangle() noexcept;
	Rectangle(int width, int length) noexcept;
	Rectangle(double width, double length) = delete;
	Rectangle(float width, float length) = delete;
	~Rectangle();
	
	Rectangle(const Rectangle& rec);
	Rectangle(Rectangle&& rec);
	
	friend ostream& operator << (ostream& s, const Rectangle& r);
	Rectangle& operator= (const Rectangle& rec);
	Rectangle& operator= (Rectangle&& rec) noexcept;
	
	bool operator== (const Rectangle& rec) const noexcept;
	bool operator== (const Rectangle&& rec) const noexcept;
	
	bool areaCompare(const Rectangle& rect) const;
	bool perimeterCompare(const Rectangle& rect) const;
	
	inline int getLength() const
	{
		return _length;
	}
	
	inline int getWidth() const
	{
		return _width;
	}
	
	inline int getArea() const
	{
		return _area;
	}
	
	inline int getPerimeter() const
	{
		return _perimeter;
	}
	
	void setLength(int length);
	void setWidth(int width);
	
	void checkValidity() const; // invariant

private:
	int _width;
	int _length;
	int _area;
	int _perimeter;
	
	void updateAreaPerimeter();
};

#endif
