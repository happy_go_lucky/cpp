#include "PRQuadData.h"

PRQuadData::PRQuadData() noexcept
{
	// nothing to do
}

PRQuadData::PRQuadData(const PRQuadData& node)
{
	_offsetList = node._offsetList;
	_lat = node._lat;
	_long = node._long;
}

PRQuadData::PRQuadData(const QuadTreeRecord& record)
{
	_offsetList.push_back(record.recordByteOffset);
	_lat = record.primary_lat_dms;
	_long = record.primary_long_dms;
}

PRQuadData::~PRQuadData()
{
	
}

PRQuadData& PRQuadData::operator =(const PRQuadData& node)
{
	if (this != &node)
	{
		//~ helpfxns::printVector(_offsetList);
		//~ helpfxns::printVector(node._offsetList);
		_offsetList = node._offsetList;
		_lat = node._lat;
		_long = node._long;
	}
	
	return *this;
}

PRQuadData& PRQuadData::operator =(const QuadTreeRecord& record)
{
	_offsetList.push_back(record.recordByteOffset);
	_lat = record.primary_lat_dms;
	_long = record.primary_long_dms;
	
	return *this;
}

bool PRQuadData::operator ==(const PRQuadData& node)
{
	if (_lat == node._lat && _long == node._long)
	{
		if (_offsetList.size() != node._offsetList.size())
		{
			return false;
		}
		
		const size_t& size = node._offsetList.size();
		for (size_t ii = 0; ii < size; ii++)
		{
			if (_offsetList[ii] != node._offsetList[ii])
			{
				return false;
			}
		}
		
		return true;
	}
	return false;
}

bool PRQuadData::operator ==(const QuadTreeRecord& record)
{
	if (_lat == record.primary_lat_dms && _long == record.primary_long_dms)
	{
		const size_t& size = _offsetList.size();
		for (size_t ii = 0; ii < size; ii++)
		{
			if (_offsetList[ii] == record.recordByteOffset)
			{
				return true;
			}
		}
		
		return false;
	}
	return false;
}

ostream& operator <<(ostream& out, const PRQuadData& node)
{
	if (node.isNull())
	{
		out << "*";
	}
	else
	{
		out << "[(" << node._lat.getTotalSeconds() << ", " 
			<< node._long.getTotalSeconds() << "), ";
		const size_t& size = node._offsetList.size();
		for (size_t ii = 0; ii < size; ii++)
		{
			out << node._offsetList[ii];
			if (ii < size - 1)
			{
				out << ", ";
			}
		}
		out << "]";
	}
	return out;
}

void PRQuadData::setValue(const QuadTreeRecord& record)
{
	_offsetList.push_back(record.recordByteOffset);
	_lat = record.primary_lat_dms;
	_long = record.primary_long_dms;
}

bool PRQuadData::isNull() const noexcept
{
	return _offsetList.size() == 0;
}

void PRQuadData::updateOffset(size_t value)
{
	_offsetList.push_back(value);
}

void PRQuadData::updateOffset(const vector<size_t>& listOffset)
{
	const size_t& size = listOffset.size();
	for (size_t ii = 0; ii < size; ii++)
	{
		_offsetList.push_back(listOffset[ii]);
	}
}

DMSCoord PRQuadData::getLat() const noexcept
{
	return _lat;
}

DMSCoord PRQuadData::getLong() const noexcept
{
	return _long;
}

const vector<size_t>& PRQuadData::getOffsetList() const noexcept
{
	return _offsetList;
}

bool PRQuadData::isSameOffset(const PRQuadData& record) const
{
	const vector<size_t>& list = record.getOffsetList();
	const size_t& size = list.size();
	if (size != _offsetList.size())
	{
		return false;
	}
	
	for (size_t ii = 0; ii < size; ii++)
	{
		if (_offsetList[ii] != list[ii])
		{
			return false;
		}
	}
	return true;
}

bool PRQuadData::isSameCoords(const PRQuadData& record) const
{
	return _lat.getTotalSeconds() == record.getLat().getTotalSeconds() &&
			_long.getTotalSeconds() == record.getLong().getTotalSeconds();
}
