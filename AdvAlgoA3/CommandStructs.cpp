#include "CommandStructs.h"

void WorldCommand::startBuild()
{
	parameterType = static_cast<int>(WorldCommand::WEST_LONG);
}

void WorldCommand::build(char* paramString)
{
	if (parameterType == static_cast<int>(WorldCommand::WEST_LONG))
	{
		westLongitude = paramString;
		parameterType = static_cast<int>(WorldCommand::EAST_LONG);
	}
	else if (parameterType == static_cast<int>(WorldCommand::EAST_LONG))
	{
		eastLongitude = paramString;
		parameterType = static_cast<int>(WorldCommand::SOUTH_LONG);
	}
	else if (parameterType == static_cast<int>(WorldCommand::SOUTH_LONG))
	{
		southLatitude = paramString;
		parameterType = static_cast<int>(WorldCommand::NORTH_LONG);
	}
	else if (parameterType == static_cast<int>(WorldCommand::NORTH_LONG))
	{
		northLatitude = paramString;
		parameterType = static_cast<int>(WorldCommand::INVALID);
	}
	else
	{
		cerr << "incorrect command parameter encountered for workd command" << endl;
	}
}

void WorldCommand::finishBuild()
{
	parameterType = static_cast<int>(WorldCommand::INVALID);
}

void WorldCommand::execute()
{
	(*logFileStream) << "executing world command" << endl;
	printDMS();
	printDD();
	printBoundaries();
}

void WorldCommand::printDMS()
{
	(*logFileStream) << "World boundaries in DMS:" << endl;
	(*logFileStream) << setw(25) << northLatitude << endl;
	(*logFileStream) << setw(15) << westLongitude;
	(*logFileStream) << setw(15) << eastLongitude << endl;
	(*logFileStream) << setw(25) << southLatitude << endl;
}

void WorldCommand::printDD()
{
	(*logFileStream) << "World boundaries in decimal degrees:" << endl;
	(*logFileStream) << setw(21) << helpfxns::toDecimal(northLatitude) << endl;
	(*logFileStream) << setw(11) << helpfxns::toDecimal(westLongitude);
	(*logFileStream) << setw(23) << helpfxns::toDecimal(eastLongitude) << endl;
	(*logFileStream) << setw(21) << helpfxns::toDecimal(southLatitude) << endl;
}

void WorldCommand::printBoundaries()
{
	(*logFileStream) << "World boundaries in seconds:" << endl;
	(*logFileStream) << setw(21) << northLatitude.getTotalSeconds() << endl;
	(*logFileStream) << setw(11) << westLongitude.getTotalSeconds();
	(*logFileStream) << setw(23) << eastLongitude.getTotalSeconds() << endl;
	(*logFileStream) << setw(21) << southLatitude.getTotalSeconds() << endl;
}
	
void ImportCommand::startBuild()
{
	importedRecords = 0;
	importedNodes = 0;
	totalRecords = 0;
	parameterType = static_cast<int>(ImportCommand::FILE_NAME);
	
	tree->setWorldRect(worldCommand->westLongitude, worldCommand->eastLongitude,
						worldCommand->southLatitude, worldCommand->northLatitude);
}

void ImportCommand::build(char* paramString)
{
	if (parameterType == static_cast<int>(ImportCommand::FILE_NAME))
	{
		gisPSVFileName = paramString;
		parameterType = static_cast<int>(ImportCommand::INVALID);
	}
	else
	{
		cerr << "incorrect command parameter encountered for workd command" << endl;
	}
}

void ImportCommand::finishBuild()
{
	parameterType = static_cast<int>(ImportCommand::INVALID);
}

void ImportCommand::execute()
{
	(*logFileStream) << "executing import command" << endl;
	
	openFileStream();
	
	if (!isFileStreamValid())
	{
		return;
	}
	
	parseFileStream();
	closeFileStream();
	printHashStats();
	printTreeStats();
}

void ImportCommand::openFileStream()
{
	(*logFileStream) << "opening: " << gisPSVFileName << endl;
	dataFileStream.open(gisPSVFileName, ios::in);
}

bool ImportCommand::isFileStreamValid()
{
	if (!dataFileStream || dataFileStream.fail())
	{
		cerr << "failed to find data file " << gisPSVFileName << endl;
		return false;
	}
	
	return true;
}

void ImportCommand::parseFileStream()
{
	char rawDataRecord[ImportCommand::FILE_STREAM_BUFFER_SIZE];
	size_t recordBeginOffset = dbFileStream->tellg();
	size_t recordLength = 0;
	// get the number of characters in the db file to fix the offset
	char character = '\0';
	
	do
	{
		recordBeginOffset++;
		character = dbFileStream->peek();
	} while (character != EOF);
	
	// clear the dirty bits caused by reading eof
	dbFileStream->clear();
	// return to position before eof
	dbFileStream->seekg(--recordBeginOffset);
	
	//~ helpfxns::checkStreamErrors(dbFileStream);
	
	// this is to ignore the first line because it is a header
	dataFileStream.getline(rawDataRecord, ImportCommand::FILE_STREAM_BUFFER_SIZE);
	
	while (!dataFileStream.eof())
	{
		dataFileStream.getline(rawDataRecord, ImportCommand::FILE_STREAM_BUFFER_SIZE);
		recordLength = dataFileStream.gcount();
		if (!dataFileStream.eof())
		{
			parseDataRecord(rawDataRecord, recordBeginOffset, recordLength);
		}
	}
}

void ImportCommand::parseDataRecord(char* rawDataRecord, size_t& offset, size_t recordLength)
{
	if (rawDataRecord == nullptr)
	{
		cerr << "data record can't be empty" << endl;
		return;
	}
	
	char* rawRecord = rawDataRecord;
	
	GISData dataRecord;
	int columnCount = static_cast<int>(GISData::FEATURE_ID);
	int numColumns = static_cast<int>(GISData::DATE_EDITED);
	char* dataStr;
	
	while (columnCount <= numColumns)
	{
		dataStr = tokenizecstr(rawDataRecord, DATA_SEPARATOR);
		
		switch (columnCount)
		{
			case static_cast<int>(GISData::FEATURE_ID):
				if (dataStr == nullptr || *dataStr == '\0')
				{
					dataRecord.feature_id = GISData::INVALID_NUMERIC;
				}
				else
				{
					dataRecord.feature_id = stoi(dataStr);
				}
			break;
			
			case static_cast<int>(GISData::FEATURE_NAME):
				if (dataStr == nullptr || *dataStr == '\0')
				{
					cerr << "feature name can't be null" << endl;
					return;
				}
				dataRecord.feature_name = dataStr;
			break;
			
			case static_cast<int>(GISData::FEATURE_CLASS):
				if (dataStr == nullptr || *dataStr == '\0')
				{
					dataRecord.feature_class = GISData::INVALID_STR;
				}
				else
				{
					dataRecord.feature_class = dataStr;
				}
			break;
			
			case static_cast<int>(GISData::STATE_ALPHA):
				if (dataStr == nullptr || *dataStr == '\0')
				{
					cerr << "state name can't be null" << endl;
					return;
				}
				dataRecord.state_alpha = dataStr;
			break;
			
			case static_cast<int>(GISData::STATE_NUMERIC):
				if (dataStr == nullptr || *dataStr == '\0')
				{
					dataRecord.state_numeric = GISData::INVALID_NUMERIC;
				}
				else
				{
					dataRecord.state_numeric = stoi(dataStr);
				}
			break;
			
			case static_cast<int>(GISData::COUNTY_NAME):
				if (dataStr == nullptr || *dataStr == '\0')
				{
					dataRecord.county_name = GISData::INVALID_STR;
				}
				else
				{
					dataRecord.county_name = dataStr;
				}
			break;
			
			case static_cast<int>(GISData::COUNTY_NUMERIC):
				if (dataStr == nullptr || *dataStr == '\0')
				{
					dataRecord.county_numeric = GISData::INVALID_NUMERIC;
				}
				else
				{
					dataRecord.county_numeric = stoi(dataStr);
				}
			break;
			
			case static_cast<int>(GISData::PRIM_LAT_DMS):
				if (dataStr == nullptr || *dataStr == '\0')
				{
					cerr << "primary latitude can't be null" << endl;
					return;
				}
				dataRecord.primary_lat_dms = dataStr;
			break;
			
			case static_cast<int>(GISData::PRIM_LONG_DMS):
				if (dataStr == nullptr || *dataStr == '\0')
				{
					cerr << "primary longitude can't be null" << endl;
					return;
				}
				dataRecord.primary_long_dms = dataStr;
			break;
			
			case static_cast<int>(GISData::PRIM_LAT_DEC):
				if (dataStr == nullptr || *dataStr == '\0')
				{
					cerr << "primary latitude can't be null" << endl;
					return;
				}
				dataRecord.primary_lat_dec = dataStr;
			break;
			
			case static_cast<int>(GISData::PRIM_LONG_DEC):
				if (dataStr == nullptr || *dataStr == '\0')
				{
					cerr << "primary longitude can't be null" << endl;
					return;
				}
				dataRecord.primary_long_dec = dataStr;
			break;
			
			case static_cast<int>(GISData::SOURCE_LAT_DMS):
				if (dataStr == nullptr || *dataStr == '\0')
				{
					dataRecord.source_lat_dms = nullptr;
				}
				else
				{
					dataRecord.source_lat_dms = dataStr;
				}
			break;
			
			case static_cast<int>(GISData::SOURCE_LONG_DMS):
				if (dataStr == nullptr || *dataStr == '\0')
				{
					dataRecord.source_long_dms = nullptr;
				}
				else
				{
					dataRecord.source_long_dms = dataStr;
				}
			break;
			
			case static_cast<int>(GISData::SOURCE_LAT_DEC):
				if (dataStr == nullptr || *dataStr == '\0')
				{
					dataRecord.source_lat_dec = nullptr;
				}
				else
				{
					dataRecord.source_lat_dec = dataStr;
				}
			break;
			
			case static_cast<int>(GISData::SOURCE_LONG_DEC):
				if (dataStr == nullptr || *dataStr == '\0')
				{
					dataRecord.source_long_dec = nullptr;
				}
				else
				{
					dataRecord.source_long_dec = dataStr;
				}
			break;
			
			case static_cast<int>(GISData::ELEV_IN_M):
				if (dataStr == nullptr || *dataStr == '\0')
				{
					dataRecord.elev_in_m = GISData::INVALID_NUMERIC;
				}
				else
				{
					dataRecord.elev_in_m = stoi(dataStr);
				}
			break;
			
			case static_cast<int>(GISData::ELEV_IN_FT):
				if (dataStr == nullptr || *dataStr == '\0')
				{
					dataRecord.elev_in_ft = GISData::INVALID_NUMERIC;
				}
				else
				{
					dataRecord.elev_in_ft = stoi(dataStr);
				}
			break;
			
			case static_cast<int>(GISData::MAP_NAME):
				if (dataStr == nullptr || *dataStr == '\0')
				{
					dataRecord.map_name = GISData::INVALID_STR;
				}
				else
				{
					dataRecord.map_name = dataStr;
				}
			break;
			
			case static_cast<int>(GISData::DATE_CREATED):
				if (dataStr == nullptr || *dataStr == '\0')
				{
					dataRecord.date_created = nullptr;
				}
				else
				{
					dataRecord.date_created = dataStr;
				}
			break;
			
			case static_cast<int>(GISData::DATE_EDITED):
				if (dataStr == nullptr || *dataStr == '\0')
				{
					dataRecord.date_edited = nullptr;
				}
				else
				{
					dataRecord.date_edited = dataStr;
				}
			break;
			
			default:
				cerr << "undefined data encountered in the data file" << endl;
			break;
		}
		
		delete dataStr;
		columnCount++;
	}
	totalRecords++;
	filterDataAndUpdate(rawRecord, dataRecord, offset, recordLength);
}

char* ImportCommand::tokenizecstr(char*& str, char delimiter)
{
	char* end = strchr(str, delimiter);
	if (end == nullptr)
	{
		return nullptr;
	}
	int size = end - str + 1;
	if (size == 0)
	{
		str += size;
		return nullptr;
	}
	else
	{
		char* token = new char[size];
		memcpy(token, str, size);
		token[size - 1] = '\0';
		str += size;
		return token;
	}
}

void ImportCommand::filterDataAndUpdate(char* rawDataRecord, const GISData& dataRecord, 
										size_t& offset, size_t recordLength)
{
	if (dataRecord.primary_long_dms.getTotalSeconds() > worldCommand->westLongitude.getTotalSeconds() && 
		dataRecord.primary_long_dms.getTotalSeconds() < worldCommand->eastLongitude.getTotalSeconds() && 
		dataRecord.primary_lat_dms.getTotalSeconds() > worldCommand->southLatitude.getTotalSeconds() && 
		dataRecord.primary_lat_dms.getTotalSeconds() < worldCommand->northLatitude.getTotalSeconds())
	{
		updateHash(dataRecord, offset);
		updateTree(dataRecord, offset);
		updateDataBaseFile(rawDataRecord, offset, recordLength);
	}
}

void ImportCommand::updateHash(const GISData& dataRecord, size_t offset)
{
	HashRecord hashRecord = {dataRecord.feature_name, dataRecord.state_alpha, offset};
	hash->insert(hashRecord);
	importedRecords++;
}

void ImportCommand::updateTree(const GISData& dataRecord, size_t offset)
{
	QuadTreeRecord treeRecord = {dataRecord.primary_lat_dms, dataRecord.primary_long_dms, offset};
	tree->insertNode(treeRecord);
	importedNodes++;
	//~ (*logFileStream) << "inserted: " << treeRecord << endl;
}

void ImportCommand::updateDataBaseFile(char* rawDataRecord, size_t& offset, size_t recordLength)
{
	dbFileStream->seekp(offset);
	dbFileStream->write(rawDataRecord, recordLength-1);
	dbFileStream->write(ImportCommand::RECORD_DELIMITER_STR, 1);
	offset += recordLength;
}

void ImportCommand::printHashStats()
{
	(*logFileStream) << "Number of records parsed: " << totalRecords << endl;
	(*logFileStream) << "Imported Features by name: " << importedRecords << endl;
	(*logFileStream) << "Longest probe sequence: " << hash->getLongestProbeSequence() << endl;
	(*logFileStream) << "Imported Locations: " << hash->getSize() << endl;
	char defaultFill = (*logFileStream).fill();
	(*logFileStream) << setfill('-') << setw(80) << '-' << endl;
	(*logFileStream) << setfill(defaultFill);
}

void ImportCommand::printTreeStats()
{
	(*logFileStream) << "Number of records parsed: " << totalRecords << endl;
	(*logFileStream) << "Imported Features by name: " << importedNodes << endl;
	char defaultFill = (*logFileStream).fill();
	(*logFileStream) << setfill('-') << setw(80) << '-' << endl;
	(*logFileStream) << setfill(defaultFill);
}


void ImportCommand::closeFileStream()
{
	dataFileStream.close();
}

void DebugCommand::startBuild()
{
	parameterType = static_cast<int>(DebugCommand::DEBUG_TYPE);
}

void DebugCommand::build(char* paramString)
{
	if (parameterType == static_cast<int>(DebugCommand::DEBUG_TYPE))
	{
		debugType = paramString;
		parameterType = static_cast<int>(DebugCommand::INVALID);
	}
	else
	{
		cerr << "incorrect command parameter encountered for workd command" << endl;
	}
}

void DebugCommand::finishBuild()
{
	parameterType = static_cast<int>(DebugCommand::INVALID);
}

void DebugCommand::execute()
{
	(*logFileStream) << "executing debug command" << endl;
	if (debugType.compare(DebugCommand::DEBUG_HASH) == 0)
	{
		(*logFileStream) << "debug hash" << endl;
		hash->printSnapshot(logFileStream);
	}
	
	if (debugType.compare(DebugCommand::DEBUG_QUADTREE) == 0)
	{
		(*logFileStream) << "debug quad" << endl;
		tree->printTree(QuadTree::LEVELORDER, logFileStream);
	}
	
	if (debugType.compare(DebugCommand::DEBUG_BUFFER_POOL) == 0)
	{
		(*logFileStream) << "debug buffer pool" << endl;
		//~ hash->printSnapshot();
	}
}

	
void WhatIsAtCommand::startBuild()
{
	parameterType = static_cast<int>(WhatIsAtCommand::LAT_COORD);
}

void WhatIsAtCommand::build(char* paramString)
{
	if (parameterType == static_cast<int>(WhatIsAtCommand::LAT_COORD))
	{
		latCoord = paramString;
		parameterType = static_cast<int>(WhatIsAtCommand::LONG_COORD);
	}
	else if (parameterType == static_cast<int>(WhatIsAtCommand::LONG_COORD))
	{
		longCoord = paramString;
		parameterType = static_cast<int>(WhatIsAtCommand::INVALID);
	}
	else
	{
		cerr << "incorrect command parameter encountered for workd command" << endl;
	}
}

void WhatIsAtCommand::finishBuild()
{
	parameterType = static_cast<int>(WhatIsAtCommand::INVALID);
}

void WhatIsAtCommand::execute()
{
	(*logFileStream) << "executing what_is_at command" << endl;
	(*logFileStream) << "what_is_at	" << latCoord << "\t" << longCoord << endl;
	vector<size_t> offsetList;
	QuadTreeRecord data = {latCoord, longCoord, 0};
	string key = to_string(latCoord.getTotalSeconds()) + 
					to_string(longCoord.getTotalSeconds());
					
	// search in cache
	map<string, vector<size_t>>::iterator itr = (*cacheTree).find(key);
	if (itr != (*cacheTree).end())
	{
		// key found
		(*logFileStream) << "found offsets in cache: ";
		helpfxns::printVector(itr->second, logFileStream);
		(*logFileStream) << endl;
	}
	else
	{
		// search in tree
		if (tree->searchNode(data, offsetList))
		{
			if ((*cacheHash).size() == WhatIsAtCommand::CACHE_SIZE)
			{
				itr = (*cacheTree).begin();
				(*cacheTree).erase(itr);
			}
			
			// add to cache
			(*cacheTree)[key] = offsetList;
			
			(*logFileStream) << "found offsets: ";
			helpfxns::printVector(offsetList, logFileStream);
			(*logFileStream) << endl;
		}
		else
		{
			(*logFileStream) << "nothing found" << endl;
		}
	}
}
	
void WhatIsInCommand::startBuild()
{
	parameterType = WhatIsInCommand::FLAG;
}

void WhatIsInCommand::build(char* paramString)
{
	if (parameterType == static_cast<int>(WhatIsInCommand::FLAG))
	{
		if (strcmp(paramString, WhatIsInCommand::MODIFIER_LONG) == 0)
		{
			isLong = true;
			parameterType = static_cast<int>(WhatIsInCommand::LAT_COORD);
		}
		if (strcmp(paramString, WhatIsInCommand::MODIFIER_FILTER) == 0)
		{
			isFilter = true;
			parameterType = static_cast<int>(WhatIsInCommand::FLAG_PARAM);
		}
	}
	else if (parameterType == static_cast<int>(WhatIsInCommand::FLAG_PARAM))
	{
		filter = paramString;
		parameterType = static_cast<int>(WhatIsInCommand::LAT_COORD);
	}
	else if (parameterType == static_cast<int>(WhatIsInCommand::LAT_COORD))
	{
		latCoord = paramString;
		parameterType = static_cast<int>(WhatIsInCommand::LONG_COORD);
	}
	else if (parameterType == static_cast<int>(WhatIsInCommand::LONG_COORD))
	{
		longCoord = paramString;
		parameterType = static_cast<int>(WhatIsInCommand::LONG_HALF_WIDTH);
	}
	else if (parameterType == static_cast<int>(WhatIsInCommand::LONG_HALF_WIDTH))
	{
		longHalfWidth = stoi(paramString);
		parameterType = static_cast<int>(WhatIsInCommand::LAT_HALF_HEIGHT);
	}
	else if (parameterType == static_cast<int>(WhatIsInCommand::LAT_HALF_HEIGHT))
	{
		latHalfHeight = stoi(paramString);
		parameterType = static_cast<int>(WhatIsInCommand::INVALID);
	}
	else
	{
		cerr << "incorrect command parameter encountered for what_is_in command" << endl;
	}
}

void WhatIsInCommand::finishBuild()
{
	parameterType = static_cast<int>(WhatIsInCommand::INVALID);
}

void WhatIsInCommand::execute()
{
	(*logFileStream) << "executing what_is_in command" << endl;
}
	
void WhatIsCommand::startBuild()
{
	parameterType = static_cast<int>(WhatIsCommand::FEATURE_NAME);
}

void WhatIsCommand::build(char* paramString)
{
	if (parameterType == static_cast<int>(WhatIsCommand::FEATURE_NAME))
	{
		featureName = paramString;
		parameterType = static_cast<int>(WhatIsCommand::STATE_CODE);
	}
	else if (parameterType == static_cast<int>(WhatIsCommand::STATE_CODE))
	{
		stateCode = paramString;
		parameterType = static_cast<int>(WhatIsCommand::INVALID);
	}
	else
	{
		cerr << "incorrect command parameter encountered for workd command" << endl;
	}
}

void WhatIsCommand::finishBuild()
{
	parameterType = static_cast<int>(WhatIsCommand::INVALID);
}

void WhatIsCommand::execute()
{
	(*logFileStream) << "-----------------------------------" << endl;
	(*logFileStream) << "executing what_is command" << endl;
	(*logFileStream) << "finding offset of: " << featureName << " " << stateCode << endl;
	HashRecord data = {featureName, stateCode, 0};
	
	// search in cache
	string key = featureName+stateCode;
	map<string, size_t>::iterator itr = (*cacheHash).find(key);
	if (itr != (*cacheHash).end())
	{
		// key found
		(*logFileStream) << "found at offset in cache: " << itr->second << endl;
	}
	else
	{
		size_t offset;
		if (hash->search(data, offset))
		{
			if ((*cacheHash).size() == WhatIsCommand::CACHE_SIZE)
			{
				itr = (*cacheHash).begin();
				(*cacheHash).erase(itr);
			}
			
			(*cacheHash)[key] = offset;
			(*logFileStream) << "found at offset: " << offset << endl;
		}
		else
		{
			(*logFileStream) << "not found" << endl;
		}
	}
}


void QuitCommand::startBuild()
{
	
}

void QuitCommand::build(char* paramString)
{
	
}

void QuitCommand::finishBuild()
{
	
}

void QuitCommand::execute()
{
	(*logFileStream) << "executing quit command" << endl;
	(*logFileStream) << "Terminating execution of commands." << endl;
}

/*
char* tokenizeCstr(char* str, char delimiter)
{
	char* begin = str;
    while (begin != nullptr)
    {
    	char* end = strchr(begin, '|');
    	if (end == nullptr)
    	{
    		begin = nullptr;
    		continue;
    	}
    	int size = end - begin;
    	if (size == 0)
    	{
    	    (*logFileStream) << "empty" << endl;
    	}
    	else
    	{
        	char* token = new char[size];
        	memcpy(token, begin, size);
        	token[size] = '\0';
        	(*logFileStream) << token << endl;
    	}
    	begin = end + 1;
}
*/
