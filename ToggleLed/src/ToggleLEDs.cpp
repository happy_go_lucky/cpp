#include <iostream>
#include <unistd.h>  // for the microsecond sleep function
#include <memory>
#include "../inc/LED.h"
#include "../inc/GpioDefines.h"
#include "../inc/GpioManager.h"
#include "../inc/Button.h"

using namespace std;

void toggleLEDs();
void testButton();
void flashLEDs();

static const int FLASH_DELAY = 50'000; // 50 milliseconds

int main(int argC, char* argV[])
{
	cout << "Starting the makeLEDs program" << endl;
	/**
	 * create a shared pointer because this object will be used to store a global
	 * reference. It's done to remove the singleton pattern from the codebase.
	 * This reference will be passed on using a weak pointer so that the ownership
	 * is not shared.
	 * */
	
	testButton();
	
	return EXIT_SUCCESS;
}

void flashLEDs()
{
	gpio::GpioManager* gpioManager = new gpio::GpioManager();
	
	LED led1(gpioManager->reservePinByGpio(gpio::GpioNumber::GPIO_20));
	LED led2(gpioManager->reservePinByGpio(gpio::GpioNumber::GPIO_21));
	
	long sleepTime = 2'000'000;
	cout << "LEDs on for " << sleepTime / 1'000'000 << " seconds" << endl;
	
	led1.turnOn();
	led2.turnOff();
	
	usleep(sleepTime);
	
	cout << "Flashing the LEDs for 5 seconds" << endl;
	for (int i=0; i<50; i++)
	{
		led1.toggleOnOff();
		led2.toggleOnOff();
		
		usleep(FLASH_DELAY);
	}
	
	cout << "The LED1 state is " << static_cast<int>(led1.getState()) << endl;
	cout << "The LED2 state is " << static_cast<int>(led2.getState()) << endl;
	
	delete gpioManager;
	cout << "Finished the flashing LEDs" << endl;
}

void testButton()
{
	gpio::GpioManager* gpioManager = new gpio::GpioManager();
	int numToggles = 0;
	Button button(gpioManager->reservePinByGpio(gpio::GpioNumber::GPIO_25));
	LED led1(gpioManager->reservePinByGpio(gpio::GpioNumber::GPIO_20));
	LED led2(gpioManager->reservePinByGpio(gpio::GpioNumber::GPIO_21));
	
	led1.turnOn();
	led2.turnOff();
	/* 
    * this is run by a different thread hence proper synchronization has to be used where required
    * */
	auto toggleLEDs = [&led1, &led2, &numToggles]() -> void
	{
		led1.toggleOnOff();
		led2.toggleOnOff();
      numToggles++;
	};
	
	button.addEventListener(Button::Event::ON_CLICK, toggleLEDs);
	
	while (numToggles < 10)
	{
		
	}
	
	button.removeEventListener(Button::Event::ON_CLICK);
   led1.turnOff();
	led2.turnOff();
	delete gpioManager;
	cout << "Finished the button press program" << endl;
}
