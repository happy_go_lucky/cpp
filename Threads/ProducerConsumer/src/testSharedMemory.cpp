/**
 * This program tests the functionality of shared memory with semaphores
 * A producer consumer problem is used to execute the test.
 * */

#include <iostream>
#include <pthread.h>
#include <iomanip>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>

#include "../inc/HelperFxns.h"
#include "../inc/Producer.h"
#include "../inc/Consumer.h"
//~ #include "../inc/SemaphoreNamed.h"
//~ #include "../inc/SemaphoreUnnamed.h"
#include "../inc/SharedMemory.h"
#include "../inc/SharedData.h"
#include "../inc/SharedMemoryDefs.h"
#include "../inc/SemaphoreDefs.h"

using namespace std;

void testSharedMemory();
void createSharedMemory();
void destroySharedMemory();
void createProducerProcess();
void producerInstructions();
void consumerInstructions();
void forkInstructions(pid_t childPid);
void createNewProcess(void (*parentInstructions)(pid_t), 
					void (*childInstructions)(void));

static constexpr const char* SHARED_MEMORY_NAME = "prodconsmem";
static const int PRODUCTION_TARGET = 100;

static const int FORK_FAILURE = -1; 
static const int FORK_CHILD = 0;

SharedMemory* unnamedMemory;
SharedMemory* namedMemory;
SharedMemory* privateMemory;

int main(int argCount, char** argVector)
{
	std::cout << "arguments passed: " << argCount << std::endl;
	testSharedMemory();
	
	return EXIT_SUCCESS;
}

void testSharedMemory()
{
	Consumer consumer;
	
	createSharedMemory();
	createNewProcess(forkInstructions, producerInstructions);
	createNewProcess(forkInstructions, consumerInstructions);
	destroySharedMemory();
}

void createSharedMemory()
{
	unnamedMemory = new SharedMemory(SharedMemoryDefs::VisibilityType::ANONYMOUS,
									SharedMemoryDefs::SharingType::SHARED);
	
	namedMemory = new SharedMemory(SharedMemoryDefs::VisibilityType::NAMED,
									SharedMemoryDefs::SharingType::SHARED);
	
	privateMemory = new SharedMemory(SharedMemoryDefs::VisibilityType::ANONYMOUS,
									SharedMemoryDefs::SharingType::PRIVATE);
	
	std::cout << "Initialiting unnamed shared memory" << std::endl;
	unnamedMemory->initAnonymousMemory();
	
	std::cout << "Initialiting named shared memory" << std::endl;
	mode_t namedMemoryPermissions = helperfxns::getAccessPermissionBits(
											helperfxns::AccessPermissions::WRX,
											helperfxns::AccessPermissions::NONE,
											helperfxns::AccessPermissions::NONE);
	namedMemory->initIdentifiedMemory(SHARED_MEMORY_NAME, 
			SharedMemoryDefs::AccessType::READ_WRITE, 
			namedMemoryPermissions,
			true, false, true);
	
	std::cout << "Initialiting unnamed private memory" << std::endl;
	privateMemory->initAnonymousMemory();
	
	SharedData sharedData;
	sharedData.storage[0] = 1;
	sharedData.storage[1] = 3;
	sharedData.storage[3] = 5;
	
	std::cout << "creating unnamed shared memory" << std::endl;
	unnamedMemory->createSharedMemory(NULL, sizeof(sharedData.storage),
				helperfxns::AccessPermissions::RW, 0, 0);
	std::cout << "creating named shared memory" << std::endl;
	namedMemory->createSharedMemory(NULL, sizeof(sharedData.storage),
				helperfxns::AccessPermissions::RW, 0, 0);
	std::cout << "creating unnamed private memory" << std::endl;
	privateMemory->createSharedMemory(NULL, sizeof(sharedData.storage),
				helperfxns::AccessPermissions::RW, 0, 0);
			
	unnamedMemory->updateData(sizeof(sharedData.storage), sharedData.storage);
	namedMemory->updateData(sizeof(sharedData.storage), sharedData.storage);
	privateMemory->updateData(sizeof(sharedData.storage), sharedData.storage);
}

void destroySharedMemory()
{
	unnamedMemory->cleanupResources();
	namedMemory->cleanupResources();
	privateMemory->cleanupResources();
	
	delete unnamedMemory;
	delete namedMemory;
	delete privateMemory;
}

void createNewProcess(void (*parentInstructions)(pid_t), 
					void (*childInstructions)(void))
{
	/**
	 * call to fork() creates a new child process, and both parent and child processes
	 * will execute the code that follows fork() call. 
	 * fork() returns pid of the child process in parent, and 0 in child.
	 * any code below fork() will be run for both parent and child, therefore 
	 * conditional check is needed
	 **/
	pid_t newPid = fork();
	
	if (newPid == FORK_FAILURE)
	{
		#ifndef NDEBUG
		std::cout << "unsuccessful attempt at creating a new process" << std::endl;
		#endif
	}
	else if (newPid == FORK_CHILD)
	{
		// child process
		pid_t childPid = getpid();
		#ifndef NDEBUG
		std::cout << "created a new child process with pid: " << childPid
				<< " and my parent is: " << getppid() << std::endl;
		#endif
		
		childInstructions();
		
		#ifndef NDEBUG
		printf( "exitting the process %d\n", getpid() );
		#endif
		exit(childPid);
	}
	else
	{
		// parent process
		
		// wait for the child process to finish, and not busy wait
		#ifndef NDEBUG
		std::cout << "parent process with pid: " << getpid() <<
				" waiting for child process with pid: " << newPid << std::endl;
		#endif
		
		parentInstructions(newPid);
	}
}

void producerInstructions()
{
	Producer producer;
	producer.init(unnamedMemory);
	producer.init(namedMemory);
	producer.init(privateMemory);
}

void consumerInstructions()
{
	Consumer consumer;
	consumer.init(unnamedMemory);
	consumer.init(namedMemory);
	consumer.init(privateMemory);
}

void forkInstructions(pid_t childPid)
{
	int exitStatus;
	waitpid(childPid, &exitStatus, WNOHANG);
	
	#ifndef NDEBUG
	std::cout << " child process with pid: " << childPid <<
			" exited with exitcode: " << exitStatus << std::endl;
	#endif
}
