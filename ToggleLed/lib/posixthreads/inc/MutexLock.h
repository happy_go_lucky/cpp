#ifndef MUTEX_LOCK_H
#define MUTEX_LOCK_H

#include <pthread.h>
#include <iostream>
#include "ISharedLock.h"

namespace posixthreads
{
	class MutexLock : public ISharedLock
	{
		public:
			// define friendship so condition variable can access mutex lock
			friend class ConditionVariable;
			
			MutexLock() noexcept;
			MutexLock(const MutexLock& lock) = delete;
			MutexLock(MutexLock&& lock) = delete;
			virtual ~MutexLock();
			
			MutexLock& operator =(const MutexLock& lock) = delete;
			MutexLock& operator =(MutexLock&& lock) = delete;
			
			void initResources();
			void lock();
			void unlock();
			void cleanupResources();
		
		private:
			pthread_mutex_t* _mutex;
	};
}
#endif
