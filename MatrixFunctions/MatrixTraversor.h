#ifndef MATRIX_TRAVERSOR_H
#define MATRIX_TRAVERSOR_H

class MatrixTraversor
{
	public:
		enum TraversorType { ROW = 0, COLUMN };
		static const unsigned int MIN_INDEX = 1;
		static const int INVALID_INDEX = 0;
		MatrixTraversor( TraversorType, unsigned, unsigned );
		~MatrixTraversor();
		
		unsigned int getRowPosition() const;
		unsigned int getColumnPosition() const;
		void traverse();
		void resetTraversor( unsigned int = 0 );
		TraversorType getType() const;
		void setType( const TraversorType );
	
	private:
		TraversorType _traversorType;
		unsigned int _matrixRows;
		unsigned int _matrixColumns;
		unsigned int _rowTraversor;		// goes over columns
		unsigned int _columnTraversor;	// goes over rows
};

#endif