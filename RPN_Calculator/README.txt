build commands:
----------------------------

for calculator test:
make -f makefile

for deque test:
make -f makefileTextD

** Although you can use COMM=DEBUG flag to enable extra debug output. But it can be messy. If interested use following commands

make -f makefile COMM=DEBUG
make -f makefileTextD COMM=DEBUG



run commands:
------------------------------

for calculator:
./calculator < input.txt > output.txt

for deque:
./deque

** you can use ./calculator without file redirection but then it is going to load harcoded "input.txt" filename. I have added it for safety.


Notes:
-------------------------------

1. Although I have added exception handling, but I have also let the program to continue after printing a exception message.

2. I have added some OS dependent code (in my test plan file) for checking if the input file is supplied through the file redirection or not. According to the online documentation, it should work (build) as intented on Linux, Windows and Mac, but I have only tested it on Linux OS. In case you get some build errors related to any of the following:

<unistd.h>
<io.h>
isatty( STDIN_FILENO )

please follow the comments in the test plan
