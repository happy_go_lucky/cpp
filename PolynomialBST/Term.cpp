#include "Term.h"

Term::Term( int coeff, int exponent_x )
{
	//~ cout << "Term" << endl;
	_coeff = coeff;
	_exponent_x = exponent_x;
}

Term::Term( const Term& otherTerm )
{
	_coeff = otherTerm.getCoeff();
	_exponent_x = otherTerm.getExponent_x();
}

Term::~Term()
{
	//~ cout << "~Term" << endl;
}

int Term::getCoeff() const
{
	return _coeff;
}

int Term::getExponent_x() const
{
	return _exponent_x;
}

void Term::setCoeff( int value )
{
	_coeff = value;
}

void Term::setExponent_x( int value )
{
	_exponent_x = value;
}

Term operator + ( const Term& termA, const Term& termB )
{
	if ( termA.getExponent_x() == termB.getExponent_x() )
	{
		Term newTerm( termA.getCoeff() + termB.getCoeff(), termA.getExponent_x() );
		return newTerm;
	}
	else
	{
		throw PolyException( ( char* ) "trying to add different exponents" );
	}
}

const Term& Term::operator = ( const Term& otherTerm )
{
	if ( this != &otherTerm )
	{
		_coeff = otherTerm.getCoeff();
		_exponent_x = otherTerm.getExponent_x();
	}
	return *this;
}

ostream& operator << ( ostream& out, const Term& term )
{
	if ( term.getCoeff() == 0 || term.getExponent_x() == 0 )
	{
		out << ( term.getCoeff() > 0 ? "+" : "" ) << term.getCoeff();
	}
	else
	{
		out << ( term.getCoeff() > 0 ? "+" : "" ) << term.getCoeff() << "x^" << term.getExponent_x();
	}
	return out;
}

bool operator < ( const Term& term1, const Term& term2 )
{
	if ( term1._exponent_x == term2._exponent_x )
	{
		return term1._coeff < term2._coeff;
	}
	else
	{
		return term1._exponent_x < term2._exponent_x;
	}
}

bool operator > ( const Term& term1, const Term& term2 )
{
	if ( term1._exponent_x == term2._exponent_x )
	{
		return term1._coeff > term2._coeff;
	}
	else
	{
		return term1._exponent_x > term2._exponent_x;
	}
}

bool operator == ( const Term& term1, const Term& term2 )
{
	return term1._exponent_x == term2._exponent_x;
}
