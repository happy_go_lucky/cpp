/***
 * Driver class for the JamBox program:
 * 
 * 
 */

#include "defines.h"
#include "ButtonPanel.h"
#include "ToneSpeaker.h"
#include "MP3_ShieldWrapper.h"
#include "LCD.h"
#include "Button.h"
#include "ScaleLEDArray.h"

const int STATUS_LED_PIN = 53;
const int SYSTEM_STATE_IDLE = 0;
const int SYSTEM_STATE_START = 1;
const int LCD_BOOT_TIME_MILLIS = 500;

enum CONTROL_BUTTONS { BUTTON_PLAY = 0, BUTTON_PAUSE, BUTTON_NEXT, BUTTON_PREV };

int state = SYSTEM_STATE_IDLE;

const int LCD_PIN = 47;

const int BUTTON_PLAY_PIN = 2;
const int BUTTON_PAUSE_PIN = 3;
const int BUTTON_NEXT_PIN = 14;
const int BUTTON_PREV_PIN = 15;

const int SERIAL_BAUD_RATE = 9600;

const int SPEAKER_PIN = 5;  // this should be an analog pin (in arduino mega: pins 2 - 13)

ButtonPanel* buttonPanel;
ToneSpeaker* toneSpeaker;
MP3_ShieldWrapper* mp3_shield;
//const bool* const * buttonMatrix;
LCD* lcd;
ScaleLEDArray scaleLED;

Button* buttonPlay;
Button* buttonPause;
Button* buttonNext;
Button* buttonPrev;

char trackName[32] = "Track"; // max that LCD can incorporate is 32

void onButtonPressed( const char buttonType )
{
  #ifdef PRINT_DEBUG
  Serial.print( "button \"" );
  Serial.print( buttonType );
  Serial.print( "\" is pressed\n" );
  #endif

  buttonLogic( buttonType );
  //buttonMatrix = buttonPanel.getButtonArray();
  //printMatrix( buttonMatrix, 4, 4 );
}


void buttonLogic( const char button )
{
  //toneSpeaker->stopTone();
  switch ( button )
  {
    case ButtonPanel::BUTTON_1:
      toneSpeaker->setScale( ToneSpeaker::SCALE_1 );
      scaleLED.setScale(1);
    break;

    case ButtonPanel::BUTTON_2:
      toneSpeaker->setScale( ToneSpeaker::SCALE_2 );
      scaleLED.setScale(2);
    break;

    case ButtonPanel::BUTTON_3:
      toneSpeaker->setScale( ToneSpeaker::SCALE_3 );
      scaleLED.setScale(3);
    break;

    case ButtonPanel::BUTTON_4:
      toneSpeaker->setScale( ToneSpeaker::SCALE_4 );
      scaleLED.setScale(4);
    break;

    case ButtonPanel::BUTTON_5:
      toneSpeaker->setScale( ToneSpeaker::SCALE_5 );
      scaleLED.setScale(5);
    break;

    case ButtonPanel::BUTTON_6:
      toneSpeaker->setScale( ToneSpeaker::SCALE_6 );
      scaleLED.setScale(6);
    break;

    case ButtonPanel::BUTTON_7:
      toneSpeaker->stopTone();
    break;

    case ButtonPanel::BUTTON_8:
      toneSpeaker->stopTone();
    break;

    case ButtonPanel::BUTTON_9:
      toneSpeaker->stopTone();
    break;

    case ButtonPanel::BUTTON_0:
      toneSpeaker->playTone( ToneSpeaker::NOTE_F );
    break;

    case ButtonPanel::BUTTON_A:
      toneSpeaker->playTone( ToneSpeaker::NOTE_A );
    break;

    case ButtonPanel::BUTTON_B:
      toneSpeaker->playTone( ToneSpeaker::NOTE_B );
    break;

    case ButtonPanel::BUTTON_C:
      toneSpeaker->playTone( ToneSpeaker::NOTE_C );
    break;

    case ButtonPanel::BUTTON_D:
      toneSpeaker->playTone( ToneSpeaker::NOTE_D );
    break;

    case ButtonPanel::BUTTON_STAR:
      toneSpeaker->playTone( ToneSpeaker::NOTE_E );
    break;

    case ButtonPanel::BUTTON_HASH:
      toneSpeaker->playTone( ToneSpeaker::NOTE_G );
    break;

    default:
      toneSpeaker->stopTone();
    break;
  }
}

void onPlayButtonClick()
{
  mp3_shield->play();
  digitalWrite( STATUS_LED_PIN, HIGH );
  trackName[6] = ( char ) mp3_shield->getTrackNumber();
  lcd->writeString( trackName, LCD::BLOCK_1_1 );
}

void onPauseButtonClick()
{
  mp3_shield->pause();
  digitalWrite( STATUS_LED_PIN, LOW );
  trackName[6] = ( char ) mp3_shield->getTrackNumber();
  lcd->writeString( trackName, LCD::BLOCK_1_1 );
}

void onNextButtonClick()
{
  mp3_shield->next();
  trackName[6] = ( char ) mp3_shield->getTrackNumber();
  lcd->writeString( trackName, LCD::BLOCK_1_1 );
}

void onPrevButtonClick()
{
  mp3_shield->prev();
  trackName[6] = ( char ) mp3_shield->getTrackNumber();
  lcd->writeString( trackName, LCD::BLOCK_1_1 );
}

void setup()
{
  // put your setup code here, to run once:
  Serial.begin( SERIAL_BAUD_RATE );

  #ifdef PRINT_DEBUG
  Serial.println( "Jambox initiated" );
  #endif

  mp3_shield = new MP3_ShieldWrapper();
  
  buttonPanel = new ButtonPanel();
  toneSpeaker = new ToneSpeaker( SPEAKER_PIN );
  buttonPanel->addEventListener( ButtonPanel::ON_DOWN, onButtonPressed );

  pinMode( STATUS_LED_PIN, OUTPUT );
  digitalWrite( STATUS_LED_PIN, LOW );
  
  // control buttons
  pinMode( BUTTON_PLAY_PIN, INPUT );
  pinMode( BUTTON_PAUSE_PIN, INPUT );
  pinMode( BUTTON_NEXT_PIN, INPUT );
  pinMode( BUTTON_PREV_PIN, INPUT );
  buttonPlay = new Button( BUTTON_PLAY, BUTTON_PLAY_PIN );
  buttonPause = new Button( BUTTON_PAUSE, BUTTON_PAUSE_PIN );
  buttonNext = new Button( BUTTON_NEXT, BUTTON_NEXT_PIN );
  buttonPrev = new Button( BUTTON_PREV, BUTTON_PREV_PIN );
  buttonPlay->addEventListener( Button::ON_CLICK, onPlayButtonClick );
  buttonPause->addEventListener( Button::ON_CLICK, onPauseButtonClick );
  buttonNext->addEventListener( Button::ON_CLICK, onNextButtonClick );
  buttonPrev->addEventListener( Button::ON_CLICK, onPrevButtonClick );

  scaleLED.setScale(1); // default scale

  lcd = new LCD( LCD_PIN, SERIAL_BAUD_RATE );
  
  delay( LCD_BOOT_TIME_MILLIS );  // wait for display to boot up
  lcd->writeString( "Jambox Ready..", LCD::BLOCK_1_1 );
}

void loop()
{
  // put your main code here, to run repeatedly:
    buttonPanel->loop();
    mp3_shield->loop();
    buttonPlay->loop();
    buttonPause->loop();
    buttonNext->loop();
    buttonPrev->loop();
}

/*
void printMatrix( const bool* const * matrix, int rows, int cols )
{
  int ii = 0, jj = 0;
  while ( ii < rows )
  {
    while ( jj < cols )
    {
      Serial.print( matrix[ii][jj] );
      Serial.print( " " );
      jj++;
    }
    Serial.print( "\n" );
    jj = 0;
    ii++;
  }

  Serial.println( "*******************************" );
}*/
