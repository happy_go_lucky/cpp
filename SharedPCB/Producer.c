#include <stdio.h>
#include <stdlib.h>
#include "SharedDefines.h"
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <assert.h>
#include <errno.h>	// for error
#include <string.h>	// for error
// for semaphores
#include <fcntl.h>
#include <sys/stat.h>
#include <semaphore.h>

int sharedBufferID = -1;
char* sharedBufferPointer = NULL;
sem_t* semaphore = NULL;

int readFileToFillBuffer( const char* filename, int* totalBytesRead );
void setupSharedBuffer( const char* filename );
void createSemaphore();
void updateSharedBuffer( sem_t* semaphore, char* sharedBufferPointer, const char* filename );
void closeSemaphore();
void removeSharedBuffer();

int main( int argC, char** argV )
{
	if ( argC != 2 )
	{
		fprintf( stderr, "Provide the output buffer file name in command line\n" );
		return EXIT_FAILURE;
	}
	
	printf("Producer created\n");
	const char* filename = *( argV + 1 );
	
	setupSharedBuffer( filename );
	createSemaphore();
	updateSharedBuffer( semaphore, sharedBufferPointer, filename );
	removeSharedBuffer();
	closeSemaphore();
	return EXIT_SUCCESS;
}

int readFileToFillBuffer( const char* filename, int* totalBytesRead )
{
	FILE *fileStream = fopen( filename, FILE_READ_FLAG );
	FILE *fileStreamIterator = fileStream;
	
	if ( fileStream == NULL )
	{
		fprintf( stderr, "unable to open file \"%s\" for reading\n", filename );
		exit( EXIT_FAILURE );
	}
	
	#ifdef DEBUG_PRINT
	printf( "opening file \"%s\" for reading\n", filename );
	#endif
	int bytesRead = 0;
	char byteBuffer;
	fscanf( ( fileStreamIterator + *totalBytesRead ), "%c%n", &byteBuffer, &bytesRead );
	*sharedBufferPointer = byteBuffer;
	*totalBytesRead += bytesRead;
	
	#ifdef DEBUG_PRINT
	printf( "read: %c", byteBuffer );
	printf( "bytes read: %d\n", bytesRead );
	#endif
	fclose( fileStream );
	
	return bytesRead;
}

void setupSharedBuffer( const char* filename )
{
	// generate an identifier key for the shared buffer
	// same key makes the sharing possible
	int pathSize = 1024;
	char* pathname = malloc( sizeof( char ) * pathSize );
	getPath( pathname, pathSize, filename );
	
	// ftok generates a psudo-random key
	// if seeking same key, ensure that pathname points to a file which is not changed during
	// the life cycle of the project, or else, different keys will be generated
	key_t sharedBufferKey = ftok( pathname, PROJECT_ID );
	assert ( sharedBufferKey > -1 );
	
	#ifdef DEBUG_PRINT
	printf( "shared buffer key: %x\n", sharedBufferKey );
	printf( "permissions: " );
	printIntToBinary( ( int ) PERMISSION );
	printf( "IPC_CREAT binary value: " );
	printIntToBinary( IPC_CREAT );
	#endif
	
	free( pathname );
	
	// create the shared buffer segment
	// buffer size is rounded off to the multiple of nearest page size (in bytes)
	// size is allocated as a chunk in bytes
	// providing proper permission is very important, otherwise memory will be created
	// but will not be accessible
	sharedBufferID = shmget( sharedBufferKey, BUFFER_SIZE_IN_BYTES, IPC_CREAT | (int) PERMISSION );
	
	#ifdef DEBUG_PRINT
	printf( "shared buffer id: %d\n", sharedBufferID );
	printf( "size of page: %lu bytes\n", getPageSize() );
	#endif
	
	if ( sharedBufferID < 0 )
	{
		fprintf( stderr, "error description\n%s\n", strerror( errno ) );
		exit( errno );
	}
	
	// attach to the shared buffer segment
	// returns a pointer to shared memory allocated above
	// because it's allocated in bytes, use a char
	sharedBufferPointer = ( char* ) shmat( sharedBufferID, NULL, 0 );
	assert( sharedBufferPointer != NULL );
	#ifdef DEBUG_PRINT
	// TODO: fix this
	printf( "size of allocated memory: %lu bytes\n", sizeof( sharedBufferPointer ) / sizeof( *sharedBufferPointer ) );
	#endif
}

void removeSharedBuffer()
{
	#ifdef DEBUG_PRINT
	printf( "trying to removed buffer with id: %d\n", sharedBufferID );
	#endif
	// detach the segment from the process when done
	if ( sharedBufferPointer != NULL )
	{
		assert( shmdt( sharedBufferPointer ) > -1 );
	}
	// free the shared memory by providing remove command
	if ( sharedBufferID > -1 )
	{
		assert( shmctl( sharedBufferID, IPC_RMID, NULL ) > -1 );
	}
}

void updateSharedBuffer( sem_t* semaphore, char* sharedBufferPointer, const char* filename )
{
	if ( semaphore == NULL )
	{
		fprintf( stderr, "create a semaphore first\n" );
		return;
	}
	
	if ( sharedBufferPointer == NULL )
	{
		fprintf( stderr, "create a shared buffer first\n" );
		return;
	}
	
	int bytesRead = 0;
	int totalBytesRead = 0;
	
	sem_wait( semaphore );
	// Critical section
	bytesRead = readFileToFillBuffer( filename, &totalBytesRead );
	if ( bytesRead > 0 )
	{
		sem_post( semaphore );
	}
	else
	{
		closeSemaphore();
	}
}

void createSemaphore()
{
	if ( semaphore == NULL )
	{
		// named semaphore, begins with a "/" + one or more characters
		semaphore = sem_open( SEMAPHORE, O_CREAT, (int) PERMISSION, SEMAPHORE_START_VALUE );
		// unlink ensures that the semaphore is destroyed once all processes are done with it 
		sem_unlink( SEMAPHORE );
	}
}

void closeSemaphore()
{
	#ifdef DEBUG_PRINT
	printf( "trying to close semaphore with id: %d\n", semaphore );
	#endif
	if ( semaphore != NULL )
	{
		semaphore = sem_close( semaphore ) == 0 ? NULL : semaphore;
	}
}
