#ifndef SHARED_DEFINES_H
#define SHARED_DEFINES_H

#include <unistd.h>
#include <assert.h>

const size_t BUFFER_SIZE_IN_BYTES = 2048;
// for binary read
const char* FILE_READ_FLAG = "rb";
const char* FILE_WRITE_FLAG = "wb";
const int PROJECT_ID = 'c';
const int BYTE_SIZE = 8;

// 3 bits for each set (read, write, execute), used octal because it makes more sense
const int PERMISSIONS_OWNER = 06;	// read and write -- 110
const int PERMISSIONS_GROUP = 06;	// read and write -- 110
const int PERMISSIONS_OTHERS = 04;	// read -- 100
#define PERMISSION ( PERMISSIONS_OWNER << 6 ) | ( PERMISSIONS_GROUP << 3 ) | PERMISSIONS_OTHERS

const char* SEMAPHORE = "/a_pc_semaphore";
const unsigned int SEMAPHORE_START_VALUE = 4;

void printIntToBinary( const int value )
{
	int ii = ( sizeof( value ) * BYTE_SIZE ) - 1;
	while ( ii >= 0 )
	{
		printf( "%d", ( value & ( 1 << ii-- ) ) > 0 ? 1 : 0 );
	}
	printf( "\n" );
}

unsigned long getPageSize()
{
	return sysconf( _SC_PAGESIZE );
}

void getPath( char* pathname, int pathSize, const char* filename )
{
	assert ( getcwd( pathname, pathSize ) != NULL );
	sprintf( pathname, "%s/%s", pathname, filename );
	#ifdef DEBUG_PRINT
	printf( "current directory: %s\n", pathname );
	#endif
}

#endif
