#include "ButtonPanel.h"

const int ButtonPanel::PIN_ARR[NUM_PINS] = { PIN_1, PIN_2, PIN_3, PIN_4, PIN_5, PIN_6, PIN_7, PIN_8 };
const int ButtonPanel::ROW_PINS[NUM_BUTTON_ROWS] = { PIN_8, PIN_7, PIN_6, PIN_5 };
const int ButtonPanel::COL_PINS[NUM_BUTTON_COLS] = { PIN_4, PIN_3, PIN_2, PIN_1 };

const char ButtonPanel::BUTTON_ARRAY[NUM_BUTTON_ROWS][NUM_BUTTON_COLS] = { 
          { BUTTON_1, BUTTON_2, BUTTON_3, BUTTON_A },
          { BUTTON_4, BUTTON_5, BUTTON_6, BUTTON_B },
          { BUTTON_7, BUTTON_8, BUTTON_9, BUTTON_C },
          { BUTTON_STAR, BUTTON_0, BUTTON_HASH, BUTTON_D }
          };

ButtonPanel::ButtonPanel()
{
  #ifdef PRINT_DEBUG
  Serial.println( "ButtonPanel::ButtonPanel" );
  #endif
  _buttonStatusMatrix = nullptr;
  _pinStatusArray = nullptr;

  _initPinMode();
  _allocButtonMatrix();
  _resetButtonMatrix();
  _initPinStatusArray();
  _initListenerArray();

  #ifdef PRINT_DEBUG
  _updatePinStatus();
  #endif
}

ButtonPanel::~ButtonPanel()
{
  #ifdef PRINT_DEBUG
  Serial.println( "ButtonPanel::~ButtonPanel" );
  #endif
  _deallocPinStatusArray();
  _deallocButtonStatusMatrix();
}

void ButtonPanel::_initPinMode()
{
  #ifdef PRINT_DEBUG
  Serial.println( "_initPinMode" );
  #endif
  for ( int ii = 0; ii < NUM_BUTTON_COLS; ii++ )
  {
    pinMode( COL_PINS[ii], OUTPUT );
  }

  for ( int ii = 0; ii < NUM_BUTTON_ROWS; ii++ )
  {
    pinMode( ROW_PINS[ii], INPUT );
  }

  for ( int ii = 0; ii < NUM_BUTTON_ROWS; ii++ )
  {
    digitalWrite( ROW_PINS[ii], LOW );
  }

  // note to self: INPUT_PULLUP can be used instead of using an external pulldown resistor, just the logic is switched.
  // HIGH means button is not pressed and LOW means button is pressed
  // where as otherwise, HIGH means button is pressed and LOW means button is not pressed
  for ( int ii = 0; ii < NUM_BUTTON_COLS; ii++ )
  {
    digitalWrite( COL_PINS[ii], LOW );
  }
}

void ButtonPanel::_allocButtonMatrix()
{
  #ifdef PRINT_DEBUG
  Serial.println( "_allocButtonMatrix" );
  #endif
  if ( _buttonStatusMatrix != nullptr )
  {
    // reuse
  }
  else
  {
    _buttonStatusMatrix = ( bool** ) malloc ( sizeof( bool* ) * NUM_BUTTON_ROWS );
    
    for ( int ii = 0; ii < NUM_BUTTON_ROWS; ii++ )
    {
      _buttonStatusMatrix[ii] = ( bool* ) malloc ( sizeof( bool ) * NUM_BUTTON_COLS );
    }
  }
}

void ButtonPanel::_resetButtonMatrix()
{
  #ifdef PRINT_DEBUG
  Serial.println( "_resetButtonMatrix" );
  #endif
  if ( _buttonStatusMatrix == nullptr )
  {
    return;
  }
  
  for ( int ii = 0; ii < NUM_BUTTON_ROWS; ii++ )
  {
    for ( int jj = 0; jj < NUM_BUTTON_COLS; jj++ )
    {
      _buttonStatusMatrix[ii][jj] = false;
    }
  }
}

void ButtonPanel::_updateButtonStatus()
{
  //Serial.println( "_updateButtonStatus" );
  if ( _buttonStatusMatrix == nullptr )
  {
    return;
  }
  // set the columns to high one by one and get the active row to get the button pressed/
  for ( int ii = 0; ii < NUM_BUTTON_COLS; ii++ )
  {
    digitalWrite( COL_PINS[ii], HIGH );
    for ( int jj = 0; jj < NUM_BUTTON_ROWS; jj++ )
    {
      _buttonStatusMatrix[jj][ii] = digitalRead( COL_PINS[ii] ) && digitalRead( ROW_PINS[jj] );
    }
    digitalWrite( COL_PINS[ii], LOW );
  }
}

void ButtonPanel::_allocPinStatusArray()
{
  #ifdef PRINT_DEBUG
  Serial.println( "_allocPinStatusArray" );
  #endif
  if ( _pinStatusArray == nullptr )
  {
    _pinStatusArray = ( bool* ) malloc( sizeof( bool ) * NUM_PINS );
  }
}

void ButtonPanel::_initPinStatusArray()
{
  #ifdef PRINT_DEBUG
  Serial.println( "_initPinStatusArray" );
  #endif
  _allocPinStatusArray();
  
  for ( int jj = 0; jj < NUM_PINS; jj++ )
  {
    _pinStatusArray[jj] = false;
  }
}

#ifdef PRINT_DEBUG
void ButtonPanel::_updatePinStatus()
{
  //Serial.println( "_updatePinStatus" );
  if ( _pinStatusArray == nullptr )
  {
    return;
  }
  for ( int jj = 0; jj < NUM_PINS; jj++ )
  {
    _pinStatusArray[jj] = digitalRead( PIN_ARR[jj] );
  }
}
#endif

void ButtonPanel::_printPinStatusArray()
{
  Serial.println( "Printing Pin Status" );
  for ( int jj = 0; jj < NUM_PINS; jj++ )
  {
    Serial.print( "pin" );
    Serial.print( jj );
    Serial.print( ": " );
    Serial.print( _pinStatusArray[jj] );
    Serial.print( "," );
  }
  Serial.print( "\n" );
}

void ButtonPanel::_initButtons()
{
  
}

const bool* const * ButtonPanel::getButtonArray()
{
  #ifdef PRINT_DEBUG
  _updatePinStatus();
  #endif
  _updateButtonStatus();
  return _buttonStatusMatrix;
}

void ButtonPanel::_deallocPinStatusArray()
{
  delete[] _pinStatusArray;
  _pinStatusArray = nullptr;
}


void ButtonPanel::_deallocButtonStatusMatrix()
{
  int ii = 0;
  while ( ii < NUM_BUTTON_ROWS )
  {
    delete[] *( _buttonStatusMatrix + ii );
    ii++;
  }
  delete[] _buttonStatusMatrix;
}

bool ButtonPanel::addEventListener( const int eventType, void (*listenerFunc)( const char ) )
{
  #ifdef PRINT_DEBUG
  Serial.println("ButtonPanel::addEventListener");
  #endif
  if ( eventType < 0 || eventType >= NUM_EVENTS )
  {
    return false;
  }
  else
  {
    listenerFuncArray[eventType] = listenerFunc;
    return true;
  }
}

bool ButtonPanel::removeEventListener( const int eventType )
{
  #ifdef PRINT_DEBUG
  Serial.println("ButtonPanel::removeEventListener");
  #endif
  if ( eventType < 0 || eventType >= NUM_EVENTS )
  {
    return false;
  }
  else
  {
    listenerFuncArray[eventType] = nullptr;
    return true;
  }
}

void ButtonPanel::_initListenerArray()
{
  for ( int ii = 0; ii < NUM_EVENTS; ii++ )
  {
    listenerFuncArray[ii] = nullptr;
  }
}

void ButtonPanel::loop()
{
  #ifdef PRINT_DEBUG
  _updatePinStatus();
  #endif
  
  _updateButtonStatus();
  
  #ifdef PRINT_DEBUG
  _printPinStatusArray();
  _printButtonStatusMatrix();
  #endif

  _onButtonDown();
  _onButtonUp();
}

void ButtonPanel::_onButtonDown()
{
  char returnVal = _checkButtonPress();
  if ( listenerFuncArray[ON_DOWN] != nullptr && returnVal != NULL_CHAR )
  {
    ( *listenerFuncArray[ON_DOWN] )( returnVal );
  }
}

void ButtonPanel::_onButtonUp()
{
  if ( listenerFuncArray[ON_UP] != nullptr )
  {
    ( *listenerFuncArray[ON_UP] )( 'c' );
  }
}

char ButtonPanel::_checkButtonPress()
{
  if ( _buttonStatusMatrix == nullptr )
  {
    return NULL_CHAR;
  }
  
  for ( int ii = 0; ii < NUM_BUTTON_ROWS; ii++ )
  {
    for ( int jj = 0; jj < NUM_BUTTON_COLS; jj++ )
    {
      if ( _buttonStatusMatrix[ii][jj] )
        return BUTTON_ARRAY[ii][jj];
    }
  }

  return NULL_CHAR;
}

void ButtonPanel::_printButtonStatusMatrix()
{
  int ii = 0, jj = 0;
  while ( ii < NUM_BUTTON_ROWS )
  {
    while ( jj < NUM_BUTTON_COLS )
    {
      Serial.print( _buttonStatusMatrix[ii][jj] );
      Serial.print( " " );
      jj++;
    }
    Serial.print( "\n" );
    jj = 0;
    ii++;
  }

  Serial.println( "*******************************" );
}

