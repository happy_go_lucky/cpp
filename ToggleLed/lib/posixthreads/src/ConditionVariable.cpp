#include "../inc/ConditionVariable.h"

namespace posixthreads
{
	ConditionVariable::ConditionVariable(MutexLock& mutex) noexcept : _mutexLock(mutex)
	{
		#ifndef NDEBUG
		std::cout << "ConditionVariable::ConditionVariable" << std::endl;
		#endif
		
		_cv = nullptr;
		_cvAttrib = nullptr;
	}

	ConditionVariable::~ConditionVariable()
	{
		#ifndef NDEBUG
		std::cout << "ConditionVariable::~ConditionVariable" << std::endl;
		#endif
		
		if (_cv != nullptr) delete _cv;
		if (_cvAttrib != nullptr) delete _cvAttrib;
	}

	void ConditionVariable::initialize(ProcessAccessType accessType)
	{
		#ifndef NDEBUG
		std::cout << "ConditionVariable::initialize" << std::endl;
		#endif
		
		_cv = new pthread_cond_t();
		_cvAttrib = new pthread_condattr_t();
		
		pthread_condattr_init(_cvAttrib);
		_setAccessType(accessType);
		
		#ifndef NDEBUG
		int state = pthread_cond_init(_cv, NULL);
		if (state != EXIT_SUCCESS)
		{
			std::cout << strerror(state) << std::endl;
			throw ThreadException((char*) "unable to initialize condition variable");
		}
		#else
		pthread_cond_init(_cv, NULL);
		#endif
	}

	void ConditionVariable::cleanup()
	{
		#ifndef NDEBUG
		std::cout << "ConditionVariable::cleanup" << std::endl;
		
		int stateCV = pthread_cond_destroy(_cv);
		int stateAtrrib = pthread_condattr_destroy(_cvAttrib);
		
		if (stateCV != EXIT_SUCCESS || stateAtrrib != EXIT_SUCCESS)
		{
			std::cout << strerror(stateCV) << std::endl;
			std::cout << strerror(stateAtrrib) << std::endl;
			throw ThreadException((char*) "unable to destroy condition variable");
		}
		#else
		pthread_cond_destroy(_cv);
		pthread_condattr_destroy(_cvAttrib);
		#endif
		
		delete _cv;
		delete _cvAttrib;
		_cv = nullptr;
		_cvAttrib = nullptr;
	}
	
	/**
	 * returns access type
	 * */
	ProcessAccessType ConditionVariable::getAccessType() const noexcept
	{
		int accessType = -1;
		int retVal = pthread_condattr_getpshared(_cvAttrib, &accessType);
		
		if (retVal == EXIT_SUCCESS)
		{
			if (accessType == PTHREAD_PROCESS_SHARED)
				return ProcessAccessType::SHARED;
			else
				return ProcessAccessType::PRIVATE;
		}
		else
		{
			return ProcessAccessType::INVALID;
		}
	}

	void ConditionVariable::_setAccessType(ProcessAccessType accessType)
	{
		if (accessType == ProcessAccessType::SHARED)
			pthread_condattr_setpshared(_cvAttrib, PTHREAD_PROCESS_SHARED);
		else
			pthread_condattr_setpshared(_cvAttrib, PTHREAD_PROCESS_PRIVATE);
	}
}
