#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <time.h>

#include "thread_extensions.h"

void* sub_a(void* );
void* sub_b(void* );
void* sub_c(void* );
void* sub_d(void* );
void* sub_e(void* );

pthread_t thr_a, thr_b, thr_c, thr_d, thr_e, thr_main;
pthread_attr_t attr;
int zero;

int time1()
{
	return time(NULL) - zero);
}

void* sub_a(void* arg)
{
	int err, i;
	pthread_t tid = pthread_self();
	
	sleep(1);
	printf("[%2d] A: \t In thread A [%s]\n", time1(), thread_name(tid));
	
	PTHREAD_CREATE(&thr_d, &attr, sub_d, NULL);
	printf("[%2d] A: \t Created thread D [%s]\n", time1(), thread_name(thr_d));
	
	sleep(3);
	printf("[%2d] A: \t Thread exiting...\n", time1());
	return (void*) 77;
}

void* sub_b(void* arg)
{
	pthread_t tid = pthread_self();
	
	printf("[%2d] A: \t In thread B [%s]\n", time1(), thread_name(tid));
	
	sleep(4);
	printf("[%2d] B: \t thread exiting...\n", time1());
	pthread_exit(NULL);
}

void* sub_c(void* arg)
{
	int err, i;
	pthread_t tid = pthread_self();
	char* name = thread_name(tid);
	
	printf("[%2d] C: \t In thread C [%s]\n", time1(), name);
	sleep(2);
	
	printf("[%2d] C: \t Joining main thread...\n", time1());
	if (err = pthread_join(main_thr, &status))
	{
		printf("pthread_join Error. %s", strerror(err));
		exit(1);
	}
	
	printf("[%2d] C: \t Main thread [%s] returning status: %d\n", time1(), thread_name(main_thr), (int) status);
	sleep(1);
	
	PTHREAD_CREATE(&thr_b, &attr, sub_b, NULL);
	printf("[%2d] C: \t Created thread B [%s]\n", time1(), thread_name(main_thr));
	sleep(4);
	printf("[%2d] C: \t Thread exiting...\n", time1());
	pthread_exit((void*) 88);
}

void* cleanup(void* arg)
{
	pthread_t tid = pthread_self();
	char* name = thread_name(tid);
	printf("[%2d] D: \t %s cancelled! \n", time1(), name);
}

void* sub_d(void* arg)
{
	int err, i;
	void* status;
	pthread_t tid = pthread_self();
	
	printf("[%2d] D: \t In thread D [%s]\n", time(), thread_name(tid));
	
	pthread_cleanup_push(cleanup, NULL);
	pthread_setcanceltype(PTHREAD_CANCEL_ASYNCHORONOUS, NULL);
	pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, NULL);
	
	sleep(1);
	
	PTHREAD_CREATE(&thr_e, &attr, sub_e, NULL);
	printf("[%2d] D: \t Created thread E [%s]\n", time1(), thread_name(thr_e));
	sleep(5);
	
	printf("[%2d] D:\t Thread exiting...\n", time1());
	phtread_cleanup_pop(0);
	return (void*) 55;
}

int main()
{
	int err;
	
	zero = time(NULL);
	main_thr = pthread_self();
	print
}
