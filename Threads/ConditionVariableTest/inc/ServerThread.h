/**
 * This class implements a worker thread which uses condition variables
 * as the events to communicate to the main thread of the process and
 * it computes a sum of 10000 random numbers between 1 - 1000
 * */

#ifndef SERVER_THREAD_H
#define SERVER_THREAD_H

#include <posixthreads/inc/Thread.h>
#include <posixthreads/inc/ConditionVariable.h>
#include <posixthreads/inc/MutexLock.h>
#include <posixthreads/inc/ThreadDefs.h>
#include <cstdlib>

class ServerThread : Thread
{
	public:
		ServerThread();
		~ServerThread();
	
	private:
		
};

#endif
