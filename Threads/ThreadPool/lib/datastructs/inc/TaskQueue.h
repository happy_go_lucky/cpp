/**
 * This is an implementation of a task queue which is slightly different
 * from a standard queue in operations. Since the interface for the queue
 * ADT didn't fit, it led to this modified ADT.
 * 
 * The task queue is a queue of tasks where the tasks are added in FIFO
 * manne. The removal of tasks is what sets this apart from the regular
 * queue. This task queue is used for current access, hence, the order of
 * removal is not serial. The tasks in the queue are picked up in order by
 * each thread, but the removal is not in order. The removal is random.
 * The operations will have to be serialized by the caller. 
 * */

#ifndef TASK_QUEUE_H
#define TASK_QUEUE_H

#include <iostream>
#include "DoublyList.h"
#include "DoublyListIterator.h"

namespace datastructs
{
	template <class Type>
	class TaskQueue
	{
		public:
			TaskQueue() noexcept;
			~TaskQueue();
			
			TaskQueue(const TaskQueue<Type>& taskQueue) noexcept;
			TaskQueue<Type>& operator =(const TaskQueue<Type>& taskQueue) & noexcept;
			
			void addTask(const Type& task) noexcept;
			void addTask(Type&& task) noexcept;
			void removeTask(const Type& task) noexcept;
			
			void emptyList() noexcept;
			int size() const noexcept;
		
		private:
			DoublyList<Type> _taskList;
	};
}

#endif
