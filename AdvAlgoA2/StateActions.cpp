#include "StateActions.h"


void StartAction::operator ()(GlobalVariables::StateActionParams& actionParams)
{
	actionParams.bracketCount = 0;
	actionParams.operatorCount = 0;
	actionParams.operandCount = 0;
	actionParams.avgOpCount = 0;
}

void EndAction::operator ()(GlobalVariables::StateActionParams& actionParams)
{
	
}

void BracketOpenAction_Infix::operator ()(GlobalVariables::StateActionParams& actionParams)
{
	actionParams.bracketCount++;
}

void BracketCloseAction_Infix::operator ()(GlobalVariables::StateActionParams& actionParams)
{
	actionParams.bracketCount--;
}

void AvgBeginAction_Infix::operator ()(GlobalVariables::StateActionParams& actionParams)
{
	actionParams.avgOpCount++;
}

void AvgEndAction_Infix::operator ()(GlobalVariables::StateActionParams& actionParams)
{
	actionParams.avgOpCount--;
}

void EndAction_PnP::operator ()(GlobalVariables::StateActionParams& actionParams)
{
	
}

void OperatorAction_PnP::operator ()(GlobalVariables::StateActionParams& actionParams)
{
	actionParams.operatorCount++;
}

void OperandAction_PnP::operator ()(GlobalVariables::StateActionParams& actionParams)
{
	actionParams.operandCount++;
}

void AvgAction_PnP::operator ()(GlobalVariables::StateActionParams& actionParams)
{
	actionParams.operatorCount += 2;
}
