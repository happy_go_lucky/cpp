#ifndef PRODUCER_H
#define PRODUCER_H

#include <iostream>
#include "SharedMemory.h"
#include "SharedData.h"

class Producer
{
	public:
		Producer();
		~Producer();
	
		void init(SharedMemory* shMem);
	private:
};

#endif
