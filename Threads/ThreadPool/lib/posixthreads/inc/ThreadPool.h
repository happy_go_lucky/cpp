/**
 * This a thread pool class which provides user a collection of threads.
 * Ideally this pool should be used for small tasks, so that the overhead
 * of creating and destroying the threads can be reduced. This also provides
 * a good way to manage the multi-threading nightmare of having too many
 * concurrent threads on the system which would do more damage than benefit.
 * We can cap the maximum allowed threads and still achieve concurrency.
 * 
 * Interface:
 * A group of threads will be spawned at the begining. These threads will
 * take a generic form of task which can be run.
 * The tasks will be managed by a queue of tasks, where the entries will be
 * managed by weak pointers. The weak pointers can help manage object's
 * life time issues because the queue doesn't have to worry about the task
 * expiry.
 * removal won't be necessary for this design, we can just check if the task object
 * is null, and if the task object is null, we can just remove it from the queue.
 * 
 * once a thread in the pool will finish it's task, it will return back to
 * wainting state where it will wait further tasks. 
 * 
 * The thread workers will be managed by a queue where each worker will be 
 * added and removed. This queue will be thread safe, i.e. the addition and
 * removal will be serialized.
 * 
 * There will be also be a processing queue for the task that are in process.
 * These tasks will be picked from the other task queue. The operation of
 * moving the task between the two queues is also serialized.
 * 
 * */

#ifndef THREAD_POOL_H
#define THREAD_POOL_H

#include <sys/sysinfo.h>
#include <iostream>
#include <vector>
#include <datastructs/inc/TaskQueue.h>
#include <memory>		// for smart pointers
#include <atomic> 		// for atomic variables
#include <functional>	// for bind and function pointer wrappers
#include "ThreadDefs.h"
#include "MutexLock.h"
#include "Thread.h"
#include "ThreadPoolTask.h"

namespace posixthreads
{
	class ThreadPool : public Thread
	{
		public:
			const static unsigned short POOL_SIZE_ALLOWED_OVERLOAD = 1;
			const static unsigned short POOL_SIZE_UNKNOWN = 0;
			
			ThreadPool(ThreadPoolSizeType sizeType, unsigned short size = POOL_SIZE_UNKNOWN);
			~ThreadPool();
			
			ThreadPool(const ThreadPool& pool) = delete;
			ThreadPool(ThreadPool&& pool) = delete;
			ThreadPool& operator =(const ThreadPool& pool) = delete;
			ThreadPool& operator =(ThreadPool& pool) = delete;
			
			void* threadRun() override;
			
			unsigned int numPendingTasks() const noexcept;
			unsigned int numActiveTasks() const noexcept;
			
			// should be serialized
			void addTask(std::weak_ptr<ThreadPoolTask> task);
			
		private:
			unsigned int _numThreads;
			// queue ops should be thread-safe
			datastructs::TaskQueue<std::weak_ptr<ThreadPoolTask>> _pendingTasks;
			datastructs::TaskQueue<std::weak_ptr<ThreadPoolTask>> _currentTasks;
			std::vector<Thread*> _threadVector;
			// used for serialization of the queues
			MutexLock* _taskUpdateLock;
			
			void _updateNumThreads(ThreadPoolSizeType sizeType, unsigned short size) noexcept;
			int _getDefaultPoolSize() const noexcept;
			void _shunTaskInProcessQueue();
			void _initThreadVector() noexcept;
			void _cleanupThreadVector() noexcept;
			void _initLocks();
			void _cleanupLocks();
	};
}

#endif
