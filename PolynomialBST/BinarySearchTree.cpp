#include "BinarySearchTree.h"

template <typename T>
BinarySearchTree<T>::BinarySearchTree()
{
	//~ cout << "BinarySearchTree()" << endl;
	_root = nullptr;
}

template <typename T>
BinarySearchTree<T>::BinarySearchTree( const BinarySearchTree<T>& otherBST )
{
	_root = nullptr;
	_root = _copyTree( otherBST._root );
}

template <typename T>
BinarySearchTree<T>::~BinarySearchTree()
{
	//~ cout << "~BinarySearchTree()" << endl;
	_deleteTree( _root );
}

template <typename T>
bool BinarySearchTree<T>::isEmpty() const
{
	return _root == nullptr;
}

template <typename T>
bool BinarySearchTree<T>::search( const T& data )
{
	return _searchHelper( data, _root );
}

template <typename T>
void BinarySearchTree<T>::clear()
{
	_deleteTree( _root );
}

template <typename T>
bool BinarySearchTree<T>::removeNode( const T& data )
{
	// 3 cases:
	// leaf node: simplest, find and delete the node
	// node with one child: delete node and make child of that node the child of parent of the deleted node (adopted by grandparent) 
	// node with two children:  bit involved
	// method 1: deletion by merging: take two child sub trees, merge them to make one tree and make the root of merged tree a child of parent of deleted node
	
	
	// to be implemented
	return true;
}

template <typename T>
bool BinarySearchTree<T>::insert( const T& data )
{
	if ( isEmpty() )
	{
		_root = new BST_Node<T>( data );
	}
	else
	{
		BST_Node<T>* itr = _root;
		while ( itr != nullptr )
		{
			if ( data == itr->getData() )
			{
				// throw exception??
				throw BST_Exception( ( char* ) "node with same data already exists", BST_Exception::EXCEP_DUPLICATE_INSERT );
			}
			
			if ( data < itr->getData() )
			{
				// goto left
				if ( itr->getLeft() != nullptr )
				{
					itr = itr->getLeft();
				}
				else
				{
					break;
				}
			}
			else
			{
				// goto right
				if ( itr->getRight() != nullptr )
				{
					itr = itr->getRight();
				}
				else
				{
					break;
				}
			}
		}
		
		if ( data < itr->getData() )
		{
			itr->setLeft( new BST_Node<T>( data ) );
		}
		else
		{
			itr->setRight( new BST_Node<T>( data ) );
		}
	}
	
	return true;
}

template <typename T>
void BinarySearchTree<T>::print( const int printType ) const
{
	PrintNode<T> printNode;
	NullFunctor<T> nullFunctor;
	
	switch ( printType )
	{
		case BREADTH_FIRST:
			_breadthFirstTraversal( _root, &printNode );
		break;
		
		case PRE_ORDER:
			_deapthFirstTraversal( _root, &printNode, &nullFunctor, &nullFunctor );
		break;
		
		case POST_ORDER:
			_deapthFirstTraversal( _root, &nullFunctor, &nullFunctor, &printNode );
		break;
		
		case IN_ORDER:
		default:
			_deapthFirstTraversal( _root, &nullFunctor, &printNode, &nullFunctor );
		break;
	}
	
}

template <typename T>
int BinarySearchTree<T>::height()
{
	// TBD
	return 0;
}

template <typename T>
int BinarySearchTree<T>::numNodes()
{
	int nodes = 0;
	if ( isEmpty() )
	{
		return nodes;
	}
	
	Counter<T> nodeCount;
	NullFunctor<T> nullFunctor;
	_deapthFirstTraversal( _root, &nodeCount, &nullFunctor, &nullFunctor );
	return nodeCount.getCount();
}

template <typename T>
const BinarySearchTree<T>& BinarySearchTree<T>::operator = ( const BinarySearchTree<T>& otherTree )
{
	if ( this != &otherTree )
	{
		if ( !isEmpty() )
		{
			_deleteTree( _root );
		}
		
		_root = _copyTree( otherTree._root );
	}
	
	return *this;
}

template <typename T>
void BinarySearchTree<T>::traverseTree( int traverseType, BaseFunct<T>* functor ) const
{
	NullFunctor<T> nullFunctor;
	
	switch ( traverseType )
	{
		case BREADTH_FIRST:
			_breadthFirstTraversal( _root, functor );
		break;
		
		case PRE_ORDER:
			_deapthFirstTraversal( _root, functor, &nullFunctor, &nullFunctor );
		break;
		
		case POST_ORDER:
			_deapthFirstTraversal( _root, &nullFunctor, &nullFunctor, functor );
		break;
		
		case IN_ORDER:
		default:
			_deapthFirstTraversal( _root, &nullFunctor, functor, &nullFunctor );
		break;
	}
}

template <typename T>
void BinarySearchTree<T>::_breadthFirstTraversal( BST_Node<T>* node, BaseFunct<T>* functor ) const
{
	if ( node == nullptr )
	{
		return;
	}
	
	Deque< BST_Node<T>* > deque;
	deque.pushBack( node );		// start with root node
	_breadthFirstHelper( deque, functor );
}

template <typename T>
void BinarySearchTree<T>::_breadthFirstHelper( Deque< BST_Node<T>* >& deque, BaseFunct<T>* functor ) const
{
	if ( deque.isEmpty() )
	{
		return;
	}
	
	BST_Node<T>* node = deque.popFront();
	( *functor )( node );
	
	if ( node->getLeft() != nullptr )
	{
		deque.pushBack( node->getLeft() );
	}
	
	if ( node->getRight() != nullptr )
	{
		deque.pushBack( node->getRight() );
	}
	
	_breadthFirstHelper( deque, functor );
}

template <typename T>
void BinarySearchTree<T>::_deapthFirstTraversal( BST_Node<T>* node, BaseFunct<T>* preOrderFunct, BaseFunct<T>* inOrderFunct, BaseFunct<T>* postOrderFunct ) const
{
	if ( node == nullptr )
	{
		return;
	}
	
	( *preOrderFunct )( node );
	_deapthFirstTraversal( node->getLeft(), preOrderFunct, inOrderFunct, postOrderFunct );
	( *inOrderFunct )( node );
	_deapthFirstTraversal( node->getRight(), preOrderFunct, inOrderFunct, postOrderFunct );
	( *postOrderFunct )( node );
}

template <typename T>
void BinarySearchTree<T>::_deleteTree( BST_Node<T>*& root )
{
	if ( root == nullptr )
	{
		return;
	}
	
	NullFunctor<T> nullFunctor;
	DeleteNode<T> deleteNode;
	
	// post order is suitable for deletion
	_deapthFirstTraversal( root, &nullFunctor, &nullFunctor, &deleteNode );
	
	root = nullptr;
}


template <typename T>
bool BinarySearchTree<T>::_searchHelper( const T& data, BST_Node<T>* node )
{
	if ( node == nullptr || node->getData() > data )
	{
		return false;
	}
	
	if ( node->getData() == data )
	{
		return true;
	}
	
	if ( data < node->getData() )
	{
		return _searchHelper( data, node->getLeft() );
	}
	else
	{
		return _searchHelper( data, node->getRight() );
	}
}

template <typename T>
BST_Node<T>* BinarySearchTree<T>::_copyTree( BST_Node<T>* srcNode )
{
	// keep building subtrees from leaf nodes to root
	if ( srcNode == nullptr )
	{
		return nullptr;
	}
	
	BST_Node<T>* newNode = new BST_Node<T>( srcNode->getData() );
	
	newNode->setLeft( _copyTree( srcNode->getLeft() ) );
	newNode->setRight( _copyTree( srcNode->getRight() ) );
	
	return newNode;
}
