#include "ExprFSM.h"

ExprFSM::ExprFSM() noexcept
{
	_expressionType = ExpressionType::INFIX_EXPR;
	initStateVector();
	_currentState = nullptr;
}

ExprFSM::ExprFSM(ExpressionType expressionType, ExprStates stateType) noexcept
{
	_expressionType = expressionType;
	initStateVector();
	updateCurrentState(stateType);
	_currentState->execute(_actionParams);
}

ExprFSM::~ExprFSM()
{
	cleanupStateVector();
}

FSM_State* ExprFSM::getCurrentState() const
{
	return _currentState;
}

void ExprFSM::setCurrentState(ExprStates stateType)
{
	_currentState = new FSM_State(stateType);
}

void ExprFSM::setExprType(ExpressionType expressionType)
{
	_expressionType = expressionType;
}

bool ExprFSM::update(ExprElement* inputElement)
{
	// pass the data to the active state and it should decide what is to be
	// done for this data.
	bool result = true;
	if (_currentState != nullptr)
	{
		updateCurrentState(_currentState->update(inputElement, _actionParams, result));
		_currentState->execute(_actionParams);
		#ifndef NDEBUG
		cout << "current element: " << *inputElement << endl;
		_currentState->debugPrint();
		cout << "result: " << result << endl;
		#endif
	}
	return result;
}

void ExprFSM::cleanupStateVector()
{
	for (int ii = 0; ii < _allStateVector.size(); ii++)
	{
		delete _allStateVector[ii];
	}
	
	_allStateVector.clear();
}

void ExprFSM::initStateVector()
{
	void (ExprFSM::*setupFunc)(ExprStates, FSM_State*);
	switch (_expressionType)
	{
		case ExpressionType::INFIX_EXPR:
			setupFunc = &ExprFSM::setupInfixStates;
		break;
		
		case ExpressionType::PREFIX_EXPR:
			setupFunc = &ExprFSM::setupPrefixStates;
		break;
		
		case ExpressionType::POSTFIX_EXPR:
			setupFunc = &ExprFSM::setupPostfixStates;
		break;
	}
	
	size_t startState = static_cast<size_t>(ExprStates::STATE_START);
	size_t endState = static_cast<size_t>(ExprStates::STATE_END);
	for (size_t ii = startState; ii < endState + 1; ii++)
	{
		_allStateVector.push_back(new FSM_State(static_cast<ExprStates>(ii)));
		// function pointer needs to know the scope so this is used here
		// for a non-member function, one can skip the leading scope
		(this->*setupFunc)(_allStateVector.back()->getStateType(), _allStateVector.back());
	}
}

void ExprFSM::updateCurrentState(ExprStates stateType)
{
	_currentState = _allStateVector[static_cast<size_t>(stateType)];
}

void ExprFSM::setupInfixStates(ExprStates stateType, FSM_State* state)
{
	FSM_Transition* transition = nullptr;
	
	switch (stateType)
	{
		case ExprStates::STATE_OPERATOR:
			transition = new FSM_Transition(ExprStates::STATE_BRACKET_OPEN, 
							new BracketOpen_TC());
			state->addTransition(transition);
			
			transition = new FSM_Transition(ExprStates::STATE_OPERAND, 
							new OperandInfix_TC());
			state->addTransition(transition);
		break;
		
		case ExprStates::STATE_OPERAND:
			// this has to be first because '=' is an operator as well
			// therefore state would go to operator instead of end
			transition = new FSM_Transition(ExprStates::STATE_END, 
							new EvalOperatorInfix_TC());
			state->addTransition(transition);
			
			transition = new FSM_Transition(ExprStates::STATE_BRACKET_CLOSE, 
							new BracketClose_TC());
			state->addTransition(transition);
			
			transition = new FSM_Transition(ExprStates::STATE_AVG_OP_BEGIN, 
							new AvgBeginInfix_TC());
			
			state->addTransition(transition);
			
			transition = new FSM_Transition(ExprStates::STATE_AVG_OP_END, 
							new AvgEndInfix_TC());
			
			state->addTransition(transition);
			
			transition = new FSM_Transition(ExprStates::STATE_OPERATOR, 
							new OperatorInfix_TC());
			
			state->addTransition(transition);
		break;
		
		case ExprStates::STATE_BRACKET_OPEN:
			transition = new FSM_Transition(ExprStates::STATE_BRACKET_OPEN, 
							new BracketOpen_TC());
			state->addTransition(transition);
			
			transition = new FSM_Transition(ExprStates::STATE_OPERAND, 
							new OperandInfix_TC());
			state->addTransition(transition);
			
			state->addUpdateAction(new BracketOpenAction_Infix());
		break;
		
		case ExprStates::STATE_BRACKET_CLOSE:
			// this has to be first because '=' is an operator as well
			// therefore state would go to operator instead of end
			transition = new FSM_Transition(ExprStates::STATE_END, 
							new EvalOperatorInfix_TC());
			state->addTransition(transition);
			
			transition = new FSM_Transition(ExprStates::STATE_BRACKET_CLOSE, 
							new BracketClose_TC());
			state->addTransition(transition);
			
			transition = new FSM_Transition(ExprStates::STATE_OPERATOR, 
							new OperatorInfix_TC());
			state->addTransition(transition);
			
			transition = new FSM_Transition(ExprStates::STATE_BRACKET_OPEN, 
							new BracketCloseToOpen_TC());
			state->addTransition(transition);
			
			transition = new FSM_Transition(ExprStates::STATE_AVG_OP_END, 
							new AvgEndLoop_TC());
			state->addTransition(transition);
			
			state->addUpdateAction(new BracketCloseAction_Infix());
		break;
		
		case ExprStates::STATE_START:
			transition = new FSM_Transition(ExprStates::STATE_BRACKET_OPEN, 
							new BracketOpen_TC());
			state->addTransition(transition);
			
			transition = new FSM_Transition(ExprStates::STATE_OPERAND, 
							new OperandInfix_TC());
			state->addTransition(transition);
			
			state->addUpdateAction(new StartAction());
		break;
		
		case ExprStates::STATE_AVG_OP_BEGIN:
			transition = new FSM_Transition(ExprStates::STATE_BRACKET_OPEN, 
							new BracketOpen_TC());
			state->addTransition(transition);
			
			transition = new FSM_Transition(ExprStates::STATE_OPERAND, 
							new OperandInfix_TC());
			state->addTransition(transition);
			
			state->addUpdateAction(new AvgBeginAction_Infix());
		break;
		
		case ExprStates::STATE_AVG_OP_END:
			// this has to be first because '=' is an operator as well
			// therefore state would go to operator instead of end
			transition = new FSM_Transition(ExprStates::STATE_END, 
							new EvalOperatorInfix_TC());
			state->addTransition(transition);
			
			transition = new FSM_Transition(ExprStates::STATE_AVG_OP_END, 
							new AvgEndLoop_TC());
			state->addTransition(transition);
			
			transition = new FSM_Transition(ExprStates::STATE_BRACKET_CLOSE, 
							new BracketClose_TC());
			state->addTransition(transition);
			
			transition = new FSM_Transition(ExprStates::STATE_BRACKET_OPEN, 
							new BracketOpen_TC());
			state->addTransition(transition);
			
			transition = new FSM_Transition(ExprStates::STATE_AVG_OP_BEGIN, 
							new AvgBeginInfix_TC());
			
			state->addTransition(transition);
			
			transition = new FSM_Transition(ExprStates::STATE_OPERATOR, 
							new OperatorInfix_TC());
			state->addTransition(transition);
			
			state->addUpdateAction(new AvgEndAction_Infix());
		break;
		
		case ExprStates::STATE_END:
		case ExprStates::STATE_NONE:
		default:
		break;
	}
}

void ExprFSM::setupPrefixStates(ExprStates stateType, FSM_State* state)
{
	FSM_Transition* transition = nullptr;
	
	switch (stateType)
	{
		case ExprStates::STATE_OPERATOR:
			transition = new FSM_Transition(ExprStates::STATE_OPERATOR, 
							new OperatorPrefix_TC());
			state->addTransition(transition);
			
			transition = new FSM_Transition(ExprStates::STATE_OPERAND, 
							new OperandPrefix_TC());
			state->addTransition(transition);
			
			state->addUpdateAction(new OperatorAction_PnP());
		break;
		
		case ExprStates::STATE_OPERAND:
			// this has to be first because '=' is an operator as well
			// therefore state would go to operator instead of end
			transition = new FSM_Transition(ExprStates::STATE_END, 
							new EvalOperatorPnP_TC());
			state->addTransition(transition);
			
			transition = new FSM_Transition(ExprStates::STATE_OPERAND, 
							new OperandPrefix_TC());
			state->addTransition(transition);
			
			transition = new FSM_Transition(ExprStates::STATE_AVG_OP_BEGIN, 
							new ToAvgPrefix_TC());
			state->addTransition(transition);
			
			transition = new FSM_Transition(ExprStates::STATE_OPERATOR, 
							new OperatorPrefix_TC());
			state->addTransition(transition);
			
			state->addUpdateAction(new OperandAction_PnP());
		break;
		
		case ExprStates::STATE_START:
			transition = new FSM_Transition(ExprStates::STATE_AVG_OP_BEGIN, 
							new ToAvgPrefix_TC());
			state->addTransition(transition);
		
			transition = new FSM_Transition(ExprStates::STATE_OPERATOR, 
							new OperatorPrefix_TC());
			state->addTransition(transition);
			
			state->addUpdateAction(new StartAction());
		break;
		
		case ExprStates::STATE_AVG_OP_BEGIN:
			transition = new FSM_Transition(ExprStates::STATE_OPERAND, 
							new OperandPrefix_TC());
			state->addTransition(transition);
			
			transition = new FSM_Transition(ExprStates::STATE_AVG_OP_BEGIN, 
							new ToAvgPrefix_TC());
			state->addTransition(transition);
			
			transition = new FSM_Transition(ExprStates::STATE_OPERATOR, 
							new OperatorPrefix_TC());
			state->addTransition(transition);
			
			state->addUpdateAction(new AvgAction_PnP());
		break;
		
		case ExprStates::STATE_END:
		case ExprStates::STATE_NONE:
		default:
		break;
	}
}

void ExprFSM::setupPostfixStates(ExprStates stateType, FSM_State* state)
{
	FSM_Transition* transition = nullptr;
	
	switch (stateType)
	{
		case ExprStates::STATE_OPERATOR:
			// this has to be first because '=' is an operator as well
			// therefore state would go to operator instead of end
			transition = new FSM_Transition(ExprStates::STATE_END, 
							new EvalOperatorPnP_TC());
			state->addTransition(transition);
			
			transition = new FSM_Transition(ExprStates::STATE_AVG_OP_BEGIN, 
							new ToAvgPostfix_TC());
			state->addTransition(transition);
		
			transition = new FSM_Transition(ExprStates::STATE_OPERATOR, 
							new OperatorPostfix_TC());
			state->addTransition(transition);
			
			transition = new FSM_Transition(ExprStates::STATE_OPERAND, 
							new OperandPostfix_TC());
			state->addTransition(transition);
			
			state->addUpdateAction(new OperatorAction_PnP());
		break;
		
		case ExprStates::STATE_OPERAND:
			transition = new FSM_Transition(ExprStates::STATE_OPERAND, 
							new OperandPostfix_TC());
			state->addTransition(transition);
			
			transition = new FSM_Transition(ExprStates::STATE_AVG_OP_BEGIN, 
							new ToAvgPostfix_TC());
			state->addTransition(transition);
			
			transition = new FSM_Transition(ExprStates::STATE_OPERATOR, 
							new OperatorPostfix_TC());
			state->addTransition(transition);
			
			state->addUpdateAction(new OperandAction_PnP());
		break;
		
		case ExprStates::STATE_START:
			transition = new FSM_Transition(ExprStates::STATE_OPERAND, 
							new OperandPostfix_TC());
			state->addTransition(transition);
			
			state->addUpdateAction(new StartAction());
		break;
		
		case ExprStates::STATE_AVG_OP_BEGIN:
			// this has to be first because '=' is an operator as well
			// therefore state would go to operator instead of end
			transition = new FSM_Transition(ExprStates::STATE_END, 
							new EvalOperatorPnP_TC());
			state->addTransition(transition);
			
			transition = new FSM_Transition(ExprStates::STATE_AVG_OP_BEGIN, 
							new ToAvgPostfix_TC());
			state->addTransition(transition);
			
			state->addUpdateAction(new AvgAction_PnP());
		break;
		
		case ExprStates::STATE_END:
		case ExprStates::STATE_NONE:
		default:
		break;
	}
}

void ExprFSM::debugPrint()
{
	#ifndef NDEBUG
	_currentState->debugPrint();
	cout << "number of initialized states: " << _allStateVector.size() << endl; 
	cout << "expression type: " << static_cast<int>(_expressionType) << endl;
	#endif
}
