#ifndef GLOBAL_VARIABLES_H
#define GLOBAL_VARIABLES_H

#include <string>
#include <stack>
#include <cmath>

using std::string;
using std::stack;

class GlobalVariables
{
	public:
		const static string LIST_OF_OPERATORS;
		const static string WHITESPACES;
		const static char EVAL_OPERATOR;
		const static char NULL_TERM;
		const static char PARENTHESIS_OPEN_OPERATOR;
		const static char PARENTHESIS_CLOSE_OPERATOR;
		const static char AVG_OPERATOR;
		const static char ADD_OPERATOR;
		const static char SUB_OPERATOR;
		const static char MUL_OPERATOR;
		const static char DIV_OPERATOR;
		const static char EXPO_OPERATOR;
		
		struct StateActionParams
		{
			short operatorCount;
			short operandCount;
			short bracketCount;
			short avgOpCount;
		};
		
		GlobalVariables() = default;
		~GlobalVariables() = default;
		GlobalVariables(GlobalVariables& vars) = default;
		GlobalVariables(GlobalVariables&& vars) = default;
		
		GlobalVariables& operator =(const GlobalVariables& var) = default;
};

#endif
