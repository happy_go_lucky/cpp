#include "../inc/ThreadException.h"

ThreadException::ThreadException(char* message)
{
	_message = message;
}

const char* ThreadException::what() const throw()
{
	return _message;
}
