#include "../inc/ReadWriteLock.h"

ReadWriteLock::ReadWriteLock()
{
	// nothing to do
}

ReadWriteLock::~ReadWriteLock()
{
	// nothing to do
}

void ReadWriteLock::init()
{
	// should use PTHREAD_RWLOCK_INITIALIZER if there's nothing for attributes
	pthread_rwlockattr_init(&_attrib);
	pthread_rwlock_init(&_lock, &_attrib);
}

void ReadWriteLock::cleanup()
{
	unlock();
	_destroyLock();
	
	pthread_rwlockattr_destroy(&_attrib);
}

/**
 * if writer doesn't hold the lock and there are no writers blocked on the lock
 * the calling thread is blocked until the writer release the lock
 * 
 * */
void ReadWriteLock::applyReadLock()
{
	int ret = pthread_rwlock_rdlock(&_lock);
	
	#ifndef NDEBUG
	if (ret != EXIT_SUCCESS)
	{
		int err = errno;
		std::cerr << "failed to apply read lock: " << strerror(err) << std::endl;
	}
	#endif
}

/**
 * The caller thread is not blocked, instead the function fails if it unable to
 * acquire the lock.
 * */
int ReadWriteLock::tryReadLock()
{
	return pthread_rwlock_tryrdlock(&_lock);
}

/**
 * The caller thread acquires the lock if no other reader/writer lock is acquired
 * The calling thread is blocked otherwise until the lock is released.
 * */
void ReadWriteLock::applyWriteLock()
{
	pthread_rwlock_wrlock(&_lock);
	
	#ifndef NDEBUG
	if (ret != EXIT_SUCCESS)
	{
		int err = errno;
		std::cerr << "failed to apply write lock: " << strerror(err) << std::endl;
	}
	#endif
}

/**
 * The caller thread is not blocked, instead the function fails if it unable to
 * acquire the lock.
 * */
int ReadWriteLock::tryWriteLock()
{
	return pthread_rwlock_tryrwlock(&_lock);
}

/**
 * if there are other threads waiting to acquire the lock, this will have no effect
 * otherwise it will unlock the read/write lock.
 * */
void ReadWriteLock::unlock()
{
	int ret = pthread_rwlock_unlock(&_lock);
	
	#ifndef NDEBUG
	if (ret != EXIT_SUCCESS)
	{
		int err = errno;
		std::cerr << "unable to unlock read/write lock: " << strerror(err) << std::endl;
	}
	#endif
}

void ReadWriteLock::_destroyLock()
{
	int ret = pthread_rwlock_destroy(&_lock);
	
	#ifndef NDEBUG
	if (ret != EXIT_SUCCESS)
	{
		int err = errno;
		std::cerr << "failed to destroy read lock: " << strerror(err) << std::endl;
	}
	#endif
}
