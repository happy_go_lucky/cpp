#ifndef PARSE_PROGRAM_LIST_H
#define PARSE_PROGRAM_LIST_H
/**
This file implements functions related to reading of a file from local drive.

- Sequential implementation.
- This will parse the list of the programs one by one, and will throw an error as it encounters.
- It should be called in a loop to get the contents.

1. Check if file is valid.
2. Read contents of the file one line at a time.
3. Check for end of the file.
*/
#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>

// return allocation should be cleared by caller
char* readNextLine( const char* fileName, const char delimiter, int* filePos );

// helper functions
void _increaseBuffer();
void _initBuffer();
void _copyBuffer( const char* source, char* destination, const int sourceLength );

#endif
