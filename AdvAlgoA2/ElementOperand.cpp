#include "ElementOperand.h"

ElementOperand::ElementOperand(const double& data) noexcept : ExprElement(ElementType::TYPE_OPERAND)
{
	_data = data;
}

ostream& operator <<(ostream& out, const ElementOperand& elem)
{
	out << elem._data;
	return out;
}
