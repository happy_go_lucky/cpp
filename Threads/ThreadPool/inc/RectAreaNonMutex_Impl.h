#ifndef RECT_AREA_NON_MUTEX_IMPL_H
#define RECT_AREA_NON_MUTEX_IMPL_H

#include <posixthreads/inc/ThreadDefs.h>
#include <posixthreads/inc/Thread.h>
#include "Rectangle.h"
#include "RectAreaImplArgs.h"

class RectAreaNonMutex_Impl : public posixthreads::Thread
{
	public:
		RectAreaNonMutex_Impl();
		~RectAreaNonMutex_Impl();
		
		void* threadRun();
	
	private:
		
	
};

#endif
