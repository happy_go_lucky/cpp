#include "../inc/Producer.h"

Producer::Producer()
{
	
}

Producer::~Producer()
{
	
}

void Producer::init(SharedMemory* shMem)
{
	std::cout << "Producer with pid: " << getpid() 
			<< " reading from shared memory" << std::endl;
	std::cout << *(int*)(shMem->getPointerToMemory()) << std::endl;
}
