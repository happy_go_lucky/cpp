echo "Building testFiles"

sharedPtr_project=testSharedPtrs
functionGlue_project=functionGlue
moveTest_project=moveTest
testFactory_project=testFactory
testSignals_project=testSignals
testMultiInheritance_project=testMultiInheritance

rm -f *.o

g++ $sharedPtr_project.cpp -o $sharedPtr_project -pthread -std=c++14
echo built $sharedPtr_project

g++ $functionGlue_project.cpp -o $functionGlue_project -pthread -std=c++14
echo built $functionGlue_project

g++ $moveTest_project.cpp -o $moveTest_project -pthread -std=c++14
echo built $moveTest_project

g++ $testFactory_project.cpp -o $testFactory_project -pthread -std=c++14
echo built $testFactory_project

g++ $testSignals_project.cpp -o $testSignals_project -pthread -std=c++14
echo built $testSignals_project

g++ $testMultiInheritance_project.cpp -o $testMultiInheritance_project -pthread -std=c++14
echo built $testMultiInheritance_project
