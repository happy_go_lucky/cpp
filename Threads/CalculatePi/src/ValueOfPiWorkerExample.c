/**
 * This program calculates the value of PI using area under the curve.
 * The mid-point rectangle rule is used to calculate the approximate area
 * under the curve. 
 * pi = lim(0-1) (4/(1 + x^2))dx
 * */

#include <iostream>
#include <pthread.h>
#include <iomanip>
#include "../inc/Rectangle.h"
#include <posixthreads/inc/MutexLock.h>
#include <posixthreads/inc/ConditionVariable.h>

using namespace std;

void singleThreadedVersion();
void multiThreadedVersion();
void* threadedAreaFunction(void* argPtr);
void multiThreadedNonMutexVersion();
void* threadedAreaFunctionNonMutex(void* argPtr);

// also decides the precision of calculation
// larger number means more precision, and longer calculation
static const long NUM_RECTS = 10000000;
static const double LOWER_LIMIT = 0.0;
static const double UPPER_LIMIT = 1.0;
static const short NUM_THREADS = 4;

MutexLock mutexLock;
double totalArea = 0.0;
Rectangle* rectArray;
double* sumArray;

int main(int argCount, char** argVector)
{
	clock_t begin = clock(); // Start the stopwatch
	singleThreadedVersion();
	clock_t end = clock(); // Stop the stopwatch
	double elapsed_time = double(end - begin) / CLOCKS_PER_SEC;
	cout << "singlethreaded time: " << setw(16) << elapsed_time << endl;
	
	
	begin = clock(); // Start the stopwatch
	multiThreadedVersion();
	end = clock(); // Stop the stopwatch
	elapsed_time = double(end - begin) / CLOCKS_PER_SEC;
	cout << "multithreaded time: " << setw(16) << elapsed_time << endl;
	
	begin = clock(); // Start the stopwatch
	multiThreadedNonMutexVersion();
	end = clock(); // Stop the stopwatch
	elapsed_time = double(end - begin) / CLOCKS_PER_SEC;
	cout << "multithreaded non mutex time: " << setw(16) << elapsed_time << endl;
	
	return EXIT_SUCCESS;
}

void* threadedAreaFunction(void* argPtr)
{
	int currentThreadNumber = *((int*) argPtr);
	
	double mid = 0.0;
	double area = 0.0;
	double deltaX = (UPPER_LIMIT - LOWER_LIMIT) / (double) NUM_RECTS;
	
	for (int ii = currentThreadNumber; ii < NUM_RECTS; ii += NUM_THREADS)
	{
		rectArray[ii].setWidth(deltaX); 
		mid = (ii + 0.5) * rectArray[ii].getWidth();
		rectArray[ii].setHeight(4.0 / (1.0 + mid * mid));
		area += rectArray[ii].getArea();
	}
	
	mutexLock.lock();
	totalArea += area;
	mutexLock.unlock();
	
	return EXIT_SUCCESS;
}

void multiThreadedVersion()
{
	rectArray = new Rectangle[NUM_RECTS]();
	pthread_t threadHandles[NUM_THREADS];
	int threadNumbers[NUM_THREADS];
	
	mutexLock.initResources();
	for (int ii = 0; ii < NUM_THREADS; ii++)
	{
		threadNumbers[ii] = ii;
		pthread_create(&threadHandles[ii], NULL, threadedAreaFunction,
						&threadNumbers[ii]);
						
		pthread_join(threadHandles[ii], NULL);
	}
	
	mutexLock.cleanupResources();
	
	printf("Computed pi = %f\n", totalArea);
	delete[] rectArray;
}

void* threadedAreaFunctionNonMutex(void* argPtr)
{
	int currentThreadNumber = *((int*) argPtr);
	
	double mid = 0.0;
	sumArray[currentThreadNumber] = 0.0;
	double deltaX = (UPPER_LIMIT - LOWER_LIMIT) / (double) NUM_RECTS;
	
	for (int ii = currentThreadNumber; ii < NUM_RECTS; ii += NUM_THREADS)
	{
		rectArray[ii].setWidth(deltaX); 
		mid = (ii + 0.5) * rectArray[ii].getWidth();
		rectArray[ii].setHeight(4.0 / (1.0 + mid * mid));
		sumArray[currentThreadNumber] += rectArray[ii].getArea();
	}
	
	return EXIT_SUCCESS;
}


void multiThreadedNonMutexVersion()
{
	rectArray = new Rectangle[NUM_RECTS]();
	sumArray = new double[NUM_THREADS];
	pthread_t threadHandles[NUM_THREADS];
	int threadNumbers[NUM_THREADS];
	totalArea = 0.0;
	
	for (int ii = 0; ii < NUM_THREADS; ii++)
	{
		threadNumbers[ii] = ii;
		pthread_create(&threadHandles[ii], NULL, threadedAreaFunction,
						&threadNumbers[ii]);
						
		pthread_join(threadHandles[ii], NULL);
	}
	
	for (int ii = 0; ii < NUM_THREADS; ii++)
	{
		totalArea += sumArray[ii];
	}
	
	printf("Computed pi = %f\n", totalArea);
	delete[] rectArray;
	delete[] sumArray;
}


void singleThreadedVersion()
{
	Rectangle* rectArray = new Rectangle[NUM_RECTS]();
	double mid, area = 0.0;
	double deltaX = (UPPER_LIMIT - LOWER_LIMIT) / (double) NUM_RECTS;
	
	for (int ii = 0; ii < NUM_RECTS; ii++)
	{
		rectArray[ii].setWidth(deltaX); 
		mid = (ii + 0.5) * rectArray[ii].getWidth();
		rectArray[ii].setHeight(4.0 / (1.0 + mid * mid));
		area += rectArray[ii].getArea();
	}
	printf("Computed pi = %f\n", area);
	
	delete[] rectArray;
}
