/**
 * This a thread pool class which provides user a collection of threads.
 * Ideally this pool should be used for small tasks, so that the overhead
 * of creating and destroying the threads can be reduced. This also provides
 * a good way to manage the multi-threading nightmare of having too many
 * concurrent threads on the system which would do more damage than benefit.
 * We can cap the maximum allowed threads and still achieve concurrency.
 * 
 * Interface:
 * A group of threads will be spawned at the begining. These threads will
 * take a generic form of task which can be run.
 * The tasks will be managed by a queue of tasks, where the entries will be
 * managed by weak pointers. The weak pointers can help manage object's
 * life time issues because the queue doesn't have to worry about the task
 * expiry.
 * 
 * once a thread in the pool will finish it's task, it will return back to
 * wainting state where it will wait further tasks. 
 * 
 * The thread workers will be managed by a queue where each worker will be 
 * added and removed.
 * 
 * */

#ifndef THREAD_POOL_H
#define THREAD_POOL_H

#include <sys/sysinfo.h>
#include <iostream>
#include <queue>
#include <memory>		// for smart pointers
#include <atomic> 		// for atomic variables
#include <functional>	// for bind and function pointer wrappers
#include "ThreadDefs.h"
#include "MutexLock.h"
#include "ThreadPoolTask.h"

class ThreadPool
{
	public:
		const static short POOL_SIZE_ALLOWED_OVERLOAD = 1;
		const static short POOL_SIZE_UNKNOWN = 0;
		
		ThreadPool(posixthreads::ThreadPoolSizeType sizeType, unsigned short size = POOL_SIZE_UNKOWN) noexcept;
		~ThreadPool();
		
		ThreadPool(const ThreadPool& pool) = delete;
		ThreadPool(ThreadPool&& pool) = delete;
		ThreadPool& operator =(const ThreadPool& pool) = delete;
		ThreadPool& operator =(ThreadPool& pool) = delete;
		
		unsigned int capacity() const noexcept;
		unsigned int size() const noexcept;
		
		
		// should be serialized
		void addTask(std::weak_ptr<ThreadPoolTask> task);
		// should be serialized
		void removeTask();
		
		
	private:
		unsigned int _numTotalThreads;
		unsigned int _numActiveThreads;
		
		// queue ops should be thread-safe
		std::queue<std::weak_ptr<ThreadPoolTask>> _tasksPendingQueue;
		std::queue<std::weak_ptr<ThreadPoolTask>> _tasksInprocessQueue;
		std::vector<Thread> _threadArray;
		MutexLock* _taskUpdateLock;
		
		void _updateNumThread(posixthreads::ThreadPoolSizeType sizeType, unsigned short size) noexcept;
		int _getDefaultPoolSize() const noexcept;
};

#endif
