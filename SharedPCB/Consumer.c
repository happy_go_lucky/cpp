#include <stdio.h>
#include <stdlib.h>
#include "SharedDefines.h"
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <assert.h>
#include <errno.h>	// for error
#include <string.h>	// for error
// for semaphores
#include <fcntl.h>
#include <sys/stat.h>
#include <semaphore.h>

int sharedBufferID = -1;
char* sharedBufferPointer = NULL;
sem_t* semaphore = NULL;

void setupSharedBuffer( const char* filename );
void createSemaphore();
void closeSemaphore();
void removeSharedBuffer();
void updateSharedBuffer( sem_t* semaphore, char* sharedBufferPointer, const char* filename );
int bufferToFile( const char* filename, int* totalBytesWritten );

int main( int argC, char** argV )
{
	if ( argC != 2 )
	{
		fprintf( stderr, "Provide the output buffer file name in command line\n" );
		return EXIT_FAILURE;
	}
	
	printf("Consumer created\n");
	
	const char* filename = *( argV + 1 );
	setupSharedBuffer( filename );
	createSemaphore();
	updateSharedBuffer( semaphore, sharedBufferPointer, filename );
	removeSharedBuffer();
	closeSemaphore();
	
	return EXIT_SUCCESS;
}

void updateSharedBuffer( sem_t* semaphore, char* sharedBufferPointer, const char* filename )
{
	if ( semaphore == NULL )
	{
		fprintf( stderr, "create a semaphore first\n" );
		return;
	}
	
	if ( sharedBufferPointer == NULL )
	{
		fprintf( stderr, "create a shared buffer first\n" );
		return;
	}
	
	int bytesRead = 0;
	int totalBytesRead = 0;
	
	sem_wait( semaphore );
	// Critical section
	bytesRead = bufferToFile( filename, &totalBytesRead );
	if ( bytesRead > 0 )
	{
		sem_post( semaphore );
	}
	else
	{
		closeSemaphore();
	}
	
}

int bufferToFile( const char* filename, int* totalBytesWritten )
{
	FILE *fileStream = fopen( filename, FILE_WRITE_FLAG );
	FILE *fileStreamIterator = fileStream;
	
	if ( fileStream == NULL )
	{
		fprintf( stderr, "unable to open file \"%s\" for reading\n", filename );
		exit( EXIT_FAILURE );
	}
	
	char byteBuffer = *sharedBufferPointer;
	int bytesWritten = 0;
	
	fprintf( fileStreamIterator, "%c%n", byteBuffer, &bytesWritten );
	*totalBytesWritten += bytesWritten;
	
	#ifdef DEBUG_PRINT
	printf( "%c", byteBuffer );
	#endif
	
	fclose( fileStream );
	return bytesWritten;
}

void setupSharedBuffer( const char* filename )
{
	// generate an identifier key for the shared buffer
	// same key makes the sharing possible
	int pathSize = 1024;
	char* pathname = malloc( sizeof( char ) * pathSize );
	getPath( pathname, pathSize, filename );
	
	// ftok generates a psudo-random key
	// if seeking same key, ensure that pathname points to a file which is not changed during
	// the life cycle of the project, or else, different keys will be generated
	key_t sharedBufferKey = ftok( pathname, PROJECT_ID );
	assert ( sharedBufferKey > -1 );
	
	#ifdef DEBUG_PRINT
	printf( "shared buffer key: %x\n", sharedBufferKey );
	printf( "permissions: " );
	printIntToBinary( ( int ) PERMISSION );
	printf( "IPC_CREAT binary value: " );
	printIntToBinary( IPC_CREAT );
	#endif
	
	free( pathname );
	
	// create the shared buffer segment
	// buffer size is rounded off to the multiple of nearest page size (in bytes)
	// size is allocated as a chunk in bytes
	// providing proper permission is very important, otherwise memory will be created
	// but will not be accessible
	sharedBufferID = shmget( sharedBufferKey, BUFFER_SIZE_IN_BYTES, (int) PERMISSION );
	
	#ifdef DEBUG_PRINT
	printf( "shared buffer id: %d\n", sharedBufferID );
	printf( "size of page: %lu bytes\n", getPageSize() );
	#endif
	
	if ( sharedBufferID < 0 )
	{
		fprintf( stderr, "error description\n%s\n", strerror( errno ) );
		exit( errno );
	}
	
	// attach to the shared buffer segment
	// returns a pointer to shared memory allocated above
	// because it's allocated in bytes, use a char
	sharedBufferPointer = ( char* ) shmat( sharedBufferID, NULL, 0 );
	assert( sharedBufferPointer != NULL );
	#ifdef DEBUG_PRINT
	// TODO: fix this
	printf( "size of allocated memory: %lu bytes\n", sizeof( sharedBufferPointer ) / sizeof( *sharedBufferPointer ) );
	#endif
}

void createSemaphore()
{
	if ( semaphore == NULL )
	{
		// named semaphore, begins with a "/" + one or more characters
		semaphore = sem_open( SEMAPHORE, O_CREAT | O_EXCL, (int) PERMISSION, SEMAPHORE_START_VALUE );
		// unlink ensures that the semaphore is destroyed once all processes are done with it 
		sem_unlink( SEMAPHORE );
	}
}

void closeSemaphore()
{
	if ( semaphore != NULL )
	{
		semaphore = sem_close( semaphore ) == 0 ? NULL : semaphore;
	}
}

void removeSharedBuffer()
{
	#ifdef DEBUG_PRINT
	printf( "trying to removed buffer with id: %d\n", sharedBufferID );
	#endif
	// detach the segment from the process when done
	if ( sharedBufferPointer != NULL )
	{
		assert( shmdt( sharedBufferPointer ) > -1 );
	}
}
