#ifndef BINARY_SEARCH_TREE_H
#define BINARY_SEARCH_TREE_H

#include "BST_Exception.h"
#include "BST_HelperFunctors.h"
#include "BST_HelperFunctors.cpp"
#include "Deque.h"
#include "Deque.cpp"

template <typename T>
class BinarySearchTree
{
	public:
		static const int IN_ORDER = 0;
		static const int PRE_ORDER = 1;
		static const int POST_ORDER = 2;
		static const int BREADTH_FIRST = 3;
		
		BinarySearchTree();
		BinarySearchTree( const BinarySearchTree<T>& );
		~BinarySearchTree();
		
		bool isEmpty() const;
		bool search( const T& );
		
		void clear();
		void traverseTree( int, BaseFunct<T>* ) const;
		
		bool removeNode( const T& );
		bool insert( const T& );
		void print( const int ) const;
		int height();
		int numNodes();
		
		const BinarySearchTree<T>& operator = ( const BinarySearchTree<T>& );
		
	private:
		BST_Node<T>* _root;
		
		/**
		 * traverses the tree nodes level by level from top to bottom or root to leaf nodes
		 * algo: using a queue
		 * 1. start with root
		 * 2. add all of its children to back of queue
		 * 3. repeat 1 to 3 for each child 
		 * 
		 * */
		void _breadthFirstTraversal( BST_Node<T>*, BaseFunct<T>* ) const;
		void _breadthFirstHelper( Deque< BST_Node<T>* >&, BaseFunct<T>* ) const;
		
		/**
		 * traverse the tree nodes from top to bottom
		 * mostly done using one of: in-order (LVR), post-order(LRV), pre-order(VLR) techniques
		 * V: visting node, L: left sub-tree, R: right sub-tree
		 * 
		 * */
		void _deapthFirstTraversal( BST_Node<T>*, BaseFunct<T>*, BaseFunct<T>*, BaseFunct<T>* ) const;
		
		void _deleteTree( BST_Node<T>*& );
		bool _searchHelper( const T&, BST_Node<T>* );
		BST_Node<T>* _copyTree( BST_Node<T>* );
};

#endif
