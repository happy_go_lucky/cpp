#ifndef CONDITION_VARIABLE_H
#define CONDITION_VARIABLE_H

#include <pthread.h>
#include <ctime>
#include <string.h>
#include "MutexLock.h"
#include "HelperFxns.h"
#include "ThreadException.h"

using namespace std;

class ConditionVariable
{
	public:
		enum ProcessAccessType
		{
			INVALID = -1,
			SHARED = 0,
			PRIVATE
		};
		
		ConditionVariable(ProcessAccessType accessType) noexcept;
		ConditionVariable(const ConditionVariable& cv) = delete;
		ConditionVariable(ConditionVariable&& cv) = delete;
		~ConditionVariable();
		
		ConditionVariable& operator =(const ConditionVariable& cv) = delete;
		ConditionVariable& operator =(ConditionVariable&& cv) = delete;
		
		void initialize(MutexLock* mutex);
		void cleanup();
		void signal();
		void signalAll();
		void waitForSignal();
		void timedWait(int waitTimeInSeconds);
		
		ProcessAccessType getAccessType() const noexcept;
		// TODO: look into pthread_condattr_setclock if needed for timed wait
	
	private:
		pthread_cond_t* _cv;
		pthread_condattr_t* _cvAttrib;
		MutexLock* _mutexLock;
		ProcessAccessType _accessType;
		
		void _setAccessType(ProcessAccessType accessType);
};

#endif
