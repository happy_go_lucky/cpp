#ifndef RECTANGLE_H
#define RECTANGLE_H

#include <iostream>
#include <assert.h>

using namespace std;

class Rectangle
{
public:
	Rectangle() noexcept;
	Rectangle(double width, double height) noexcept;
	~Rectangle();
	
	Rectangle(const Rectangle& rec);
	Rectangle(Rectangle&& rec);
	
	friend ostream& operator << (ostream& s, const Rectangle& r);
	Rectangle& operator= (const Rectangle& rec);
	Rectangle& operator= (Rectangle&& rec) noexcept;
	
	bool operator== (const Rectangle& rec) const noexcept;
	bool operator== (const Rectangle&& rec) const noexcept;
	
	bool hasEqualArea(const Rectangle& rect) const;
	bool hasEqualPerimeter(const Rectangle& rect) const;
	
	inline double getHeight() const noexcept
	{
		return _height;
	}
	
	inline double getWidth() const noexcept
	{
		return _width;
	}
	
	inline double getArea() const noexcept
	{
		return _area;
	}
	
	inline double getPerimeter() const noexcept
	{
		return _perimeter;
	}
	
	void setHeight(double height);
	void setWidth(double width);
	
	void checkValidity() const; // invariant

private:
	double _width;
	double _height;
	double _area;
	double _perimeter;
	
	void updateAreaPerimeter() noexcept;
};

#endif
