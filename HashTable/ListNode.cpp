#include "ListNode.h"

using namespace std;

template<typename TYPE>
ListNode<TYPE>::ListNode()
{
	// cout << "ListNode()" << endl;
}

template<typename TYPE>
ListNode<TYPE>::ListNode( const TYPE& data, ListNode<TYPE>* next )
{
	// cout << "ListNode( TYPE, ListNode<TYPE>* )" << endl;
	this->data = data;
	this->next = next;
}

template<typename TYPE>
ListNode<TYPE>::~ListNode()
{
	// cout << "~ListNode()" << endl;
	next = NULL;
}

template<typename TYPE>
ListNode<TYPE>* ListNode<TYPE>::getPtr() const
{
	// cout << "ListNode::getPtr() " << next << endl;
	return next;
}

template<typename TYPE>
const TYPE& ListNode<TYPE>::getData() const
{
	// cout << "ListNode::getData()" << endl;
	return data;
}

template<typename TYPE>
void ListNode<TYPE>::setData( const TYPE& data )
{
	// cout << "ListNode::setData()" << endl;
	this->data = data;
}

template<typename TYPE>
void ListNode<TYPE>::setPtr( ListNode<TYPE>* ptr )
{
	// cout << "ListNode::setPtr()" << endl;
	this->next = ptr;
}