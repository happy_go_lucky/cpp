#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>	// for bool
#include <unistd.h>		// for fork()
#include <sys/wait.h>	// for wait()
#include <sys/types.h>	// for pid_t datatype in c99
#include <fcntl.h>		// for file control and corresponding flags
#include "commands.h"
#include "c_string_fxns.h"

#define DEBUG_PRINT

void initShell();
void parseCommand( const char* command, char** args, int* argCount );
bool isWhiteSpace( const char character, const char* whitespaceChars );
void runCommand( char** commandArgs, int numArgs );
int getCommandType( const char* command );
char** getEnvPathList( int* numPaths );
void parseShellMetaCharacters( const char aChar, char* const* argumentArr );
void pipe_impl( char* commandOutput );

int main( int argc, char* argv[], char* envp[] )
{
	#ifdef DEBUG_PRINT
	printf( "%s stats:\n PID: %d\n Parent PID: %d\n", argv[0], getpid(), getppid() );
	#endif
	
	initShell();
	
	return EXIT_SUCCESS;
}

void initShell()
{
	char* command = NULL;
	char** commandArgs = NULL;
	
	int numArguments = 0;
	
	while ( true )
	{
		command = malloc( sizeof(char) * COMMAND_BUFFER );
		commandArgs = malloc( sizeof( char* ) * MAX_ARGS);
		int ii = 0;
		while ( ii < MAX_ARGS )
		{
			*( commandArgs + ii ) = malloc( sizeof( char ) * ( COMMAND_BUFFER + 1 ) );
			ii++;
		}
		numArguments = 0;
		
		printf("%s", SHELL_NAME);
		// command length is set to 1024 buffer and it will take everything except new line
		if ( scanf( "%1024[^\n]s%*c", command ) == 1 )
		{
			#ifdef DEBUG_PRINT
			printf( "You have entered: %s\n", command );
			#endif
			
			parseCommand( command, commandArgs, &numArguments );
			
			#ifdef DEBUG_PRINT
			printf( "command is: %s\n", commandArgs[0] );
			printf( "number of arguments are: %d\n", numArguments );
			#endif
			if ( cStringCompare( command, QUIT_COMMAND ) || cStringCompare( command, QUIT_COMMAND_ALT ) )
			{
				#ifdef DEBUG_PRINT
				printf( "quitting...\n" );
				#endif
				break;
			}
			// nullify the remainder argument array. This is required for the proper operation
			// of execev function call later
			ii = numArguments;
			while ( ii < MAX_ARGS )
			{
				free( *( commandArgs + ii ) );
				*( commandArgs + ii ) = NULL;
				ii++;
			}
			
			runCommand( commandArgs, numArguments );
		}
		// clear memory
		ii = 0;
		while ( ii < MAX_ARGS )
		{
			free ( commandArgs[ii] );
			ii++;
		}
		free( commandArgs );
		fgets( command, COMMAND_BUFFER, stdin );	// hack to get rid of trailing newline
		free( command );
	}
}

void parseCommand(const char* command, char** args, int* argCount)
{
	int cmdCharCount = 0;
	int argCharCount = 0;
	*argCount = 0;
	
	while ( true )
	{
		if ( isWhiteSpace( *( command + cmdCharCount ), WHITESPACE ) || *( command + cmdCharCount ) == '\0' )
		{
			if ( argCharCount > 0 )
			{
				args[*argCount][argCharCount] = '\0';
				argCharCount = 0;
				*argCount = *argCount + 1;
			}
			
			if ( *( command + cmdCharCount ) == '\0' )
			{
				break;
			}
		}
		else
		{
			args[*argCount][argCharCount] = *( command + cmdCharCount );
			argCharCount++;
		}
		
		cmdCharCount++;
	}
}

bool isWhiteSpace( const char character, const char* whitespaceChars )
{
	int ii = 0;
	while ( *( whitespaceChars + ii ) != '\0' )
	{
		if ( character == *( whitespaceChars + ii ) )
		{
			return true;
		}
		ii++;
	}
	
	return false;
}

void runCommand( char** commandArgv, int numArgs )
{
	/**
	 * call to fork() creates a new child process, and both parent and child processes will execute
	 * the code that follows fork() call. 
	 * fork() returns pid of the child process in parent, and 0 in child.
	 * any code below fork() will be run for both parent and child, therefore conditional check is needed
	 **/
	pid_t commandPid = fork();
	enum processCondition { FAILURE = -1, CHILD = 0 };
	
	if ( commandPid == FAILURE )
	{
		#ifdef DEBUG_PRINT
		printf( "unsuccessful attempt at creating a new process\n" );
		#endif
	}
	else if ( commandPid == CHILD )
	{
		#ifdef DEBUG_PRINT
		printf( "created a new child process with pid: %d, and my parent is: %d\n", getpid(), getppid() );
		#endif
		if ( getCommandType( commandArgv[0] ) == COMMAND )
		{
			int numPaths = 0;
			char** path = getEnvPathList( &numPaths );
			char* commandPath = NULL;
			
			int ii = 0;
			while ( ii < numPaths )
			{
				char* modifiedPath = cStringConcatinate( path[ii], "/" );
				commandPath = cStringConcatinate( modifiedPath, commandArgv[0] );
				
				#ifdef DEBUG_PRINT
				printf( "checking command at path: %s\n", commandPath );
				#endif
				
				int err = access( commandPath, F_OK | R_OK | X_OK );
				if ( err == 0 )
				{
					#ifdef DEBUG_PRINT
					printf( "command can be run\n" );
					#endif
					execve( commandPath, commandArgv, path );
					break;
				}
				else
				{
					#ifdef DEBUG_PRINT
					printf( "invalid command\n" );
					#endif
				}
				ii++;
				free( modifiedPath );
				free( commandPath );
			}
			// free path data structure and exit the process as it wasn't executed by execv
			while ( numPaths > 0 )
			{
				free( path[numPaths - 1] );
				numPaths--;
			}
			free( path );
			#ifdef DEBUG_PRINT
			printf( "exitting the process %d\n", getpid() );
			#endif
			exit(getpid());
		}
		else
		{
			#ifdef DEBUG_PRINT
			printf( "environment Variable\n" );
			#endif
			getenv( commandArgv[0] + 1 );
		}
	}
	else
	{
		#ifdef DEBUG_PRINT
		printf( "parent process with pid: %d, and child process with pid: %d\n", getpid(), commandPid );
		#endif
		wait(NULL);
		#ifdef DEBUG_PRINT
		printf( "process with pid: %d has terminated\n", commandPid );
		#endif
	}
}

int getCommandType( const char* command )
{
	return command[0] == '$' ? ENV_VAR : COMMAND;
}

/**
 * returns: NULL if pathNumber is bigger than available paths. It returns a valid path otherwise
 * */
char** getEnvPathList( int* numPaths )
{
	char* buffer = getenv("PATH");
	if ( buffer == NULL )
		return NULL;
	
	char** path = malloc( sizeof( char* ) * MAX_PATHS );
	int ii = 0;
	while ( ii < MAX_PATHS )
	{
		*( path + ii ) = malloc( sizeof( char ) * ( MAX_PATH_LEN + 1 ) );
		ii++;
	}
	
	*numPaths = 0;
	ii = 0;
	int jj = 0;
	while ( *( buffer + ii) != '\0' )
	{
		if ( *( buffer + ii ) == ':' )
		{
			*( path[*numPaths] + jj ) = '\0';
			
			#ifdef DEBUG_PRINT
			printf( "read path: %s, stored at position: %d\n", path[*numPaths], *numPaths );
			#endif
			
			( *numPaths )++;
			jj = 0;
		}
		else
		{
			*( path[*numPaths] + jj ) = *( buffer + ii );
			jj++;
		}
		ii++;
	}
	*( path[*numPaths] + ii ) = '\0';
	return path;
}

void parseShellMetaCharacters( const char aChar, char* const* argumentArr )
{
	int jj = 0;
	while ( argumentArr[jj] != NULL )
	{
		if ( strLen( argumentArr[jj] ) == 1 && cStringCompareChar( argumentArr[jj], META_PIPE ) )
		{
			//TODO
		}
		jj++;
	}
}

void pipe_impl( char* commandOutput )
{
	int pipeFileDescriptor[2];	// required for pipe read/write
	char pipeBuffer = '\0';
	short pipeBufferLength = 1;
	enum processCondition { FAILURE = -1, CHILD = 0 };
	
	// fork a new process for pipe
	// create pipe
	pid_t pipePid = fork();
	
	if ( pipe( pipeFileDescriptor ) == -1 )
	{
		perror("pipe");
	}
	
	if ( pipePid == FAILURE )
	{
		#ifdef DEBUG_PRINT
		printf( "unsuccessful attempt at creating a new process\n" );
		#endif
	}
	else if ( pipePid == CHILD )
	{
		#ifdef DEBUG_PRINT
		printf( "created a new child process for pipe with pid: %d, and my parent is: %d\n", getpid(), getppid() );
		#endif
		// close write end as we are not using it
		close( pipeFileDescriptor[PIPE_READ_END] );
		while ( read( pipeFileDescriptor[PIPE_READ_END], &pipeBuffer, pipeBufferLength ) > 0 )
		{
			write( STDOUT_FILENO, &pipeBuffer, 1 );
		}
		close( pipeFileDescriptor[PIPE_READ_END] );
		exit( getpid() );
	}
	else
	{
		#ifdef DEBUG_PRINT
		printf( "parent process with pid: %d, and child process for pipe with pid: %d\n", getpid(), pipePid );
		#endif
		// close write end as we are not using it
		close( pipeFileDescriptor[PIPE_WRITE_END] );
		// write stuff here
		write ( pipeFileDescriptor[PIPE_WRITE_END], commandOutput, strLen( commandOutput ) );
		close( pipeFileDescriptor[PIPE_WRITE_END] );
		wait( NULL );
		#ifdef DEBUG_PRINT
		printf( "process with pid: %d has terminated\n", pipePid );
		#endif
	}
}

void outputRedirection_impl()
{
	// FILE* fileProcId = NULL;
	// fileProcId = fopen( PIPE_BUFFER_FILE, O_WRONLY | O_CREAT );
}
