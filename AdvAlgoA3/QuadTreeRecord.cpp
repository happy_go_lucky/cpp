#include "QuadTreeRecord.h"

QuadTreeRecord& QuadTreeRecord::operator =(const QuadTreeRecord& record)
{
	if (this != &record)
	{
		primary_lat_dms = record.primary_lat_dms;
		primary_long_dms = record.primary_long_dms;
		recordByteOffset = record.recordByteOffset;
	}
	return *this;
}

bool QuadTreeRecord::operator ==(const QuadTreeRecord& record)
{
	return primary_lat_dms == record.primary_lat_dms &&
			primary_long_dms == record.primary_long_dms &&
			recordByteOffset == record.recordByteOffset;
}

ostream& operator <<(ostream& out, const QuadTreeRecord& record)
{
	out << "[(" << record.primary_lat_dms.getTotalSeconds() << " ,"
		<< record.primary_long_dms.getTotalSeconds() << ")"
		<< ", " << record.recordByteOffset << "]";
	return out;
}

bool QuadTreeRecord::isDitto(const QuadTreeRecord& record) const
{
	return primary_lat_dms.getTotalSeconds() == record.primary_lat_dms.getTotalSeconds() &&
			primary_long_dms.getTotalSeconds() == record.primary_long_dms.getTotalSeconds() &&
			recordByteOffset == record.recordByteOffset;
}

bool QuadTreeRecord::isSameOffset(const QuadTreeRecord& record) const
{
	return recordByteOffset == record.recordByteOffset;
}

bool QuadTreeRecord::isSameCoords(const QuadTreeRecord& record) const
{
	return primary_lat_dms.getTotalSeconds() == record.primary_lat_dms.getTotalSeconds() &&
			primary_long_dms.getTotalSeconds() == record.primary_long_dms.getTotalSeconds();
}
