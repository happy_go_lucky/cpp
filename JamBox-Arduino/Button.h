#ifndef BUTTON_H
#define BUTTON_H

#include "defines.h"
#include "Arduino.h"

class Button
{
  public:
    enum STATES { BUTTON_DOWN = 0, BUTTON_UP };
    enum BUTTON_EVENTS { ON_HOLD = 0, ON_CLICK };
    const static int NUM_BUTTON_EVENTS = 2;
    
    Button( const int, const int );
    ~Button();
    void loop();
    char getButtonId() const;
    bool addEventListener( const int, void ( *listenerFucn )() );
    bool removeEventListener( const int );
    
  private:
    int _pin;
    int _buttonId;
    int _state;
    void ( *listenerFuncArray[NUM_BUTTON_EVENTS] )();
    bool _clickEngaged;

    void _onButtonHold();
    void _onButtonClick();
    void _initListenerArray();
    void _checkState();
};

#endif
