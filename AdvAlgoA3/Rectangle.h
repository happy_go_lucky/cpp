#ifndef RECTANGLE_H
#define RECTANGLE_H

#include <iostream>
#include <cmath>

using namespace std;

struct Rectangle
{
public:
	int x1;
	int x2;
	int y1;
	int y2;
	
	friend ostream& operator << (ostream& s, const Rectangle& r);
	Rectangle& operator= (const Rectangle& rec);
	
	bool operator== (const Rectangle& rec) const noexcept;
	
	int length() const;
	int width() const;
	int centerX() const;
	int centerY() const;
	bool isConverged() const;
};

#endif
