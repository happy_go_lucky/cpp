#ifndef DOUBLY_LIST_ITERATOR_H
#define DOUBLY_LIST_ITERATOR_H

#include <iterator>
#include "DoublyListNode.h"

namespace datastructs
{
	template <class Type>
	class DoublyListIterator final : public std::iterator<std::bidirectional_iterator_tag,
									DoublyListNode<Type>>
	{
		public:
			template <typename T>
			friend class DoublyList;
		
			DoublyListIterator() noexcept;
			DoublyListIterator(DoublyListNode<Type>* nodePtr) noexcept
			{
				_nodePtr = nodePtr;
			}
			
			DoublyListIterator(const DoublyListIterator<Type>& itr) noexcept : _nodePtr(itr._nodePtr)
			{
				
			}
			
			~DoublyListIterator() = default;
			
			DoublyListIterator<Type>& operator =(const DoublyListIterator<Type>& itr) & noexcept;
			DoublyListIterator<Type>& operator =(DoublyListIterator<Type>&& itr) & noexcept;
			DoublyListIterator<Type>& operator =(DoublyListNode<Type>* nodePtr) & noexcept;
			
			Type& operator *() const noexcept;
			// prefix
			DoublyListIterator<Type>& operator ++() noexcept;
			DoublyListIterator<Type>& operator --() noexcept;
			// postfix
			DoublyListIterator<Type> operator ++(int) noexcept;
			DoublyListIterator<Type> operator --(int) noexcept;
			
			bool operator ==(const DoublyListIterator<Type>& itr) const noexcept;
			bool operator !=(const DoublyListIterator<Type>& itr) const noexcept;
			
			void advance(unsigned int n = 1) noexcept;
			void retreat(unsigned int n = 1) noexcept;
			Type& getData() const noexcept;
			bool isNextNodeValid() const noexcept;
			bool isPrevNodeValid() const noexcept;
			bool isNodeValid() const noexcept;
		
		private:
			DoublyListNode<Type>* _nodePtr;
	};
}
#endif
