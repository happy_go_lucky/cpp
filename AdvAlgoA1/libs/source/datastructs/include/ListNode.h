#ifndef LIST_NODE_H
#define LIST_NODE_H

#include <utility>
#include <iostream>

template <class T>
class ListNode
{
	public:
		ListNode() noexcept;
		ListNode(const T& data, ListNode<T>* nextPtr = nullptr) noexcept;
		ListNode(T&& data, ListNode<T>* nextPtr = nullptr) noexcept;
		ListNode(const ListNode<T>& node);
		ListNode(ListNode<T>&& node) noexcept;
		
		~ListNode();
		
		ListNode<T>& operator =(const ListNode<T>& node);
		ListNode<T>& operator =(ListNode<T>&& node) noexcept;
		bool operator ==(const ListNode<T>& node) noexcept;
		bool operator ==(const ListNode<T>&& node) noexcept;
		bool operator !=(const ListNode<T>& node) noexcept;
		bool operator !=(const ListNode<T>&& node) noexcept;
		bool operator <(const ListNode<T>& node) noexcept;
		bool operator <(const ListNode<T>&& node) noexcept;
		bool operator >(const ListNode<T>& node) noexcept;
		bool operator >(const ListNode<T>&& node) noexcept;
		
		template <typename U>
		friend std::ostream& operator <<(std::ostream& out, const ListNode<U>& node);
		
		inline const T& getData() const
		{
			return _data;
		}
		
		inline ListNode<T>* getNextNode() const
		{
			return _nextNode;
		}
		
		inline void setData(const T& data)
		{
			_data = data;
		}
		
		inline void setData(T&& data)
		{
			_data = std::move(data);
		}
		
		inline void setNextNode(ListNode<T>* nextPtr)
		{
			_nextNode = nextPtr;
		}
		
	private:
		T _data;
		ListNode<T>* _nextNode;
};

#endif
